unit uSerialReader;

interface
  uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls,
  shellapi, TlHelp32, dateutils, System.Win.ScktComp, ComObj, ActiveX, StrUtils;

type

  TChip = record
    Kod: string;
    isDone: boolean;
  end;

  TSerialReader = class;

  TChipScannerThread = class(TThread)
    private
      prOwner: TSerialReader;
      prCurrentRFID: String;
      procedure SyncHandleRFID;
      procedure OpenCom;
      procedure CloseCom;
      procedure PurgeCom;
    protected
      procedure Execute; override;
    public
      pubIsTerminated: boolean;
      constructor Create(AOwner: TSerialReader);
      destructor Destroy; override;
  end;

  TSerialReader = class
  private
    prChipXMLArr: array of string;
    prTetelArr: array of string;
    prSender: string;
    prHandleCodes: TTimer;
    prChipScannerThread: TChipScannerThread;
    procedure handleCodesTick(Sender: TObject);
  public
    pubCodes: array of TChip;
    pubPort: string;
    pubPortConfig: string;
    pubComHandle: THandle;
    IsStarted: boolean;
    procedure Execute;
    procedure Stop;
    procedure HandleRFID(rfid: string);
    function IndexOf(arr: array of string; haystack: string): integer;
    Constructor Create(pSender: string; pPort: string; pPortConfig: string); {Overload;}
    destructor Destroy; override;
  end;

implementation
  uses uPufferRaktarBevetelezesParameterek, uPufferKiadOsszerendel, uPufferVisszavetelezes, uDM,
  uPufferAttarolasKiadas, uPufferAttarolasBevet;

{
  TChipScannerThread
}
constructor TChipScannerThread.Create(AOwner: TSerialReader);
begin
  inherited Create(True);
  FreeOnTerminate := true;
  pubIsTerminated := false;
  prOwner := AOwner;
  OpenCom;
  {...}
end;

destructor TChipScannerThread.Destroy;
begin
  {...}
  CloseCom;
  inherited;
end;

procedure TChipScannerThread.OpenCom;
var
  lcDevicePath: string;
  lcDCB: DCB;
  CommTimeOuts : TCOMMTIMEOUTS;
begin
  lcDevicePath := format('\.\\%s', [prOwner.pubPort]);
  prOwner.pubComHandle := CreateFile(PChar(lcDevicePath),  GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if prOwner.pubComHandle = INVALID_HANDLE_VALUE then
    raise Exception.Create('TSerialReader: Cannot open the serial port.');
  SetupComm(prOwner.pubComHandle, 1024, 1024);  //fileid,1024,1024
  FillChar(lcDCB, sizeof(DCB),0);
  lcDevicePath := format('%s:%s', [prOwner.pubPort, prOwner.pubPortConfig]);
  BuildCommDCB(PChar(lcDevicePath),lcDCB);
  SetCommState(prOwner.pubComHandle, lcDCB);
  FillChar(CommTimeOuts, sizeof(TCOMMTIMEOUTS), 0);
  CommTimeouts.ReadTotalTimeoutMultiplier:=1;
  SetCommTimeouts(prOwner.pubComHandle, CommTimeOuts);
  PurgeCom;
end;

procedure TChipScannerThread.CloseCom;
begin
  CloseHandle(prOwner.pubComHandle);
end;

procedure TChipScannerThread.PurgeCom;
begin
  PurgeComm( prOwner.pubComHandle, PURGE_RXCLEAR
    or PURGE_TXCLEAR
    or PURGE_RXABORT
    or PURGE_TXABORT
  );
  EscapeCommFunction( prOwner.pubComHandle, CLRDTR );
  EscapeCommFunction( prOwner.pubComHandle, CLRRTS );
end;

procedure TChipScannerThread.Execute;
var
  lcBytesReaded: DWORD;
  lcCurrentCode: string;
  lcChar: ansichar;
begin
  repeat
    Application.ProcessMessages;
    if not ReadFile(prOwner.pubComHandle, lcChar, 1, lcBytesReaded, nil) then
      begin

      end;
    if (lcChar <> '') AND (lcBytesReaded > 0) then
      Begin
        if (lcChar <> Chr(42)) and (lcChar <> '')  and (lcChar <> ' ')then
          begin
            if (lcCurrentCode <> '') AND (lcChar = chr(10)) AND (lcCurrentCode[length(lcCurrentCode)] = chr(10)) then
              setLength(lcCurrentCode, length(lcCurrentCode)-1)
            else
              lcCurrentCode := lcCurrentCode + lcChar;

            if length(lcCurrentCode) > 2 then
              begin
                if (lcCurrentCode[lcCurrentCode.Length] = Chr(10)) and (lcCurrentCode[lcCurrentCode.Length - 1] = Chr(13)) then
                  begin
                    //SendRFIDToDataForm(lcCurrentCode);
                    prCurrentRFID := lcCurrentCode;
                    Synchronize(SyncHandleRFID);
                    lcCurrentCode := '';
                  end;
              end;
          end;
      end;
    sleep(10);
  until Terminated;
  if Terminated then
    pubIsTerminated := true;
end;

procedure TChipScannerThread.SyncHandleRFID;
var currentChip: TChip;
begin
//  prOwner.HandleRFID(prCurrentRFID);
  setLength(prOwner.pubCodes, length(prOwner.pubCodes)+1);
  currentChip.Kod := prCurrentRFID;
  currentChip.isDone := false;
  prOwner.pubCodes[length(prOwner.pubCodes)-1] := currentChip;
  prCurrentRFID := '';
end;
{
  End of TChipScannerThread
}

constructor TSerialReader.Create(pSender: string; pPort: string; pPortConfig: string);
begin
  prSender := pSender;
  pubPort := pPort;
  pubPortConfig := pPortConfig;
  SetLength(pubCodes, 0);
  prHandleCodes := TTimer.Create(Application);
  prHandleCodes.Enabled := false;
  prHandleCodes.Interval := 1000;
  prHandleCodes.OnTimer := handleCodesTick;
  prHandleCodes.Tag := 0;

end;

destructor TSerialReader.Destroy;
begin
  prHandleCodes.Tag := 1;
  prHandleCodes.Enabled := false;
  prHandleCodes.Destroy;
end;

procedure TSerialReader.handleCodesTick(Sender: TObject);
var i: integer;
begin
  prHandleCodes.Enabled := false;
  for i := 0 to length(pubCodes)-1 do
    begin
      if not pubCodes[i].isDone then
        begin
          HandleRFID(pubCodes[i].Kod);
          pubCodes[i].isDone := true;
        end;
      if prHandleCodes.Tag = 1 then
        break;
    end;
  if prHandleCodes.Tag = 0 then
    prHandleCodes.Enabled := true;
end;

procedure TSerialReader.Execute;
var i: integer;
begin
  setLength(pubCodes, 0);
  setLength(prChipXMLArr, 0);
  setLength(prTetelArr, 0);

  DM.CDSChip.First;
  i := 0;
  { Chip.xml-b�l adatok m�sol�sa t�mbbe }
  while not DM.CDSChip.Eof do
    begin
      if (DM.CDSChip.FieldByName('cms_chipkod').AsString <> '') AND (DM.CDSChip.FieldByName('cms_chipkod').AsString <> 'NULL') then
        begin
          setLength(prChipXMLArr, length(prChipXMLArr)+1);
          prChipXMLArr[i] := DM.CDSChip.FieldByName('cms_chipkod').AsString;
          i := i+1;
        end;
      DM.CDSChip.Next;
    end;

  { Szinkronra v�r� chip k�dok m�sol�sa t�mbbe }
  DM.FDMemTablePufferRaktarSynchs.First;
  while not DM.FDMemTablePufferRaktarSynchs.Eof do
    begin
      if (DM.FDMemTablePufferRaktarSynchs.FieldByName('chip_kod').AsString <> '') AND (DM.FDMemTablePufferRaktarSynchs.FieldByName('chip_kod').AsString <> 'NULL') then
        begin
          setLength(prChipXMLArr, length(prChipXMLArr)+1);
          prChipXMLArr[i] := DM.FDMemTablePufferRaktarSynchs.FieldByName('chip_kod').AsString;
          i := i+1;
        end;
      DM.FDMemTablePufferRaktarSynchs.Next;
    end;

  if prSender = 'bevet' then
    { �ppen aktu�lis bev�telez�s adatainak m�sol�sa t�mbbe }
    begin
      if (DM.FDMemTablePufferRakBevet.RecordCount <> 0) then
        begin
          DM.FDMemTablePufferRakBevet.First;
          i := 0;
          while not DM.FDMemTablePufferRakBevet.Eof do
            begin
              if (DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').asInteger <> 0) then
                begin
                  setLength(prTetelArr, length(prTetelArr)+1);
                  prTetelArr[i] := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString;
                  i := i + 1;
                end;
              DM.FDMemTablePufferRakBevet.Next;
            end;
        end;

      { Puffer rakt�rban l�v� r�gz�tett chip-ek (netr�l)}
      if (DM.CDSChipPuffer.RecordCount > 0) then
        begin
          DM.CDSChipPuffer.First;
          i := 0;
          while not DM.CDSChipPuffer.Eof do
            begin
              setLength(prChipXMLArr, length(prChipXMLArr)+1);
              prChipXMLArr[i] := DM.CDSChipPuffer.FieldByName('chipkod').AsString;
              i := i + 1;
              DM.CDSChipPuffer.Next;
            end;
        end;
    end;
  prHandleCodes.Tag := 0;
  prHandleCodes.Enabled := true;
  prChipScannerThread := TChipScannerThread.Create(Self);
  IsStarted := true;
  prChipScannerThread.Resume;
end;

procedure TSerialReader.Stop;
begin
  prHandleCodes.Tag := 1;
  prHandleCodes.Enabled := false;
  if prSender = 'bevet' then
    frmPufferRaktarBevetelezesParameterek.pubBeolvasasokSzama := frmPufferRaktarBevetelezesParameterek.pubBeolvasasokSzama + 1;
  if assigned(prChipScannerThread) then
    begin
      prChipScannerThread.Terminate;
      while not prChipScannerThread.pubIsTerminated do
        Application.ProcessMessages;
      prChipScannerThread := nil;
    end;
  IsStarted := false;
end;

function TSerialReader.IndexOf(arr: array of string; haystack: string): integer;
var i: integer;
begin
  result := -1;
  for i := 0 to Length(arr)-1 do
    begin
      if arr[i] = haystack then
        begin
          result := i;
          break;
        end;
    end;
end;

procedure TSerialReader.HandleRFID(rfid: string);
begin
  rfid := StringReplace(StringReplace(rfid, #10, '', [rfReplaceAll]), #13, '', [rfReplaceAll]);
  if (prSender = 'bevet') then
    begin
      {Ha puffer rakt�r bev�telez�s form.}
      if IndexOf(prTetelArr, rfid) <> -1 then
        begin
          { Ez a chip m�r szerepel az aktu�lis puffer bev�telez�sen -> Mem�ba �rni. }
          frmPufferRaktarBevetelezesParameterek.mChipList.Lines.Add('A '+rfid+' chipk�d m�r szerepel a bev�telez�s t�telei k�z�tt.');
          exit;
        end
      else
        begin
          { Chip volt-e m�r beolvasva a chip.xml-be? }
          if IndexOf(prChipXMLArr, rfid) <> -1 then
            begin
              { Ez a chip m�r szerepel a chip.xml-ben vagy a szinkronra v�r� chip k�dok k�z�tt -> Mem�ba �rni. }
              frmPufferRaktarBevetelezesParameterek.mChipList.Lines.Add('A '+rfid+' chipk�d m�r kor�bban r�gz�tve lett a rendszerbe.');
              exit;
            end
          else
            begin
              if (length(rfid) > 5) (*
                and ((rfid[1] = 'E') or (rfid[1] = 'e'))
              *) then
                begin
                  { Bev�telez�s... }
                  DM.FDMemTablePufferRakBevet.Insert;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger := 1;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString := rfid;
                  DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger := frmPufferRaktarBevetelezesParameterek.pubBeolvasasokSzama;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_fajta_id').AsVariant := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilFajta.KeyValue;
                  DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilFajta.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_allapot_id').AsVariant := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilAllapot.KeyValue;
                  DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilAllapot.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_szin_id').AsVariant := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.KeyValue;
                  DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilMeret.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_meret_id').AsVariant := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilMeret.KeyValue;
                  DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString := frmPufferRaktarBevetelezesParameterek.DBLookupComboTextilMagassag.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_magassag_id').AsVariant := frmPufferRaktarBevetelezesParameterek.DBLookupComboTextilMagassag.KeyValue;
                  DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxMegnevezes.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_kpt_id').AsVariant := frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxMegnevezes.KeyValue;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_megjegyzes').AsString := frmPufferRaktarBevetelezesParameterek.DBMemoTetelMegjegyzes.Text;
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_create_date').AsDateTime := now();
                  DM.FDMemTablePufferRakBevet.FieldByName('pbtet_lastupdate_date').AsDateTime := now();
                  DM.FDMemTablePufferRakBevet.Append();
                  setLength(prTetelArr, length(prTetelArr)+1);
                  prTetelArr[length(prTetelArr)-1] := rfid;
                  frmPufferRaktarBevetelezesParameterek.FDMemTableBeolvasottTextil.Insert;
                  frmPufferRaktarBevetelezesParameterek.FDMemTableBeolvasottTextil.FieldByName('chipkod').AsString := rfid;
                  frmPufferRaktarBevetelezesParameterek.FDMemTableBeolvasottTextil.Post;
                  frmPufferRaktarBevetelezesParameterek.mChipList.Lines.Add('A '+rfid+' chipk�d r�gz�tve.');
                end
              else
                begin
                  frmPufferRaktarBevetelezesParameterek.mChipList.Lines.Add('A '+rfid+' chipk�d nem megfelel� form�tum.');
                end;
            end;
        end;
    end
  else if (prSender = 'kiad_osszerendel') then
    begin
      {Ha puffer rakt�r kiad�s-�sszerendel�s form.}
      frmPufferKiadOsszerendel.pubHandleRFID(rfid);
    end
  else if (prSender = 'visszavet') then
    begin
      frmPufferVisszavetel.pubHandleRFID(rfid);
    end
  else if (prSender = 'attarolaskiad') then
    begin
      frmPufferAttarolasKiadas.pubHandleRFID(rfid);
    end
  else if (prSender = 'attarolasbevet') then
    begin
      frmPufferAttarolasBevet.pubHandleRFID(rfid);
    end;
end;

end.

