unit uSzennyesUj;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Datasnap.DBClient, System.ImageList, Vcl.ImgList,
  System.Rtti, IniFiles, Vcl.ExtDlgs, frxClass, frxDBSet, Math;

type
  TfrmSzennyesUj = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    gbTextilek: TGroupBox;
    dbgTextil: TDBGrid;
    gbTextildb: TGroupBox;
    eTexDarab: TEdit;
    lTextilNev: TLabel;
    gbTextilatvesz: TGroupBox;
    DBGridAtveszText: TDBGrid;
    btnBezar: TButton;
    btnAtvetel: TButton;
    lSzSzR: TImageList;
    gbKivOszt: TGroupBox;
    lKivOsztaly: TLabel;
    pMessage: TPanel;
    lUzenet: TLabel;
    lFolyamat: TLabel;
    eBekeszit: TEdit;
    btnKesobb: TButton;
    btnBovites: TButton;
    pTextilLista: TPanel;
    gbTListaPanel: TGroupBox;
    dbgTextilBovites: TDBGrid;
    btnPanelBezar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure eTexDarabKeyPress(Sender: TObject; var Key: Char);
    procedure dbgTextilDblClick(Sender: TObject);
    procedure eTexDarabExit(Sender: TObject);
    procedure btnAtvetelClick(Sender: TObject);
    procedure btnBezarClick(Sender: TObject);
    procedure DBGridAtveszTextDblClick(Sender: TObject);
    procedure eBekeszitExit(Sender: TObject);
    procedure btnKesobbClick(Sender: TObject);
    procedure btnBovitesClick(Sender: TObject);
    procedure btnPanelBezarClick(Sender: TObject);
    procedure dbgTextilBovitesDblClick(Sender: TObject);
  private
    { Private declarations }
    prUtvonal, prUtMent : String;
    prBekeszitID : Integer;
    prBovites, prJavitas : Boolean;
    procedure PanelShow(AShow : Boolean; ASzoveg: String);
    procedure TextilPanelShow(AShow : Boolean);
    function SzallitoSorGen : String;
  public
    { Public declarations }
  end;

var
  frmSzennyesUj: TfrmSzennyesUj;

implementation

uses
  uTMRMain, uDM, uFgv, uHTTPs, uLogs;

{$R *.dfm}

function TfrmSzennyesUj.SzallitoSorGen : String;
var
  lclabel : String;
  lcSzallSorszam : TfgvUnit;
  lcMozgasSorszam : TClientDataSet;
begin
  //Sz�ll�t�lev�l sorsz�m gener�l�s START.
  lcMozgasSorszam := TClientDataSet.Create(self);
  lcMozgasSorszam.LoadFromFile('sorszam.xml');
  lcMozgasSorszam.LogChanges := false;
  lFolyamat.Caption := 'Sz�ll�t�lev�l sorsz�m gener�l�s...';
  DM.CDSMozgnem.LoadFromFile('mozgasnem.xml');
  DM.CDSMozgnem.LogChanges := false;
  lclabel := DM.Szinkronizalas_2('mozgsorszam/');
  DM.CDSSzallSorszam.LoadFromFile('sorszam.xml');
  DM.CDSSzallSorszam.LogChanges := false;
  DM.CDSSzallSorszam.First;
  lcMozgasSorszam.First;
  if lclabel = 'Sikeres!' then
    begin
      while not DM.CDSSzallSorszam.Eof do
        begin
          lcMozgasSorszam.First;
          while not lcMozgasSorszam.Eof do
            begin
              if (DM.CDSSzallSorszam.FieldByName('szam_id').AsInteger = lcMozgasSorszam.FieldByName('szam_id').AsInteger) AND
                 (DM.CDSSzallSorszam.FieldByName('szam_partnerid').AsInteger = lcMozgasSorszam.FieldByName('szam_id').AsInteger) AND
                 (DM.CDSSzallSorszam.FieldByName('szam_felhasznalo').AsInteger = lcMozgasSorszam.FieldByName('szam_felhasznalo').AsInteger) AND
                 (DM.CDSSzallSorszam.FieldByName('szam_nev').AsString = lcMozgasSorszam.FieldByName('szam_nev').AsString) AND
                 (DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger < lcMozgasSorszam.FieldByName('szam_sorszam').AsInteger) then
                 Begin
                   DM.CDSSzallSorszam.Edit;
                   DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger := lcMozgasSorszam.FieldByName('szam_sorszam').AsInteger;
                   DM.CDSSzallSorszam.Post;
                 End;
              lcMozgasSorszam.Next;
            end;
          DM.CDSSzallSorszam.Next;
        end;
    end;
  lcSzallSorszam := TfgvUnit.Create();
  GSzallSorsz := lcSzallSorszam.SzallSorGen;
  DM.FDMemTextilek.First;
  while not DM.FDMemTextilek.Eof do
    begin
      DM.FDMemTextilek.Edit;
      DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
      DM.FDMemTextilek.Next;
    end;
  lFolyamat.Caption := 'Sz�ll�t�lev�l sorsz�m gener�l�s...Sikeres!';
 //Sz�ll�t�lev�l sorsz�m gener�l�s END.
end;

procedure TfrmSzennyesUj.TextilPanelShow(AShow: Boolean);
begin
  if AShow then
    begin
      pTextilLista.Top := (DBGridAtveszText.Height div 2) - (pTextilLista.Height div 2);
      pTextilLista.Left:= (DBGridAtveszText.Width div 2) - (pTextilLista.Width div 2);
    end;
  pTextilLista.Visible := AShow;
end;

procedure TfrmSzennyesUj.PanelShow(AShow : Boolean; ASzoveg: String);
begin
  if AShow then
    begin
      pMessage.Top:= (DBGridAtveszText.Height div 2) - (pMessage.Height div 2);
      pMessage.Left:= (DBGridAtveszText.Width div 2) - (pMessage.Width div 2);
      lUzenet.Caption := ASzoveg;
    end;
  pMessage.Visible := AShow;
end;


procedure TfrmSzennyesUj.btnAtvetelClick(Sender: TObject);
var
  lcFajlNev, lcMentFajlNev, lcS_0, lcIPCim, lcUtvonal, lcTisztaNev : String;
  lcI_0, lcMentesTSZID: Integer;
  lcSearchResult : TSearchRec;
  lcMappaCreate, lcFajl, lcDataSetToXML, lcPartnerMappa, lcOsztNev, lcMozg : TfgvUnit;
  lcGeneral, lcTextilID : TfgvUnit;
  lcXmlPost, lcXmlSave : THttpsUnit;
  lciniFile : TIniFile;
  lcOsszSuly : Double;
  lcSzallSorszam : TfgvUnit;
  lclabel, lcValasz, lcParamGen, lcEredmeny, lcSzallFej, lcTisztaSM : String;
begin
  //A grid ment�se xml-be.
  lcParamGen := '';
  btnBezar.Enabled :=false;
  btnAtvetel.Enabled := false;
  lcOsszSuly := 0.0;
  DM.CDSXmlPost.Active := false;
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcUtvonal := lciniFile.ReadString('utvonal','path','');
  PanelShow(true,'K�rem v�rjon...');
  Application.ProcessMessages;

 if DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString = '' then
    begin
      SzallitoSorGen;
    end
 else if DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString <> '' then
    begin
      GSzallSorsz := DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString;
    end;

  lcFajlNev := prUtvonal;
  SetCurrentDir(prUtvonal);
  lcPartnerMappa := TfgvUnit.Create();
  if not DirectoryExists(lcPartnerMappa.PartnerMappa(GPartner)) then
    begin
      CreateDir(lcPartnerMappa.PartnerMappa(GPartner));
      prUtvonal := prUtvonal + '\' + lcPartnerMappa.PartnerMappa(GPartner);
    end
   else
     prUtvonal := prUtvonal + lcPartnerMappa.PartnerMappa(GPartner);
  SetCurrentDir(prUtvonal);
  lcMappaCreate := TfgvUnit.Create();
  if not directoryexists(lcMappaCreate.Mappa(GDate)) then
    begin
      CreateDir(lcMappaCreate.Mappa(GDate));
      prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
    end
  else
    prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
  SetCurrentDir(prUtvonal);
  lcFajl := TfgvUnit.Create();
  lcMentFajlNev := lcFajl.FajlNev(GDate);
  lcS_0 := lcMentFajlNev;
  lcI_0 := 0;

  DM.CDSParameterek.First;
  while not DM.CDSParameterek.Eof do
    begin
      if DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner then
        begin
          lcParamGen := DM.CDSParameterek.FieldByName('prm_tisztagenuzem').AsString;
        end;
      DM.CDSParameterek.Next;
    end;

  //Ellen�rizni kell l�tezik-e m�r ilyen f�jl a kiv�lasztott napra.
  //Ha igen akkor sorsz�mozni kell. Pl.: 2018052813_0.xml
  if (FindFirst(lcS_0+'.xml', faAnyFile, lcSearchResult) = 0) And (GTFajlNev = lcS_0+'.xml') AND (lcParamGen = 'Nincs' ) then
    begin
      while FindFirst(lcMentFajlNev +'.xml', faAnyFile, lcSearchResult) = 0 do
        begin
          lcMentFajlNev := lcS_0 + '_' + IntToStr(lcI_0);
          inc(lcI_0);
        end;
    end;

  if (FindFirst(lcS_0+'.xml', faAnyFile, lcSearchResult) = 0) AND (GTFajlNev = lcS_0+'.xml') AND (lcParamGen = 'Manu�lis' ) AND (GOldal = 'S')then
    begin
      while FindFirst(lcMentFajlNev +'.xml', faAnyFile, lcSearchResult) = 0 do
        begin
          lcMentFajlNev := lcS_0 + '_' + IntToStr(lcI_0);
          inc(lcI_0);
        end;
    end;
//Darabos tiszta textil ment�s...
  if (FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) = 0) AND (GTFajlNev = lcS_0+'.xml') then
    begin
      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcMentFajlNev+'.xml';
          if GOldal = 'T' then
            DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := 1
          else if GOldal = 'S' then
            DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := 0;
          DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=  (round((DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger * DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat)*100))/100;
          lcOsszSuly := lcOsszSuly + DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat;
          DM.FDMemTextilek.FieldByName('szfej_lastuser').AsInteger := GUserID;
          DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := GUserID;
          DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := GUserID;
          DM.FDMemTextilek.FieldByName('sztet_createuser').AsInteger := GUserID;
          if DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString = '' then
            begin
              DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
            end;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;

      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('szfej_osszsuly').AsFloat := (round(lcOsszSuly*100))/100;
          //System.Dec(new, Round(StrToFloat(Edit3.Text)*1000000));
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;
      try
        lcDataSetToXML := TfgvUnit.Create();
        lcDataSetToXML.DataSetToXML(DM.FDMemTextilek, lcMentFajlNev+'.xml');
        lFolyamat.Caption := 'Sikeres sz�ll�t� ment�s!';
        //MessageBox(handle,'Sikeres sz�ll�t� ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
        lcDataSetToXML.Free;
      except
        on E : Exception do
           begin
             MessageBox(handle,'Sikertelen sz�ll�t� ment�s!', 'Hibajelz�s!', MB_ICONERROR);
           end;
      end;

        DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
        DM.CDSPartnerek.First;
        DM.frxDBDataset1.DataSet := DM.CDSLoadXml;
        if GOldal = 'S' then
          begin
            TfrxMemoView(DM.frxRepSzall.FindObject('mLBekeszit')).Visible := false;
            TfrxMemoView(DM.frxRepSzall.FindObject('frxDBDSSzallsztet_bekeszit')).Visible := false;
            TfrxMemoView(DM.frxRepSzall.FindObject('mKiszDatum')).Text := DateToStr(GDate);
            TfrxMemoView(DM.frxRepSzall.FindObject('Memo16')).Text := 'Szennyes d�tuma: ';
          end;
        lcOsztNev := TfgvUnit.Create();
        TfrxMemoView(DM.frxRepSzall.FindObject('mOsztNev')).Text := lcOsztNev.GetOsztalyNev(GOsztalyID);
        TfrxMemoView(DM.frxRepSzall.FindObject('mMozgnem')).Text := GOldal+GOlTipus;

        if GOldal = 'T' then
          begin
            TfrxMemoView(DM.frxRepSzall.FindObject('mLBekeszit')).Visible := true;
            TfrxMemoView(DM.frxRepSzall.FindObject('frxDBDSSzallsztet_bekeszit')).Font.Style := [fsBold];
            TfrxMemoView(DM.frxRepSzall.FindObject('frxDBDSSzallsztet_bekeszit')).Visible := true;
            TfrxMemoView(DM.frxRepSzall.FindObject('Memo16')).Text := 'Kisz�ll�t�s d�tuma: ';
            TfrxMemoView(DM.frxRepSzall.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          end;
        DM.CDSUzem.First;
        while not DM.CDSUzem.Eof do
          begin
            if DM.CDSUzem.FieldByName('uzem_id').AsInteger = DM.CDSLoadXml.FieldByName('szfej_uzem').AsInteger  then
              begin
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
              end;
            DM.CDSUzem.Next;
          end;

        DM.CDSFelhasz.First;
        while not DM.CDSFelhasz.Eof do
          begin
            if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
              begin
                TfrxMemoView(DM.frxRepSzall.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
              end;
            DM.CDSFelhasz.Next;
          end;

        while not DM.CDSPartnerek.Eof do
          begin
            if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
              begin
                TfrxMemoView(DM.frxRepSzall.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
              end;
            DM.CDSPartnerek.Next;
          end;
        DM.frxDBDataset1.First;
        while not DM.frxDBDataset1.Eof do
          begin
            if DM.frxDBDataset1.DataSet.FieldByName('sztet_javitott').AsInteger = 0 then
              TfrxMemoView(DM.frxRepSzall.FindObject('mJavitott')).Text := 'Nem'
            else if DM.frxDBDataset1.DataSet.FieldByName('sztet_javitott').AsInteger = 1 then
              TfrxMemoView(DM.frxRepSzall.FindObject('mJavitott')).Text := 'Igen' ;
            DM.frxDBDataset1.Next;
          end;

        {PDF ment�s Start}
        DM.frxPDFExport1.FileName := lcMentFajlNev+'.pdf';
        DM.frxPDFExport1.DefaultPath := GetCurrentDir;
        DM.frxPDFExport1.ShowDialog := false;
        DM.frxRepSzall.PrepareReport(true);
        DM.frxRepSzall.Export(DM.frxPDFExport1);
        {PDF ment�s End}

        DM.frxRepSzall.PrintOptions.Copies := 3;
        DM.frxRepSzall.PrintOptions.Collate := True;
        DM.frxRepSzall.PrepareReport(true);
        DM.frxRepSzall.ShowPreparedReport;

        //Darabos bek�sz�tett k�ld�s...START
        try
          lcXmlPost := THttpsUnit.Create();
          DM.CDSXMLMentes.LoadFromFile(lcMentFajlNev+'.xml');
          DM.CDSXMLMentes.LogChanges;
//          lcXmlSave := THttpsUnit.Create();
//          lcEredmeny := lcXmlSave.GetHttpsFajlok(lcIPCim,lcUtvonal,'feltoltottfajl/mozgas',lcMentFajlNev+'.xml',GUserID);
//          lcEredmeny := Copy(lcEredmeny, lcEredmeny.Length, lcEredmeny.Length);
          if ((DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger) <> 0) and (DM.CDSXMLMentes.FieldByName('szfej_lezart').AsInteger = 1) then
            begin
          //Ha m�r felt�lt�sre ker�lt, akkor csak update.
              if lcXmlPost.TisztaPutHttps(lcIPCim, lcUtvonal,'tisztaszlev', DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger, DM.CDSXMLMentes.XMLData) <> '' then
                lFolyamat.Caption := 'Sikeres sz�ll�t� k�ld�s!'
              else
                MessageBox(handle,'Sikertelen sz�ll�t� k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
            end;
             //Ha m�g nem volt felt�ltve akkor insert.
         if ((DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger) = 0) and (DM.CDSXMLMentes.FieldByName('szfej_lezart').AsInteger = 1) then
           begin
             lcSzallFej := lcXmlPost.PostHttps(lcMentFajlNev+'.xml', lcIPCim, lcUtvonal,'szallitolevel/' );
             //A visszakapott szfejID-t be kell �rni!
              if lcSzallFej <> '' then
                    begin
                      DM.CDSXMLMentes.First;
                      while not DM.CDSXMLMentes.Eof do
                        begin
                          DM.CDSXMLMentes.Edit;
                          DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger := StrToInt(lcSzallFej);
                          DM.CDSXMLMentes.Post;
                          DM.CDSXMLMentes.Next;
                        end;
                    end;
           end;
          //A szfejid-val updatelni kell a f�jlt.
          lcDataSetToXML := TfgvUnit.Create();
          lcDataSetToXML.DataSetToXML(DM.CDSXMLMentes, lcMentFajlNev+'.xml');
          lFolyamat.Caption := 'Sikeres azonos�t� ment�s!';
          //MessageBox(handle,'Sikeres sz�ll�t� ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
          lcDataSetToXML.Free;
          lcXmlSave.Free;
        except
          on E : Exception do
           begin
             MessageBox(handle,'Sikertelen sz�ll�t� k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
             lcXmlSave.Free;
           end;
        end;
        //Darabos bek�sz�tett k�ld�s...END
    end;

  if (FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) <> 0) then
    begin
      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcMentFajlNev+'.xml';
          lcOsszSuly := lcOsszSuly + DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat;
          if GOldal = 'T' then
            DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := 1
          else if GOldal = 'S' then
            DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := 0;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;

      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('szfej_osszsuly').AsFloat := lcOsszSuly;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;

      try
        lcDataSetToXML := TfgvUnit.Create();
        lcDataSetToXML.DataSetToXML(DM.FDMemTextilek, lcMentFajlNev+'.xml');
        lFolyamat.Caption := 'Sikeres sz�ll�t� ment�s!';
        //MessageBox(handle,'Sikeres sz�ll�t� ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
        lcDataSetToXML.Free;
      except
        on E : Exception do
           begin
             MessageBox(handle,'Sikertelen sz�ll�t� ment�s!', 'Hibajelz�s!', MB_ICONERROR);
           end;
      end;

      //A tiszta f�jl gener�l�s...
      try
        DM.CDSParameterek.First;
        while not DM.CDSParameterek.Eof do
          begin
            if (DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner) AND (DM.CDSParameterek.FieldByName('prm_tisztagenpart').AsString = 'Automatikus') then
              begin
                GOldal := 'T';
                if GVer = 'TH' then
                  begin
                    if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'O') AND (GOlTipus = 'O') then
                      begin
                        //Oszt�lyos tiszta gener�l�s....
                        DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                        DM.CDSLoadXml.LogChanges := false;
                        DM.CDSPartnerek.First;
                        lcGeneral := TfgvUnit.Create();
                        if lcGeneral.GenTiszta(DM.CDSLoadXml, false) then
                          begin
                            lcGeneral.Free;
                          end;
                      end
                    else if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'K') AND (GOlTipus = 'K') then
                      begin
                        //K�rh�zi szint�.....
                        DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                        DM.CDSLoadXml.LogChanges := false;
                        DM.CDSPartnerek.First;
                        lcGeneral := TfgvUnit.Create();
                        if lcGeneral.GenTiszta(DM.CDSLoadXml, false) then
                          begin
                            lcGeneral.Free;
                          end;
                      end;
                  end
                else if GVer = 'RA' then
                  begin
                    if DM.CDSParameterek.FieldByName('prm_tisztagenuzem').AsString = 'Automatikus' then
                      begin
                        if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'O') AND (GOlTipus = 'O') then
                          begin
                            //Oszt�lyos tiszta gener�l�s....
                            DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                            DM.CDSLoadXml.LogChanges := false;
                            DM.CDSPartnerek.First;
                            lcGeneral := TfgvUnit.Create();
                            if lcGeneral.GenTiszta(DM.CDSLoadXml, false) then
                              begin
                                lcGeneral.Free;
                              end;
                          end
                        else if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'K') AND (GOlTipus = 'K') then
                          begin
                            //K�rh�zi szint�.....
                            DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                            DM.CDSLoadXml.LogChanges := false;
                            DM.CDSPartnerek.First;
                            lcGeneral := TfgvUnit.Create();
                            if lcGeneral.GenTiszta(DM.CDSLoadXml, false) then
                              begin
                                lcGeneral.Free;
                              end;
                          end;
                      end;
                  end;
                GOldal := 'S';
              end;
            DM.CDSParameterek.Next;
          end;
      except
        on E : Exception do
           begin
             MessageBox(handle,'Sikertelen tiszta gener�l�s!', 'Hibajelz�s!', MB_ICONERROR);
           end;
      end;
//Szennyes k�ld�s kezd�s...
      //GOldal := 'S';
      try
        DM.CDSTisztaEllenorzes.LoadFromFile(lcMentFajlNev+'.xml');
        DM.CDSTisztaEllenorzes.LogChanges := false;
        lcXmlPost := THttpsUnit.Create();
        lcValasz := lcXmlPost.PostHttps(lcMentFajlNev+'.xml', lcIPCim, lcUtvonal,'szallitolevel/' );
        if lcValasz <> '' then
          begin
            while not DM.CDSTisztaEllenorzes.Eof do
              begin
                DM.CDSTisztaEllenorzes.Edit;
                DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger := StrToInt(lcValasz);
                DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger := 1;
                DM.CDSTisztaEllenorzes.Post;
                DM.CDSTisztaEllenorzes.Next;
              end;
            lcDataSetToXML.DataSetToXML(DM.CDSTisztaEllenorzes,lcMentFajlNev+'.xml');
            lFolyamat.Caption := 'Sikeres sz�ll�t� k�ld�s!'
          end
          //MessageBox(handle,'Sikeres sz�ll�t� k�ld�s!', 'T�j�koztat�s!', MB_ICONINFORMATION)
        else
          MessageBox(handle,'Sikertelen sz�ll�t� k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
      except
        on E : Exception do
           begin
             MessageBox(handle,'Sikertelen sz�ll�t� k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
           end;
      end;
//Szennyes k�ld�s v�ge...
      try
        //Nyomtat�si adatok �ssze�ll�t�sa.
        DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
        DM.CDSLoadXml.LogChanges := false;
        DM.CDSPartnerek.First;
        DM.frxDBDataset1.DataSet := DM.CDSLoadXml;
        if GOldal = 'S' then
          begin
            TfrxMemoView(DM.frxRepSzall.FindObject('mLBekeszit')).Visible := false;
            TfrxMemoView(DM.frxRepSzall.FindObject('frxDBDSSzallsztet_bekeszit')).Visible := false;
            TfrxMemoView(DM.frxRepSzall.FindObject('mKiszDatum')).Text := DateToStr(GDate);
            TfrxMemoView(DM.frxRepSzall.FindObject('Memo16')).Text := 'Szennyes d�tuma: ';
          end;
        lcOsztNev := TfgvUnit.Create();
        TfrxMemoView(DM.frxRepSzall.FindObject('mOsztNev')).Text := lcOsztNev.GetOsztalyNev(GOsztalyID);
        TfrxMemoView(DM.frxRepSzall.FindObject('mMozgnem')).Text := GOldal+GOlTipus;

        if GOldal = 'T' then
          begin
            TfrxMemoView(DM.frxRepSzall.FindObject('Memo16')).Text := 'Kisz�ll�t�s d�tuma: ';
            TfrxMemoView(DM.frxRepSzall.FindObject('mKiszDatum')).Text := DateToStr(GDate);
            TfrxMemoView(DM.frxRepSzall.FindObject('mLBekeszit')).Visible := true;
            TfrxMemoView(DM.frxRepSzall.FindObject('frxDBDSSzallsztet_bekeszit')).Font.Style := [fsBold];
            TfrxMemoView(DM.frxRepSzall.FindObject('frxDBDSSzallsztet_bekeszit')).Visible := true;
          end;
        DM.CDSUzem.First;
        while not DM.CDSUzem.Eof do
          begin
            if DM.CDSUzem.FieldByName('uzem_id').AsInteger = DM.CDSLoadXml.FieldByName('szfej_uzem').AsInteger  then
              begin
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
                TfrxMemoView(DM.frxRepSzall.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
              end;
            DM.CDSUzem.Next;
          end;

        DM.CDSFelhasz.First;
        while not DM.CDSFelhasz.Eof do
          begin
            if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
              begin
                TfrxMemoView(DM.frxRepSzall.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
              end;
            DM.CDSFelhasz.Next;
          end;

        while not DM.CDSPartnerek.Eof do
          begin
            if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
              begin
                TfrxMemoView(DM.frxRepSzall.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
              end;
            DM.CDSPartnerek.Next;
          end;
        DM.frxDBDataset1.First;
        while not DM.frxDBDataset1.Eof do
          begin
            if DM.frxDBDataset1.DataSet.FieldByName('sztet_javitott').AsInteger = 0 then
              TfrxMemoView(DM.frxRepSzall.FindObject('mJavitott')).Text := 'Nem'
            else if DM.frxDBDataset1.DataSet.FieldByName('sztet_javitott').AsInteger = 1 then
              TfrxMemoView(DM.frxRepSzall.FindObject('mJavitott')).Text := 'Igen' ;
            DM.frxDBDataset1.Next;
          end;

        {PDF ment�s Start}
        DM.frxPDFExport1.FileName := lcMentFajlNev+'.pdf';
        DM.frxPDFExport1.DefaultPath := GetCurrentDir;
        DM.frxPDFExport1.ShowDialog := false;
        DM.frxRepSzall.PrepareReport(true);
        DM.frxRepSzall.Export(DM.frxPDFExport1);
        {PDF ment�s End}

        DM.frxRepSzall.PrintOptions.Copies := 3;
        DM.frxRepSzall.PrintOptions.Collate := True;
        DM.frxRepSzall.PrepareReport(true);
        DM.frxRepSzall.ShowPreparedReport;

        {Beolvasott chip lista nyomtat�si adatok �ssze�ll�t�sa Start}
      if not GEnable then
        begin
        DM.CDSPartnerek.First;
        DM.FDMemChipOlvas.IndexFieldNames := 'Helyszinnev;Dolgozonev';
        DM.FDMemChipOlvas.IndexesActive := true;
        lcOsztNev := TfgvUnit.Create();
        TfrxMemoView(DM.frxOlvasChip.FindObject('mOsztalyNev')).Text := lcOsztNev.GetOsztalyNev(GOsztalyID);
        if GOldal = 'T' then
          begin
            TfrxMemoView(DM.frxOlvasChip.FindObject('Memo16')).Text := 'Kisz�ll�t�s d�tuma: ';
            TfrxMemoView(DM.frxOlvasChip.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          end;
        if GOldal = 'S' then
          begin
            TfrxMemoView(DM.frxOlvasChip.FindObject('Memo16')).Text := 'Szennyes d�tuma: ';
            TfrxMemoView(DM.frxOlvasChip.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          end;


        DM.CDSUzem.First;
        while not DM.CDSUzem.Eof do
          begin
            if DM.CDSUzem.FieldByName('uzem_id').AsInteger = DM.CDSLoadXml.FieldByName('szfej_uzem').AsInteger  then
              begin
                TfrxMemoView(DM.frxOlvasChip.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
                TfrxMemoView(DM.frxOlvasChip.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
                TfrxMemoView(DM.frxOlvasChip.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
                TfrxMemoView(DM.frxOlvasChip.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
                TfrxMemoView(DM.frxOlvasChip.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
              end;
            DM.CDSUzem.Next;
          end;

        while not DM.CDSPartnerek.Eof do
          begin
            if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
              begin
                TfrxMemoView(DM.frxOlvasChip.FindObject('mMegr')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
              end;
            DM.CDSPartnerek.Next;
          end;

        {Beolvasott chip lista nyomtat�si adatok �ssze�ll�t�sa Start}
        lcMentFajlNev := GOldal +'M' + copy(lcMentFajlNev,2,lcMentFajlNev.Length);

        DM.FDMemChipOlvas.First;
        while not DM.FDMemChipOlvas.Eof do
          begin
            DM.FDMemChipOlvas.Edit;
            DM.FDMemChipOlvas.FieldByName('Fajlnev').AsString := lcMentFajlNev+'.xml';
            DM.FDMemChipOlvas.FieldByName('Bizonylat').AsString := GSzallSorsz;
            DM.FDMemChipOlvas.Post;
            DM.FDMemChipOlvas.Next;
          end;

        {Beolvasott chip r�szletes lista pdf export Start}
        DM.frxPDFExport1.FileName := lcMentFajlNev+'.pdf';
        DM.frxPDFExport1.DefaultPath := GetCurrentDir;
        DM.frxPDFExport1.ShowDialog := false;
        DM.frxOlvasChip.PrepareReport(true);
        DM.frxOlvasChip.Export(DM.frxPDFExport1);
        {Beolvasott chip r�szletes lista pdf export End}

        {Beolvasott chip lista nyomtat�s Start}
        DM.FDMemChipOlvas.Filtered := true;

        if DM.FDMemChipOlvas.FieldByName('Vonalkod').AsString <> '' then
          begin
            DM.frxOlvasChip.PrepareReport(true);
            DM.frxOlvasChip.ShowPreparedReport;
          end;
        {Beolvasott chip lista nyomtat�s End}
        end;
      except
        on E : Exception do
          begin
            MessageBox(handle,'Nyomtat�si hiba!', 'Hibajelz�s!', MB_ICONERROR);
          end;
      end;
    end;

    if not GEnable then
      Begin
        {CHIP mozg�s f�jl ment�s Start}
        try
          if GOldal = 'T' then
            lcMentFajlNev := GOldal + 'M' + copy(lcMentFajlNev,2,lcMentFajlNev.Length);

          //Ellen�rizz�k van-e bizonylatsz�m.
          DM.FDMemChipOlvas.First;
          while not DM.FDMemChipOlvas.Eof do
            begin
              if DM.FDMemChipOlvas.FieldByName('Bizonylat').AsString = '' then
                begin
                  DM.FDMemChipOlvas.Edit;
                  DM.FDMemChipOlvas.FieldByName('Bizonylat').AsString := GSzallSorsz;
                  DM.FDMemChipOlvas.Post;
                end;
              if DM.FDMemChipOlvas.FieldByName('Fajlnev').AsString = '' then
                begin
                  DM.FDMemChipOlvas.Edit;
                  DM.FDMemChipOlvas.FieldByName('Fajlnev').AsString := lcMentFajlNev+'.xml';
                  DM.FDMemChipOlvas.Post;
                end;

              DM.FDMemChipOlvas.Next;
            end;

          lcDataSetToXML := TfgvUnit.Create();
          lcDataSetToXML.DataSetToXML(DM.FDMemChipOlvas, lcMentFajlNev+'.xml');
          lFolyamat.Caption := 'Sikeres mozg�s ment�s!';
         // MessageBox(handle,'Sikeres mozg�s ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
          lcDataSetToXML.Free;
        except
          on E : Exception do
             begin
               MessageBox(handle,'Sikertelen mozg�s ment�s!', 'Hibajelz�s!', MB_ICONERROR);
             end;
        end;
       {CHIP mozg�s f�jl ment�s End}

       {CHIP mozg�s https k�ld�s Start}
        //GOldal := 'S';
        try
          lcXmlPost := THttpsUnit.Create();
          if lcXmlPost.PostHttps(lcMentFajlNev+'.xml', lcIPCim, lcUtvonal,'chipmozgas/' ) <> '' then
            lFolyamat.Caption := 'Sikeres chip mozg�s k�ld�s!'
           // MessageBox(handle,'Sikeres chip mozg�s k�ld�s!', 'T�j�koztat�s!', MB_ICONINFORMATION)
          else
            MessageBox(handle,'Sikertelen k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
        except
          on E : Exception do
             begin
               MessageBox(handle,'Sikertelen chip mozg�s k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
             end;
        end;
       {CHIP mozg�s https k�ld�s End}

       {Chip lista ment�s Start}
        try
          lcDataSetToXML := TfgvUnit.Create();
          lcDataSetToXML.DataSetToXML(DM.CDSChipEllenorzes, GFajlNev+'.xml');
          lFolyamat.Caption := 'Sikeres chip lista ment�s!';
         // MessageBox(handle,'Sikeres chip lista ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
          lcDataSetToXML.Free;
        except
          on E : Exception do
             begin
               MessageBox(handle,'Sikertelen chip lista ment�s!', 'Hibajelz�s!', MB_ICONERROR);
             end;
        end;

      End;
  {Chip lista ment�s End}
  MessageBox(handle,'K�sz!', 'T�j�koztat�s!', MB_ICONINFORMATION);
  DM.FDMemChipOlvas.IndexFieldNames := '';
  DM.FDMemChipOlvas.IndexesActive := false;
  PanelShow(false,'');
  Application.ProcessMessages;

  SetCurrentDir(prUtMent);
  GOsztNev := '';
  GOsztalyID := 0;
  GSzallSorsz := '';
  Close;
end;

procedure TfrmSzennyesUj.btnBezarClick(Sender: TObject);
var
  lcLogs : TlogsUnit;
begin
  case MessageDlg('Nem mentette el az adatokat! Elfognak veszni. Biztos be akarja z�rni az ablakot? ', mtWarning, [mbYes, mbNo], 0) of
     mrNo:
       begin
         Abort;
       end;
     mrYes:
       begin
         DM.FDMemTextilek.Close;
         DM.CDSTextilek.Close;
         GOsztNev := '';
         GOldal := '';
         lcLogs := TlogsUnit.Create();
         lcLogs.LogFajlWrite(' - Program - frmSzennyesUj bez�r�s ment�s n�lk�l! ');
         lcLogs.Free;
         Close;
       end;
  end;
end;

procedure TfrmSzennyesUj.btnBovitesClick(Sender: TObject);
begin
//A bek�sz�tend� list�ban nem tal�lhat� egy textil, hozz� lehet adni.
  gbTextilek.Enabled := true;
  pFelso.Enabled := true;
  TextilPanelShow(true);
end;

procedure TfrmSzennyesUj.btnKesobbClick(Sender: TObject);
var
  lcMentFajlNev, lclabel, lcFajlNev, lcS_0 : String;
  lcOsszSuly : Double;
  lcPartnerMappa, lcMappaCreate, lcFajl, lcDataSetToXML : TfgvUnit;
  lcI_0 : Integer;
  lcSearchResult : TSearchRec;
begin
  btnBezar.Enabled :=false;
  btnAtvetel.Enabled := false;
  lcOsszSuly := 0;
  PanelShow(true,'K�rem v�rjon...');
  Application.ProcessMessages;

  lcFajlNev := prUtvonal;
  SetCurrentDir(prUtvonal);
  lcPartnerMappa := TfgvUnit.Create();
  if not DirectoryExists(lcPartnerMappa.PartnerMappa(GPartner)) then
    begin
      CreateDir(lcPartnerMappa.PartnerMappa(GPartner));
      prUtvonal := prUtvonal + '\' + lcPartnerMappa.PartnerMappa(GPartner);
    end
   else
     prUtvonal := prUtvonal + '\' + lcPartnerMappa.PartnerMappa(GPartner);
  SetCurrentDir(prUtvonal);
  lcMappaCreate := TfgvUnit.Create();
  if not directoryexists(lcMappaCreate.Mappa(GDate)) then
    begin
      CreateDir(lcMappaCreate.Mappa(GDate));
      prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
    end
  else
    prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
  SetCurrentDir(prUtvonal);
  lcFajl := TfgvUnit.Create();
  lcMentFajlNev := lcFajl.FajlNev(GDate);
  lcS_0 := lcMentFajlNev;
  lcI_0 := 0;
  //Ellen�rizni kell l�tezik-e m�r ilyen f�jl a kiv�lasztott napra.
  //Ha igen akkor sorsz�mozni kell. Pl.: 2018052813_0.xml
  if (FindFirst(lcS_0+'.xml', faAnyFile, lcSearchResult) = 0) AND (GTFajlNev <> lcS_0+'.xml') then
    begin
      while FindFirst(lcMentFajlNev +'.xml', faAnyFile, lcSearchResult) = 0 do
        begin
          lcMentFajlNev := lcS_0 + '_' + IntToStr(lcI_0);
          inc(lcI_0);
        end;
    end;

//A bek�sz�t�s k�s�bb folytat�shoz...
   if (FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) = 0) AND (GTFajlNev = lcS_0+'.xml') then
    begin
      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcMentFajlNev+'.xml';
          DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat := DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger * DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
          lcOsszSuly := lcOsszSuly + DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;

      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('szfej_osszsuly').AsFloat := lcOsszSuly;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;
      try
        lcDataSetToXML := TfgvUnit.Create();
        lcDataSetToXML.DataSetToXML(DM.FDMemTextilek, lcMentFajlNev+'.xml');
        lFolyamat.Caption := 'Sikeres sz�ll�t� ment�s!';
        //MessageBox(handle,'Sikeres sz�ll�t� ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
        lcDataSetToXML.Free;
      except
        on E : Exception do
           begin
             MessageBox(handle,'Sikertelen sz�ll�t� ment�s!', 'Hibajelz�s!', MB_ICONERROR);
           end;
      end;
    end;

   if (FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) <> 0) AND (GTFajlNev = '') then
    begin
      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcMentFajlNev+'.xml';
          DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat := DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger * DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
          lcOsszSuly := lcOsszSuly + DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;

      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('szfej_osszsuly').AsFloat := lcOsszSuly;
          DM.FDMemTextilek.Post;
          DM.FDMemTextilek.Next;
        end;
      try
        lcDataSetToXML := TfgvUnit.Create();
        lcDataSetToXML.DataSetToXML(DM.FDMemTextilek, lcMentFajlNev+'.xml');
        lFolyamat.Caption := 'Sikeres sz�ll�t� ment�s!';
        GTFajlNev := '';
        //MessageBox(handle,'Sikeres sz�ll�t� ment�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
        lcDataSetToXML.Free;
      except
        on E : Exception do
           begin
             MessageBox(handle,'Sikertelen sz�ll�t� ment�s!', 'Hibajelz�s!', MB_ICONERROR);
             GTFajlNev := '';
           end;
      end;
    end;


  MessageBox(handle,'K�sz!', 'T�j�koztat�s!', MB_ICONINFORMATION);
  PanelShow(false,'');
  Application.ProcessMessages;

  SetCurrentDir(prUtMent);
  GOsztNev := '';
  GOsztalyID := 0;
  GSzallSorsz := '';
  Close;
end;

procedure TfrmSzennyesUj.btnPanelBezarClick(Sender: TObject);
begin
  prBovites := false;
  TextilPanelShow(false);
end;

procedure TfrmSzennyesUj.DBGridAtveszTextDblClick(Sender: TObject);
var
  lcTalalt : Boolean;
begin
//Bek�sz�t�s. A bek�sz�tett darabsz�m be�r�sa.
  if GOldal = 'T' then
    begin
      DM.CDSTextilek.First;
      while not DM.CDSTextilek.Eof do
        begin
          if DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger = DBGridAtveszText.DataSource.DataSet.FieldByName('sztet_textilid').AsInteger then
            begin
              prBovites := false;
              break;
            end;
          DM.CDSTextilek.Next;
        end;

      while not DM.CDSTextilek.Eof do
        begin
          if (DM.CDSTextilek.FieldByName('kpt_id').AsInteger = DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texchip').AsBoolean = true) then
            begin
             MessageBox(handle,'Chipes textil nem szerkeszthet�!','Figyelmeztet�s!',MB_ICONWARNING);
             break;
            end
          else if (DM.CDSTextilek.FieldByName('kpt_id').AsInteger = DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texchip').AsBoolean = false) then
            begin
              lTextilNev.Caption := DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString;
              prBekeszitID := DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger;
              eBekeszit.SetFocus;
            end;
          DM.CDSTextilek.Next;
        end;
    end;
  if GOldal = 'S' then
    begin
      DM.CDSTextilek.First;
      while not DM.CDSTextilek.Eof do
        begin
          if (DM.CDSTextilek.FieldByName('kpt_id').AsInteger = DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texchip').AsBoolean = true) then
            begin
             MessageBox(handle,'Chipes textil nem szerkeszthet�!','Figyelmeztet�s!',MB_ICONWARNING);
             break;
            end
          else if (DM.CDSTextilek.FieldByName('kpt_id').AsInteger = DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texchip').AsBoolean = false) then
            begin
              prJavitas := true; //Ha a szennyesn�l jav�t�s kell, akkor figyelni kell.
              lTextilNev.Caption := DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString;
              prBekeszitID := DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger;
              eTexDarab.SetFocus;
            end;
          DM.CDSTextilek.Next;
        end;
    end;
end;

procedure TfrmSzennyesUj.dbgTextilBovitesDblClick(Sender: TObject);
var
  lcTalalt : Boolean;
begin
//A b�v�t�ssel felvitt textil hozz�ad�sa.
  DM.FDMemTextilek.First;
  lcTalalt := false;
  prBovites := false;
  if DM.FDMemTextilek.RecordCount > 0 then
    begin
      while not DM.FDMemTextilek.Eof do
        begin
          if dbgTextil.DataSource.DataSet.FieldByName('kpt_id').AsInteger = DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger then
            begin
              MessageBox(handle, 'Ezt m�r hozz�adtad!','Inform�ci�!', MB_ICONINFORMATION);
              lcTalalt := true;
              exit;
            end;
          DM.FDMemTextilek.Next;
        end;
    end;
  if not lcTalalt then
    begin
      lTextilNev.Caption := dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_smegnev').AsString;
      prBovites := true;
      eBekeszit.SetFocus;
      TextilPanelShow(false);
    end;
end;

procedure TfrmSzennyesUj.dbgTextilDblClick(Sender: TObject);
var
  lcTalalt : Boolean;
begin
  lcTalalt := false;
  if dbgTextil.DataSource.DataSet.FieldByName('kpt_texchip').AsBoolean = true then
    begin
      MessageBox(handle, 'Chipes textil nem szerkeszthet�!','Inform�ci�!', MB_ICONINFORMATION);
      lcTalalt := true;
      exit;
    end;


  DM.FDMemTextilek.First;
  if DM.FDMemTextilek.RecordCount > 0 then
    begin
      while not DM.FDMemTextilek.Eof do
        begin
          if dbgTextil.DataSource.DataSet.FieldByName('kpt_id').AsInteger = DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger then
            begin
              MessageBox(handle, 'Ezt m�r hozz�adtad!','Inform�ci�!', MB_ICONINFORMATION);
              lcTalalt := true;
            end;
          DM.FDMemTextilek.Next;
        end;
    end;
  if (not lcTalalt) and (DM.FDMemTextilek.RecordCount = 0) then
    begin
      lTextilNev.Caption := dbgTextil.DataSource.DataSet.FieldByName('kpt_smegnev').AsString;
      eTexDarab.SetFocus;
    end;
  if (not lcTalalt) and (DM.FDMemTextilek.RecordCount > 0) then
    begin
      lTextilNev.Caption := dbgTextil.DataSource.DataSet.FieldByName('kpt_smegnev').AsString;
      eTexDarab.SetFocus;
    end;
end;

procedure TfrmSzennyesUj.eBekeszitExit(Sender: TObject);
var
  lcMozgID, lcSzfejID : Integer;
  lcSzallito, lcFajlnev : String;
  lcKiszDatum : TDateTime;
  lcMozg : TfgvUnit;
begin
  //Bek�sz�t mez�t kell fel�l �rni itt.
  lcSzfejID := 0;
  if (eBekeszit.Text <> '') and (StrToInt(eBekeszit.Text) > 0) and not prBovites then
    begin
      try
        DM.FDMemTextilek.First;
        while not DM.FDMemTextilek.Eof do
          begin
            if DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger = prBekeszitID then
              begin
                DM.FDMemTextilek.Edit;
                DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := StrToInt(eBekeszit.Text);
                DM.FDMemTextilek.Post;
                eBekeszit.Text := '';
                lTextilNev.Caption := '';
                exit;
              end;
            DM.FDMemTextilek.Next;
          end;
      except
        eBekeszit.Text := '';
      end;
    end;

  //Adatgy�jt�s b�v�t�shez...
   lcSzallito := DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString;
   if DBGridAtveszText.DataSource.DataSet.RecordCount = 0 then
     begin
       lcKiszDatum := GDate;
       lcMozg := TfgvUnit.Create();
       lcMozgID := lcMozg.Mozgasnem;
     end
   else
     begin
       lcKiszDatum := DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime;
       lcMozgID := DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger;
       lcSzFejID := DM.FDMemTextilek.FieldByName('szfej_id').AsInteger;
     end;
   lcFajlnev := DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString;
  //B�v�t�s...
  if (eBekeszit.Text <> '') and (strtoint(eBekeszit.Text) > 0) and prBovites then
    begin
      try
        with DM.FDMemTextilek do
          begin
            Append;
            if lcSzFejID <> 0 then
              begin
                DM.FDMemTextilek.FieldByName('szfej_id').AsInteger := lcSzFejID;
              end;
            DM.FDMemTextilek.FieldByName('szfej_partner').AsInteger := dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_partnerId').AsInteger;
            DM.FDMemTextilek.FieldByName('szfej_uzem').AsInteger := dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_uzem').AsInteger;
            DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger := lcMozgID;
            DM.FDMemTextilek.FieldByName('szfej_osztid').AsInteger := GOsztalyID;
            DM.FDMemTextilek.FieldByName('szfej_osszsuly').Asfloat := 0.0;
            DM.FDMemTextilek.FieldByName('szfej_lastuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('szfej_lastup').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('szfej_delete').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('szfej_createdate').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := lcSzallito;
            DM.FDMemTextilek.FieldByName('szfej_kijelol').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat := DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat;
            DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime := lcKiszDatum;
            DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('sztet_osztid').AsInteger := GOsztalyID;
            DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_textsuly').AsFloat * StrToInt(eBekeszit.Text);
            DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger := dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_id').AsInteger;
            DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString := dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_smegnev').AsString;
            DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := dbgTextilBovites.DataSource.DataSet.FieldByName('kpt_textsuly').AsFloat;
            DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('sztet_lastup').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('sztet_delete').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('sztet_createuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('sztet_createdate').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('sztet_javitott').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := StrToInt(eBekeszit.Text);
            DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcFajlnev;
            DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger := DM.FDMemTextilek.RecordCount + 1;
            Post;
          end;
      except
        eBekeszit.Text := '';
      end;
    end;
  lcSzFejID := 0;
  prBovites := false;
  eBekeszit.Text := '';
  lTextilNev.Caption := '';
  gbTextilek.Enabled := false;
//  pFelso.Enabled := false;
end;

procedure TfrmSzennyesUj.eTexDarabExit(Sender: TObject);
var
  lcFajl, lcMozg : TfgvUnit;
begin
  //Szennyes jav�t�s!!!
  if prJavitas then
    begin
      DM.FDMemTextilek.First;
      while not DM.FDMemTextilek.Eof do
        begin
          if DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger = prBekeszitID then //prBekeszitID itt a textil id ja van benne.
            begin
              DM.FDMemTextilek.Edit;
              DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := StrToInt(eTexDarab.Text);
              DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat := DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat * StrToInt(eTexDarab.Text);
              DM.FDMemTextilek.Post;
            end;
          DM.FDMemTextilek.Next;
        end;
      eTexDarab.Text := '';
      lTextilNev.Caption := '';
      prJavitas := false;
    end;

  //itt ker�l bele a gridbe.
  if (eTexDarab.Text <> '') and (strtoint(eTexDarab.Text) > 0) and not prJavitas then
    begin
      try
        with DM.FDMemTextilek do
          begin
            Append;
            DM.FDMemTextilek.FieldByName('szfej_partner').AsInteger := dbgTextil.DataSource.DataSet.FieldByName('kpt_partnerId').AsInteger;
            DM.FDMemTextilek.FieldByName('szfej_uzem').AsInteger := dbgTextil.DataSource.DataSet.FieldByName('kpt_uzem').AsInteger;
            lcMozg := TfgvUnit.Create();
            DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger := lcMozg.Mozgasnem;
            DM.FDMemTextilek.FieldByName('szfej_osztid').AsInteger := GOsztalyID;
            DM.FDMemTextilek.FieldByName('szfej_osszsuly').Asfloat := 0.0;
            DM.FDMemTextilek.FieldByName('szfej_lastuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('szfej_lastup').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('szfej_delete').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('szfej_createdate').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
            DM.FDMemTextilek.FieldByName('szfej_kijelol').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat := GMertSuly;
            DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime := GDate;
            DM.FDMemTextilek.FieldByName('sztet_osztid').AsInteger := GOsztalyID;
            DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=dbgTextil.DataSource.DataSet.FieldByName('kpt_textsuly').AsFloat * StrToInt(eTexDarab.Text);
            DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger := dbgTextil.DataSource.DataSet.FieldByName('kpt_id').AsInteger;
            DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString := dbgTextil.DataSource.DataSet.FieldByName('kpt_smegnev').AsString;
            DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := StrToInt(eTexDarab.Text);
            DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := dbgTextil.DataSource.DataSet.FieldByName('kpt_textsuly').AsFloat;
            DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('sztet_lastup').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('sztet_delete').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('sztet_createuser').AsInteger := GUserID;
            DM.FDMemTextilek.FieldByName('sztet_createdate').AsDateTime := now();
            DM.FDMemTextilek.FieldByName('sztet_javitott').AsInteger := 0;
            DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := 0;
            lcFajl := TfgvUnit.Create();
            DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcFajl.FajlNev(GDate);
            DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger := DM.FDMemTextilek.RecordCount + 1;
            Post;
          end;
      except
        eTexDarab.Text := '';
      end;
    eTexDarab.Text := '';
    lTextilNev.Caption := '';
    end;
end;

procedure TfrmSzennyesUj.eTexDarabKeyPress(Sender: TObject; var Key: Char);
begin
 if (key in ['0'..'9',',']) or (key = #8) then
      inherited
 else key:= #0;
end;

procedure TfrmSzennyesUj.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DM.FDMemTextilek.Close;
  Action := caFree;
end;

procedure TfrmSzennyesUj.FormShow(Sender: TObject);
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes, lcFajl, lcMozg, lcTextilID : TfgvUnit;
  lcRekordSorszam, lcTextil, lcRecSorszam : Integer;
  lcLogs : TlogsUnit;
begin
  lcLogs := TlogsUnit.Create();
  lcLogs.LogFajlWrite(' - Program - frmSzennyesUj megnyit�sa! ');
  lcLogs.Free;
  prBovites := false;
  prJavitas := false;
  if GOldal = 'T' then
    begin
      gbTextilek.Enabled := false;
      gbKivOszt.Caption := 'Tiszta sz�ll�t�lev�l r�gz�t�se! - Kiv�lasztott partner - oszt�ly/telephely';
      DM.CDSTextilek.Filtered := false;
      DM.CDSTextilek.Filter := '';
      DM.CDSTextilek.Filtered := true;
      DM.CDSTextilek.First;
      while not DM.CDSTextilek.Eof do
        begin
          if DM.CDSTextilek.FieldByName('kpt_texchip').AsBoolean = false then
            begin
              //gbTextilek.Enabled := true;
              prBovites := true;
            end;
          DM.CDSTextilek.Next;
        end;
      eTexDarab.Visible := false;
    end;
  if GOldal = 'S' then
    begin
      gbKivOszt.Caption := 'Szennyes sz�ll�t�lev�l r�gz�t�se! - Kiv�lasztott partner - oszt�ly/telephely';
      eBekeszit.Visible := false;
      btnKesobb.Visible := false;
      btnBovites.Visible := false;
    end;

  DM.CDSTextilek.Filtered := false;
  DM.CDSTextilek.Filter := '';
  DM.CDSTextilek.Filtered := true;

  DM.CDSPartnerek.First;
  DM.CDSPartnerek.Filtered := false;
  DM.CDSPartnerek.Filter := 'par_id=''' + IntToStr(GPartner) + '''';
  DM.CDSPartnerek.Filtered := true;
  lKivOsztaly.Caption := DM.CDSPartnerek.FieldByName('par_nev').AsString + ' - ' + GOsztNev;
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  lcRecSorszam := 0;
  if GEnable and (GOldal = 'T') and (not GNincsTiszta) and (GTFajlNev <> '') then
    begin
      DM.FDMemTextilek.Close;
      DM.FDMemTextilek.Open;
      if GDatasetValtas then
        DM.CDSXMLMentes := DM.CDSBetoltTiszta;

      DM.CDSXMLMentes.First;
      while not DM.CDSXMLMentes.Eof do
        begin
          DM.FDMemTextilek.Append;
          DM.FDMemTextilek.FieldByName('szfej_partner').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_partner').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_uzem').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_uzem').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_mozgid').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_osztid').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_osztid').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_osszsuly').Asfloat := DM.CDSXMLMentes.FieldByName('szfej_osszsuly').AsFloat;
          DM.FDMemTextilek.FieldByName('szfej_lastuser').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_lastuser').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_lastup').AsDateTime := DM.CDSXMLMentes.FieldByName('szfej_lastup').AsDateTime;
          DM.FDMemTextilek.FieldByName('szfej_delete').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_delete').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_createuser').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_createdate').AsDateTime := DM.CDSXMLMentes.FieldByName('szfej_lastup').AsDateTime;

          if (DM.CDSXMLMentes.FieldByName('szfej_szallszam').AsString = '') then
            begin
              DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
            end
          else
            begin
              DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := DM.CDSXMLMentes.FieldByName('szfej_szallszam').AsString;
            end;

          DM.FDMemTextilek.FieldByName('szfej_kijelol').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_kijelol').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat := DM.CDSXMLMentes.FieldByName('szfej_mertsuly').AsFloat;
          DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime := DM.CDSXMLMentes.FieldByName('szfej_kiszdatum').AsDateTime;
          DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_lezart').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_osztid').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_osztid').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat := DM.CDSXMLMentes.FieldByName('sztet_osztsuly').AsFloat;
          DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_textilid').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString := DM.CDSXMLMentes.FieldByName('sztet_textilnev').AsString;
          DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_textildb').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := DM.CDSXMLMentes.FieldByName('sztet_textilsuly').AsFloat;
          DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_lastuser').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_lastup').AsDateTime := DM.CDSXMLMentes.FieldByName('sztet_lastup').AsDateTime;
          DM.FDMemTextilek.FieldByName('sztet_delete').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_delete').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_createuser').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_createuser').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_createdate').AsDateTime := DM.CDSXMLMentes.FieldByName('sztet_createdate').AsDateTime;
          DM.FDMemTextilek.FieldByName('sztet_javitott').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_javitott').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_bekeszit').AsInteger;
          DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := DM.CDSXMLMentes.FieldByName('sztet_fajlnev').AsString;
          DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_recsorsz').AsInteger;
          DM.FDMemTextilek.FieldByName('szfej_id').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger;
          DM.FDMemTextilek.Post;
          DM.CDSXMLMentes.Next;
        end;
    end;
   SetCurrentDir(prUtMent);
  if not GEnable then
    begin
      //pFelso.Enabled := false;
      if prBovites = false then
        begin
          btnBovites.Enabled := false;
          btnKesobb.Enabled := false;
        end;
    end;
 { if not GEnable then
    begin
      try
        DM.FDMemTextilek.Close;
        DM.FDMemTextilek.Open;
        lcRekordSorszam := 1;
        DM.FDMemChipOlvas.First;
        while not DM.FDMemChipOlvas.Eof do
          begin
            DM.FDMemTextilek.Filtered := false;
            DM.FDMemTextilek.Filter := 'sztet_textilnev=''' + (DM.FDMemChipOlvas.FieldByName('cikknev').AsString) + '''';
            DM.FDMemTextilek.Filtered := true;
            if DM.FDMemTextilek.RecordCount > 0 then
              begin
                lcTextilID := TfgvUnit.Create();
                lcTextil := lcTextilID.GetTextilID(DM.FDMemChipOlvas.FieldByName('cikknev').AsString);
                DM.CDSTextilek.Filtered := false;
                DM.CDSTextilek.Filter := 'kpt_id=''' +  IntToStr(lcTextil) + '''';
                DM.CDSTextilek.Filtered := true;
                DM.FDMemTextilek.Edit;
                if GOldal = 'T' then
                  begin
                    DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger + 1;
                    DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := 0;
                  end;
                if GOldal = 'S' then
                  begin
                    DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger + 1;
                  end;
                DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := DM.CDSTextilek.FieldByName('kpt_textsuly').AsFloat;
                DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger*DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
                DM.FDMemTextilek.Post;
              end
            else if DM.FDMemTextilek.RecordCount = 0 then
              begin
                DM.FDMemTextilek.Insert;
                DM.FDMemTextilek.FieldByName('szfej_partner').AsInteger := GPartner;
                DM.FDMemTextilek.FieldByName('szfej_uzem').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                lcMozg := TfgvUnit.Create();
                DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger := lcMozg.Mozgasnem;
                DM.FDMemTextilek.FieldByName('szfej_osztid').AsInteger := GOsztalyID;
                DM.FDMemTextilek.FieldByName('szfej_osszsuly').Asfloat := 0.0;
                DM.FDMemTextilek.FieldByName('szfej_lastuser').Asfloat := GUserID;
                DM.FDMemTextilek.FieldByName('szfej_lastup').AsDateTime := now();
                DM.FDMemTextilek.FieldByName('szfej_delete').AsInteger := 0;
                DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := GUserID;
                DM.FDMemTextilek.FieldByName('szfej_createdate').AsDateTime := DM.FDMemChipOlvas.FieldByName('Datum').AsDateTime;
                DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
                DM.FDMemTextilek.FieldByName('szfej_kijelol').AsInteger := 0;
                DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat := GMertSuly;
                DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime := GDate;
                DM.FDMemTextilek.FieldByName('sztet_osztid').AsInteger := GOsztalyID;
                lcTextilID := TfgvUnit.Create();
                DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger := lcTextilID.GetTextilID(DM.FDMemChipOlvas.FieldByName('cikknev').AsString);
                DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString := DM.FDMemChipOlvas.FieldByName('cikknev').AsString;
                DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := 1;
                DM.CDSTextilek.Filtered := false;
                DM.CDSTextilek.Filter := 'kpt_id=''' + IntToStr(DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger) + '''';
                DM.CDSTextilek.Filtered := true;
                DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := DM.CDSTextilek.FieldByName('kpt_textsuly').AsFloat;
                DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger*DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
                DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := GUserID;
                DM.FDMemTextilek.FieldByName('sztet_lastup').AsDateTime := now();
                DM.FDMemTextilek.FieldByName('sztet_delete').AsInteger := 0;
                DM.FDMemTextilek.FieldByName('sztet_createuser').AsInteger := GUserID;
                DM.FDMemTextilek.FieldByName('sztet_createdate').AsDateTime := now();
                DM.FDMemTextilek.FieldByName('sztet_javitott').AsInteger := 0;
                DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := 0;
                lcFajl := TfgvUnit.Create();
                DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcFajl.FajlNev(GDate);
                DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger := lcRekordSorszam;
                DM.FDMemTextilek.Post;
              end;
            DM.FDMemTextilek.Filtered := false;
            DM.FDMemTextilek.Filter := '';
            DM.FDMemTextilek.Filtered := true;
            DM.CDSTextilek.Filtered := false;
            DM.CDSTextilek.Filter := '';
            DM.CDSTextilek.Filtered := true;

            lcRekordSorszam := lcRekordSorszam +1;
            DM.FDMemChipOlvas.Next;
          end;
      except
        eTexDarab.Text := '';
      end;

    end;   }
  DM.CDSPartnerek.Filtered := false;
  DM.CDSPartnerek.Filter := '';
  DM.CDSPartnerek.Filtered := true;
end;

end.
