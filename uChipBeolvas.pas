unit uChipBeolvas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, Data.DB, Vcl.Grids, Vcl.DBGrids, INIFiles;

type
  TDataMars = class
  private
  public
    procedure open_com(Port,Param:string);
    procedure close_com;
    procedure Purge;
  end;

  TfrmChipBeolvas = class(TForm)
    pFelso: TPanel;
    pAlso: TPanel;
    btnChipStart: TButton;
    btnChipUjra: TButton;
    btnChipEnd: TButton;
    ilChip: TImageList;
    gbChipList: TGroupBox;
    gbTextilList: TGroupBox;
    gbInfo: TGroupBox;
    dbgTextilList: TDBGrid;
    mChipList: TMemo;
    lFelirat: TLabel;
    tOlvasas: TTimer;
    lWarning: TLabel;
    lValPartner: TLabel;
    gbOsszLista: TGroupBox;
    dbgOsszLista: TDBGrid;
    procedure tOlvasasTimer(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure Hozzaad;
    procedure OsszHozzaad;
    procedure BekeszitendoBetolt;
  public
    { Public declarations }
    portszam, portertek : String;
    beolvkod1 : String[16];
  end;

var
  frmChipBeolvas: TfrmChipBeolvas;
  fileid: Thandle;
  DataM : TDataMars;
  atveteliranya : Char;
  xtime : String;
  beolvkod : String[16];

implementation

uses uSzennyesUj, uTMRMain, uDM, uFgv, uLogs;
{$R *.dfm}

procedure TDataMars.open_com(Port,Param:string);
var
  Device,kiir : string;
  m : Dword;
  i : integer;
  kiirc : char;
  aDCB : DCB;
  CommTimeOuts : TCOMMTIMEOUTS;
begin
  Device:=format('\.\\%s', [Port]);
  fileid := CreateFile(PChar(Device), GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

  If fileid = INVALID_HANDLE_VALUE then
    raise Exception.Create('TDataMars: CreateFile failed');
  SetupComm(fileid,1024,1024);  //fileid,1024,1024
  FillChar(aDCB,sizeof(DCB),0);
  Device:=format('%s:%s',[Port,Param]);
  BuildCommDCB(PChar(Device),aDCB);
  SetCommState(fileid,aDCB);
  FillChar(CommTimeOuts, sizeof(TCOMMTIMEOUTS), 0);
//  CommTimeouts.ReadIntervalTimeout:=1;     //en�lk�l is megy
  CommTimeouts.ReadTotalTimeoutMultiplier:=1;
  SetCommTimeouts(fileid, CommTimeOuts);
    //soros portra �r�s
  //m:=0; //i:=0;
 // kiir :='*d'+Chr(10)+chr(13);
 { for i := 1 to Length(kiir) do
    begin
      kiirc:=kiir[i];
      WriteFile(fileid,kiirc,sizeof(kiirc),m,nil);
    end;}
end;

procedure TDataMars.close_com;
begin
  CloseHandle(fileid);
end;

procedure TDataMars.Purge;   // reset
begin
  PurgeComm(fileid,PURGE_TXCLEAR or PURGE_RXCLEAR);
end;

procedure TfrmChipBeolvas.BekeszitendoBetolt;
begin
  //Memtable megnyit�sa az adatok bet�lt�s�hez.
//  DM.FDMemTextilek.Close;
//  DM.FDMemTextilek.Open;
  if GDatasetValtas then
    DM.CDSXMLMentes := DM.CDSBetoltTiszta;

  if not GNincsTiszta then
    begin
  DM.CDSXMLMentes.Active := true;
  DM.CDSXMLMentes.First;
  //A cliensdatasetbe bet�lt�tt xml f�jl �tt�lt�se a memtable-be.
  while not DM.CDSXMLMentes.Eof do
    begin
      DM.FDMemTextilek.Append;
      DM.FDMemTextilek.FieldByName('szfej_id').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_partner').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_partner').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_uzem').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_uzem').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_mozgid').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_osztid').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_osztid').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_osszsuly').AsFloat := DM.CDSXMLMentes.FieldByName('szfej_osszsuly').AsFloat;
      DM.FDMemTextilek.FieldByName('szfej_lastuser').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_lastuser').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_lastup').AsDateTime := DM.CDSXMLMentes.FieldByName('szfej_lastup').AsDateTime;
      DM.FDMemTextilek.FieldByName('szfej_delete').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_delete').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_createuser').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_createdate').AsDateTime := DM.CDSXMLMentes.FieldByName('szfej_createdate').AsDateTime;
      DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := DM.CDSXMLMentes.FieldByName('szfej_szallszam').AsString;
      DM.FDMemTextilek.FieldByName('szfej_kijelol').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_kijelol').AsInteger;
      DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat := DM.CDSXMLMentes.FieldByName('szfej_mertsuly').AsFloat;
      DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime := DM.CDSXMLMentes.FieldByName('szfej_kiszdatum').AsDateTime;
      DM.FDMemTextilek.FieldByName('szfej_lezart').AsInteger := DM.CDSXMLMentes.FieldByName('szfej_lezart').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_osztid').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_osztid').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat := DM.CDSXMLMentes.FieldByName('sztet_osztsuly').AsFloat;
      DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_textilid').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString := DM.CDSXMLMentes.FieldByName('sztet_textilnev').AsString;
      DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_textildb').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := DM.CDSXMLMentes.FieldByName('sztet_textilsuly').AsFloat;
      DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_lastuser').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_lastup').AsDateTime := DM.CDSXMLMentes.FieldByName('sztet_lastup').AsDateTime;
      DM.FDMemTextilek.FieldByName('sztet_delete').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_delete').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_createdate').AsDateTime := DM.CDSXMLMentes.FieldByName('sztet_createdate').AsDateTime;
      DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := DM.CDSXMLMentes.FieldByName('sztet_fajlnev').AsString;
      DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_recsorsz').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_javitott').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_javitott').AsInteger;
      DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := DM.CDSXMLMentes.FieldByName('sztet_bekeszit').AsInteger;
      DM.FDMemTextilek.Post;
      DM.CDSXMLMentes.Next;
    end;
    end;
  if GNincsTiszta then
    begin
      DM.FDMemTextilek.Close;
      DM.FDMemTextilek.Open;
    end;
end;

procedure TfrmChipBeolvas.OsszHozzaad;
var
  lcRekordSorszam, lcTextil : integer;
  lcTextilID, lcMozg, lcFajl : TfgvUnit;
  lcLogs : TlogsUnit;
begin
  try
    begin
      DM.FDMemTextilek.Filtered := false;
      DM.FDMemTextilek.Filter := 'sztet_textilnev=''' + (DM.FDMemChipOlvas.FieldByName('cikknev').AsString) + '''';
      DM.FDMemTextilek.Filtered := true;
      if DM.FDMemTextilek.RecordCount > 0 then
        begin
          lcTextilID := TfgvUnit.Create();
          lcTextil := lcTextilID.GetTextilID(DM.FDMemChipOlvas.FieldByName('cikknev').AsString);
          DM.CDSTextilek.Filtered := false;
          DM.CDSTextilek.Filter := 'kpt_id=''' +  IntToStr(lcTextil) + '''';
          DM.CDSTextilek.Filtered := true;
          DM.FDMemTextilek.Edit;
          DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := DM.CDSTextilek.FieldByName('kpt_textsuly').AsFloat;
          if GOldal = 'T' then
            begin
              DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger + 1;
              DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger*DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
            end;
          if GOldal = 'S' then
            begin
              DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger + 1;
              DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger*DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
            end;
          DM.FDMemTextilek.Post;
        end
      else if DM.FDMemTextilek.RecordCount = 0 then
        begin
          DM.FDMemTextilek.Insert;
          DM.FDMemTextilek.FieldByName('szfej_partner').AsInteger := GPartner;
          DM.FDMemTextilek.FieldByName('szfej_uzem').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
          lcMozg := TfgvUnit.Create();
          DM.FDMemTextilek.FieldByName('szfej_mozgid').AsInteger := lcMozg.Mozgasnem;
          DM.FDMemTextilek.FieldByName('szfej_osztid').AsInteger := GOsztalyID;
          DM.FDMemTextilek.FieldByName('szfej_osszsuly').Asfloat := 0.0;
          DM.FDMemTextilek.FieldByName('szfej_lastuser').Asfloat := GUserID;
          DM.FDMemTextilek.FieldByName('szfej_lastup').AsDateTime := now();
          DM.FDMemTextilek.FieldByName('szfej_delete').AsInteger := 0;
          DM.FDMemTextilek.FieldByName('szfej_createuser').AsInteger := GUserID;
          DM.FDMemTextilek.FieldByName('szfej_createdate').AsDateTime := DM.FDMemChipOlvas.FieldByName('Datum').AsDateTime;
          DM.FDMemTextilek.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
          DM.FDMemTextilek.FieldByName('szfej_kijelol').AsInteger := 0;
          DM.FDMemTextilek.FieldByName('szfej_mertsuly').AsFloat := GMertSuly;
          DM.FDMemTextilek.FieldByName('szfej_kiszdatum').AsDateTime := GDate;
          DM.FDMemTextilek.FieldByName('sztet_osztid').AsInteger := GOsztalyID;
          lcTextilID := TfgvUnit.Create();
          DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger := lcTextilID.GetTextilID(DM.FDMemChipOlvas.FieldByName('cikknev').AsString);
          DM.FDMemTextilek.FieldByName('sztet_textilnev').AsString := DM.FDMemChipOlvas.FieldByName('cikknev').AsString;
          if GOldal='S' then
            begin
              DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger := 1;
              DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=DM.FDMemTextilek.FieldByName('sztet_textildb').AsInteger*DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
            end;
          DM.CDSTextilek.Filtered := false;
          DM.CDSTextilek.Filter := 'kpt_id=''' + IntToStr(DM.FDMemTextilek.FieldByName('sztet_textilid').AsInteger) + '''';
          DM.CDSTextilek.Filtered := true;
          DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat := DM.CDSTextilek.FieldByName('kpt_textsuly').AsFloat;
          DM.FDMemTextilek.FieldByName('sztet_lastuser').AsInteger := GUserID;
          DM.FDMemTextilek.FieldByName('sztet_lastup').AsDateTime := now();
          DM.FDMemTextilek.FieldByName('sztet_delete').AsInteger := 0;
          DM.FDMemTextilek.FieldByName('sztet_createuser').AsInteger := GUserID;
          DM.FDMemTextilek.FieldByName('sztet_createdate').AsDateTime := now();
          DM.FDMemTextilek.FieldByName('sztet_javitott').AsInteger := 0;
          if GOldal = 'T' then
            begin
              DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger := 1;
              DM.FDMemTextilek.FieldByName('sztet_osztsuly').AsFloat :=DM.FDMemTextilek.FieldByName('sztet_bekeszit').AsInteger*DM.FDMemTextilek.FieldByName('sztet_textilsuly').AsFloat;
            end;
            lcFajl := TfgvUnit.Create();
          DM.FDMemTextilek.FieldByName('sztet_fajlnev').AsString := lcFajl.FajlNev(GDate);
          DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger := DM.FDMemTextilek.FieldByName('sztet_recsorsz').AsInteger+1;
          DM.FDMemTextilek.Post;
        end;
      DM.FDMemTextilek.Filtered := false;
      DM.FDMemTextilek.Filter := '';
      DM.FDMemTextilek.Filtered := true;
      DM.CDSTextilek.Filtered := false;
      DM.CDSTextilek.Filter := '';
      DM.CDSTextilek.Filtered := true;
    end;
  except
    begin
      lcLogs := TlogsUnit.Create();
      lcLogs.LogFajlWrite(' - Hiba - MemTable-be t�lt�skor! ');
      lcLogs.Free;
    end;
  end;
end;

procedure TfrmChipBeolvas.Hozzaad;
var
  lcTextilID, lcMozg : TfgvUnit;
  lcSzoveg : String;

begin
  //Hozz�ad�s a beolvasotthoz.
  DM.CDSChip.First;
  lWarning.Caption := '';
  lcSzoveg := '';

  DM.CDSChip.filtered := false;
  DM.CDSChip.filter := 'cms_chipkod=''' + beolvkod + '''';
  DM.CDSChip.filtered := true;
  DM.CDSChip.IndexFieldNames:='cms_jelolesdatum';
  DM.CDSChip.Last;

  if (DM.CDSChip.FieldByName('cms_kpopartner').AsInteger <> GPartner) and (DM.CDSChip.RecordCount > 0) then
    begin
      mChipList.Lines.Append(beolvkod + ' - Rossz partner!');
      MessageBox(handle, PWideChar('Ezt a ruh�t rossz partnerhez r�gz�ted! - ' + DM.CDSChip.FieldByName('cms_vonalkod').AsString), 'Figyelmeztet�s!', MB_ICONWARNING);
      exit;
    end;

  if DM.CDSChip.RecordCount = 0 then
    begin
      mChipList.Lines.Append(beolvkod + ' - Ismeretlen chip!');
      gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
      exit;
    end;
  DM.CDSChip.Filtered := false;
  DM.CDSChip.Filter := '';
  DM.CDSChip.IndexFieldNames := '';
  DM.CDSChip.Filtered := true;

  with DM.CDSChipEllenorzes do
    begin
      filtered := false;
      filter := 'Delete = 0 and Chipkod=''' + beolvkod + '''';
      filtered := true;
      DM.CDSChip.filtered := false;
      DM.CDSChip.filter := 'cms_chipkod=''' + beolvkod + '''';
      DM.CDSChip.filtered := true;
      DM.CDSChip.IndexFieldNames:='cms_jelolesdatum';
      DM.CDSChip.Last;
      if RecordCount > 0 then
        begin
          filtered := false;
          filter := '';
          filtered := true;
          if DM.CDSChip.FieldByName('cms_statusz').AsString = 'K' then
            begin
              mChipList.Lines.Append(beolvkod + ' - Ma m�r ezt beolvastad! - Kivezetett! - ' + DM.CDSChip.FieldByName('cms_vonalkod').AsString + ' - ' + DM.CDSChip.FieldByName('cms_cikknev').AsString );
            end;
          if DM.CDSChip.FieldByName('cms_statusz').AsString = 'S' then
            begin
              mChipList.Lines.Append(beolvkod + ' - Ma m�r ezt beolvastad! - Selejt! - ' + DM.CDSChip.FieldByName('cms_vonalkod').AsString + ' - ' + DM.CDSChip.FieldByName('cms_cikknev').AsString );
            end;
          if DM.CDSChip.FieldByName('cms_statusz').AsString = 'E' then
            begin
              mChipList.Lines.Append(beolvkod + ' - Ma m�r ezt beolvastad! - Elvett! - ' + DM.CDSChip.FieldByName('cms_vonalkod').AsString + ' - ' + DM.CDSChip.FieldByName('cms_cikknev').AsString );
            end;
          if (DM.CDSChip.FieldByName('cms_statusz').AsString <> 'E') AND (DM.CDSChip.FieldByName('cms_statusz').AsString <> 'S') AND (DM.CDSChip.FieldByName('cms_statusz').AsString <> 'K') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Ma m�r ezt beolvastad! - ' + DM.CDSChip.FieldByName('cms_vonalkod').AsString + ' - ' + DM.CDSChip.FieldByName('cms_cikknev').AsString );
            end;
          gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
          exit;
        end
      else if RecordCount = 0 then
        begin
          filtered := false;
          filter :='';
          filtered := true;
          DM.CDSChipEllenorzes.Last;
          DM.CDSChipEllenorzes.Append;
          DM.CDSChipEllenorzes.FieldByName('Chipkod').AsString := beolvkod;
          DM.CDSChipEllenorzes.FieldByName('PartnerId').AsInteger := GPartner;
          DM.CDSChipEllenorzes.FieldByName('UzemId').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
          DM.CDSChipEllenorzes.FieldByName('OsztalyId').AsInteger := GOsztalyID;
          DM.CDSChipEllenorzes.FieldByName('MozgasId').AsInteger := lcMozg.Mozgasnem;
          lcTextilID := TfgvUnit.Create();
          DM.CDSChipEllenorzes.FieldByName('TextilId').AsInteger := lcTextilID.GetTextilID(DM.CDSChip.FieldByName('cms_cikknev').AsString);
          DM.CDSChipEllenorzes.FieldByName('FelhasznaloId').AsInteger := GUserID;
          DM.CDSChipEllenorzes.FieldByName('LastUp').AsDateTime := now();
          DM.CDSChipEllenorzes.FieldByName('Delete').AsInteger := 0;
          DM.CDSChipEllenorzes.FieldByName('CreateDate').AsDateTime := now();
          DM.CDSChipEllenorzes.FieldByName('SzallSorszam').AsString := GSzallSorsz;
          DM.CDSChipEllenorzes.FieldByName('FajlNev').AsString := GFajlNev;
          DM.CDSChipEllenorzes.Post;
        end;
    end;

  with DM.FDMemChipOlvas do
    begin
      filtered := false;
      filter := 'Chipkod=''' + beolvkod + '''';
      filtered := true;
      if RecordCount > 0 then
        begin
          filtered := false;
          filter := '';
          filtered := true;
          mChipList.Lines.Append(beolvkod + ' - Ezt m�r beolvastad! - ' + DM.FDMemChipOlvas.FieldByName('Vonalkod').AsString + ' - ' + DM.FDMemChipOlvas.FieldByName('cikknev').AsString );
          gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
          exit;
        end;
    end;
  DM.FDMemChipOlvas.filtered := false;
  DM.FDMemChipOlvas.filter := '';
  DM.FDMemChipOlvas.filtered := true;
  DM.CDSChip.filtered := false;
  DM.CDSChip.filter := '';
  DM.CDSChip.IndexFieldNames := '';
  DM.CDSChip.filtered := true;

  with DM.CDSChip do
    begin
      filtered := false;
      filter := 'cms_chipkod=''' + beolvkod + '''';
      filtered := true;
      IndexFieldNames:='cms_jelolesdatum';
      Last;
      if RecordCount > 0 then
        begin
          if (FieldByName('cms_statusz').AsString <> 'E') AND (FieldByName('cms_statusz').AsString <> 'K') and
          (FieldByName('cms_statusz').AsString <> 'S') AND (GOldal = 'T') then
            begin
              mChipList.Lines.Append(beolvkod);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              //DM.FDMemChipOlvas.Last;
              DM.FDMemChipOlvas.Append;
              DM.FDMemChipOlvas.FieldByName('Chipkod').AsString := beolvkod;
              DM.FDMemChipOlvas.FieldByName('Vonalkod').AsString := FieldByName('cms_vonalkod').AsString;
              DM.FDMemChipOlvas.FieldByName('Helyszinnev').AsString := FieldByName('cms_helyszinnev').AsString;
              DM.FDMemChipOlvas.FieldByName('Datum').AsDateTime := now();
              DM.FDMemChipOlvas.FieldByName('Dolgozonev').AsString := FieldByName('cms_dolgozonev').AsString;
              DM.FDMemChipOlvas.FieldByName('cikknev').AsString := FieldByName('cms_cikknev').AsString;
              if GOldal = 'S' then
                DM.FDMemChipOlvas.FieldByName('MozgasTipus').AsString := 'Tiszt�t�sra �tv�tel'
              else if GOldal = 'T' then
                DM.FDMemChipOlvas.FieldByName('MozgasTipus').AsString := 'Telephelyre kihelyez�s';
              DM.FDMemChipOlvas.FieldByName('Telephelykod').AsString := FieldByName('cms_telephelykod').AsString;
    //          DM.FDMemChipOlvas.FieldByName('Kivezetesoka').AsString := FieldByName('cms_telephelykod').AsString;
              DM.FDMemChipOlvas.FieldByName('Felhasznalo').AsInteger := DM.CDSFelhasz.FieldByName('felh_id').AsInteger;
              DM.FDMemChipOlvas.FieldByName('Bizonylat').AsString := GSzallSorsz;
              DM.FDMemChipOlvas.FieldByName('Mosoda').AsString := DM.CDSUzem.FieldByName('uzem_SAPparkod').AsString;
              DM.FDMemChipOlvas.FieldByName('Szekreny').AsString := FieldByName('cms_szekrenynev').AsString;;
              DM.FDMemChipOlvas.Post;
              gbTextilList.Caption := 'Beolvasott textil lista - azonos�tott ruh�k: ' + IntToStr(dbgTextilList.DataSource.DataSet.RecordCount) + ' db';
              OsszHozzaad;
            end
          else if (GOldal = 'S') AND (FieldByName('cms_statusz').AsString <> 'E') AND
          (FieldByName('cms_statusz').AsString <> 'K') and (FieldByName('cms_statusz').AsString <> 'S') then
            begin
              mChipList.Lines.Append(beolvkod);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              //DM.FDMemChipOlvas.Last;
              DM.FDMemChipOlvas.Append;
              DM.FDMemChipOlvas.FieldByName('Chipkod').AsString := beolvkod;
              DM.FDMemChipOlvas.FieldByName('Vonalkod').AsString := FieldByName('cms_vonalkod').AsString;
              DM.FDMemChipOlvas.FieldByName('Helyszinnev').AsString := FieldByName('cms_helyszinnev').AsString;
              DM.FDMemChipOlvas.FieldByName('Datum').AsDateTime := now();
              DM.FDMemChipOlvas.FieldByName('Dolgozonev').AsString := FieldByName('cms_dolgozonev').AsString;
              DM.FDMemChipOlvas.FieldByName('cikknev').AsString := FieldByName('cms_cikknev').AsString;
              if GOldal = 'S' then
                DM.FDMemChipOlvas.FieldByName('MozgasTipus').AsString := 'Tiszt�t�sra �tv�tel'
              else if GOldal = 'T' then
                DM.FDMemChipOlvas.FieldByName('MozgasTipus').AsString := 'Telephelyre kihelyez�s';
              DM.FDMemChipOlvas.FieldByName('Telephelykod').AsString := FieldByName('cms_telephelykod').AsString;
    //          DM.FDMemChipOlvas.FieldByName('Kivezetesoka').AsString := FieldByName('cms_telephelykod').AsString;
              DM.FDMemChipOlvas.FieldByName('Felhasznalo').AsInteger := DM.CDSFelhasz.FieldByName('felh_id').AsInteger;
              DM.FDMemChipOlvas.FieldByName('Bizonylat').AsString := GSzallSorsz;
              DM.FDMemChipOlvas.FieldByName('Mosoda').AsString := DM.CDSUzem.FieldByName('uzem_SAPparkod').AsString;
              DM.FDMemChipOlvas.FieldByName('Szekreny').AsString := FieldByName('cms_szekrenynev').AsString;;
              DM.FDMemChipOlvas.Post;
              gbTextilList.Caption := 'Beolvasott textil lista - azonos�tott ruh�k: ' + IntToStr(dbgTextilList.DataSource.DataSet.RecordCount) + ' db';
              OsszHozzaad;
            end
          else if (FieldByName('cms_statusz').AsString = 'K') AND (GOldal = 'S') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Kivezetett ruha! - ' + FieldByName('cms_vonalkod').AsString + ' - ' + FieldByName('cms_cikknev').AsString);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              exit;
            end
          else if (FieldByName('cms_statusz').AsString = 'S') AND (GOldal = 'S') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Selejtezett ruha!' + FieldByName('cms_vonalkod').AsString + ' - ' + FieldByName('cms_cikknev').AsString);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              exit;
            end
          else if (FieldByName('cms_statusz').AsString = 'E') AND (GOldal = 'S') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Elvett ruha!' + FieldByName('cms_vonalkod').AsString + ' - ' + FieldByName('cms_cikknev').AsString);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              exit;
            end
          else if (FieldByName('cms_statusz').AsString = 'K') AND (GOldal = 'T') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Kivezetett ruha! - ' + FieldByName('cms_vonalkod').AsString + ' - ' + FieldByName('cms_cikknev').AsString);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              lcSzoveg := (FieldByName('cms_vonalkod').AsString) + ' - Ezt a ruh�t kivezett�k nem adhat� ki!';
              MessageBox(handle, PWideChar(lcSzoveg), 'Figyelmeztet�s!', MB_ICONWARNING);
              exit;
            end
          else if (FieldByName('cms_statusz').AsString = 'S') AND (GOldal = 'T') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Selejtezett ruha! - ' + FieldByName('cms_vonalkod').AsString + ' - ' + FieldByName('cms_cikknev').AsString);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              lcSzoveg := FieldByName('cms_vonalkod').AsString + ' - Ezt a ruh�t selejtezt�k, nem adhat� ki!';
              MessageBox(handle, PWideChar(lcSzoveg), 'Figyelmeztet�s!', MB_ICONWARNING);
              exit;
            end
          else if (FieldByName('cms_statusz').AsString = 'E') AND (GOldal = 'T') then
            begin
              mChipList.Lines.Append(beolvkod + ' - Elvett ruha! - ' + FieldByName('cms_vonalkod').AsString + ' - ' + FieldByName('cms_cikknev').AsString);
              gbChipList.Caption := 'Beolvasott chip lista - beolvasott chip azonos�t�k: ' + IntToStr(mChipList.Lines.Count) + ' db';
              lcSzoveg := FieldByName('cms_vonalkod').AsString + ' - Ezt a ruh�t elvett�k, nem adhat� ki!';
              MessageBox(handle, PWideChar(lcSzoveg), 'Figyelmeztet�s!', MB_ICONWARNING);
              exit;
            end;
       end;
    end;

  DM.CDSChipEllenorzes.Filtered := false;
  DM.CDSChipEllenorzes.Filter := '';
  DM.CDSChipEllenorzes.Filtered := true;

  DM.CDSChip.Filtered := false;
  DM.CDSChip.Filter := '';
  DM.CDSChip.IndexFieldNames := '';
  DM.CDSChip.Filtered := true;
  beolvkod := '';
end;

procedure TfrmChipBeolvas.btnChipEndClick(Sender: TObject);
var jo : boolean;
begin
  case MessageDlg('Van m�g sz�moland� textil? ', mtConfirmation, [mbYes, mbNo], 0) of
      mrYes:
        begin
          jo:=False;
        end;
      mrNo:
        begin
          jo:=True;
        end;
  end;
  if jo then
    begin   {b}
      case MessageDlg('Biztos vagy benne?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            jo:=True;
          end;
        mrNo:
          begin
            jo:=False;
          end;
      end;
      if jo then
        begin  {c}
          tOlvasas.Enabled:=False;
          DataM.close_com;
          DataM.Purge;
          xtime:=TimeToStr(time);

          //chip_lerak;
          btnChipStart.Enabled:=True;
          btnChipEnd.Enabled := False;
          //GEnable := false;
          frmTMRMain.CreateModal(TfrmSzennyesUj);
          Close;
        end;  {c}
    end;{b}
end;

procedure TfrmChipBeolvas.btnChipStartClick(Sender: TObject);
begin
  if atveteliranya='K' then
    begin
      xtime:=TimeToStr(time);
    end;
  DataM.Purge ;
  DataM.open_com(portszam,portertek);   //('COM1,'19200,N,8,1')
  tOlvasas.Enabled:=True;
  btnChipStart.Enabled:=False;
  btnChipEnd.Enabled := true;
end;

procedure TfrmChipBeolvas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmChipBeolvas.FormShow(Sender: TObject);
var
  iniFile : TiniFile;
  lcUtvonalMent, prUtvonal, lcS_0 : String;
  lcPartnerMappa, lcMappaCreate, lcFajl : TfgvUnit;
  lcSearchResult : TSearchRec;
 // beallitasfile : TextFile;
  //portszam,portertek : string;
begin
  // Be�ll�t�sok kiolvals�sa a sz�vegf�jlb�l
  //men�ben a szennyes vagy tiszt�t letiltani
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  //  par:=IniFile.ReadString('kliens','Par',par);
  //  jel:=IniFile.ReadString('kliens','Jel',jel);
  portszam:=IniFile.ReadString('port','szam',portszam);
  portertek:=IniFile.ReadString('port','ertek',portertek);

  //  Label4.Caption:=IntToStr(IniFile.ReadInteger('kliens','chipossz',0));
  IniFile.Free;

  DM.FDMemChipOlvas.Close;
  DM.FDMemChipOlvas.Open;

  //Chip ellen�rz� f�jl bet�lt�se START.
  lcUtvonalMent := GetCurrentDir;
  SetCurrentDir(lcUtvonalMent+'\adat\');
  lcPartnerMappa := TfgvUnit.Create();
  if DirectoryExists(lcPartnerMappa.PartnerMappa(GPartner)) then
    begin
      prUtvonal := lcUtvonalMent +'\adat\' + lcPartnerMappa.PartnerMappa(GPartner) + '\';
    end;
  SetCurrentDir(prUtvonal);
  lcMappaCreate := TfgvUnit.Create();
  if directoryexists(lcMappaCreate.Mappa(GDate)) then
    begin
      prUtvonal := prUtvonal + lcMappaCreate.Mappa(GDate) + '\';
    end;
  SetCurrentDir(prUtvonal);

  lcFajl := TfgvUnit.Create();
  GFajlNev := GOldal + 'C' + lcFajl.Mappa(GDate);

  //�sszes�t� grid oszlop sz�less�gek be�ll�t�sa.
  DM.FDMemTextilek.FieldByName('sztet_textilnev').DisplayWidth := 18;
  DM.FDMemTextilek.FieldByName('sztet_textildb').DisplayWidth := 6;

  //Ellen�rizni kell l�tezik-e m�r ilyen f�jl a kiv�lasztott napra.
  //Ha igen akkor bet�ltj�k.
  if FindFirst(GFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
    begin
      DM.CDSChipEllenorzes.LoadFromFile(GFajlNev+'.xml');
      DM.CDSChipEllenorzes.LogChanges := false;
    end
  else
    begin
      //clientdataset strukt�ra l�trehoz�sa.
      DM.CDSChipEllenorzes.FieldDefs.Clear;
      DM.CDSChipEllenorzes.Active := false;
      DM.CDSChipEllenorzes.FieldDefs.Add('Chipkod',ftString,24);
      DM.CDSChipEllenorzes.FieldDefs.Add('PartnerId',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('UzemId',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('OsztalyId',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('MozgasId',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('TextilId',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('FelhasznaloId',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('LastUp',ftDateTime);
      DM.CDSChipEllenorzes.FieldDefs.Add('Delete',ftInteger);
      DM.CDSChipEllenorzes.FieldDefs.Add('CreateDate',ftDateTime);
      DM.CDSChipEllenorzes.FieldDefs.Add('SzallSorszam',ftString,45);
      DM.CDSChipEllenorzes.FieldDefs.Add('FajlNev',ftString,45);
      DM.CDSChipEllenorzes.CreateDataSet;
    end;
  //Chip ellen�rz� f�jl bet�lt�se END.

  if GOldal = 'T' then
    BekeszitendoBetolt; //Ha tiszta oldal �s van tiszta f�jl bet�ltj�k!

  SetCurrentDir(lcUtvonalMent);

  lValPartner.Caption := '';
  DM.CDSPartnerek.First;
  while not DM.CDSPartnerek.Eof do
    begin
      if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
        begin
          lValPartner.Caption := 'V�lasztott Partner: ' + DM.CDSPartnerek.FieldByName('par_nev').AsString;
          exit;
        end;
      DM.CDSPartnerek.Next;
    end;
end;

procedure TfrmChipBeolvas.tOlvasasTimer(Sender: TObject);
var
  lcM : DWORD;
  beolv : char;
  lcI, lcK, lcSzamol: Integer;  //beolv1
  beolva : array[1..180] of ansichar;
  beolvs : String;

begin
  Application.ProcessMessages;
  tOlvasas.Enabled:=False;
  //m:=0;
//  beolv:=chr(42); //hib�s beolvas�sok miatt

 { for lcI := 1 to 180 do
    begin
      beolva[lcI] := chr(42);
    end;
  }
 // ReadFile(fileid, beolv, sizeof(1), lcM, nil);
  if not ReadFile(fileid, beolva, sizeof(beolva), lcM, nil) then
    begin
//      Kiv�telkezel�s
    end;
  lcSzamol := 0;
  for lcK := 1 to lcM-2 do
    begin
      if (beolva[lcK]<>'') and (beolva[lcK]<>Chr(10)) and (beolva[lcK]<>Chr(13)) and (beolva[lcK]<>chr(42)) then
        begin
          beolvs := beolvs+beolva[lcK];
          beolvkod := beolvs;
          if (beolva[lcK+1]=Chr(13)) then
            begin
              if (beolva[lcK+2]=Chr(10)) then
                begin
                  Hozzaad;
                  beolvs := '';
                  beolvkod := '';
                end;
            end;
        end;
    end;
{  if length(beolvs) > 4 then
    begin
      beolvkod := beolvs;
      mChipList.Lines.Append(beolvkod);
      Hozzaad;
      beolvs := '';
    end;}

{  if (beolv=Chr(10)) or (beolv=Chr(13)) then beolvkod:='';  //ha entert olvas be null�z
  if (beolv<>'') and (beolv<>Chr(10)) and (beolv<>Chr(13)) and (beolv<>chr(42)) then
  begin
    beolvkod:=beolvkod+beolv;
    if Length(beolvkod)=16 then
      begin
        beolvkod1:=beolvkod;
        mChipList.Lines.Append(beolvkod);
      {  for lcI := 0 to mChipList.Lines.Count do
          begin
            if mChipList.Lines[lcI]=beolvkod
              then beolvkod1 := 'van';
          end;
        mChipList.Lines.Append(beolvkod);
//        Label1.Caption := IntToStr(mChipList.Lines.Count);     }
//        DM.FDMemChipOlvas.DisableControls;
//        Hozzaad;
//        beolvkod:='';
//      end;
//    end;
  tOlvasas.Enabled:=True;
end;

end.
