unit uPufferAttarolasBevet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, System.ImageList, Vcl.ImgList,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.ExtCtrls, INIFiles,
  Datasnap.DBClient, frxClass, frxDBSet, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfrmPufferAttarolasBevet = class(TForm)
    gbOsszLista: TGroupBox;
    ScrollBox1: TScrollBox;
    samplePanel: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    gbChipList: TGroupBox;
    mChipList: TMemo;
    gbInfo: TGroupBox;
    lInfoString: TLabel;
    gbTextilList: TGroupBox;
    dbgTextilList: TDBGrid;
    pAlso: TPanel;
    btnChipStart: TButton;
    btnChipEnd: TButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    GroupBox1: TGroupBox;
    btnChipHozzaad: TButton;
    chipEdit: TEdit;
    lKivalasztottTetel: TLabel;
    iPufferAttarolasBevet: TImageList;
    FDMemTableAttarolasBevet: TFDMemTable;
    FDMemTableAttarolasBevetchipkod: TStringField;
    FDMemTableAttarolasBevetvonalkod: TStringField;
    FDMemTableAttarolasBevettextilid: TIntegerField;
    FDMemTableAttarolasBevettextilnev: TStringField;
    FDMemTableAttarolasBevetfajtaid: TIntegerField;
    FDMemTableAttarolasBevetfajtanev: TStringField;
    FDMemTableAttarolasBevetallapotid: TIntegerField;
    FDMemTableAttarolasBevetallapotnev: TStringField;
    FDMemTableAttarolasBevetmeretid: TIntegerField;
    FDMemTableAttarolasBevetmeretnev: TStringField;
    FDMemTableAttarolasBevetmagassagid: TIntegerField;
    FDMemTableAttarolasBevetmagassagnev: TStringField;
    FDMemTableAttarolasBevetszinid: TIntegerField;
    FDMemTableAttarolasBevetszinnev: TStringField;
    FDMemTableAttarolasBevetdatum: TDateTimeField;
    FDMemTableAttarolasBevetmegjegyzes: TStringField;
    FDMemTableAttarolasBevetkivetelezesokid: TIntegerField;
    FDMemTableAttarolasBevetkivetelezesokanev: TStringField;
    DSAttarolasBevet: TDataSource;
    FDMemTableAttarolasBevetmennyiseg: TIntegerField;
    FDMemTableAttarolasBevetbevetelezve: TIntegerField;
    FDMemTableAttarolasBevetid: TIntegerField;
    FDMemTablePKBXML: TFDMemTable;
    FDMemTablePKBXMLpbfej_id: TIntegerField;
    FDMemTablePKBXMLpbfej_lezart: TIntegerField;
    FDMemTablePKBXMLpbfej_uzem_id: TIntegerField;
    FDMemTablePKBXMLpbfej_partner_id: TIntegerField;
    FDMemTablePKBXMLpbfej_osztaly_id: TIntegerField;
    FDMemTablePKBXMLpbfej_sorszam: TStringField;
    FDMemTablePKBXMLpbfej_mozgas_id: TIntegerField;
    FDMemTablePKBXMLpbfej_felhasznalo_id: TIntegerField;
    FDMemTablePKBXMLpbfej_megjegyzes: TStringField;
    FDMemTablePKBXMLpbfej_create_date: TDateTimeField;
    FDMemTablePKBXMLpbfej_create_user: TIntegerField;
    FDMemTablePKBXMLpbfej_lastupdate_date: TDateTimeField;
    FDMemTablePKBXMLpbfej_lastupdate_user: TIntegerField;
    FDMemTablePKBXMLpbfej_selected_date: TDateTimeField;
    FDMemTablePKBXMLpbtet_darabszam: TIntegerField;
    FDMemTablePKBXMLpbtet_fajta_id: TIntegerField;
    FDMemTablePKBXMLpbtet_allapot_id: TIntegerField;
    FDMemTablePKBXMLpbtet_meret_id: TIntegerField;
    FDMemTablePKBXMLpbtet_magassag_id: TIntegerField;
    FDMemTablePKBXMLpbtet_szin_id: TIntegerField;
    FDMemTablePKBXMLpbtet_textil_id: TIntegerField;
    FDMemTablePKBXMLpbtet_megjegyzes: TStringField;
    FDMemTablePKBXMLpbtet_create_date: TDateTimeField;
    FDMemTablePKBXMLpbtet_create_user: TIntegerField;
    FDMemTablePKBXMLpbtet_lastupdate_date: TDateTimeField;
    FDMemTablePKBXMLpbtet_lastupdate_user: TIntegerField;
    FDMemTablePKBXMLpbtet_darabosdb: TIntegerField;
    FDMemTablePKBXMLpbtet_chiphez_rendelve: TIntegerField;
    FDMemTablePKBXMLpbtet_fajlnev: TStringField;
    FDMemTablePKBXMLallapotText: TStringField;
    FDMemTablePKBXMLfajtaText: TStringField;
    FDMemTablePKBXMLszinText: TStringField;
    FDMemTablePKBXMLmeretText: TStringField;
    FDMemTablePKBXMLmagassagText: TStringField;
    FDMemTablePKBXMLmegnevezesText: TStringField;
    FDMemTablePKBMXML: TFDMemTable;
    FDMemTablePKBMXMLsorszam: TStringField;
    FDMemTablePKBMXMLpartner_id: TIntegerField;
    FDMemTablePKBMXMLuzem_id: TIntegerField;
    FDMemTablePKBMXMLosztaly_id: TIntegerField;
    FDMemTablePKBMXMLprszall_fej_id: TIntegerField;
    FDMemTablePKBMXMLvonalkod: TStringField;
    FDMemTablePKBMXMLchipkod: TStringField;
    FDMemTablePKBMXMLmozgas_id: TIntegerField;
    FDMemTablePKBMXMLtextil_kpt_id: TIntegerField;
    FDMemTablePKBMXMLtextil: TStringField;
    FDMemTablePKBMXMLfajta_id: TIntegerField;
    FDMemTablePKBMXMLfajta: TStringField;
    FDMemTablePKBMXMLallapot_id: TIntegerField;
    FDMemTablePKBMXMLallapot: TStringField;
    FDMemTablePKBMXMLmeret_id: TIntegerField;
    FDMemTablePKBMXMLmeret: TStringField;
    FDMemTablePKBMXMLmagassag_id: TIntegerField;
    FDMemTablePKBMXMLmagassag: TStringField;
    FDMemTablePKBMXMLszin_id: TIntegerField;
    FDMemTablePKBMXMLszin: TStringField;
    FDMemTablePKBMXMLdatum: TDateTimeField;
    FDMemTablePKBMXMLfelhasznalo_id: TIntegerField;
    FDMemTablePKBMXMLfajlnev: TStringField;
    FDMemTablePKBMXMLDarabosbol: TBooleanField;
    FDMemTablePKBMXMLkivezetesoka: TIntegerField;
    FDMemTablePKBMXMLkivezetesokanev: TStringField;
    FDMemTablePKBMXMLuploaded: TIntegerField;
    frxReportPKB: TfrxReport;
    frxDBDatasetPKB: TfrxDBDataset;
    frxReportPKBM: TfrxReport;
    frxDBDatasetPKBM: TfrxDBDataset;
    CheckBox1: TCheckBox;
    FDMemTablePKBXMLbevetelezve: TIntegerField;
    FDMemTablePKBMXMLbevetelezve: TStringField;
    FDMemTablePKBXMLmennyiseg: TIntegerField;
    FDMemTablePKBXMLpbfej_ellenuzem_id: TIntegerField;
    FDMemTablePKBMXMLellenuzem_id: TIntegerField;
    FDMemTableAttarolasBevetpartnerid: TIntegerField;
    FDMemTableAttarolasBevetuzemid: TIntegerField;
    FDMemTablePKBXMLpbfej_ellen_sorszam: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure dbgTextilListCellClick(Column: TColumn);
    procedure dbgTextilListDblClick(Sender: TObject);
    procedure btnChipHozzaadClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure chipEditKeyPress(Sender: TObject; var Key: Char);
    procedure DSAttarolasBevetDataChange(Sender: TObject; Field: TField);
    procedure dbgTextilListDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
    prCloseOnSave: boolean;
    prDataLoaded: boolean;
    prEditId: integer;
    prPanels: array of TComponent;
    prNextPanel: integer;
    procedure RedrawOsszPanel;
  public
    { Public declarations }
    pubSzfejId, pubFeladoUzem: integer;
    pubEllenSorszam: string;
    procedure pubHandleRFID(rfid: string);
  end;

var
  frmPufferAttarolasBevet: TfrmPufferAttarolasBevet;

implementation

{$R *.dfm}


uses
  uSerialReader, uDM, uFgv, uHttps;

var
  SerialReader: TSerialReader;

  { Component Copy }
function CopyComponent(Component,AParent: TComponent; NewComponentName: String): TComponent;
var
 Stream: TMemoryStream;
 S: String;
begin
 Result := TComponentClass(Component.ClassType).Create(Component.Owner);
 S := Component.Name;
 Component.Name := NewComponentName;
 Stream := TMemoryStream.Create;
 try
   Stream.WriteComponent(Component);
   Component.Name := S;
   Stream.Seek(0, soFromBeginning);
   TWinControl(AParent).InsertControl(TControl(Result));
   Stream.ReadComponent(Result);
 finally
   Stream.Free;
 end;
end;
// AComponent: the original component
// AOwner, AParent: the owner and parent of the new copy
// returns the new component

function CopyComponents(AComponent, AOwner, AParent: TComponent;Index:Integer): TComponent;
var
  i : integer;
  NewComponent: TComponent;
begin
  NewComponent := CopyComponent(AComponent,AParent,AComponent.Name+IntToStr(Index))as TComponentClass(AComponent.ClassType);{}
 // now, search for subcomponents
 for i:=0 to AComponent.Owner.ComponentCount-1 do
   if TWinControl(AComponent.Owner.Components[i]).Parent=AComponent then
     // and copy them too
     CopyComponents(AComponent.Owner.Components[i], AOwner, NewComponent,Index);
  Result:=NewComponent;
end;

procedure SetPanelLabels(AComponent: TComponent; AFajta, AAllapot, ASzin, AMeret, AMagassag, AMegnev: string; AMennyiseg: integer);
var i: integer;
begin
  for i := 0 to AComponent.Owner.ComponentCount-1 do
    begin
      if (AComponent.Owner.Components[i].ClassType = TLabel) and (TWinControl(AComponent.Owner.Components[i]).Parent = AComponent) then
        begin
          if TLabel(AComponent.Owner.Components[i]).Caption = 'lFajta' then
            TLabel(AComponent.Owner.Components[i]).Caption := AFajta
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'l�llapot' then
            TLabel(AComponent.Owner.Components[i]).Caption := AAllapot
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lSz�n' then
            TLabel(AComponent.Owner.Components[i]).Caption := ASzin
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lM�' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMeret
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMa' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMagassag
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lDarab' then
            TLabel(AComponent.Owner.Components[i]).Caption := IntToStr(AMennyiseg)
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMegnev' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMegnev
        end;
    end;
end;

{ End of Component Copy }

function GetTextilNev(id: integer): string;
begin
  result := '';
  DM.CDSTextilek.Filtered := false;
  DM.CDSTextilek.Filter := 'kpt_id='+IntToStr(id);
  DM.CDSTextilek.Filtered := true;
  DM.CDSTextilek.First;
  result := DM.CDSTextilek.FieldByName('kpt_smegnev').AsString;
  DM.CDSTextilek.Filtered := false;
end;

function GetTextilFajta(id: integer): string;
begin
  result := '';
  DM.CDSTextilFajta.Filtered := false;
  DM.CDSTextilFajta.Filter := 'fajta_id='+IntToStr(id);
  DM.CDSTextilFajta.Filtered := true;
  DM.CDSTextilFajta.First;
  result := DM.CDSTextilFajta.FieldByName('fajta_nev').AsString;
  DM.CDSTextilFajta.Filtered := false;
end;

function GetTextilAllapot(id: integer): string;
begin
  result := '';
  DM.CDSTextilAllapot.Filtered := false;
  DM.CDSTextilAllapot.Filter := 'allapot_id='+IntToStr(id);
  DM.CDSTextilAllapot.Filtered := true;
  DM.CDSTextilAllapot.First;
  result := DM.CDSTextilAllapot.FieldByName('allapot_nev').AsString;
  DM.CDSTextilAllapot.Filtered := false;
end;

function GetTextilMeret(id: integer): string;
begin
  result := '';
  DM.CDSTextilMeret.Filtered := false;
  DM.CDSTextilMeret.Filter := 'meret_id='+IntToStr(id);
  DM.CDSTextilMeret.Filtered := true;
  DM.CDSTextilMeret.First;
  result := DM.CDSTextilMeret.FieldByName('meret_nev').AsString;
  DM.CDSTextilMeret.Filtered := false;
end;

function GetTextilMagassag(id: integer): string;
begin
  result := '';
  DM.CDSTextilMagassag.Filtered := false;
  DM.CDSTextilMagassag.Filter := 'magassag_id='+IntToStr(id);
  DM.CDSTextilMagassag.Filtered := true;
  DM.CDSTextilMagassag.First;
  result := DM.CDSTextilMagassag.FieldByName('magassag_nev').AsString;
  DM.CDSTextilMagassag.Filtered := false;
end;

function GetTextilSzin(id, textilid: integer): string;
begin
  result := '';
  DM.CDSTextilSzinek.Filtered := false;
  DM.CDSTextilSzinek.Filter := 'kpsz_szin_id='+IntToStr(id)+' and kpsz_kpt_id='+IntToStr(textilid);
  DM.CDSTextilSzinek.Filtered := true;
  DM.CDSTextilSzinek.First;
  result := DM.CDSTextilSzinek.FieldByName('szin_nev').AsString;
  DM.CDSTextilSzinek.Filtered := false;
end;


procedure TfrmPufferAttarolasBevet.pubHandleRFID(rfid: string);
begin
  // Chip olvas�s
  FDMemTableAttarolasBevet.First;
  while not FDMemTableAttarolasBevet.Eof do
    begin
      if FDMemTableAttarolasBevet.FieldByName('chipkod').AsString = rfid then
        begin
          if FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger = 0 then
            begin
              FDMemTableAttarolasBevet.Edit;
              FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger := 1;
              FDMemTableAttarolasBevet.Post;
              mChipList.Lines.Add('A '+rfid+' chipk�d bev�telezve.');
              RedrawOsszPanel;
            end
          else
            begin
              mChipList.Lines.Add('A '+rfid+' chipk�d m�r be van v�telezve.');
            end;
          exit;
        end;
      FDMemTableAttarolasBevet.Next;
    end;
  mChipList.Lines.Add('A '+rfid+' chipk�d nem szerepel a bizonylaton.');
end;

procedure TfrmPufferAttarolasBevet.btnChipEndClick(Sender: TObject);
begin
  btnChipStart.Enabled := true;
  btnChipEnd.Enabled := false;
  SerialReader.Stop;
end;

procedure TfrmPufferAttarolasBevet.btnChipHozzaadClick(Sender: TObject);
begin
  FDMemTableAttarolasBevet.Tag := 1;
  FDMemTableAttarolasBevet.First;
  while not FDMemTableAttarolasBevet.Eof do
    begin
      if prEditId = FDMemTableAttarolasBevet.FieldByName('id').AsInteger then
        begin
          if StrToInt(chipEdit.Text) < 0 then
            begin
              ShowMessage('Negat�v mennyis�get nem lehet megadni!');
              Exit;
            end;
          if FDMemTableAttarolasBevet.FieldByName('mennyiseg').AsInteger >= StrToInt(chipEdit.Text) then
            begin
              FDMemTableAttarolasBevet.Edit;
              FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger := StrToInt(chipEdit.Text);
              FDMemTableAttarolasBevet.Post;
              FDMemTableAttarolasBevet.Tag := 0;
              RedrawOsszPanel;
              FDMemTableAttarolasBevet.Tag := 1;
              prEditId := -1;
              lKivalasztottTetel.Caption := '';
              chipEdit.Enabled := false;
              FDMemTableAttarolasBevet.First;
              while not FDMemTableAttarolasBevet.Eof do
                begin
                  if prEditId = FDMemTableAttarolasBevet.FieldByName('id').AsInteger then
                    begin
                      FDMemTableAttarolasBevet.Tag := 0;
                      exit;
                    end;
                  FDMemTableAttarolasBevet.Next;
                end;
              FDMemTableAttarolasBevet.Tag := 0;
            end
          else
            begin
              ShowMessage('A bev�telezett mennyis�g nem haladhatja meg a bizonylaton szerepl� be�rkezett mennyis�get!');
            end;
          FDMemTableAttarolasBevet.Tag := 0;
          exit;
        end;
      FDMemTableAttarolasBevet.Next;
    end;
  FDMemTableAttarolasBevet.Tag := 0;
end;

procedure TfrmPufferAttarolasBevet.btnChipStartClick(Sender: TObject);
begin
  btnChipStart.Enabled := False;
  btnChipEnd.Enabled := true;
  SerialReader.Execute;
end;

procedure TfrmPufferAttarolasBevet.Button1Click(Sender: TObject);

  function FindInOsszesitesPKB: boolean;
  begin
    result := false;
    FDMemTablePKBXML.First;
    while not FDMemTablePKBXML.Eof do
      begin
        if
          (FDMemTablePKBXML.FieldByName('megnevezesText').AsString = FDMemTableAttarolasBevet.FieldByName('textilnev').AsString) and
          (FDMemTablePKBXML.FieldByName('fajtaText').AsString = FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString) and
          (FDMemTablePKBXML.FieldByName('allapotText').AsString = FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString) and
          (FDMemTablePKBXML.FieldByName('meretText').AsString = FDMemTableAttarolasBevet.FieldByName('meretnev').AsString) and
          (FDMemTablePKBXML.FieldByName('magassagText').AsString = FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString) and
          (FDMemTablePKBXML.FieldByName('szinText').AsString = FDMemTableAttarolasBevet.FieldByName('szinnev').AsString)
        then
          begin
            result := true;
            exit;
          end;
        FDMemTablePKBXML.Next;
      end;
  end;

  function prGetMozgasnemNev(A_mozgId: Integer): String;
  Begin
    Result:= '';
    DM.CDSMozgnem.First;
    while not DM.CDSMozgnem.Eof do
      begin
        if DM.CDSMozgnem.FieldByName('moz_id').AsInteger = A_mozgId then
          Begin
            Result:= DM.CDSMozgnem.FieldByName('moz_kod').asString;
            exit;
          End;
        DM.CDSMozgnem.Next;
      end;
  End;

  procedure ChangeFieldValueInCDS(ADataSet: TClientDataSet; AFilePath, AFieldName: String; AValue: Variant; Rename: boolean);
  var lcFgv: TFgvUnit;
  begin
    lcFgv := TfgvUnit.Create;
    ADataSet.LoadFromFile(AFilePath);
    ADataSet.LogChanges := false;
    ADataSet.First;
    while not ADataSet.Eof do
      begin
        ADataSet.Edit;
        ADataSet.FieldByName(AFieldName).AsVariant := AValue;
        ADataSet.Post;
        ADataSet.Next;
      end;
    lcFgv.DataSetToXML(ADataSet, AFilePath);
    if Rename then
      RenameFile(AFilePath, '_uploaded_'+AFilePath);
    lcFgv.Free;
  end;

var
  lcFgv: TfgvUnit;
  lcMost: TDateTime;
  lcRootDir: string;
  lcAdatMappa: string;
  lcAdatUtvonal: string;
  lcSearchResult: TSearchRec;
  lcHttpRequest: THttpsUnit;
  lcHttpResponse: string;
  lcIniFile: TIniFile;
  lcIPCim, lcUtvonal: string;
  lcDataset: TClientDataSet;
  lcPKBFajlNev: String;
  lcPKBMFajlNev: String;
  lcSorszam: String;
  lcTmpId: string;
  lcOsztalyId: Integer;
  lcOsztalyNev: String;
  i, pufferPartner: integer;
  lcPufferPartnerek: TClientDataSet;
begin
  case MessageDlg('Biztosan lez�rja a bev�telez�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        FDMemTableAttarolasBevet.Filtered := false;
        if FDMemTableAttarolasBevet.RecordCount = 0 then
          begin
            ShowMessage('�res bev�telez�st nem lehet r�gz�teni.');
            exit;
          end;
        // Create-k
        lcFgv := TfgvUnit.Create;
        lcHttpRequest := THttpsUnit.Create();
        lcDataset := TClientDataSet.Create(Application);
        lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
        lcIPCim := lciniFile.ReadString('serverip','ip','');
        lcUtvonal := lciniFile.ReadString('utvonal','path','');
        //K�vetkez� sorsz�m a lok�lis f�jlb�l. Nem k�ldj�k el a v�ltoztat�st a szervernek!
        lcSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PKB', GPartner, GUserID, true, false);

        FDMemTablePKBXML.Close;
        FDMemTablePKBXML.Open;

        FDMemTablePKBMXML.Close;
        FDMemTablePKBMXML.Open;

        lcMost := now();

        lcPufferPartnerek := TClientDataSet.Create(self);
        lcPufferPartnerek.LoadFromFile('uzemek.xml');
        lcPufferPartnerek.LogChanges := false;
        lcPufferPartnerek.First;
        while not lcPufferPartnerek.Eof do
          begin
            if (lcPufferPartnerek.FieldByName('uzem_id').AsInteger = pubFeladoUzem) then
              begin
                pufferPartner := lcPufferPartnerek.FieldByName('uzem_puffer_raktar_id').AsInteger;
              end;
            lcPufferPartnerek.Next;
          end;


        // �tvonalak, f�jlok
        lcRootDir := GRootPath;
        lcAdatMappa := GetCurrentDir + '\adat\';
        SetCurrentDir(lcAdatMappa);

        if not DirectoryExists(lcFgv.PartnerMappa(GPartner)) then
          CreateDir(lcFgv.PartnerMappa(GPartner));

        lcAdatUtvonal := lcAdatMappa + lcFgv.PartnerMappa(GPartner);
        SetCurrentDir(lcAdatUtvonal);

        if not DirectoryExists(Trim(lcFgv.Mappa(lcMost))) then
          CreateDir(Trim(lcFgv.Mappa(lcMost)));

        lcAdatUtvonal := lcAdatUtvonal + '\' + Trim(lcFgv.Mappa(lcMost));

        GOldal := 'PKB';
        lcPKBFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';

        GOldal := 'PKBM';
        lcPKBMFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        i := 0;
        try
          // Adapterek felt�lt�se
          FDMemTableAttarolasBevet.First;
          while not FDMemTableAttarolasBevet.Eof do
            begin
              // PKK.XML

              if not FindInOsszesitesPKB then
                begin

                  FDMemTablePKBXML.Insert;
                  FDMemTablePKBXML.FieldByName('fajtaText').AsString := FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString;
                  FDMemTablePKBXML.FieldByName('allapotText').AsString := FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString;
                  FDMemTablePKBXML.FieldByName('meretText').AsString := FDMemTableAttarolasBevet.FieldByName('meretnev').AsString;
                  FDMemTablePKBXML.FieldByName('magassagText').AsString := FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString;
                  FDMemTablePKBXML.FieldByName('szinText').AsString := FDMemTableAttarolasBevet.FieldByName('szinnev').AsString;
                  FDMemTablePKBXML.FieldByName('megnevezesText').AsString := FDMemTableAttarolasBevet.FieldByName('textilnev').AsString;

                  FDMemTablePKBXML.FieldByName('pbfej_id').AsInteger := -1;
                  FDMemTablePKBXML.FieldByName('pbfej_lezart').AsInteger := 1;
                  FDMemTablePKBXML.FieldByName('pbfej_sorszam').AsString := lcSorszam;
                  FDMemTablePKBXML.FieldByName('pbfej_uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbfej_partner_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('partnerid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbfej_ellenuzem_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('uzemid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbfej_osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePKBXML.FieldByName('pbfej_mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PKB;
                  FDMemTablePKBXML.FieldByName('pbfej_felhasznalo_id').AsInteger := GUserID;
                  FDMemTablePKBXML.FieldByName('pbfej_megjegyzes').AsString := '';
                  FDMemTablePKBXML.FieldByName('pbfej_create_date').AsDateTime := lcMost;
                  FDMemTablePKBXML.FieldByName('pbfej_create_user').AsInteger := GUserID;
                  FDMemTablePKBXML.FieldByName('pbfej_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePKBXML.FieldByName('pbfej_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePKBXML.FieldByName('pbfej_selected_date').AsDateTime := GDate; //lcMost;
                  FDMemTablePKBXML.FieldByName('pbfej_ellen_sorszam').AsString := pubEllenSorszam;
                  FDMemTablePKBXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
                  FDMemTablePKBXML.FieldByName('mennyiseg').AsInteger := FDMemTableAttarolasBevet.FieldByName('mennyiseg').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_fajta_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('fajtaid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_allapot_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('allapotid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_textil_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('textilid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_meret_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('meretid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_magassag_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('magassagid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_szin_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('szinid').AsInteger;
                  FDMemTablePKBXML.FieldByName('pbtet_megjegyzes').AsString := '';
                  FDMemTablePKBXML.FieldByName('pbtet_create_date').AsDateTime := lcMost;
                  FDMemTablePKBXML.FieldByName('pbtet_create_user').AsInteger := GUserID;
                  FDMemTablePKBXML.FieldByName('pbtet_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePKBXML.FieldByName('pbtet_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePKBXML.FieldByName('pbtet_fajlnev').AsString := lcPKBFajlNev;
                  FDMemTablePKBXML.FieldByName('pbtet_chiphez_rendelve').AsInteger := 0;
                  if (FDMemTableAttarolasBevet.FieldByName('chipkod').AsString = '') then
                    begin
                      FDMemTablePKBXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePKBXML.FieldByName('pbtet_darabosdb').AsInteger + FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
                    end;
                  FDMemTablePKBXML.FieldByName('bevetelezve').AsInteger := FDMemTablePKBXML.FieldByName('bevetelezve').AsInteger + FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
                  FDMemTablePKBXML.Post;
                end
              else
                begin
                  // PKK
                  FDMemTablePKBXML.Edit;
                  FDMemTablePKBXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTablePKBXML.FieldByName('pbtet_darabszam').AsInteger + FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
                  FDMemTablePKBXML.FieldByName('bevetelezve').AsInteger := FDMemTablePKBXML.FieldByName('bevetelezve').AsInteger + FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
                  if (FDMemTableAttarolasBevet.FieldByName('chipkod').AsString = '') then
                    begin
                      FDMemTablePKBXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePKBXML.FieldByName('pbtet_darabosdb').AsInteger + FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
                    end
                  else
                    begin
                      FDMemTablePKBXML.FieldByName('mennyiseg').AsInteger := FDMemTablePKBXML.FieldByName('mennyiseg').AsInteger + 1;
                    end;
                  FDMemTablePKBXML.Post;
                end;

              // PKKM.xml
              if (FDMemTableAttarolasBevet.FieldByName('chipkod').AsString <> '') then
                begin
                  FDMemTablePKBMXML.Insert;
                  FDMemTablePKBMXML.FieldByName('sorszam').AsString := lcSorszam;
                  FDMemTablePKBMXML.FieldByName('partner_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('partnerid').AsInteger;
                  FDMemTablePKBMXML.FieldByName('ellenuzem_id').AsInteger := FDMemTableAttarolasBevet.FieldByName('uzemid').AsInteger;
                  FDMemTablePKBMXML.FieldByName('uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePKBMXML.FieldByName('osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePKBMXML.FieldByName('prszall_fej_id').AsInteger := -1;
                  FDMemTablePKBMXML.FieldByName('vonalkod').AsString := lcFgv.GetVonalkodByChipkod(FDMemTableAttarolasBevet.FieldByName('Chipkod').AsString);
                  FDMemTablePKBMXML.FieldByName('chipkod').AsString := FDMemTableAttarolasBevet.FieldByName('Chipkod').AsString;
                  FDMemTablePKBMXML.FieldByName('mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PKB;
                  FDMemTablePKBMXML.FieldByName('textil_kpt_id').AsVariant := FDMemTableAttarolasBevet.FieldByName('textilid').AsVariant;
                  FDMemTablePKBMXML.FieldByName('textil').AsString := FDMemTableAttarolasBevet.FieldByName('textilnev').AsString;
                  FDMemTablePKBMXML.FieldByName('fajta_id').AsVariant := FDMemTableAttarolasBevet.FieldByName('fajtaid').AsVariant;
                  FDMemTablePKBMXML.FieldByName('fajta').AsString := FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString;
                  FDMemTablePKBMXML.FieldByName('allapot_id').AsVariant := FDMemTableAttarolasBevet.FieldByName('allapotid').AsVariant;
                  FDMemTablePKBMXML.FieldByName('allapot').AsString := FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString;
                  FDMemTablePKBMXML.FieldByName('meret_id').AsVariant := FDMemTableAttarolasBevet.FieldByName('meretid').AsVariant;
                  FDMemTablePKBMXML.FieldByName('meret').AsString := FDMemTableAttarolasBevet.FieldByName('meretnev').AsString;
                  FDMemTablePKBMXML.FieldByName('magassag_id').AsVariant := FDMemTableAttarolasBevet.FieldByName('magassagid').AsVariant;
                  FDMemTablePKBMXML.FieldByName('magassag').AsString := FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString;
                  FDMemTablePKBMXML.FieldByName('szin_id').AsVariant := FDMemTableAttarolasBevet.FieldByName('szinid').AsVariant;
                  FDMemTablePKBMXML.FieldByName('szin').AsString := FDMemTableAttarolasBevet.FieldByName('szinnev').AsString;
                  FDMemTablePKBMXML.FieldByName('datum').AsDateTime := lcMost;
                  FDMemTablePKBMXML.FieldByName('felhasznalo_id').AsInteger := GUserId;
                  FDMemTablePKBMXML.FieldByName('fajlnev').AsString := lcPKBMFajlNev;
                  FDMemTablePKBMXML.FieldByName('kivezetesoka').AsInteger := FDMemTableAttarolasBevet.FieldByName('kivezetesokaid').AsInteger;
                  FDMemTablePKBMXML.FieldByName('kivezetesokanev').AsString := FDMemTableAttarolasBevet.FieldByName('kivezetesokanev').AsString;
                  FDMemTablePKBMXML.FieldByName('uploaded').AsInteger := -1;
                  if FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger = 0 then
                    begin
                      FDMemTablePKBMXML.FieldByName('bevetelezve').AsString := 'Hi�nyzik';
                    end
                  else
                    begin
                      FDMemTablePKBMXML.FieldByName('bevetelezve').AsString := 'OK';
                    end;
                  FDMemTablePKBMXML.Post;
                end;
              FDMemTableAttarolasBevet.Next;
            end;

          DM.CDSUzem.First;
          TfrxMemoView(frxReportPKB.FindObject('mKiszDatum')).Text := DateToStr(GDate); // DateToStr(lcMost);
          TfrxMemoView(frxReportPKB.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPKB.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPKB.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPKB.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPKB.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPKB.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PKB);

          TfrxMemoView(frxReportPKBM.FindObject('mKiszDatum')).Text := DateToStr(GDate); // DateToStr(lcMost);
          TfrxMemoView(frxReportPKBM.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPKBM.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPKBM.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPKBM.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPKBM.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPKBM.FindObject('frxDBDSSzallszfej_szallszam')).Text := lcSorszam;
          TfrxMemoView(frxReportPKBM.FindObject('frxDBDSSzallszfej_szallszam1')).Text := lcSorszam;
          TfrxMemoView(frxReportPKBM.FindObject('frxDBDSSzallszfej_createdate')).Text := DateToStr(lcMost);
          TfrxMemoView(frxReportPKBM.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PKB);

          DM.CDSFelhasz.First;
          while not DM.CDSFelhasz.Eof do
            begin
              if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
                begin
                  TfrxMemoView(frxReportPKB.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  TfrxMemoView(frxReportPKBM.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                end;
              DM.CDSFelhasz.Next;
            end;

          while not DM.CDSUzemek.Eof do
            begin
              if DM.CDSUzemek.FieldByName('uzem_id').AsInteger = FDMemTableAttarolasBevet.FieldByName('uzemid').AsInteger then
                begin
                  TfrxMemoView(frxReportPKB.FindObject('mPartnerNev')).Text := DM.CDSUzemek.FieldByName('uzem_nev').AsString;
                  TfrxMemoView(frxReportPKBM.FindObject('mPartnerNev')).Text := DM.CDSUzemek.FieldByName('uzem_nev').AsString;
                end;
              DM.CDSUzemek.Next;
            end;
          SetCurrentDir(lcAdatUtvonal);
          FDMemTablePKBXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPKBFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPKB.PrepareReport(true);
          frxReportPKB.Export(DM.frxPDFExport1);

          frxReportPKB.PrintOptions.Copies := 3;
          frxReportPKB.PrintOptions.Collate := True;
          frxReportPKB.PrepareReport(true);
          frxReportPKB.ShowPreparedReport;

          // A Megn�velt sorsz�m felk�ld�se a szervernek
          SetCurrentDir(lcRootDir);
          lcSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PKB', GPartner, GUserID, false, true);
          SetCurrentDir(lcAdatUtvonal);

          lcFgv.DataSetToXML(TDataSet(FDMemTablePKBXML), lcPKBFajlNev);

          FDMemTablePKBMXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPKBMFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPKBM.PrepareReport(true);
          frxReportPKBM.Export(DM.frxPDFExport1);

          frxReportPKBM.PrintOptions.Copies := 3;
          frxReportPKBM.PrintOptions.Collate := True;
          frxReportPKBM.PrepareReport(true);
          frxReportPKBM.ShowPreparedReport;

          lcFgv.DataSetToXML(TDataSet(FDMemTablePKBMXML), lcPKBMFajlNev);

          lcFgv.SetPufferRaktar('bevet', false, false, TClientDataSet(FDMemTablePKBXML));
          lcFgv.SetPufferRaktar('bevet', true, false, TClientDataSet(FDMemTablePKBMXML));

          lcHttpResponse := '';
          // Az�rt pufBevRakXMLUpload-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s, nincs r� k�l�n request.
          lcHttpResponse := lcHttpRequest.PostHttps(lcPKBFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
          if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
            begin
              lcTmpId := lcHttpResponse;
              ChangeFieldValueInCDS(lcDataset, lcPKBFajlNev, 'pbfej_id', lcTmpId, true);
              lcHttpResponse := '';
              // Az�rt pufBevRakXMLUpload_PBM-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s chip mozg�sok, nincs r� k�l�n request.
              ChangeFieldValueInCDS(lcDataset, lcPKBMFajlNev, 'prszall_fej_id', lcTmpId, false);
              lcHttpResponse := lcHttpRequest.PostHttps(lcPKBMFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
              if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                begin
                  ChangeFieldValueInCDS(lcDataset, lcPKBMFajlNev, 'uploaded', 1, true);
                end
              else
                showmessage('A felt�lt�s nem lehets�ges. (PKBMxxxx.xml szakasz) - ' + lcHttpResponse);
            end
          else
            showmessage('A felt�lt�s nem lehets�ges. (PKBxxxx.xml szakasz) - ' + lcHttpResponse);

        finally
          SetCurrentDir(lcRootDir);
          DM.CDSPKKSzamok.First;
          while not DM.CDSPKKSzamok.Eof do
            begin
              if DM.CDSPKKSzamok.FieldByName('prszfej_id').AsInteger = pubSzfejId then
                begin
                  DM.CDSPKKSzamok.Delete;
                  break;
                end;
            end;
          lcFgv.DataSetToXML(DM.CDSPKKSzamok, 'pkkszamok.xml');
        end;
        // Destroy-ok
        lcFgv.Destroy;
        lcHttpRequest.Destroy;
        lcDataset.Destroy;
        lcIniFile.Destroy;
        prCloseOnSave := true;
        Close;
      end;
  end;
end;

procedure TfrmPufferAttarolasBevet.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPufferAttarolasBevet.Button3Click(Sender: TObject);
begin
  dbgTextilListDblClick(nil);
end;

procedure TfrmPufferAttarolasBevet.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then
    begin
      FDMemTableAttarolasBevet.Filtered := false;
      FDMemTableAttarolasBevet.Filter := 'mennyiseg <> bevetelezve';
      FDMemTableAttarolasBevet.Filtered := true;
    end
  else
    begin
      FDMemTableAttarolasBevet.Filtered := false;
    end;
end;

procedure TfrmPufferAttarolasBevet.chipEditKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    btnChipHozzaadClick(nil);
end;

procedure TfrmPufferAttarolasBevet.dbgTextilListCellClick(Column: TColumn);
begin
  dbgTextilList.SelectedRows.CurrentRowSelected := True;
end;

procedure TfrmPufferAttarolasBevet.dbgTextilListDblClick(Sender: TObject);
begin
  if dbgTextilList.SelectedRows.Count > 0 then
    begin
      if FDMemTableAttarolasBevet.FieldByName('chipkod').AsString = '' then
        begin
          prEditId := FDMemTableAttarolasBevet.FieldByName('id').AsInteger;
          lKivalasztottTetel.Caption := FDMemTableAttarolasBevet.FieldByName('textilnev').AsString + ' ' +
          FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString + ' ' +
          FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString + ' ' +
          FDMemTableAttarolasBevet.FieldByName('meretnev').AsString + ' ' +
          FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString + ' ' +
          FDMemTableAttarolasBevet.FieldByName('szinnev').AsString;
          chipEdit.Text := FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsString;
          chipEdit.Enabled := true;
          chipEdit.SetFocus;
        end;
    end
end;

procedure TfrmPufferAttarolasBevet.dbgTextilListDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var hideRect: TRect;
begin

end;

procedure TfrmPufferAttarolasBevet.DSAttarolasBevetDataChange(Sender: TObject;
  Field: TField);
begin
  if FDMemTableAttarolasBevet.Tag = 0 then
    begin
      prEditId := -1;
      lKivalasztottTetel.Caption := '';
      chipEdit.Enabled := false;
    end;
end;

procedure TfrmPufferAttarolasBevet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if prCloseOnSave then
    begin
      SerialReader.Stop;
      Action := caFree;
    end
  else
    begin
      case MessageDlg('Biztos megszak�tja a m�veletet?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            SerialReader.Stop;
            Action := caFree;
          end;
        mrNo: Action := caNone;
      end;
    end;
  if Action = caFree then
    frmPufferAttarolasBevet := nil;
end;

procedure TfrmPufferAttarolasBevet.FormCreate(Sender: TObject);
var
  i: integer;
  IniFile: TIniFile;
  prPort, prPortConfig: string;
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;
  // Hide tab from TabControll
  for i := 0 to PageControl1.PageCount -1  do
    begin
      PageControl1.Pages[i].TabVisible := false;
    end;
  PageControl1.ActivePageIndex := 0;
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  prPort := IniFile.ReadString('port', 'szam', prPort);
  prPortConfig := IniFile.ReadString('port', 'ertek', prPortConfig);
  IniFile.Free;
  SerialReader := TSerialReader.Create('attarolasbevet', prPort, prPortConfig);
  prCloseOnSave := false;
  FDMemTableAttarolasBevet.Active := true;
  prDataLoaded := false;
end;

procedure TfrmPufferAttarolasBevet.FormShow(Sender: TObject);
var
  i: integer;
begin
  if prDataLoaded then
    Exit;
  DM.CDSPKKSzallitok.Filtered := false;
  DM.CDSPKKSzallitok.Filter := 'pbfej_id='+IntToStr(pubSzfejId);
  DM.CDSPKKSzallitok.Filtered := true;
  DM.CDSPKKChipmozgas.Filtered := false;
  DM.CDSPKKChipmozgas.Filter := 'prszall_fej_id='+IntToStr(pubSzfejId);
  DM.CDSPKKChipmozgas.Filtered := true;

  DM.CDSPKKSzallitok.First;
  while not DM.CDSPKKSzallitok.Eof do
    begin
      FDMemTableAttarolasBevet.Insert;
      FDMemTableAttarolasBevet.FieldByName('id').AsInteger := i;
      i := i + 1;
      FDMemTableAttarolasBevet.FieldByName('textilid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_textil_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('textilnev').AsString := GetTextilNev(DM.CDSPKKSzallitok.FieldByName('pbtet_textil_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('fajtaid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_fajta_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString := GetTextilFajta(DM.CDSPKKSzallitok.FieldByName('pbtet_fajta_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('allapotid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_allapot_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString := GetTextilAllapot(DM.CDSPKKSzallitok.FieldByName('pbtet_allapot_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('meretid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_meret_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('meretnev').AsString := GetTextilMeret(DM.CDSPKKSzallitok.FieldByName('pbtet_meret_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('magassagid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_magassag_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString := GetTextilMagassag(DM.CDSPKKSzallitok.FieldByName('pbtet_magassag_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('szinid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_szin_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('szinnev').AsString := GetTextilSzin(DM.CDSPKKSzallitok.FieldByName('pbtet_szin_id').AsInteger, DM.CDSPKKSzallitok.FieldByName('pbtet_textil_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('mennyiseg').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbtet_darabosdb').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('partnerid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbfej_partner_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('uzemid').AsInteger := DM.CDSPKKSzallitok.FieldByName('pbfej_uzem_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger := 0;
      FDMemTableAttarolasBevet.Post;
      DM.CDSPKKSzallitok.Next;
    end;

  DM.CDSPKKChipmozgas.First;
  while not DM.CDSPKKChipmozgas.Eof do
    begin
      FDMemTableAttarolasBevet.Insert;
      FDMemTableAttarolasBevet.FieldByName('id').AsInteger := i;
      i := i + 1;
      FDMemTableAttarolasBevet.FieldByName('chipkod').AsString := DM.CDSPKKChipmozgas.FieldByName('chipkod').AsString;
      FDMemTableAttarolasBevet.FieldByName('textilid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('textil_kpt_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('textilnev').AsString := GetTextilNev(DM.CDSPKKChipmozgas.FieldByName('textil_kpt_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('fajtaid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('fajta_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString := GetTextilFajta(DM.CDSPKKChipmozgas.FieldByName('fajta_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('allapotid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('allapot_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString := GetTextilAllapot(DM.CDSPKKChipmozgas.FieldByName('allapot_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('meretid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('meret_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('meretnev').AsString := GetTextilMeret(DM.CDSPKKChipmozgas.FieldByName('meret_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('magassagid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('magassag_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString := GetTextilMagassag(DM.CDSPKKChipmozgas.FieldByName('magassag_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('szinid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('szin_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('szinnev').AsString := GetTextilSzin(DM.CDSPKKChipmozgas.FieldByName('szin_id').AsInteger, DM.CDSPKKChipmozgas.FieldByName('textil_kpt_id').AsInteger);
      FDMemTableAttarolasBevet.FieldByName('mennyiseg').AsInteger := 1;
      FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger := 0;
      FDMemTableAttarolasBevet.FieldByName('partnerid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('partner_id').AsInteger;
      FDMemTableAttarolasBevet.FieldByName('uzemid').AsInteger := DM.CDSPKKChipmozgas.FieldByName('uzem_id').AsInteger;
      FDMemTableAttarolasBevet.Post;
      DM.CDSPKKChipmozgas.Next;
    end;
  FDMemTableAttarolasBevet.Filtered := false;
  FDMemTableAttarolasBevet.Filter := 'mennyiseg <> bevetelezve';
  FDMemTableAttarolasBevet.Filtered := true;
  prDataLoaded := true;
end;


procedure TfrmPufferAttarolasBevet.RedrawOsszPanel;

  function lcIndexOf(arr: array of TBevetTetel; haystack: TBevetTetel): integer;
  var i: integer;
  begin
    result := -1;
    for i := 0 to Length(arr)-1 do
    begin
      if (arr[i].Fajta = haystack.Fajta) and
         (arr[i].Allapot = haystack.Allapot) and
         (arr[i].Szin = haystack.Szin) and
         (arr[i].Meret = haystack.Meret) and
         (arr[i].Megnev = haystack.Megnev) and
         (arr[i].Magassag = haystack.Magassag) then
        begin
          result := i;
          break;
        end;
    end;
  end;

var i, j: integer;
    lcTetelek: array of TBevetTetel;
    lcCurrentTetel: TBevetTetel;
    lcFindIndex: integer;
    lcScrollPosition: integer;
    lcFiltered: boolean;
begin
  if FDMemTableAttarolasBevet.Tag = 1 then
    Exit;
  FDMemTableAttarolasBevet.Tag := 1;
  lcFiltered := FDMemTableAttarolasBevet.Filtered;
  FDMemTableAttarolasBevet.Filtered := false;
  SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(false), 0);
  try
    lcScrollPosition :=  ScrollBox1.VertScrollBar.ScrollPos;
    for i := 0 to length(prPanels)-1 do
      begin
        if assigned(prPanels[i]) then
          begin
            prPanels[i].Destroy;
          end;
      end;
    setLength(prPanels, 0);
    FDMemTableAttarolasBevet.First;
    for i := 0 to FDMemTableAttarolasBevet.RecordCount-1 do
      begin
        lcCurrentTetel.Fajta := FDMemTableAttarolasBevet.FieldByName('fajtanev').AsString;
        lcCurrentTetel.Allapot := FDMemTableAttarolasBevet.FieldByName('allapotnev').AsString;
        lcCurrentTetel.Szin := FDMemTableAttarolasBevet.FieldByName('szinnev').AsString;
        lcCurrentTetel.Meret := FDMemTableAttarolasBevet.FieldByName('meretnev').AsString;
        lcCurrentTetel.Magassag := FDMemTableAttarolasBevet.FieldByName('magassagnev').AsString;
        lcCurrentTetel.Megnev := FDMemTableAttarolasBevet.FieldByName('textilnev').AsString;
        lcCurrentTetel.Mennyiseg := FDMemTableAttarolasBevet.FieldByName('bevetelezve').AsInteger;
        lcFindIndex := lcIndexOf(lcTetelek, lcCurrentTetel);
        if (lcFindIndex > -1) then
          begin
            lcTetelek[lcFindIndex].Mennyiseg := lcTetelek[lcFindIndex].Mennyiseg + lcCurrentTetel.Mennyiseg;
          end
        else
          begin
            SetLength(lcTetelek, length(lcTetelek)+1);
            lcTetelek[length(lcTetelek)-1] := lcCurrentTetel;
          end;
          FDMemTableAttarolasBevet.Next;
      end;
    setLength(prPanels, length(lcTetelek));
    j := 0;
    for i := 0 to length(lcTetelek)-1 do
      begin
      if (lcTetelek[i].Mennyiseg <> 0) then
        begin
          prPanels[i] := CopyComponents(samplePanel, samplePanel.Owner, samplePanel.Parent, prNextPanel);
          SetPanelLabels(prPanels[i], lcTetelek[i].Fajta, lcTetelek[i].Allapot, lcTetelek[i].Szin, lcTetelek[i].Meret, lcTetelek[i].Magassag, lcTetelek[i].Megnev, lcTetelek[i].Mennyiseg);
          TPanel(prPanels[i]).Top := (samplePanel.Height*j)+4;
          TPanel(prPanels[i]).visible := true;
          prNextPanel := prNextPanel + 1;
          j := j + 1;
        end;
      end;
  finally
    begin
      ScrollBox1.VertScrollBar.Position := lcScrollPosition;
      SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(true), 0);
      ScrollBox1.Repaint;
    end;
  end;
  FDMemTableAttarolasBevet.Filtered := lcFiltered;
  FDMemTableAttarolasBevet.Tag := 0;
end;

end.
