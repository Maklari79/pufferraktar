unit uBeallitasok;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, INIFiles, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Datasnap.DBClient;

type
  TfrmBeallitasok = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    lBeallit: TImageList;
    gbSzinkron: TGroupBox;
    gbSzerver: TGroupBox;
    lIp: TLabel;
    eIpCim: TEdit;
    eUtvonal: TEdit;
    lPort: TLabel;
    btnMentes: TButton;
    btnBezar: TButton;
    gbPartner: TGroupBox;
    DBGridPartnerek: TDBGrid;
    btnJelol: TButton;
    btnVissza: TButton;
    CDSPartFrom: TClientDataSet;
    btnPartMent: TButton;
    DSPartFrom: TDataSource;
    CDSPartTo: TClientDataSet;
    DSPartTo: TDataSource;
    DBGridPartTo: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBezarClick(Sender: TObject);
    procedure btnMentesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnJelolClick(Sender: TObject);
    procedure btnVisszaClick(Sender: TObject);
    procedure btnPartMentClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBeallitasok: TfrmBeallitasok;

implementation

uses uFgv;
{$R *.dfm}



procedure TfrmBeallitasok.btnBezarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmBeallitasok.btnJelolClick(Sender: TObject);
var
  lcTalalt : Boolean;
begin
  lcTalalt := false;
  CDSPartTo.First;
  while not CDSPartTo.Eof do
    begin
      if DBGridPartnerek.DataSource.DataSet.FieldByName('par_id').AsInteger = CDSPartTo.FieldByName('par_id').AsInteger then
        begin
          lcTalalt := true;
        end;
      CDSPartTo.Next;
    end;
  if not lcTalalt then
    begin
      CDSPartTo.Append;
      if CDSPartTo.State in [dsEdit,dsInsert] then
        begin
          CDSPartTo.FieldByName('par_id').AsInteger := DBGridPartnerek.DataSource.DataSet.FieldByName('par_id').AsInteger;
          CDSPartTo.FieldByName('par_nev').AsString := DBGridPartnerek.DataSource.DataSet.FieldByName('par_nev').AsString;
          CDSPartTo.FieldByName('par_rovnev').AsString := DBGridPartnerek.DataSource.DataSet.FieldByName('par_rovnev').AsString;
          CDSPartTo.FieldByName('par_kod').AsString := DBGridPartnerek.DataSource.DataSet.FieldByName('par_kod').AsString;
          CDSPartTo.FieldByName('par_lastuser').AsInteger := DBGridPartnerek.DataSource.DataSet.FieldByName('par_lastuser').AsInteger;
          CDSPartTo.FieldByName('par_lastup').AsDateTime := DBGridPartnerek.DataSource.DataSet.FieldByName('par_lastup').AsDateTime;
          CDSPartTo.FieldByName('par_szamlaalap').AsInteger := DBGridPartnerek.DataSource.DataSet.FieldByName('par_szamlaalap').AsInteger;
          CDSPartTo.Post;
        end
      else
        begin
          ShowMessage('A cliensdataset nem szerkeszthet�!');
        end;
    end;
end;

procedure TfrmBeallitasok.btnMentesClick(Sender: TObject);
var
  lciniFile : TIniFile;
begin
  //Ini f�jlba ment�s.
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  lciniFile.WriteString('serverip','ip',eIpCim.Text);
  lciniFile.WriteString('utvonal','path',eUtvonal.Text);
  lciniFile.Free;
end;

procedure TfrmBeallitasok.btnPartMentClick(Sender: TObject);
var
  lcXML : TfgvUnit;
begin
  //kiv�lasztott partnerek ment�se xml-be.
  lcXML.DataSetToXML(CDSPartTo, GetCurrentDir +'\partnerek.xml');
end;

procedure TfrmBeallitasok.btnVisszaClick(Sender: TObject);
begin
  DBGridPartTo.DataSource.DataSet.Delete;
end;

procedure TfrmBeallitasok.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBeallitasok.FormCreate(Sender: TObject);
var
  lciniFile : TIniFile;
begin
  // Ini f�jl bet�lt�se.
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  eIpCim.Text := lciniFile.ReadString('serverip','ip','');
  eUtvonal.Text := lciniFile.ReadString('utvonal','path','');
  lciniFile.Free;
  //Partnerek2.xml bet�lt�se.
  CDSPartFrom.LoadFromFile('partnerek2.xml');
  CDSPartFrom.LogChanges := false;
  CDSPartFrom.Filter := 'par_puffer_raktar=0';
  CDSPartFrom.FieldByName('par_id').DisplayLabel := 'Azonos�t�';
  CDSPartFrom.FieldByName('par_nev').DisplayLabel := 'Partnern�v';
//  CDSPartTo.FieldDefs.Clear;
//  CDSPartTo.Active := false;
{  CDSPartTo.FieldDefs.Add('par_id',ftInteger);
  CDSPartTo.FieldDefs.Add('par_nev',ftString,80);
  CDSPartTo.FieldDefs.Add('par_rovnev',ftString,5);
  CDSPartTo.FieldDefs.Add('par_kod',ftString,7);
  CDSPartTo.FieldDefs.Add('par_lastuser',ftInteger);
  CDSPartTo.FieldDefs.Add('par_lastup',ftDateTime);
  CDSPartTo.FieldDefs.Add('par_szamlaalap',ftInteger);
  CDSPartTo.FieldDefs.Add('par_delete',ftInteger);
  CDSPartTo.CreateDataSet;}
  CDSPartTo.LoadFromFile('partnerek.xml');
  CDSPartTo.LogChanges := false;
  CDSPartTo.FieldByName('par_id').DisplayLabel := 'Azonos�t�';
  CDSPartTo.FieldByName('par_nev').DisplayLabel := 'Partnern�v';
  CDSPartTo.FieldByName('par_rovnev').Visible := false;
  CDSPartTo.FieldByName('par_kod').Visible := false;
  CDSPartTo.FieldByName('par_lastuser').Visible := false;
  CDSPartTo.FieldByName('par_lastup').Visible := false;
  CDSPartTo.FieldByName('par_szamlaalap').Visible := false;
end;

end.
