unit uLogs;

interface

uses System.Classes, System.SysUtils, Vcl.Forms;

type
  TlogsUnit = class
  private
  public
  procedure LogFajlOpen;
  procedure LogFajlWrite(ALogs : String);
  procedure LogFajlClose;
  end;

var
  LogFile : TextFile;

implementation

uses uFgv;

procedure TlogsUnit.LogFajlOpen;
var
  lcLogFile : TStringList;
  lcUt, lcLogfajl : String;
  lcFajlNev : TfgvUnit;
  lcSearchResult : TSearchRec;
begin
  lcFajlNev := TfgvUnit.Create();
  lcLogFile := TStringList.Create;
  lcUt := ExtractFilePath(Application.ExeName);
  lcLogfajl := lcFajlNev.Mappa(now());
  if FindFirst(lcUt+'\logs\logs' + lcLogfajl + '.txt', faAnyFile, lcSearchResult) = 0 then
    begin
      AssignFile(LogFile, lcUt+'\logs\logs' + lcLogfajl + '.txt');
    end
  else  if FindFirst(lcUt+'\logs\logs' + lcLogfajl + '.txt', faAnyFile, lcSearchResult) <> 0 then
    begin
      lcLogFile.SaveToFile(lcUt+'\logs\logs' + lcLogfajl + '.txt');
      AssignFile(LogFile, lcUt+'\logs\logs' + lcLogfajl + '.txt');
    end;
  lcFajlNev.Free;
  lcLogFile.Free;
end;

procedure TlogsUnit.LogFajlWrite(ALogs : String);
begin
  LogFajlOpen;
  Append(LogFile);
  WriteLn(LogFile, FormatDateTime('YYYY.MM.DD HH:NN:SS',now()) + ALogs);
  LogFajlClose;
end;

procedure TlogsUnit.LogFajlClose;
begin
  CloseFile(LogFile);
end;

end.
