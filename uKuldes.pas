unit uKuldes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, Vcl.ComCtrls, System.IOUtils;

type
  TfrmKuldes = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    btnKuld: TButton;
    btnBezar: TButton;
    lKuld: TImageList;
    mcAdat: TMonthCalendar;
    mLog: TMemo;
    OpenDialog1: TOpenDialog;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBezarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mcAdatClick(Sender: TObject);
    procedure btnKuldClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmKuldes: TfrmKuldes;

implementation

uses uFgv, uDM;
{$R *.dfm}

procedure TfrmKuldes.btnBezarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmKuldes.btnKuldClick(Sender: TObject);
begin
  //A k�ld�s.
end;

procedure TfrmKuldes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmKuldes.FormCreate(Sender: TObject);
begin
  btnKuld.Enabled := false;
end;

procedure TfrmKuldes.mcAdatClick(Sender: TObject);
var
  lcMappaCreate : TfgvUnit;
  MySearch: TSearchRec;
  FindResult: Integer;
begin
  //Nap kiv�laszt�sa. Adat �sszegy�jt�se.
  GDate := mcAdat.Date;
  mlog.Lines.Clear;
  mlog.Lines.Add('Adatok �sszegy�jt�se!');
  lcMappaCreate := TfgvUnit.Create();
  FindResult:=FindFirst(GetCurrentDir+ '\adat\' + lcMappaCreate.Mappa(GDate) +'\*.*', faAnyFile, MySearch);
  if (MySearch.Name<>'.')and(MySearch.Name<>'..') then
    mlog.Lines.Add(MySearch.Name);
  while FindNext(MySearch)=0 do
  begin
    if (MySearch.Attr<>faDirectory) and (MySearch.Name<>'.') and (MySearch.Name<>'..') then
      mlog.Lines.Add(MySearch.Name);
  end;
  mlog.Lines.Add('Adatok �sszegy�jt�se K�SZ!');
  btnKuld.Enabled := true;
end;

end.
