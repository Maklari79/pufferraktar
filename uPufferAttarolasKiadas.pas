unit uPufferAttarolasKiadas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, System.ImageList, Vcl.ImgList,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.ExtCtrls, INIFiles,
  Datasnap.DBClient, frxClass, frxDBSet, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfrmPufferAttarolasKiadas = class(TForm)
    gbOsszLista: TGroupBox;
    ScrollBox1: TScrollBox;
    samplePanel: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    gbChipList: TGroupBox;
    mChipList: TMemo;
    gbInfo: TGroupBox;
    lInfoString: TLabel;
    gbTextilList: TGroupBox;
    dbGridChip: TDBGrid;
    pAlso: TPanel;
    btnChipStart: TButton;
    btnChipEnd: TButton;
    Button8: TButton;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    btnChipHozzaad: TButton;
    chipEdit: TEdit;
    Button1: TButton;
    Button2: TButton;
    iPufferAttarolasKiadas: TImageList;
    Button3: TButton;
    Panel1: TPanel;
    Panel3: TPanel;
    Button6: TButton;
    Button7: TButton;
    GroupBox2: TGroupBox;
    dbGridDarabos: TDBGrid;
    FDMemTableAttarolasKiadas: TFDMemTable;
    FDMemTableAttarolasKiadaschipkod: TStringField;
    FDMemTableAttarolasKiadasvonalkod: TStringField;
    FDMemTableAttarolasKiadastextilid: TIntegerField;
    FDMemTableAttarolasKiadastextilnev: TStringField;
    FDMemTableAttarolasKiadasfajtaid: TIntegerField;
    FDMemTableAttarolasKiadasfajtanev: TStringField;
    FDMemTableAttarolasKiadasallapotid: TIntegerField;
    FDMemTableAttarolasKiadasallapotnev: TStringField;
    FDMemTableAttarolasKiadasmeretid: TIntegerField;
    FDMemTableAttarolasKiadasmeretnev: TStringField;
    FDMemTableAttarolasKiadasmagassagid: TIntegerField;
    FDMemTableAttarolasKiadasmagassagnev: TStringField;
    FDMemTableAttarolasKiadasszinid: TIntegerField;
    FDMemTableAttarolasKiadasszinnev: TStringField;
    FDMemTableAttarolasKiadasdatum: TDateTimeField;
    FDMemTableAttarolasKiadasmegjegyzes: TStringField;
    FDMemTableAttarolasKiadaskivetelezesokid: TIntegerField;
    FDMemTableAttarolasKiadaskivetelezesokanev: TStringField;
    DSAttarolasKiadas: TDataSource;
    Label1: TLabel;
    darabEdit: TEdit;
    CDSDarabosValaszto: TClientDataSet;
    CDSDarabosValasztouzemid: TIntegerField;
    CDSDarabosValasztopartnerid: TIntegerField;
    CDSDarabosValasztotextilid: TIntegerField;
    CDSDarabosValasztotextilnev: TStringField;
    CDSDarabosValasztofajtaid: TIntegerField;
    CDSDarabosValasztofajtanev: TStringField;
    CDSDarabosValasztoallapotid: TIntegerField;
    CDSDarabosValasztoallapotnev: TStringField;
    CDSDarabosValasztomeretid: TIntegerField;
    CDSDarabosValasztomeretnev: TStringField;
    CDSDarabosValasztomagassagid: TIntegerField;
    CDSDarabosValasztomagassagnev: TStringField;
    CDSDarabosValasztoszinid: TIntegerField;
    CDSDarabosValasztoszinnev: TStringField;
    CDSDarabosValasztodarabosossz: TIntegerField;
    CDSDarabosValasztoid: TIntegerField;
    CDSDarabosValasztotextilsuly: TFloatField;
    DSDarabosValaszto: TDataSource;
    FDMemTableAttarolasKiadasid: TIntegerField;
    FDMemTableAttarolasKiadasmennyiseg: TIntegerField;
    FDMemTablePKKXML: TFDMemTable;
    FDMemTablePKKXMLpbfej_id: TIntegerField;
    FDMemTablePKKXMLpbfej_uzem_id: TIntegerField;
    FDMemTablePKKXMLpbfej_partner_id: TIntegerField;
    FDMemTablePKKXMLpbfej_osztaly_id: TIntegerField;
    FDMemTablePKKXMLpbfej_sorszam: TStringField;
    FDMemTablePKKXMLpbfej_mozgas_id: TIntegerField;
    FDMemTablePKKXMLpbfej_felhasznalo_id: TIntegerField;
    FDMemTablePKKXMLpbfej_megjegyzes: TStringField;
    FDMemTablePKKXMLpbfej_create_date: TDateTimeField;
    FDMemTablePKKXMLpbfej_create_user: TIntegerField;
    FDMemTablePKKXMLpbfej_lastupdate_date: TDateTimeField;
    FDMemTablePKKXMLpbfej_lastupdate_user: TIntegerField;
    FDMemTablePKKXMLpbfej_selected_date: TDateTimeField;
    FDMemTablePKKXMLpbtet_darabszam: TIntegerField;
    FDMemTablePKKXMLpbtet_fajta_id: TIntegerField;
    FDMemTablePKKXMLpbtet_allapot_id: TIntegerField;
    FDMemTablePKKXMLpbtet_meret_id: TIntegerField;
    FDMemTablePKKXMLpbtet_magassag_id: TIntegerField;
    FDMemTablePKKXMLpbtet_szin_id: TIntegerField;
    FDMemTablePKKXMLpbtet_textil_id: TIntegerField;
    FDMemTablePKKXMLpbtet_megjegyzes: TStringField;
    FDMemTablePKKXMLpbtet_create_date: TDateTimeField;
    FDMemTablePKKXMLpbtet_create_user: TIntegerField;
    FDMemTablePKKXMLpbtet_lastupdate_date: TDateTimeField;
    FDMemTablePKKXMLpbtet_lastupdate_user: TIntegerField;
    FDMemTablePKKXMLpbtet_darabosdb: TIntegerField;
    FDMemTablePKKXMLpbtet_chiphez_rendelve: TIntegerField;
    FDMemTablePKKXMLpbtet_fajlnev: TStringField;
    FDMemTablePKKXMLallapotText: TStringField;
    FDMemTablePKKXMLfajtaText: TStringField;
    FDMemTablePKKXMLszinText: TStringField;
    FDMemTablePKKXMLmeretText: TStringField;
    FDMemTablePKKXMLmagassagText: TStringField;
    FDMemTablePKKXMLmegnevezesText: TStringField;
    FDMemTablePKKMXML: TFDMemTable;
    FDMemTablePKKMXMLsorszam: TStringField;
    FDMemTablePKKMXMLpartner_id: TIntegerField;
    FDMemTablePKKMXMLuzem_id: TIntegerField;
    FDMemTablePKKMXMLosztaly_id: TIntegerField;
    FDMemTablePKKMXMLprszall_fej_id: TIntegerField;
    FDMemTablePKKMXMLvonalkod: TStringField;
    FDMemTablePKKMXMLchipkod: TStringField;
    FDMemTablePKKMXMLmozgas_id: TIntegerField;
    FDMemTablePKKMXMLtextil_kpt_id: TIntegerField;
    FDMemTablePKKMXMLtextil: TStringField;
    FDMemTablePKKMXMLfajta_id: TIntegerField;
    FDMemTablePKKMXMLfajta: TStringField;
    FDMemTablePKKMXMLallapot_id: TIntegerField;
    FDMemTablePKKMXMLallapot: TStringField;
    FDMemTablePKKMXMLmeret_id: TIntegerField;
    FDMemTablePKKMXMLmeret: TStringField;
    FDMemTablePKKMXMLmagassag_id: TIntegerField;
    FDMemTablePKKMXMLmagassag: TStringField;
    FDMemTablePKKMXMLszin_id: TIntegerField;
    FDMemTablePKKMXMLszin: TStringField;
    FDMemTablePKKMXMLdatum: TDateTimeField;
    FDMemTablePKKMXMLfelhasznalo_id: TIntegerField;
    FDMemTablePKKMXMLfajlnev: TStringField;
    FDMemTablePKKMXMLDarabosbol: TBooleanField;
    FDMemTablePKKMXMLkivezetesoka: TIntegerField;
    FDMemTablePKKMXMLkivezetesokanev: TStringField;
    frxReportPKK: TfrxReport;
    frxDBDatasetPKK: TfrxDBDataset;
    frxReportPKKM: TfrxReport;
    frxDBDatasetPKKM: TfrxDBDataset;
    FDMemTablePKKXMLpbfej_lezart: TIntegerField;
    FDMemTablePKKMXMLuploaded: TIntegerField;
    CDSDarabosValasztopartnernev: TStringField;
    FDMemTablePKKXMLprszfej_ellenuzem_id: TIntegerField;
    FDMemTablePKKMXMLellenuzem_id: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure btnChipHozzaadClick(Sender: TObject);
    procedure dbGridDarabosCellClick(Column: TColumn);
    procedure dbGridDarabosDblClick(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure RedrawOsszPanel;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chipEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    prCloseOnSave: boolean;
    prPanels: array of TComponent;
    prNextPanel: integer;
  public
    { Public declarations }
    pubCelUzem: integer;
    procedure pubHandleRFID(rfid: string);
  end;

var
  frmPufferAttarolasKiadas: TfrmPufferAttarolasKiadas;

implementation

{$R *.dfm}

uses
  uSerialReader, uFgv, uHttps, uDM;

var
  SerialReader: TSerialReader;
  prKivalasztottTetel: TPufferRaktarTextil;

{ Component Copy }
function CopyComponent(Component,AParent: TComponent; NewComponentName: String): TComponent;
var
 Stream: TMemoryStream;
 S: String;
begin
 Result := TComponentClass(Component.ClassType).Create(Component.Owner);
 S := Component.Name;
 Component.Name := NewComponentName;
 Stream := TMemoryStream.Create;
 try
   Stream.WriteComponent(Component);
   Component.Name := S;
   Stream.Seek(0, soFromBeginning);
   TWinControl(AParent).InsertControl(TControl(Result));
   Stream.ReadComponent(Result);
 finally
   Stream.Free;
 end;
end;
// AComponent: the original component
// AOwner, AParent: the owner and parent of the new copy
// returns the new component

function CopyComponents(AComponent, AOwner, AParent: TComponent;Index:Integer): TComponent;
var
  i : integer;
  NewComponent: TComponent;
begin
  NewComponent := CopyComponent(AComponent,AParent,AComponent.Name+IntToStr(Index))as TComponentClass(AComponent.ClassType);{}
 // now, search for subcomponents
 for i:=0 to AComponent.Owner.ComponentCount-1 do
   if TWinControl(AComponent.Owner.Components[i]).Parent=AComponent then
     // and copy them too
     CopyComponents(AComponent.Owner.Components[i], AOwner, NewComponent,Index);
  Result:=NewComponent;
end;

procedure SetPanelLabels(AComponent: TComponent; AFajta, AAllapot, ASzin, AMeret, AMagassag, AMegnev: string; AMennyiseg: integer);
var i: integer;
begin
  for i := 0 to AComponent.Owner.ComponentCount-1 do
    begin
      if (AComponent.Owner.Components[i].ClassType = TLabel) and (TWinControl(AComponent.Owner.Components[i]).Parent = AComponent) then
        begin
          if TLabel(AComponent.Owner.Components[i]).Caption = 'lFajta' then
            TLabel(AComponent.Owner.Components[i]).Caption := AFajta
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'l�llapot' then
            TLabel(AComponent.Owner.Components[i]).Caption := AAllapot
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lSz�n' then
            TLabel(AComponent.Owner.Components[i]).Caption := ASzin
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lM�' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMeret
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMa' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMagassag
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lDarab' then
            TLabel(AComponent.Owner.Components[i]).Caption := IntToStr(AMennyiseg)
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMegnev' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMegnev
        end;
    end;
end;

{ End of Component Copy }

procedure TfrmPufferAttarolasKiadas.pubHandleRFID(rfid: string);
var lcFgv: TFgvUnit;
    lcMsgString: string;
    lcFind: boolean;
    lcKivalasztottTetel: TPufferRaktarTextil;
    lcDataset: TFDMemTable;
begin
  // Chip olvas�s
  lcFgv := TFgvUnit.Create;
  lcMsgString := '';
  FDMemTableAttarolasKiadas.First;
  while not FDMemTableAttarolasKiadas.Eof do
    begin
      if FDMemTableAttarolasKiadas.FieldByName('chipkod').AsString = rfid then
        begin
          mChipList.Lines.Add('A '+rfid+' chipk�d m�r szerepel ezen a kiad�son.');
          exit;
        end;
      FDMemTableAttarolasKiadas.Next;
    end;

    if FileExists('./tmp_osszerendel.xml') then
      begin
        lcDataset := TFDMemTable.Create(application);
        lcDataset.LoadFromFile('./tmp_osszerendel.xml');
        lcDataset.Active := true;
        lcDataset.LogChanges := false;
        lcDataset.First;
        while not lcDataset.Eof do
          begin
            if (lcDataset.FieldByName('Chipkod').AsString = rfid) then
              begin
                mChipList.Lines.Add('A '+rfid+' chipk�d r�gz�tve van egy be nem fejezett �sszerendel�sen.');
                exit;
              end;
              lcDataset.Next;
          end;
      end;

  if (lcFgv.IsInPufferRaktar(rfid, false, nil, '', lcMsgString)) then
    begin
      lcKivalasztottTetel := lcFgv.GetTextilFromPufferRaktarByChipkod(rfid);
      mChipList.Lines.Add('A '+rfid+' hozz�adva a kiad�shoz.');
      FDMemTableAttarolasKiadas.Insert;
      FDMemTableAttarolasKiadas.FieldByName('id').AsInteger := -1;
      FDMemTableAttarolasKiadas.FieldByName('chipkod').AsString := rfid;
      FDMemTableAttarolasKiadas.FieldByName('textilid').AsInteger := lcKivalasztottTetel.textilid;
      FDMemTableAttarolasKiadas.FieldByName('textilnev').AsString := lcKivalasztottTetel.textilnev;
      FDMemTableAttarolasKiadas.FieldByName('fajtaid').AsInteger := lcKivalasztottTetel.fajtaid;
      FDMemTableAttarolasKiadas.FieldByName('fajtanev').AsString := lcKivalasztottTetel.fajtanev;
      FDMemTableAttarolasKiadas.FieldByName('allapotid').AsInteger := lcKivalasztottTetel.allapotid;
      FDMemTableAttarolasKiadas.FieldByName('allapotnev').AsString := lcKivalasztottTetel.allapotnev;
      FDMemTableAttarolasKiadas.FieldByName('meretid').AsInteger := lcKivalasztottTetel.meretid;
      FDMemTableAttarolasKiadas.FieldByName('meretnev').AsString := lcKivalasztottTetel.meretnev;
      FDMemTableAttarolasKiadas.FieldByName('magassagid').AsInteger := lcKivalasztottTetel.magassagid;
      FDMemTableAttarolasKiadas.FieldByName('magassagnev').AsString := lcKivalasztottTetel.magassagnev;
      FDMemTableAttarolasKiadas.FieldByName('szinid').AsInteger := lcKivalasztottTetel.szinid;
      FDMemTableAttarolasKiadas.FieldByName('szinnev').AsString := lcKivalasztottTetel.szinnev;
      FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger := 1;
      FDMemTableAttarolasKiadas.FieldByName('datum').AsDateTime := Now();
      FDMemTableAttarolasKiadas.Post;
      RedrawOsszPanel;
    end
  else
    begin
      mChipList.Lines.Add('A '+rfid+' chipk�d nincs a puffer rakt�rban.');
    end;
end;

procedure TfrmPufferAttarolasKiadas.btnChipEndClick(Sender: TObject);
begin
  btnChipStart.Enabled := true;
  btnChipEnd.Enabled := false;
  SerialReader.Stop;
end;

procedure TfrmPufferAttarolasKiadas.btnChipHozzaadClick(Sender: TObject);
begin
  if (btnChipHozzaad.Tag = 0) then
    begin
      btnChipHozzaad.Tag := 1;
      btnChipHozzaad.caption := 'Hozz�ad�s';
      chipEdit.Enabled := true;
    end
  else
    begin
      if (chipEdit.Text =  '') then
        begin
          ShowMessage('T�ltse ki a chipk�d mez�t!');
          exit;
        end
      else
        pubHandleRFID(chipEdit.Text);
    end;
end;

procedure TfrmPufferAttarolasKiadas.btnChipStartClick(Sender: TObject);
begin
  btnChipStart.Enabled := False;
  btnChipEnd.Enabled := true;
  SerialReader.Execute;
end;

procedure TfrmPufferAttarolasKiadas.Button1Click(Sender: TObject);

  function FindInOsszesitesPK: boolean;
  begin
    result := false;
    FDMemTablePKKXML.First;
    while not FDMemTablePKKXML.Eof do
      begin
        if
          (FDMemTablePKKXML.FieldByName('megnevezesText').AsString = FDMemTableAttarolasKiadas.FieldByName('textilnev').AsString) and
          (FDMemTablePKKXML.FieldByName('fajtaText').AsString = FDMemTableAttarolasKiadas.FieldByName('fajtanev').AsString) and
          (FDMemTablePKKXML.FieldByName('allapotText').AsString = FDMemTableAttarolasKiadas.FieldByName('allapotnev').AsString) and
          (FDMemTablePKKXML.FieldByName('meretText').AsString = FDMemTableAttarolasKiadas.FieldByName('meretnev').AsString) and
          (FDMemTablePKKXML.FieldByName('magassagText').AsString = FDMemTableAttarolasKiadas.FieldByName('magassagnev').AsString) and
          (FDMemTablePKKXML.FieldByName('szinText').AsString = FDMemTableAttarolasKiadas.FieldByName('szinnev').AsString)
        then
          begin
            result := true;
            exit;
          end;
        FDMemTablePKKXML.Next;
      end;
  end;

  function prGetMozgasnemNev(A_mozgId: Integer): String;
  Begin
    Result:= '';
    DM.CDSMozgnem.First;
    while not DM.CDSMozgnem.Eof do
      begin
        if DM.CDSMozgnem.FieldByName('moz_id').AsInteger = A_mozgId then
          Begin
            Result:= DM.CDSMozgnem.FieldByName('moz_kod').asString;
            exit;
          End;
        DM.CDSMozgnem.Next;
      end;
  End;

  procedure ChangeFieldValueInCDS(ADataSet: TClientDataSet; AFilePath, AFieldName: String; AValue: Variant; Rename: boolean);
  var lcFgv: TFgvUnit;
  begin
    lcFgv := TfgvUnit.Create;
    ADataSet.LoadFromFile(AFilePath);
    ADataSet.LogChanges := false;
    ADataSet.First;
    while not ADataSet.Eof do
      begin
        ADataSet.Edit;
        ADataSet.FieldByName(AFieldName).AsVariant := AValue;
        ADataSet.Post;
        ADataSet.Next;
      end;
    lcFgv.DataSetToXML(ADataSet, AFilePath);
    if Rename then
      RenameFile(AFilePath, '_uploaded_'+AFilePath);
    lcFgv.Free;
  end;

var
  lcFgv: TfgvUnit;
  lcMost: TDateTime;
  lcRootDir: string;
  lcAdatMappa: string;
  lcAdatUtvonal: string;
  lcSearchResult: TSearchRec;
  lcHttpRequest: THttpsUnit;
  lcHttpResponse: string;
  lcIniFile: TIniFile;
  lcIPCim, lcUtvonal: string;
  lcDataset: TClientDataSet;
  lcPKKFajlNev: String;
  lcPKKMFajlNev: String;
  lcSorszam: String;
  lcTmpId: string;
  lcOsztalyId: Integer;
  lcOsztalyNev: String;
  i: integer;
begin
  if FDMemTableAttarolasKiadas.RecordCount = 0 then
    begin
      ShowMessage('�res kiad�st nem lehet r�gz�teni.');
      exit;
    end;
  case MessageDlg('Biztosan lez�rja a kiad�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        // Create-k
        lcFgv := TfgvUnit.Create;
        lcHttpRequest := THttpsUnit.Create();
        lcDataset := TClientDataSet.Create(Application);
        lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
        lcIPCim := lciniFile.ReadString('serverip','ip','');
        lcUtvonal := lciniFile.ReadString('utvonal','path','');

        //K�vetkez� sorsz�m a lok�lis f�jlb�l. Nem k�ldj�k el a v�ltoztat�st a szervernek!
        lcSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PKK', GPartner, GUserID, true, false);

        FDMemTablePKKXML.Close;
        FDMemTablePKKXML.Open;

        FDMemTablePKKMXML.Close;
        FDMemTablePKKMXML.Open;

        lcMost := now();

        // �tvonalak, f�jlok
        lcRootDir := GRootPath;
        lcAdatMappa := GetCurrentDir + '\adat\';
        SetCurrentDir(lcAdatMappa);

        if not DirectoryExists(lcFgv.PartnerMappa(GPartner)) then
          CreateDir(lcFgv.PartnerMappa(GPartner));

        lcAdatUtvonal := lcAdatMappa + lcFgv.PartnerMappa(GPartner);
        SetCurrentDir(lcAdatUtvonal);

        if not DirectoryExists(Trim(lcFgv.Mappa(lcMost))) then
          CreateDir(Trim(lcFgv.Mappa(lcMost)));

        lcAdatUtvonal := lcAdatUtvonal + '\' + Trim(lcFgv.Mappa(lcMost));

        GOldal := 'PKK';
        lcPKKFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';

        GOldal := 'PKKM';
        lcPKKMFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        i := 0;
        try
          // Adapterek felt�lt�se
          FDMemTableAttarolasKiadas.Filtered := false;
          FDMemTableAttarolasKiadas.First;
          while not FDMemTableAttarolasKiadas.Eof do
            begin
              // PKK.XML
              if not FindInOsszesitesPK then
                begin
                  FDMemTablePKKXML.Insert;
                  FDMemTablePKKXML.FieldByName('fajtaText').AsString := FDMemTableAttarolasKiadas.FieldByName('fajtanev').AsString;
                  FDMemTablePKKXML.FieldByName('allapotText').AsString := FDMemTableAttarolasKiadas.FieldByName('allapotnev').AsString;
                  FDMemTablePKKXML.FieldByName('meretText').AsString := FDMemTableAttarolasKiadas.FieldByName('meretnev').AsString;
                  FDMemTablePKKXML.FieldByName('magassagText').AsString := FDMemTableAttarolasKiadas.FieldByName('magassagnev').AsString;
                  FDMemTablePKKXML.FieldByName('szinText').AsString := FDMemTableAttarolasKiadas.FieldByName('szinnev').AsString;
                  FDMemTablePKKXML.FieldByName('megnevezesText').AsString := FDMemTableAttarolasKiadas.FieldByName('textilnev').AsString;

                  FDMemTablePKKXML.FieldByName('pbfej_id').AsInteger := -1;
                  FDMemTablePKKXML.FieldByName('pbfej_lezart').AsInteger := 0;
                  FDMemTablePKKXML.FieldByName('pbfej_sorszam').AsString := lcSorszam;
                  FDMemTablePKKXML.FieldByName('pbfej_uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbfej_partner_id').AsInteger := GPartner;
                  FDMemTablePKKXML.FieldByName('pbfej_ellenuzem_id').AsInteger := pubCelUzem;
                  FDMemTablePKKXML.FieldByName('pbfej_osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePKKXML.FieldByName('pbfej_mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PKK;
                  FDMemTablePKKXML.FieldByName('pbfej_felhasznalo_id').AsInteger := GUserID;
                  FDMemTablePKKXML.FieldByName('pbfej_megjegyzes').AsString := '';
                  FDMemTablePKKXML.FieldByName('pbfej_create_date').AsDateTime := lcMost;
                  FDMemTablePKKXML.FieldByName('pbfej_create_user').AsInteger := GUserID;
                  FDMemTablePKKXML.FieldByName('pbfej_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePKKXML.FieldByName('pbfej_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePKKXML.FieldByName('pbfej_selected_date').AsDateTime := GDate; //lcMost;
                  FDMemTablePKKXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_fajta_id').AsInteger := FDMemTableAttarolasKiadas.FieldByName('fajtaid').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_allapot_id').AsInteger := FDMemTableAttarolasKiadas.FieldByName('allapotid').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_textil_id').AsInteger := FDMemTableAttarolasKiadas.FieldByName('textilid').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_meret_id').AsInteger := FDMemTableAttarolasKiadas.FieldByName('meretid').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_magassag_id').AsInteger := FDMemTableAttarolasKiadas.FieldByName('magassagid').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_szin_id').AsInteger := FDMemTableAttarolasKiadas.FieldByName('szinid').AsInteger;
                  FDMemTablePKKXML.FieldByName('pbtet_megjegyzes').AsString := '';
                  FDMemTablePKKXML.FieldByName('pbtet_create_date').AsDateTime := lcMost;
                  FDMemTablePKKXML.FieldByName('pbtet_create_user').AsInteger := GUserID;
                  FDMemTablePKKXML.FieldByName('pbtet_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePKKXML.FieldByName('pbtet_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePKKXML.FieldByName('pbtet_fajlnev').AsString := lcPKKFajlNev;
                  FDMemTablePKKXML.FieldByName('pbtet_chiphez_rendelve').AsInteger := 0;
                  if (FDMemTableAttarolasKiadas.FieldByName('id').AsInteger > -1) then
                    begin
                      FDMemTablePKKXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePKKXML.FieldByName('pbtet_darabosdb').AsInteger + FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger;
                    end;
                  FDMemTablePKKXML.Post;
                end
              else
                begin
                  // PKK
                  FDMemTablePKKXML.Edit;
                  FDMemTablePKKXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTablePKKXML.FieldByName('pbtet_darabszam').AsInteger + FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger;
                  if (FDMemTableAttarolasKiadas.FieldByName('id').AsInteger > -1) then
                    begin
                      FDMemTablePKKXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePKKXML.FieldByName('pbtet_darabosdb').AsInteger + FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger;
                    end;
                  FDMemTablePKKXML.Post;
                end;

              // PKKM.xml
              if (FDMemTableAttarolasKiadas.FieldByName('id').AsInteger = -1) then
                begin
                  FDMemTablePKKMXML.Insert;
                  FDMemTablePKKMXML.FieldByName('sorszam').AsString := lcSorszam;
                  FDMemTablePKKMXML.FieldByName('partner_id').AsInteger := GPartner;
                  FDMemTablePKKMXML.FieldByName('uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePKKMXML.FieldByName('osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePKKMXML.FieldByName('prszall_fej_id').AsInteger := -1;
                  FDMemTablePKKMXML.FieldByName('vonalkod').AsString := lcFgv.GetVonalkodByChipkod(FDMemTableAttarolasKiadas.FieldByName('Chipkod').AsString);
                  FDMemTablePKKMXML.FieldByName('chipkod').AsString := FDMemTableAttarolasKiadas.FieldByName('Chipkod').AsString;
                  FDMemTablePKKMXML.FieldByName('mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PKK;
                  FDMemTablePKKMXML.FieldByName('textil_kpt_id').AsVariant := FDMemTableAttarolasKiadas.FieldByName('textilid').AsVariant;
                  FDMemTablePKKMXML.FieldByName('textil').AsString := FDMemTableAttarolasKiadas.FieldByName('textilnev').AsString;
                  FDMemTablePKKMXML.FieldByName('fajta_id').AsVariant := FDMemTableAttarolasKiadas.FieldByName('fajtaid').AsVariant;
                  FDMemTablePKKMXML.FieldByName('fajta').AsString := FDMemTableAttarolasKiadas.FieldByName('fajtanev').AsString;
                  FDMemTablePKKMXML.FieldByName('allapot_id').AsVariant := FDMemTableAttarolasKiadas.FieldByName('allapotid').AsVariant;
                  FDMemTablePKKMXML.FieldByName('allapot').AsString := FDMemTableAttarolasKiadas.FieldByName('allapotnev').AsString;
                  FDMemTablePKKMXML.FieldByName('meret_id').AsVariant := FDMemTableAttarolasKiadas.FieldByName('meretid').AsVariant;
                  FDMemTablePKKMXML.FieldByName('meret').AsString := FDMemTableAttarolasKiadas.FieldByName('meretnev').AsString;
                  FDMemTablePKKMXML.FieldByName('magassag_id').AsVariant := FDMemTableAttarolasKiadas.FieldByName('magassagid').AsVariant;
                  FDMemTablePKKMXML.FieldByName('magassag').AsString := FDMemTableAttarolasKiadas.FieldByName('magassagnev').AsString;
                  FDMemTablePKKMXML.FieldByName('szin_id').AsVariant := FDMemTableAttarolasKiadas.FieldByName('szinid').AsVariant;
                  FDMemTablePKKMXML.FieldByName('szin').AsString := FDMemTableAttarolasKiadas.FieldByName('szinnev').AsString;
                  FDMemTablePKKMXML.FieldByName('datum').AsDateTime := FDMemTableAttarolasKiadas.FieldByName('Datum').AsDateTime;
                  FDMemTablePKKMXML.FieldByName('felhasznalo_id').AsInteger := GUserId;
                  FDMemTablePKKMXML.FieldByName('fajlnev').AsString := lcPKKMFajlNev;
                  FDMemTablePKKMXML.FieldByName('kivezetesoka').AsInteger := FDMemTableAttarolasKiadas.FieldByName('kivezetesokaid').AsInteger;
                  FDMemTablePKKMXML.FieldByName('kivezetesokanev').AsString := FDMemTableAttarolasKiadas.FieldByName('kivezetesokanev').AsString;
                  FDMemTablePKKMXML.FieldByName('ellenuzem_id').AsInteger := pubCelUzem;
                  FDMemTablePKKMXML.FieldByName('uploaded').AsInteger := -1;
                  FDMemTablePKKMXML.Post;
                end;
              FDMemTableAttarolasKiadas.Next;
            end;

          DM.CDSUzem.First;
          TfrxMemoView(frxReportPKK.FindObject('mKiszDatum')).Text := DateToStr(GDate); // DateToStr(lcMost);
          TfrxMemoView(frxReportPKK.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPKK.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPKK.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPKK.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPKK.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPKK.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PKK);

          TfrxMemoView(frxReportPKKM.FindObject('mKiszDatum')).Text := DateToStr(GDate); // DateToStr(lcMost);
          TfrxMemoView(frxReportPKKM.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPKKM.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPKKM.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPKKM.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPKKM.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPKKM.FindObject('frxDBDSSzallszfej_szallszam')).Text := lcSorszam;
          TfrxMemoView(frxReportPKKM.FindObject('frxDBDSSzallszfej_szallszam1')).Text := lcSorszam;
  //        TfrxMemoView(frxReportPKKM.FindObject('frxDBDSSzallszfej_createdate')).Text := DateToStr(lcMost);
          TfrxMemoView(frxReportPKKM.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PKK);

          DM.CDSFelhasz.First;
          while not DM.CDSFelhasz.Eof do
            begin
              if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
                begin
                  TfrxMemoView(frxReportPKK.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  TfrxMemoView(frxReportPKKM.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                end;
              DM.CDSFelhasz.Next;
            end;
          DM.CDSUzemek.First;
          while not DM.CDSUzemek.Eof do
            begin
              if DM.CDSUzemek.FieldByName('uzem_id').AsInteger = pubCelUzem then
                begin
                  TfrxMemoView(frxReportPKK.FindObject('mPartnerNev')).Text := DM.CDSUzemek.FieldByName('uzem_nev').AsString;
                  TfrxMemoView(frxReportPKKM.FindObject('mPartnerNev')).Text := DM.CDSUzemek.FieldByName('uzem_nev').AsString;
                end;
              DM.CDSUzemek.Next;
            end;
          SetCurrentDir(lcAdatUtvonal);
          FDMemTablePKKXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPKKFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPKK.PrepareReport(true);
          frxReportPKK.Export(DM.frxPDFExport1);

          frxReportPKK.PrintOptions.Copies := 3;
          frxReportPKK.PrintOptions.Collate := True;
          frxReportPKK.PrepareReport(true);
          frxReportPKK.ShowPreparedReport;

          // A Megn�velt sorsz�m felk�ld�se a szervernek
          SetCurrentDir(lcRootDir);
          lcSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PKK', GPartner, GUserID, false, true);
          SetCurrentDir(lcAdatUtvonal);

          lcFgv.DataSetToXML(TDataSet(FDMemTablePKKXML), lcPKKFajlNev);

          FDMemTablePKKMXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPKKMFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPKKM.PrepareReport(true);
          frxReportPKKM.Export(DM.frxPDFExport1);

          frxReportPKKM.PrintOptions.Copies := 3;
          frxReportPKKM.PrintOptions.Collate := True;
          frxReportPKKM.PrepareReport(true);
          frxReportPKKM.ShowPreparedReport;

          lcFgv.DataSetToXML(TDataSet(FDMemTablePKKMXML), lcPKKMFajlNev);

          lcFgv.SetPufferRaktar('kiad', false, false, TClientDataSet(FDMemTablePKKXML));
          lcFgv.SetPufferRaktar('kiad', true, false, TClientDataSet(FDMemTablePKKMXML));

          lcHttpResponse := '';
          // Az�rt pufBevRakXMLUpload-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s, nincs r� k�l�n request.
          lcHttpResponse := lcHttpRequest.PostHttps(lcPKKFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
          if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
            begin
              lcTmpId := lcHttpResponse;
              ChangeFieldValueInCDS(lcDataset, lcPKKFajlNev, 'pbfej_id', lcTmpId, true);
              lcHttpResponse := '';
              // Az�rt pufBevRakXMLUpload_PBM-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s chip mozg�sok, nincs r� k�l�n request.
              ChangeFieldValueInCDS(lcDataset, lcPKKMFajlNev, 'prszall_fej_id', lcTmpId, false);
              lcHttpResponse := lcHttpRequest.PostHttps(lcPKKMFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
              if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                begin
                  ChangeFieldValueInCDS(lcDataset, lcPKKMFajlNev, 'uploaded', 1, true);
                end
              else
                showmessage('A felt�lt�s nem lehets�ges. (PKKMxxxx.xml szakasz) - ' + lcHttpResponse);
            end
          else
            showmessage('A felt�lt�s nem lehets�ges. (PKKxxxx.xml szakasz) - ' + lcHttpResponse);

        finally
          SetCurrentDir(lcRootDir);
        end;
        // Destroy-ok
        lcFgv.Destroy;
        lcHttpRequest.Destroy;
        lcDataset.Destroy;
        lcIniFile.Destroy;
        prCloseOnSave := true;
        Close;
      end;
    mrNo: exit;
  end;
end;

procedure TfrmPufferAttarolasKiadas.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPufferAttarolasKiadas.Button3Click(Sender: TObject);
var
  lcId: integer;
begin
  if FDMemTableAttarolasKiadas.RecordCount = 0 then
    exit;
  case MessageDlg('Biztos t�r�lni akarja a kiv�lasztott t�telt?', mtConfirmation, [mbYes, mbNo], 0) of
    mrNo:
      begin
        Abort;
      end;
    mrYes:
      begin
        lcId := CDSDarabosValaszto.FieldByName('id').AsInteger;
        if FDMemTableAttarolasKiadas.FieldByName('id').AsInteger > -1 then
          begin
            CDSDarabosValaszto.Filtered := false;
            CDSDarabosValaszto.Filter := 'id='+IntToStr(FDMemTableAttarolasKiadas.FieldByName('id').AsInteger);
            CDSDarabosValaszto.Filtered := true;
            CDSDarabosValaszto.First;
            CDSDarabosValaszto.Edit;
            CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger + FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger;
            CDSDarabosValaszto.Post;
            CDSDarabosValaszto.Filtered := false;
            CDSDarabosValaszto.First;
            while not lcId = CDSDarabosValaszto.FieldByName('id').AsInteger do
              CDSDarabosValaszto.Next;
          end;
        FDMemTableAttarolasKiadas.Delete;
        RedrawOsszPanel;
      end;
  end;
end;

procedure TfrmPufferAttarolasKiadas.Button6Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmPufferAttarolasKiadas.Button7Click(Sender: TObject);

  function FindInKiadas: boolean;
  begin
    result := false;
    FDMemTableAttarolasKiadas.First;
    while not FDMemTableAttarolasKiadas.Eof do
      begin
        if (FDMemTableAttarolasKiadas.FieldByName('id').AsInteger = prKivalasztottTetel.id) then
          begin
            result := true;
            exit;
          end;
        FDMemTableAttarolasKiadas.Next;
      end;
  end;

begin
  if prKivalasztottTetel.darabosossz >= StrToInt(darabEdit.Text) then
    begin
      if FindInKiadas then
        begin
          FDMemTableAttarolasKiadas.Edit;
          FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger := FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger + StrToInt(darabEdit.Text);
          FDMemTableAttarolasKiadas.Post;
        end
      else
        begin
          FDMemTableAttarolasKiadas.Insert;
          FDMemTableAttarolasKiadas.FieldByName('id').AsInteger := prKivalasztottTetel.id;
          FDMemTableAttarolasKiadas.FieldByName('chipkod').AsString := '';
          FDMemTableAttarolasKiadas.FieldByName('textilid').AsInteger := prKivalasztottTetel.textilid;
          FDMemTableAttarolasKiadas.FieldByName('textilnev').AsString := prKivalasztottTetel.textilnev;
          FDMemTableAttarolasKiadas.FieldByName('fajtaid').AsInteger := prKivalasztottTetel.fajtaid;
          FDMemTableAttarolasKiadas.FieldByName('fajtanev').AsString := prKivalasztottTetel.fajtanev;
          FDMemTableAttarolasKiadas.FieldByName('allapotid').AsInteger := prKivalasztottTetel.allapotid;
          FDMemTableAttarolasKiadas.FieldByName('allapotnev').AsString := prKivalasztottTetel.allapotnev;
          FDMemTableAttarolasKiadas.FieldByName('meretid').AsInteger := prKivalasztottTetel.meretid;
          FDMemTableAttarolasKiadas.FieldByName('meretnev').AsString := prKivalasztottTetel.meretnev;
          FDMemTableAttarolasKiadas.FieldByName('magassagid').AsInteger := prKivalasztottTetel.magassagid;
          FDMemTableAttarolasKiadas.FieldByName('magassagnev').AsString := prKivalasztottTetel.magassagnev;
          FDMemTableAttarolasKiadas.FieldByName('szinid').AsInteger := prKivalasztottTetel.szinid;
          FDMemTableAttarolasKiadas.FieldByName('szinnev').AsString := prKivalasztottTetel.szinnev;
          FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger := StrToInt(darabEdit.Text);
          FDMemTableAttarolasKiadas.FieldByName('datum').AsDateTime := Now();
          FDMemTableAttarolasKiadas.Post;
        end;
      CDSDarabosValaszto.First;
      while not CDSDarabosValaszto.Eof do
        begin
          if CDSDarabosValaszto.FieldByName('id').AsInteger = prKivalasztottTetel.id then
            begin
              CDSDarabosValaszto.Edit;
              CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger - StrToInt(darabEdit.Text);
              CDSDarabosValaszto.Post;
              break;
            end;
          CDSDarabosValaszto.Next;
        end;
      darabEdit.Text := '';
      Button7.Enabled := false;
      PageControl1.ActivePageIndex := 0;
      RedrawOsszPanel;
    end
  else
    begin
      ShowMessage('Nincs ekkora el�rhet� mennyis�g a kiv�lasztott t�telb�l.');
    end;
end;

procedure TfrmPufferAttarolasKiadas.Button8Click(Sender: TObject);
begin
  CDSDarabosValaszto.Filtered := false;
  CDSDarabosValaszto.Filter := 'partnerid='+IntToStr(GPartner);
  CDSDarabosValaszto.Filtered := true;
  CDSDarabosValaszto.First;

  PageControl1.ActivePageIndex := 1;
end;

procedure TfrmPufferAttarolasKiadas.chipEditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Ord(Key) = VK_RETURN then
    btnChipHozzaadClick(nil);
end;

procedure TfrmPufferAttarolasKiadas.dbGridDarabosCellClick(Column: TColumn);
begin
  if CDSDarabosValaszto.Active then
    begin
      DBGridDarabos.SelectedRows.CurrentRowSelected := True;
    end;
end;

procedure TfrmPufferAttarolasKiadas.dbGridDarabosDblClick(Sender: TObject);
begin
  if CDSDarabosValaszto.Active then
    begin
      if DBGridDarabos.SelectedRows.Count > 0 then
        begin
          if CDSDarabosValaszto.FieldByName('darabosossz').AsInteger = 0 then
            begin
              ShowMessage('Ebb�l a t�telb�l nincs m�r �sszerendelhet� mennyis�g!');
              exit;
            end;
          prKivalasztottTetel.textilid := CDSDarabosValaszto.FieldByName('textilid').AsInteger;
          prKivalasztottTetel.textilnev := CDSDarabosValaszto.FieldByName('textilnev').AsString;
          prKivalasztottTetel.fajtaid := CDSDarabosValaszto.FieldByName('fajtaid').AsInteger;
          prKivalasztottTetel.fajtanev := CDSDarabosValaszto.FieldByName('fajtanev').AsString;
          prKivalasztottTetel.allapotid := CDSDarabosValaszto.FieldByName('allapotid').AsInteger;
          prKivalasztottTetel.allapotnev := CDSDarabosValaszto.FieldByName('allapotnev').AsString;
          prKivalasztottTetel.meretid := CDSDarabosValaszto.FieldByName('meretid').AsInteger;
          prKivalasztottTetel.meretnev := CDSDarabosValaszto.FieldByName('meretnev').AsString;
          prKivalasztottTetel.magassagid := CDSDarabosValaszto.FieldByName('magassagid').AsInteger;
          prKivalasztottTetel.magassagnev := CDSDarabosValaszto.FieldByName('magassagnev').AsString;
          prKivalasztottTetel.szinid := CDSDarabosValaszto.FieldByName('szinid').AsInteger;
          prKivalasztottTetel.szinnev := CDSDarabosValaszto.FieldByName('szinnev').AsString;
          prKivalasztottTetel.darabosossz := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger;
          prKivalasztottTetel.id := CDSDarabosValaszto.FieldByName('id').AsInteger;
          darabEdit.SetFocus;
          Button7.Enabled := true;
        end;
    end;
end;

procedure TfrmPufferAttarolasKiadas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if prCloseOnSave then
    begin
      SerialReader.Stop;
      Action := caFree;
    end
  else
    begin
      case MessageDlg('Biztos megszak�tja a m�veletet?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            SerialReader.Stop;
            Action := caFree;
          end;
        mrNo: Action := caNone;
      end;
    end;
  if Action = caFree then
    frmPufferAttarolasKiadas:= nil;
end;

procedure TfrmPufferAttarolasKiadas.FormCreate(Sender: TObject);
var
  i: integer;
  IniFile: TIniFile;
  prPort, prPortConfig: string;
  lcDataset: TFDMemTable;
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;
  // Hide tab from TabControll
  btnChipHozzaad.Tag := 0;
  for i := 0 to PageControl1.PageCount -1  do
    begin
      PageControl1.Pages[i].TabVisible := false;
    end;
  if FileExists('./darabospuffer.xml') then
    begin
      CDSDarabosValaszto.LoadFromFile('./darabospuffer.xml');
      CDSDarabosValaszto.Active := true;
      CDSDarabosValaszto.LogChanges := false;
      CDSDarabosValaszto.Filtered := false;
      CDSDarabosValaszto.Filter := 'partnerid='+IntToStr(GPartner);
      CDSDarabosValaszto.Filtered := true;
      CDSDarabosValaszto.First;
      if FileExists('./tmp_osszerendel.xml') then
        begin
          lcDataset := TFDMemTable.Create(application);
          lcDataset.LoadFromFile('./tmp_osszerendel.xml');
          lcDataset.Active := true;
          lcDataset.LogChanges := false;
          lcDataset.First;
          while not lcDataset.Eof do
            begin
              while not CDSDarabosValaszto.Eof do
                begin
                  if (CDSDarabosValaszto.FieldByName('textilnev').AsString = lcDataset.FieldByName('textilnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('fajtanev').AsString = lcDataset.FieldByName('fajtanev').AsString) and
                      (CDSDarabosValaszto.FieldByName('allapotnev').AsString = lcDataset.FieldByName('allapotnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('meretnev').AsString = lcDataset.FieldByName('meretnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('magassagnev').AsString = lcDataset.FieldByName('magassagnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('szinnev').AsString = lcDataset.FieldByName('szinnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('partnerid').AsInteger = lcDataset.FieldByName('partner').AsInteger) then
                    begin
                      CDSDarabosValaszto.Edit;
                      CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger-1;
                      CDSDarabosValaszto.Post;
                    end;
                  CDSDarabosValaszto.Next;
                end;
              lcDataset.Next;
            end;
        end;
    end;

  PageControl1.ActivePageIndex := 0;
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  prPort := IniFile.ReadString('port', 'szam', prPort);
  prPortConfig := IniFile.ReadString('port', 'ertek', prPortConfig);
  IniFile.Free;
  SerialReader := TSerialReader.Create('attarolaskiad', prPort, prPortConfig);
  prCloseOnSave := false;
  FDMemTableAttarolasKiadas.Active := true;
end;

procedure TfrmPufferAttarolasKiadas.RedrawOsszPanel;

  function lcIndexOf(arr: array of TBevetTetel; haystack: TBevetTetel): integer;
  var i: integer;
  begin
    result := -1;
    for i := 0 to Length(arr)-1 do
    begin
      if (arr[i].Fajta = haystack.Fajta) and
         (arr[i].Allapot = haystack.Allapot) and
         (arr[i].Szin = haystack.Szin) and
         (arr[i].Meret = haystack.Meret) and
         (arr[i].Megnev = haystack.Megnev) and
         (arr[i].Magassag = haystack.Magassag) then
        begin
          result := i;
          break;
        end;
    end;
  end;

var i: integer;
    lcTetelek: array of TBevetTetel;
    lcCurrentTetel: TBevetTetel;
    lcFindIndex: integer;
    lcScrollPosition: integer;
begin
  if FDMemTableAttarolasKiadas.Tag = 1 then
    Exit;
  FDMemTableAttarolasKiadas.Tag := 1;
  SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(false), 0);
  try
    lcScrollPosition :=  ScrollBox1.VertScrollBar.ScrollPos;
    for i := 0 to length(prPanels)-1 do
      begin
        if assigned(prPanels[i]) then
          begin
            prPanels[i].Destroy;
          end;
      end;
    setLength(prPanels, 0);
    FDMemTableAttarolasKiadas.First;
    for i := 0 to FDMemTableAttarolasKiadas.RecordCount-1 do
      begin
        lcCurrentTetel.Fajta := FDMemTableAttarolasKiadas.FieldByName('fajtanev').AsString;
        lcCurrentTetel.Allapot := FDMemTableAttarolasKiadas.FieldByName('allapotnev').AsString;
        lcCurrentTetel.Szin := FDMemTableAttarolasKiadas.FieldByName('szinnev').AsString;
        lcCurrentTetel.Meret := FDMemTableAttarolasKiadas.FieldByName('meretnev').AsString;
        lcCurrentTetel.Magassag := FDMemTableAttarolasKiadas.FieldByName('magassagnev').AsString;
        lcCurrentTetel.Megnev := FDMemTableAttarolasKiadas.FieldByName('textilnev').AsString;
        lcCurrentTetel.Mennyiseg := FDMemTableAttarolasKiadas.FieldByName('mennyiseg').AsInteger;
        lcFindIndex := lcIndexOf(lcTetelek, lcCurrentTetel);
        if (lcFindIndex > -1) then
          begin
            lcTetelek[lcFindIndex].Mennyiseg := lcTetelek[lcFindIndex].Mennyiseg + lcCurrentTetel.Mennyiseg;
          end
        else
          begin
            SetLength(lcTetelek, length(lcTetelek)+1);
            lcTetelek[length(lcTetelek)-1] := lcCurrentTetel;
          end;
          FDMemTableAttarolasKiadas.Next;
      end;
    setLength(prPanels, length(lcTetelek));
    for i := 0 to length(lcTetelek)-1 do
      begin
      if (lcTetelek[i].Mennyiseg <> 0) then
        begin
          prPanels[i] := CopyComponents(samplePanel, samplePanel.Owner, samplePanel.Parent, prNextPanel);
          SetPanelLabels(prPanels[i], lcTetelek[i].Fajta, lcTetelek[i].Allapot, lcTetelek[i].Szin, lcTetelek[i].Meret, lcTetelek[i].Magassag, lcTetelek[i].Megnev, lcTetelek[i].Mennyiseg);
          TPanel(prPanels[i]).Top := (samplePanel.Height*i)+4;
          TPanel(prPanels[i]).visible := true;
          prNextPanel := prNextPanel + 1;
        end;
      end;
  finally
    begin
      ScrollBox1.VertScrollBar.Position := lcScrollPosition;
      SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(true), 0);
      ScrollBox1.Repaint;
    end;
  end;
  FDMemTableAttarolasKiadas.Tag := 0;
end;

end.
