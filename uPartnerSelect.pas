unit uPartnerSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, Data.DB, Vcl.Grids, Vcl.DBGrids;

type
  TfrmPartnerSelect = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    iLPartner: TImageList;
    btnRendben: TButton;
    btnBezar: TButton;
    gbPartner: TGroupBox;
    gbValPart: TGroupBox;
    dbgPartner: TDBGrid;
    lPartnerSelect: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBezarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgPartnerDblClick(Sender: TObject);
    procedure btnRendbenClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    prUtvonal, prUtMent : String;
    prOnCloseButton : Boolean;
  public
    { Public declarations }
  end;

var
  frmPartnerSelect: TfrmPartnerSelect;

implementation

uses uDM, uTMRMain;
{$R *.dfm}

procedure TfrmPartnerSelect.btnBezarClick(Sender: TObject);
begin
  prOnCloseButton := true;
  close;
end;

procedure TfrmPartnerSelect.btnRendbenClick(Sender: TObject);
begin
  if GPartner <> 0 then
    begin
//      frmTMRMain.btnSzennyes.Enabled := true;
//      frmTMRMain.btnTiszta.Enabled := true;
//    //  frmTMRMain.btnOsszesit.Enabled := true;
//      frmTMRMain.btnOsszerend.Enabled := true;
//      frmTMRMain.btnOsszLista.Enabled := true;
      frmTMRMain.btnPufferBevetBtn.Enabled := true;
      frmTMRMain.btnPufferKiadOsszerendel.Enabled := true;
      frmTMRMain.btnPufferVisszavet.Enabled := true;
      frmTMRMain.btnPufferAttarolas.Enabled := true;
      frmTMRMain.Caption := 'Textil Mos�si Rendszer - T.M.R. ' + ' - ' + lPartnerSelect.Caption;
      close;
    end;
end;

procedure TfrmPartnerSelect.dbgPartnerDblClick(Sender: TObject);
begin
  GPartner := dbgPartner.DataSource.DataSet.FieldByName('par_id').AsInteger;
  GPartnerNev := dbgPartner.DataSource.DataSet.FieldByName('par_nev').AsString;
  lPartnerSelect.Caption := dbgPartner.DataSource.DataSet.FieldByName('par_nev').AsString;
end;

procedure TfrmPartnerSelect.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  if (prOnCloseButton) then
    begin
      frmTMRMain.btnSzennyes.Enabled := false;
      frmTMRMain.btnTiszta.Enabled := false;
      frmTMRMain.btnOsszesit.Enabled := false;
      frmTMRMain.btnOsszerend.Enabled := false;
      frmTMRMain.btnOsszLista.Enabled := false;
      frmTMRMain.btnPufferBevetBtn.Enabled := false;
      frmTMRMain.btnPufferKiadOsszerendel.Enabled := false;
      frmTMRMain.btnPufferVisszavet.Enabled := false;
      frmTMRMain.btnPufferAttarolas.Enabled := false;
      GPartner := 0;
      frmTMRMain.Caption := 'Textil Mos�si Rendszer - T.M.R.';
    end;
end;

procedure TfrmPartnerSelect.FormCreate(Sender: TObject);
begin
  prOnCloseButton := false;
end;

procedure TfrmPartnerSelect.FormShow(Sender: TObject);
begin
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  dbgPartner.SetFocus;
  DM.CDSPartSelect.LoadFromFile('partnerek.xml');
  DM.CDSPartSelect.LogChanges := false;
  DM.CDSPartSelect.Filter := 'par_puffer_raktar=0';
  DM.CDSPartSelect.FieldByName('par_id').Visible := false;
  DM.CDSPartSelect.FieldByName('par_rovnev').Visible := false;
  DM.CDSPartSelect.FieldByName('par_kod').Visible := false;
  DM.CDSPartSelect.FieldByName('par_lastuser').Visible := false;
  DM.CDSPartSelect.FieldByName('par_lastup').Visible := false;
 // DM.CDSTextilek.FieldByName('par_delete').Visible := false;
  DM.CDSPartSelect.FieldByName('par_szamlaalap').Visible := false;
  DM.CDSPartSelect.FieldByName('par_nev').DisplayLabel := 'Partner n�v';
end;

end.
