unit uFgv;

interface

uses System.UITypes, System.SysUtils, Vcl.Forms, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
     FireDAC.Stan.Param, Datasnap.DBClient, Data.DB, INIFiles, System.DateUtils, System.Variants;

type
  TPufferRaktarTextil = Record
    textilid: integer;
    textilnev: string;
    textilsuly: double;
    fajtaid: integer;
    fajtanev: string;
    allapotid: integer;
    allapotnev: string;
    meretid: integer;
    meretnev: string;
    magassagid: integer;
    magassagnev: string;
    szinid: integer;
    szinnev: string;
    darabosossz: integer;
    id: integer;
    chipkod: string;
    vonalkod: string;
    kivezetesokaid: integer;
    kivezetesokanev: string;
  end;

type
  TVonalkodAdatok = Record
    Dolgozonev: string;
    Helyszinnev: string;
    Szekreny: string;
    Telephelykod: string;
  End;

type
  TBevetTetel = record
    Fajta: string;
    Allapot: string;
    Szin: string;
    Meret: string;
    Magassag: string;
    Mennyiseg: integer;
    Megnev: string;
  end;

type
  TfgvUnit = class
  private
    prUtvonal : String;
    prUtMent : String;
    prSzallMent : String;

  public
    function FajlNev(AFajlDate : TDate) : String;
    function Mappa(ADate : TDate) : String;
    function FajlOsztalyEll(AOsztalyID : Integer) : String;
    function TorzsAdatEll(APartner : Integer; AUtvonal, AMappa : String) : Boolean;
    function PartnerMappa(APartner : Integer) : String;
    function SzFajlEll(APartner : Integer; AOsztaly : Integer) : String;
    function SzallSorGen : String;
    function Mozgasnem : Integer;
    function SzennyesSMent(ASorsz : Integer) : Boolean;
    function TisztaTMent(ASorsz : Integer) : Boolean;
    function SetOsztalyID(AOsztNev : String) : Integer;
    function GetOsztalyNev(AOsztID : Integer) : String;
    function GenTiszta(DataSet : TDataSet; AOSszTiszta: Boolean) : Boolean;
    function CreateCDSField(ACreate : Boolean) : Boolean;
    function FajlNev_2(AFajlDate : TDate; AOsztID : Integer) : String;
    function SetMappa(ATorzs: Boolean; APartnerID : Integer; AFajl: String) : String;
    function GetNaptar(ADate : TDate) : Boolean;
    function GetVonalkod(AVonalkod : String) : Boolean;
    function GetChipkod(AChipkod : String) : Boolean;
    function EmptyVonalkod(AVonalkod : String) : Boolean;
    function GetDolgozonev(AVonalkod : String) : String;
    function GetCikknev(AVonalkod : String) : String;
    function GetTextilID(ATextilNev : String) : Integer;
    function SorszamGeneralas(ANev: string; APartner, AFelh: integer; AInc, APut: boolean) : string;
    procedure DataSetToXML(DataSet : TDataSet; const FileName:string);
    function IsInPufferRaktar(AChipKod: String; ACheckIn: boolean; AWhere: TClientDataSet; AFilter: string; var msgString: string) : Boolean;
    function GetTextilFromPufferRaktarByChipkod(AChipKod: string): TPufferRaktarTextil;
    function IsNumber(const S: string): Boolean;
    function GetVonalkodAdatokByVonalkod(AVonalkod: string): TVonalkodAdatok;
    procedure SetPufferRaktar(AMoveDir: string; AChipes, AModifyChipXml: boolean; ADataSet: TClientDataSet);
    function GetVonalkodByChipkod(AChipKod: string): string;
    function GetMeretCsoportByTextilId(textil_id: integer; isSimple: boolean): integer;
  end;

implementation

uses uDM, uHttps, uPufferVisszavetelezes;

function TfgvUnit.GetTextilID(ATextilNev : String) : Integer;
begin
  Result := -99999;
  DM.CDSTextilek.First;
  while not DM.CDSTextilek.Eof do
    begin
      if Trim(DM.CDSTextilek.FieldByName('kpt_smegnev').AsString) = Trim(ATextilNev) then
        begin
          Result := DM.CDSTextilek.FieldByName('kpt_id').AsInteger;
        end;
      DM.CDSTextilek.Next;
    end;
end;

function TfgvUnit.GetCikknev(AVonalkod : String) : String;
begin
  Result := '';
  DM.CDSChip.First;
  while not DM.CDSChip.Eof do
    begin
      if AVonalkod = DM.CDSChip.FieldByName('cms_vonalkod').AsString then
        begin
          Result := DM.CDSChip.FieldByName('cms_cikknev').AsString;
          exit;
        end
      else
        begin
          Result := '';
        end;
      DM.CDSChip.Next;
    end;
end;

function TfgvUnit.GetDolgozonev(AVonalkod : String) : String;
begin
  Result := '';
  DM.CDSChip.First;
  while not DM.CDSChip.Eof do
    begin
      if AVonalkod = DM.CDSChip.FieldByName('cms_vonalkod').AsString then
        begin
          Result := DM.CDSChip.FieldByName('cms_dolgozonev').AsString;
          exit;
        end
      else
        begin
          Result := '';
        end;
      DM.CDSChip.Next;
    end;
end;

function TfgvUnit.EmptyVonalkod(AVonalkod : String) : Boolean;
begin
  Result := false;
  DM.CDSChip.First;
  while not DM.CDSChip.Eof do
    begin
      if (uppercase(AVonalkod)) = (uppercase(DM.CDSChip.FieldByName('cms_vonalkod').AsString)) then
        begin
          Result := true;
          exit;
        end
      else
        begin
          Result := false;
        end;
      DM.CDSChip.Next;
    end;
end;

function TfgvUnit.GetChipkod(AChipkod : String) : Boolean;
begin
  //Chipk�d ellen�rz�se a chipbtx.xml f�jlban.
  Result := false;
  DM.CDSChip.First;
  while not DM.CDSChip.Eof do
    begin
      if AChipkod = DM.CDSChip.FieldByName('cms_chipkod').AsString then
        begin
          if DM.CDSChip.FieldByName('cms_vonalkod').AsString <> '' then
            begin
              Result := true;
            end
          else
            begin
              Result := false;
            end;
        end;
      DM.CDSChip.Next;
    end;
end;

function TfgvUnit.GetVonalkod(AVonalkod : String) : Boolean;
begin
  //Vonalk�d k�d ellen�rz�se a chipbtx.xml -ben, hogy benne van-e �s, hogy van-e hozz� chipk�d rendelve.
  Result := true;
  DM.CDSChip.First;
  while not DM.CDSChip.Eof do
    begin
      if AVonalkod = DM.CDSChip.FieldByName('cms_vonalkod').AsString then
        begin
          if DM.CDSChip.FieldByName('cms_chipkod').AsString <> '' then
            begin
              Result := true;
            end
          else
            begin
              Result := false;
            end;
        end;
      DM.CDSChip.Next;
    end;
end;

function TfgvUnit.GetNaptar(ADate : TDate) : Boolean;
begin
  result := false;
  DM.CDSNaptar.First;
  while not DM.CDSNaptar.Eof do
    begin
      if CompareDate(ADate, TDate(DM.CDSNaptar.FieldByName('nap_datum').AsDateTime)) = 0 then
        begin
          result := true;
        end;
      DM.CDSNaptar.Next;
    end;
end;

function TfgvUnit.SetMappa(ATorzs: Boolean; APartnerID : Integer; AFajl: String) : String;
var
  lcMappa, lcUtvonal : String;
begin
  if ATorzs then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\torzs_' + IntToStr(APartnerID);
      DM.CDSTextilek.LoadFromFile(lcUtvonal+'\'+AFajl);
      DM.CDSTextilek.LogChanges := false;
      result := lcUtvonal+'\'+AFajl;
    end;
end;

function TfgvUnit.CreateCDSField(ACreate : Boolean) : Boolean;
begin
  DM.CDSTiszta.FieldDefs.Clear;
  DM.CDSTiszta.Active := false;
  if ACreate then
    begin
      DM.CDSTiszta.FieldDefs.Add('szfej_partner',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_uzem',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_mozgid',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_osztid',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_osszsuly',ftFloat);
      DM.CDSTiszta.FieldDefs.Add('szfej_lastuser',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_lastup',ftDateTime);
      DM.CDSTiszta.FieldDefs.Add('szfej_delete',ftSmallint);
      DM.CDSTiszta.FieldDefs.Add('szfej_createuser',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_createdate',ftDateTime);
      DM.CDSTiszta.FieldDefs.Add('szfej_szallszam',ftString,45);
      DM.CDSTiszta.FieldDefs.Add('szfej_kijelol',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_mertsuly',ftFloat);
      DM.CDSTiszta.FieldDefs.Add('sztet_osztid',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('sztet_osztsuly',ftFloat);
      DM.CDSTiszta.FieldDefs.Add('sztet_textilid',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('sztet_textilnev',ftString,60);
      DM.CDSTiszta.FieldDefs.Add('sztet_textildb',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('sztet_textilsuly',ftFloat);
      DM.CDSTiszta.FieldDefs.Add('sztet_lastuser',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('sztet_lastup',ftDateTime);
      DM.CDSTiszta.FieldDefs.Add('sztet_delete',ftSmallint);
      DM.CDSTiszta.FieldDefs.Add('sztet_createuser',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('sztet_createdate',ftDateTime);
      DM.CDSTiszta.FieldDefs.Add('sztet_fajlnev',ftString,45);
      DM.CDSTiszta.FieldDefs.Add('sztet_recsorsz',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('sztet_javitott',ftSmallint);
      DM.CDSTiszta.FieldDefs.Add('sztet_bekeszit',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_lezart',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_id',ftInteger);
      DM.CDSTiszta.FieldDefs.Add('szfej_kiszdatum',ftDate);
      DM.CDSTiszta.CreateDataSet;
    end;
end;

function TfgvUnit.GenTiszta(DataSet: TDataSet; AOSszTiszta : Boolean) : Boolean;
  procedure RecordAppend(ASzallGen:Boolean; ASzfejID:Integer);
  var
    lcSzall : TfgvUnit;
  begin
    DM.CDSTiszta.Append;
    DM.CDSTiszta.FieldByName('szfej_partner').AsInteger := DataSet.FieldByName('szfej_partner').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_uzem').AsInteger := DataSet.FieldByName('szfej_uzem').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_mozgid').AsInteger := DataSet.FieldByName('szfej_mozgid').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_osztid').AsInteger := DataSet.FieldByName('szfej_osztid').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_osszsuly').AsFloat := DataSet.FieldByName('szfej_osszsuly').AsFloat;
    DM.CDSTiszta.FieldByName('szfej_lastuser').AsInteger := DataSet.FieldByName('szfej_lastuser').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_lastup').AsDateTime := DataSet.FieldByName('szfej_lastup').AsDateTime;
    DM.CDSTiszta.FieldByName('szfej_delete').AsInteger := DataSet.FieldByName('szfej_delete').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_createuser').AsInteger := DataSet.FieldByName('szfej_createuser').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_createdate').AsDateTime := DataSet.FieldByName('szfej_createdate').AsDateTime;
    if ASzallGen then
      begin
        lcSzall := TfgvUnit.Create();
        DM.CDSTiszta.FieldByName('szfej_szallszam').AsString := lcSzall.SzallSorGen;
        prSzallMent := DM.CDSTiszta.FieldByName('szfej_szallszam').AsString;
        DM.CDSTiszta.FieldByName('szfej_id').AsInteger := 0;
        lcSzall.Free;
      end
    else if not ASzallGen then
      begin
        DM.CDSTiszta.FieldByName('szfej_szallszam').AsString := prSzallMent;
        DM.CDSTiszta.FieldByName('szfej_id').AsInteger := ASzfejID;
      end;
    DM.CDSTiszta.FieldByName('szfej_kijelol').AsInteger := DataSet.FieldByName('szfej_kijelol').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_mertsuly').AsFloat := DataSet.FieldByName('szfej_mertsuly').AsFloat;
    DM.CDSTiszta.FieldByName('sztet_osztid').AsInteger := DataSet.FieldByName('sztet_osztid').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_osztsuly').AsFloat := DataSet.FieldByName('sztet_osztsuly').AsFloat;
    DM.CDSTiszta.FieldByName('sztet_textilid').AsInteger := DataSet.FieldByName('sztet_textilid').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_textilnev').AsString := DataSet.FieldByName('sztet_textilnev').AsString;
    DM.CDSTiszta.FieldByName('sztet_textildb').AsInteger := DataSet.FieldByName('sztet_textildb').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_textilsuly').AsFloat := DataSet.FieldByName('sztet_textilsuly').AsFloat;
    DM.CDSTiszta.FieldByName('sztet_lastuser').AsInteger := DataSet.FieldByName('sztet_lastuser').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_lastup').AsDateTime := DataSet.FieldByName('sztet_lastup').AsDateTime;
    DM.CDSTiszta.FieldByName('sztet_delete').AsInteger := DataSet.FieldByName('sztet_delete').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_createuser').AsInteger := DataSet.FieldByName('sztet_createuser').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_createdate').AsDateTime := DataSet.FieldByName('sztet_createdate').AsDateTime;
    DM.CDSTiszta.FieldByName('sztet_fajlnev').AsString := DataSet.FieldByName('sztet_fajlnev').AsString;
    DM.CDSTiszta.FieldByName('sztet_recsorsz').AsInteger := DataSet.FieldByName('sztet_recsorsz').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_javitott').AsInteger := DataSet.FieldByName('sztet_javitott').AsInteger;
    DM.CDSTiszta.FieldByName('sztet_bekeszit').AsInteger := DataSet.FieldByName('sztet_bekeszit').AsInteger;
    DM.CDSTiszta.FieldByName('szfej_kiszdatum').AsDateTime := DataSet.FieldByName('szfej_kiszdatum').AsDateTime;
    DM.CDSTiszta.FieldByName('szfej_lezart').AsInteger := 0;
    DM.CDSTiszta.Post;
  end;

var
  lcSearchResult : TSearchRec;
  lcFajlNev, lcVissza, lcUtvonal, lcUtvonalMent, lcUt : String;
  lcFajl, lcXmlMent, lcMappaCreate : TfgvUnit;
  lcFound, lcSzfejID : Integer;
begin
  result := false;
  Dataset.First;
  lcUtvonalMent := GetCurrentDir;
  lcSzfejID := -99999;
  //a k�rh�zi szint� tiszta gener�l�s kezd�s.
  if AOSszTiszta then
    begin
      while not DataSet.Eof do
        begin
          DM.CDSTextilek.First;
          while not DM.CDSTextilek.Eof do
            begin
              if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '0') then
                begin
                  lcFajl := TfgvUnit.Create();
                  lcFajlNev := lcFajl.FajlNev(GDate);
                  //Itt kell vizsg�lni a napt�r xml-t.
                  lcFajlNev := 'OT' + copy(lcFajlNev,2,lcFajlNev.Length);
                  lcMappaCreate := TfgvUnit.Create();
                  lcUtvonal := GetCurrentDir;
                  SetCurrentDir(lcUtvonal);
                  if not directoryexists(lcMappaCreate.Mappa(GDate)) then
                    begin
                      CreateDir(lcMappaCreate.Mappa(GDate));
                      SetCurrentDir(lcMappaCreate.Mappa(GDate));
                      //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
                    end
                  else
                    SetCurrentDir(lcMappaCreate.Mappa(GDate));
                    //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
                  if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                    begin
                      DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                      DM.CDSTiszta.LogChanges := false;
                      lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                      RecordAppend(false,lcSzfejID);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                    end
                  else
                    begin
                      //...
                      CreateCDSField(true);
                      RecordAppend(true,0);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                     end;
                end
              else if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '24') then
                begin
                  lcFajl := TfgvUnit.Create();
                  lcFajlNev := lcFajl.FajlNev(GDate+1);
                  lcFajlNev := 'OT' + copy(lcFajlNev,2,lcFajlNev.Length);
                  lcMappaCreate := TfgvUnit.Create();
                  lcUtvonal := GetCurrentDir;
                  SetCurrentDir(lcUtvonal);
                  if not directoryexists(lcMappaCreate.Mappa(GDate+1)) then
                    begin
                      CreateDir(lcMappaCreate.Mappa(GDate+1));
                      SetCurrentDir(lcMappaCreate.Mappa(GDate+1));
                      //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+1);
                    end
                  else
                    SetCurrentDir(lcMappaCreate.Mappa(GDate+1));
                    //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+1);
                  if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                    begin
                      //...
                      DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                      DM.CDSTiszta.LogChanges := false;
                      lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                      RecordAppend(false,lcSzfejID);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                    end
                  else
                    begin
                      //...
                      CreateCDSField(true);
                      RecordAppend(true,0);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                     end;
                  SetCurrentDir(lcUtvonal);
                end
              else if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '48') then
                begin
                  lcFajl := TfgvUnit.Create();
                  lcFajlNev := lcFajl.FajlNev(GDate+2);
                  lcFajlNev := 'OT' + copy(lcFajlNev,2,lcFajlNev.Length);
                  lcMappaCreate := TfgvUnit.Create();
                  lcUtvonal := GetCurrentDir;
                  SetCurrentDir(lcUtvonal);
                  if not directoryexists(lcMappaCreate.Mappa(GDate+2)) then
                    begin
                      CreateDir(lcMappaCreate.Mappa(GDate+2));
                      SetCurrentDir(lcMappaCreate.Mappa(GDate+2));
                      //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+2);
                    end
                  else
                    SetCurrentDir(lcMappaCreate.Mappa(GDate+2));
                    //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+2);
                  if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                    begin
                      //...
                      DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                      DM.CDSTiszta.LogChanges := false;
                      lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                      RecordAppend(false,lcSzfejID);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                    end
                  else
                    begin
                      //...
                      CreateCDSField(true);
                      RecordAppend(true,0);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                     end;
                end
              else if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '72') then
                begin
                  lcFajl := TfgvUnit.Create();
                  lcFajlNev := lcFajl.FajlNev(GDate+3);
                  lcFajlNev := 'OT' + copy(lcFajlNev,2,lcFajlNev.Length);
                  lcMappaCreate := TfgvUnit.Create();
                  lcUtvonal := GetCurrentDir;
                  SetCurrentDir(lcUtvonal);
                  if not directoryexists(lcMappaCreate.Mappa(GDate+3)) then
                    begin
                      CreateDir(lcMappaCreate.Mappa(GDate+3));
                      SetCurrentDir(lcMappaCreate.Mappa(GDate+3));
                    end
                  else
                    SetCurrentDir(lcMappaCreate.Mappa(GDate+3));
                  if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                    begin
                      //...
                      DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                      DM.CDSTiszta.LogChanges := false;
                      lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                      RecordAppend(false,lcSzfejID);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                    end
                  else
                    begin
                      //...
                      CreateCDSField(true);
                      RecordAppend(true,0);
                      lcXmlMent := TfgvUnit.Create();
                      lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                      lcXmlMent.Free;
                      result := true;
                     end;
                end;
              DM.CDSTextilek.Next;
              SetCurrentDir(lcUtvonalMent);
            end;
          DataSet.Next;
        end;
    end
  //a k�rh�zi szint� gener�l�s vege.

  //oszt�ly szint� tiszta gener�l�s.
  else if (not AOSszTiszta) then
  begin
  while not DataSet.Eof do
    begin
      DM.CDSTextilek.First;
      while not DM.CDSTextilek.Eof do
        begin
          if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '0') then
            begin
              lcFajl := TfgvUnit.Create();
//Ellen�rizni kell a tiszta f�jl d�tum�t a naptar.xml-b�l, hogy azon a napon dolozunk-e.

              lcFajlNev := lcFajl.FajlNev(GDate);
              lcFajlNev := 'T' + copy(lcFajlNev,2,lcFajlNev.Length);
              lcMappaCreate := TfgvUnit.Create();
              lcUtvonal := GetCurrentDir;
              SetCurrentDir(lcUtvonal);
              SetCurrentDir(copy(lcUtvonal,1,lcUtvonal.Length-8));
              if not directoryexists(lcMappaCreate.Mappa(GDate)) then
                begin
                  CreateDir(lcMappaCreate.Mappa(GDate));
                  SetCurrentDir(lcMappaCreate.Mappa(GDate));
                  //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
                end
              else
                SetCurrentDir(lcMappaCreate.Mappa(GDate));
                //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate);
              if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                begin
                  DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                  DM.CDSTiszta.LogChanges := false;
                  DM.CDSTiszta.First;
                  lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                  RecordAppend(false,lcSzfejID);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                end
              else
                begin
                  //...
                  CreateCDSField(true);
                  RecordAppend(true,0);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                 end;
            end
          else if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '24') then
            begin
              lcFajl := TfgvUnit.Create();
              lcFajlNev := lcFajl.FajlNev(GDate+1);
              lcFajlNev := 'T' + copy(lcFajlNev,2,lcFajlNev.Length);
              lcMappaCreate := TfgvUnit.Create();
              lcUtvonal := GetCurrentDir;
              SetCurrentDir(lcUtvonal);
              SetCurrentDir(copy(lcUtvonal,1,lcUtvonal.Length-8));
              if not directoryexists(lcMappaCreate.Mappa(GDate+1)) then
                begin
                  CreateDir(lcMappaCreate.Mappa(GDate+1));
                  SetCurrentDir(lcMappaCreate.Mappa(GDate+1));
                  //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+1);
                end
              else
                SetCurrentDir(lcMappaCreate.Mappa(GDate+1));
                //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+1);
              if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                begin
                  //...
                  DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                  DM.CDSTiszta.LogChanges := false;
                  lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                  RecordAppend(false,lcSzfejID);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                end
              else
                begin
                  //...
                  CreateCDSField(true);
                  RecordAppend(true,0);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                 end;
              SetCurrentDir(lcUtvonal);
            end
          else if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '48') then
            begin
              lcFajl := TfgvUnit.Create();
              lcFajlNev := lcFajl.FajlNev(GDate+2);
              lcFajlNev := 'T' + copy(lcFajlNev,2,lcFajlNev.Length);
              lcMappaCreate := TfgvUnit.Create();
              lcUtvonal := GetCurrentDir;
              SetCurrentDir(lcUtvonal);
              SetCurrentDir(copy(lcUtvonal,1,lcUtvonal.Length-8));
              if not directoryexists(lcMappaCreate.Mappa(GDate+2)) then
                begin
                  CreateDir(lcMappaCreate.Mappa(GDate+2));
                  SetCurrentDir(lcMappaCreate.Mappa(GDate+2));
                  //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+2);
                end
              else
                SetCurrentDir(lcMappaCreate.Mappa(GDate+2));
                //prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate+2);
              if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                begin
                  //...
                  DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                  DM.CDSTiszta.LogChanges := false;
                  lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                  RecordAppend(false,lcSzfejID);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                end
              else
                begin
                  //...
                  CreateCDSField(true);
                  RecordAppend(true,0);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                 end;
            end
          else if (DataSet.FieldByName('sztet_textilid').AsInteger = DM.CDSTextilek.FieldByName('kpt_id').AsInteger) and (DM.CDSTextilek.FieldByName('kpt_texora').AsString = '72') then
            begin
              lcFajl := TfgvUnit.Create();
              lcFajlNev := lcFajl.FajlNev(GDate+3);
              lcFajlNev := 'T' + copy(lcFajlNev,2,lcFajlNev.Length);
              lcMappaCreate := TfgvUnit.Create();
              lcUtvonal := GetCurrentDir;
              SetCurrentDir(lcUtvonal);
              SetCurrentDir(copy(lcUtvonal,1,lcUtvonal.Length-8));
              if not directoryexists(lcMappaCreate.Mappa(GDate+3)) then
                begin
                  CreateDir(lcMappaCreate.Mappa(GDate+3));
                  SetCurrentDir(lcMappaCreate.Mappa(GDate+3));
                end
              else
                SetCurrentDir(lcMappaCreate.Mappa(GDate+3));
              if FindFirst(lcFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 then
                begin
                  //...
                  DM.CDSTiszta.LoadFromFile(lcFajlNev+'.xml');
                  DM.CDSTiszta.LogChanges := false;
                  lcSzfejID := DM.CDSTiszta.FieldByName('szfej_id').AsInteger;
                  RecordAppend(false,lcSzfejID);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                end
              else
                begin
                  //...
                  CreateCDSField(true);
                  RecordAppend(true,0);
                  lcXmlMent := TfgvUnit.Create();
                  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcFajlNev+'.xml');
                  lcXmlMent.Free;
                  result := true;
                 end;
            end;
          DM.CDSTextilek.Next;
        end;
      DataSet.Next;
    end;
   end;
  SetCurrentDir(lcUtvonalMent);
end;

function TfgvUnit.GetOsztalyNev(AOsztID: Integer) : String;
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;
begin
//ID alapj�n az oszt�ly nev�t adja vissza.
  result := '';
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
      DM.CDSOsztalyok.LoadFromFile(lcUtvonal+'\osztaly.xml');
      DM.CDSOsztalyok.LogChanges := false;
      DM.CDSOsztalyok.FieldByName('kpo_id').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_partner').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_osztaly').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_kod').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_csoport').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_uzem').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastuser').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastup').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_delete').Visible := false;
      DM.CDSOsztalyok.FieldByName('oszt_nev').DisplayLabel := 'Oszt�ly n�v';
    end;
  DM.CDSOsztalyok.First;
  while not DM.CDSOsztalyok.Eof do
    begin
      if DM.CDSOsztalyok.FieldByName('kpo_id').AsInteger = AOsztID then
        begin
          result := DM.CDSOsztalyok.FieldByName('oszt_nev').AsString;
        end;
      DM.CDSOsztalyok.Next;
    end;
end;

function TfgvUnit.SetOsztalyID(AOsztNev : String) : Integer;
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;
begin
//Osszes�t�s eset�ny a K�rh�z mint oszt�ly ID adja vissza.
  result := 0;
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
      DM.CDSOsztalyok.LoadFromFile(lcUtvonal+'\osztaly.xml');
      DM.CDSOsztalyok.LogChanges := false;
      DM.CDSOsztalyok.FieldByName('kpo_id').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_partner').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_osztaly').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_kod').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_csoport').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_uzem').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastuser').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastup').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_delete').Visible := false;
      DM.CDSOsztalyok.FieldByName('oszt_nev').DisplayLabel := 'Oszt�ly n�v';
    end;
  DM.CDSOsztalyok.First;
  while not DM.CDSOsztalyok.Eof do
    begin
      if DM.CDSOsztalyok.FieldByName('oszt_nev').AsString = 'K�rh�z' then
        begin
          result := DM.CDSOsztalyok.FieldByName('kpo_id').AsInteger;
        end;
      DM.CDSOsztalyok.Next;
    end;
end;

function TfgvUnit.SzFajlEll(APartner, AOsztaly : Integer) : String;
var
  lcFile,lcPath,lcMappa : String;
begin
  lcPath := GetCurrentDir;
  lcMappa := lcPath +'\adat\adat_' + IntToStr(APartner) + '\';
  SetCurrentDir(lcMappa);
  if DirectoryExists(Mappa(GDate)) then
    begin
      lcPath := GetCurrentDir;
      lcMappa := lcPath +'\'+ Mappa(GDate);
      SetCurrentDir(lcMappa);
      lcFile := FajlNev(GDate) +'.xml';
      if FileExists(lcFile) then
        begin
          result := lcFile;
        end
      else
        result := '';
    end
  else
    begin
      result := '';
    end;
  SetCurrentDir(lcPath);
end;

function TfgvUnit.TorzsAdatEll(APartner : Integer; AUtvonal, AMappa : String) : Boolean;
var
  lcPath : String;
begin
  //Ellen�rz�s
  lcPath := GetCurrentDir;
  SetCurrentDir(AUtvonal);
  if DirectoryExists(AMappa) then
    begin
      Result := True;
    end
  else
    begin
      Result := False;
    end;
  SetCurrentDir(lcPath);
end;

function TfgvUnit.FajlOsztalyEll(AOsztalyID : Integer) : String;
var
  lcFile,lcPath,lcMappa : String;
begin
  if DirectoryExists(Mappa(GDate)) then
    begin
      lcPath := GetCurrentDir;
      lcMappa := lcPath +'\'+ Mappa(GDate);
      SetCurrentDir(lcMappa);
      lcFile := FajlNev(GDate)+'.xml';
      if FileExists(lcFile) then
        begin
          result := lcFile;
        end
      else
        result := '';
    end
  else
    begin
      result := '';
    end;
  SetCurrentDir(lcPath);
end;

function TfgvUnit.PartnerMappa(APartner : Integer) : String;
var
  lcS : String;
begin
  lcS := 'adat_'+ IntToStr(APartner);
  result := lcS;
end;

function TfgvUnit.Mappa(ADate : TDate) : String;
var
  lcS : String;
begin
  lcS := DateToStr(ADate);
  lcS := StringReplace(lcS, '.', '',[rfReplaceAll]);
  lcS := copy(lcS,1,8);
  result := lcS;
end;

function TfgvUnit.FajlNev(AFajlDate : TDate) : String;
var
  lcS : String;
begin
  lcS := DateToStr(AFajlDate);
  lcS := StringReplace(lcS, '.', '',[rfReplaceAll]);
  lcS := GOldal + copy(lcS,1,8)+ IntToStr(GOsztalyID);
  result := lcS;
end;

function TfgvUnit.FajlNev_2(AFajlDate : TDate; AOsztID : Integer) : String;
var
  lcS : String;
begin
  lcS := DateToStr(AFajlDate);
  lcS := StringReplace(lcS, '.', '',[rfReplaceAll]);
  if AOsztID <> -1 then
    lcS := GOldal + copy(lcS,1,8)+ IntToStr(AOsztID)
  else
    lcS := GOldal + copy(lcS,1,8)+ IntToStr(GPartner);
  result := lcS;
end;

procedure TfgvUnit.DataSetToXML(DataSet : TDataSet; const FileName:string);
var
  lcUtvonal : String;
  lcMem: TFDMemTable;
begin
   lcUtvonal := GetCurrentDir;
   try
     DataSet.First; //Az�rt kell, hogy ne csak az els� �s az utols� rekordot mentse el.
     //DM.DSPSzallMent.DataSet:=DataSet;
     lcMem := TFDMemTable.Create(Application);
     lcMem.CopyDataSet(DataSet, [coStructure, coRestart, coAppend]);
     DM.DSPSzallMent.DataSet:= TDataSet(lcMem);
     lcUtvonal := GetCurrentDir;
     try
       DataSet.DisableControls;
       try
        if not DataSet.Active then
          DataSet.Active:=True;
        DM.CDSSzallMent.SetProvider(DM.DSPSzallMent);
//        DM.CDSSzallMent.Active:=True;
        DM.CDSSzallMent.Open;
        DM.CDSSzallMent.SaveToFile(FileName, dfXMLUTF8);
       finally
         DataSet.EnableControls;
       end;
     finally
     //  LClient.Free;
     end;
   finally
//     DM.CDSSzallMent.Active := false;
     DM.CDSSzallMent.Close;
     lcMem.Close;
     lcMem.Destroy;
     //LProvider.Free;
   end;
end;

//Kivezetni.....
function TfgvUnit.Mozgasnem : Integer;
  function lcMozgKeres(AParam : String) : Integer;
    begin
      DM.CDSMozg.First;
      while not DM.CDSMozg.Eof do
        begin
          if DM.CDSMozg.FieldByName('moz_kod').AsString = (AParam) then
           begin
             result := DM.CDSMozg.FieldByName('moz_id').AsInteger;
           end;
          DM.CDSMozg.Next;
        end;
    end;
begin
  result := lcMozgKeres(GOldal+GOlTipus);
//  DM.CDSMozg.Active := false;
end;

function TfgvUnit.SzallSorGen : String;
var
  lciniFile : TIniFile;
  lcMozgID, lcDataSetToXML : TfgvUnit;
  lcSorszS, lcSorszT, lcS_0, lcSzam, lcKod, lcIPCim, lcUtvonal, lcMozgNev : String;
  lcSorMent : Boolean;
  lcSorszam, lcID : Integer;
  lcPut : THttpsUnit;
begin
  //SZ-2016-SK-0010-000001
  {SZ v. T - szennyes vagy tiszta sz�ll�t�, �vsz�m, mozg�snem, partner k�d, sorsz�m. }
  if GOldal = 'S' then
    lcS_0 := 'SZ-' + (formatdatetime('yyyy',now())) + '-'
  else if GOldal = 'T' then
    lcS_0 := 'T-' + (formatdatetime('yyyy',now())) + '-';
  lcMozgID := TfgvUnit.Create();
  DM.CDSMozgnem.First;
  while not DM.CDSMozgnem.Eof do
    begin
      if DM.CDSMozgnem.FieldByName('moz_kod').AsString = (GOldal+GOlTipus) then
        begin
          lcS_0 := lcS_0 + DM.CDSMozgnem.FieldByName('moz_kod').AsString + '-';
          break;
        end;
      DM.CDSMozgnem.Next;
    end;
  DM.CDSSzallSorszam.First;
  while not DM.CDSSzallSorszam.Eof do
    begin
      lcKod := stringreplace(lcS_0, '-', '_',[rfReplaceAll]); //DM.CDSSzallSorszam.FieldByName('szam_nev').AsString;
      lcKod := copy(lcKod,1,lcKod.Length-1);
      if (lcKod = DM.CDSSzallSorszam.FieldByName('szam_nev').AsString) AND (GPartner = DM.CDSSzallSorszam.FieldByName('szam_partnerid').AsInteger) AND (GUserID = DM.CDSSzallSorszam.FieldByName('szam_felhasznalo').AsInteger) then
        begin
          lcSorszam := DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger;
          lcID := DM.CDSSzallSorszam.FieldByName('szam_id').AsInteger;
          lcMozgNev := DM.CDSSzallSorszam.FieldByName('szam_nev').AsString;
          break;
        end;
      DM.CDSSzallSorszam.Next;
    end;

  lcSorszS := IntToStr(lcSorszam);
  lcSorszT := IntToStr(lcSorszam);

  DM.CDSSzallSorszam.Filtered := false;
  DM.CDSSzallSorszam.Filter := 'szam_id='+ IntToStr(lcID);
  DM.CDSSzallSorszam.Filtered := true;
  if (DM.CDSSzallSorszam.RecordCount > 0) AND (DM.CDSSzallSorszam.RecordCount < 2) then
    begin
      lcSorszam := StrToInt(lcSorszS);
      DM.CDSSzallSorszam.Edit;
      DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger := StrToInt(lcSorszS) + 1;
      DM.CDSSzallSorszam.Post;
    end;

  if GOldal = 'S' then
    begin
      lcS_0 := lcS_0 + IntToStr(GUserID).PadLeft(4,'0') + '-' + IntToStr(GPartner).PadLeft(4,'0') + '-' + (lcSorszS).PadLeft(6,'0');
      lcSorszam := StrToInt(lcSorszS);
      DM.CDSSzallSorszam.Edit;
      DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger := StrToInt(lcSorszS) + 1;
      DM.CDSSzallSorszam.Post;
    end

  else if GOldal = 'T' then
    begin
      lcS_0 := lcS_0 + IntToStr(GUserID).PadLeft(4,'0') + '-' + IntToStr(GPartner).PadLeft(4,'0') + '-' + (lcSorszT).PadLeft(6,'0');
      lcSorszam := StrToInt(lcSorszT);
      DM.CDSSzallSorszam.Edit;
      DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger := StrToInt(lcSorszT) + 1;
      DM.CDSSzallSorszam.Post;
    end;

  DM.CDSSzallSorszam.Filtered := false;
  DM.CDSSzallSorszam.Filter := '';
  DM.CDSSzallSorszam.Filtered := true;
  lcDataSetToXML := TfgvUnit.Create();
  lcDataSetToXML.DataSetToXML(DM.CDSSzallSorszam, 'sorszam.xml');

  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcUtvonal := lciniFile.ReadString('utvonal','path','');

  try
    lcPut := THttpsUnit.Create();
    if lcPut.PutHttps(lcIPCim, lcUtvonal,'upmozgsorszam/',lcMozgNev,(StrToInt(lcSorszS) + 1),GPartner, GUserID) <> '' then
      begin
//        result := 'Sikeres';
      end;
    except
      on E : Exception do
         begin
//           result := 'hiba';
         end;
    end;
  result := lcS_0;
end;

function TfgvUnit.SzennyesSMent(ASorsz : Integer) : Boolean;
var
  lciniFile : TIniFile;
begin
  //Szennyes sorsz�m ment�se az inibe.
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  lciniFile.WriteString('szallsorsz','szalls',IntToStr(ASorsz));
  lciniFile.Free;
  result := true;
end;

function TfgvUnit.TisztaTMent(ASorsz : Integer) : Boolean;
var
  lciniFile : TIniFile;
begin
  //Tiszta sorsz�m ment�se az inibe.
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  lciniFile.WriteString('szallsort','szallt',IntToStr(ASorsz));
  lciniFile.Free;
  result := true;
end;

{
  Sorsz�m gener�l�s
  Ha AInc �s nem APut, ideiglenesen lok�lisan n�veli a sorsz�mot, k�s�bb el kell k�ldeni Nem AInc-el �s APut-al.
  Ha Nem AInc �s APut a lok�lis f�jl legutols� v�ltozat�t k�ldi el a szervernek.
  Ha AInc �s APut, akkor azonnal v�ltoztat a lok�lis f�jlon �s k�ldi a szerver fel�.
}
function TfgvUnit.SorszamGeneralas(ANev: string; APartner, AFelh: integer; AInc, APut: boolean) : string;
var
  lcSorszamCheck: TClientDataSet;
  lcNextSorszamId: integer;
  lcPut: THttpsUnit;
  lcIniFile : TIniFile;
  lcIPCim, lcUtvonal: string;
begin
  lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcUtvonal := lciniFile.ReadString('utvonal','path','');

  lcSorszamCheck := TClientDataSet.Create(nil);
  lcSorszamCheck.LoadFromFile('sorszam.xml');
  lcSorszamCheck.LogChanges := false;

  lcSorszamCheck.Filtered := false;
  lcSorszamCheck.Filter := 'szam_nev = ''' + ANev + ''' AND szam_partnerid = ' + IntToStr(APartner) + ' AND szam_felhasznalo = ' + IntToStr(AFelh);
  lcSorszamCheck.Filtered := true;
  lcSorszamCheck.First;
  if lcSorszamCheck.RecordCount = 0 then
    begin
      if AInc then
        begin
          lcSorszamCheck.Last;

          lcSorszamCheck.Insert;
          lcSorszamCheck.FieldByName('szam_nev').AsString := ANev;
          lcSorszamCheck.FieldByName('szam_partnerid').AsInteger := APartner;
          lcSorszamCheck.FieldByName('szam_felhasznalo').AsInteger := AFelh;
          lcSorszamCheck.FieldByName('szam_sorszam').AsInteger := 1;
          lcSorszamCheck.Post;
        end
      else
        begin
          result := ANev + '_'+IntToStr(GUserID).PadLeft(4, '0')+'_'+IntToStr(GPartner).PadLeft(4, '0') + '_' + IntToStr(1).PadLeft(6, '1');
          result := StringReplace(result, '_', '-', [rfReplaceAll, rfIgnoreCase]);
          exit;
        end;

    end
  else
    begin
      lcSorszamCheck.Edit;
      if AInc then
        lcSorszamCheck.FieldByName('szam_sorszam').AsInteger := lcSorszamCheck.FieldByName('szam_sorszam').AsInteger+1
      else
        lcSorszamCheck.FieldByName('szam_sorszam').AsInteger := lcSorszamCheck.FieldByName('szam_sorszam').AsInteger;
      lcSorszamCheck.Post;
    end;
  result := ANev + '_'+IntToStr(GUserID).PadLeft(4, '0')+'_'+IntToStr(GPartner).PadLeft(4, '0') +  '_' + IntToStr(lcSorszamCheck.FieldByName('szam_sorszam').AsInteger).PadLeft(6, '0');
  result := StringReplace(result, '_', '-', [rfReplaceAll, rfIgnoreCase]);
  lcNextSorszamId := lcSorszamCheck.FieldByName('szam_sorszam').AsInteger;
  lcSorszamCheck.Filtered := false;
  DataSetToXML(lcSorszamCheck, 'sorszam.xml');
  if APut then
    begin
      try
        lcPut := THttpsUnit.Create();
        if lcPut.PutHttps(lcIPCim, lcUtvonal,'upmozgsorszam/', ANev, lcNextSorszamId, GPartner, GUserID) <> '' then
        begin
    //        result := 'Sikeres';
        end;
        lcPut.Destroy;
      except
        on E : Exception do
           begin
    //           result := 'hiba';
             lcPut.Destroy;
           end;
      end;
    end;

  lcSorszamCheck.Close;
  lcSorszamCheck.Destroy;
  lcIniFile.Destroy;
end;

function TfgvUnit.IsInPufferRaktar(AChipKod: String; ACheckIn: boolean; AWhere: TClientDataSet; AFilter: string; var msgString: string) : Boolean;
begin
  // �j lok�lis f�jlokba bele�r�s elj�r�s miatt v�ltozik.
  result := false;
  // Netr�l leszedett chipk�dok
  DM.CDSChipPuffer.Filtered := false;
  DM.CDSChipPuffer.Filter := 'chipkod = '''+AChipKod+''' AND partnerid='+IntToStr(GPartner)+' AND uzemid='+IntToStr(DM.CDSUzem.FieldByName('uzem_id').AsInteger);
  DM.CDSChipPuffer.Filtered := true;
  (*
  // M�g nem szinkroniz�lt bev�telez�sek
  DM.FDMemTablePufferRaktarSynchs.Filtered := false;
  DM.FDMemTablePufferRaktarSynchs.Filter := 'chip_kod = '''+AChipKod+'''';
  DM.FDMemTablePufferRaktarSynchs.Filtered := true;
  // M�g nem szinkroniz�lt visszav�telez�sek
  DM.FDMemTablePufferVisszavetSynchs.Filtered := false;
  DM.FDMemTablePufferVisszavetSynchs.Filter := 'chip_kod = '''+AChipKod+'''';
  DM.FDMemTablePufferVisszavetSynchs.Filtered := true;
  // Ha ezek egyik�ben sem szerepel akkor nincs a puffer rakt�rban.
  *)
  if (DM.CDSChipPuffer.RecordCount = 0) (*and (DM.FDMemTablePufferRaktarSynchs.RecordCount = 0) and (DM.FDMemTablePufferVisszavetSynchs.RecordCount = 0) *)then
    begin
      DM.CDSChipPuffer.Filtered := false;
      msgString := 'Ezzel a chipk�ddal nincs textil a puffer rakt�rban!';
      exit;
    end
  else
    // Ha szerepelt valamelyikben meg kell n�zni, hogy nincs-e m�g kiadva.
    begin
      (*
      DM.CDSChipPuffer.Filtered := false;
      DM.FDMemTablePufferRaktarSynchs.Filtered := false;
      *)
      // Param�terben �tadhat� DS-t n�zz�k el�sz�r.
      // Ez pl. �sszerendel�sn�l az aktu�lis �sszerendel�st kapja meg param�ternek.
      // Ez a ponton bev�telezettnek veszem, ha valamelyik k�vetkez� ellen�rz�sen megbukik akkor �ll vissza false-ra.
      result := true;
      if (ACheckIn = true) and (assigned(AWhere)) then
        begin
          AWhere.Filtered := false;
          AWhere.Filter := AFilter;
          AWhere.Filtered := true;
          // Ha nem szerepel a param�terben �tadott DS-ben, akkor m�g nem volt kiadva.
          if AWhere.RecordCount <> 0 then
            begin
              result := false;
              msgString := 'A chipk�d m�r r�gz�tve lett ebben az �sszerendel�sben!';
            end;
          AWhere.Filtered := false;
        end;
      (*
      // ... Tov�bbi ellen�rz�sek ... //
      DM.FDMemTablePufferKiadSynchs.Filtered := false;
      DM.FDMemTablePufferKiadSynchs.Filter := 'chip_kod = '''+AChipKod+'''';
      DM.FDMemTablePufferKiadSynchs.Filtered := true;
      if (DM.FDMemTablePufferKiadSynchs.RecordCount > 0) then
        begin
          DM.FDMemTablePufferKiadSynchs.Filtered := false;
          result := false;
          msgString := 'Ez a chip m�r ki lett adva!';
          exit;
        end;
      DM.FDMemTablePufferKiadSynchs.Filtered := false;
      *)
    end;
end;

function TfgvUnit.GetTextilFromPufferRaktarByChipkod(AChipKod: string): TPufferRaktarTextil;
begin
  // Netr�l leszedett chipk�dok
  DM.CDSChipPuffer.Filtered := false;
  DM.CDSChipPuffer.Filter := 'chipkod = '''+AChipKod+'''';
  DM.CDSChipPuffer.Filtered := true;
  if DM.CDSChipPuffer.RecordCount > 0 then
    begin
      DM.CDSChipPuffer.First;
      result.textilid := DM.CDSChipPuffer.FieldByName('textilid').AsInteger;
      result.textilnev := DM.CDSChipPuffer.FieldByName('textilnev').AsString;
      result.textilsuly := DM.CDSChipPuffer.FieldByName('textilsuly').AsFloat;
      result.fajtaid := DM.CDSChipPuffer.FieldByName('fajtaid').AsInteger;
      result.fajtanev := DM.CDSChipPuffer.FieldByName('fajtanev').AsString;
      result.allapotid := DM.CDSChipPuffer.FieldByName('allapotid').AsInteger;
      result.allapotnev := DM.CDSChipPuffer.FieldByName('allapotnev').AsString;
      result.meretid := DM.CDSChipPuffer.FieldByName('meretid').AsInteger;
      result.meretnev := DM.CDSChipPuffer.FieldByName('meretnev').AsString;
      result.magassagid := DM.CDSChipPuffer.FieldByName('magassagid').AsInteger;
      result.magassagnev := DM.CDSChipPuffer.FieldByName('magassagnev').AsString;
      result.szinid := DM.CDSChipPuffer.FieldByName('szinid').AsInteger;
      result.szinnev := DM.CDSChipPuffer.FieldByName('szinnev').AsString;
    end
  else
    result.textilid := -1;
  DM.CDSChipPuffer.Filtered := false;
  DM.CDSChipPuffer.Filter := '';
end;

function TfgvUnit.IsNumber(const S: string): Boolean;
begin
  Result := True;
  try
    StrToInt(S);
  except
    Result := False;
  end;
end;

function TfgvUnit.GetVonalkodAdatokByVonalkod(AVonalkod: string): TVonalkodAdatok;
begin
  Result.Dolgozonev := '';
  Result.Telephelykod := '';
  Result.Szekreny := '';
  Result.Helyszinnev := '';
  DM.CDSChip.IndexFieldNames := 'cms_jelolesdatum';
  DM.CDSChip.Filtered := false;
  DM.CDSChip.Filter := 'cms_vonalkod = '''+AVonalkod+'''';
  DM.CDSChip.Filtered := true;
  DM.CDSChip.First;
  while not DM.CDSChip.Eof do
    begin
      Result.Dolgozonev := DM.CDSChip.FieldByName('cms_dolgozonev').AsString;
      Result.Telephelykod := DM.CDSChip.FieldByName('cms_telephelykod').AsString;
      Result.Szekreny := DM.CDSChip.FieldByName('cms_szekrenynev').AsString;
      Result.Helyszinnev := DM.CDSChip.FieldByName('cms_helyszinnev').AsString;
      DM.CDSChip.Next;
    end;
  DM.CDSChip.Filtered := false;
  DM.CDSChip.Filter := '';
  DM.CDSChip.IndexFieldNames := '';
end;

procedure TfgvUnit.SetPufferRaktar(AMoveDir: string; AChipes, AModifyChipXml: boolean; ADataSet: TClientDataSet);

  function GetTextilNev(id: integer): string;
  begin
    result := '';
    DM.CDSTextilek.Filtered := false;
    DM.CDSTextilek.Filter := 'kpt_id='+IntToStr(id);
    DM.CDSTextilek.Filtered := true;
    DM.CDSTextilek.First;
    result := DM.CDSTextilek.FieldByName('kpt_smegnev').AsString;
    DM.CDSTextilek.Filtered := false;
  end;

  function GetTextilFajta(id: integer): string;
  begin
    result := '';
    DM.CDSTextilFajta.Filtered := false;
    DM.CDSTextilFajta.Filter := 'fajta_id='+IntToStr(id);
    DM.CDSTextilFajta.Filtered := true;
    DM.CDSTextilFajta.First;
    result := DM.CDSTextilFajta.FieldByName('fajta_nev').AsString;
    DM.CDSTextilFajta.Filtered := false;
  end;

  function GetTextilAllapot(id: integer): string;
  begin
    result := '';
    DM.CDSTextilAllapot.Filtered := false;
    DM.CDSTextilAllapot.Filter := 'allapot_id='+IntToStr(id);
    DM.CDSTextilAllapot.Filtered := true;
    DM.CDSTextilAllapot.First;
    result := DM.CDSTextilAllapot.FieldByName('allapot_nev').AsString;
    DM.CDSTextilAllapot.Filtered := false;
  end;

  function GetTextilMeret(id: integer): string;
  begin
    result := '';
    DM.CDSTextilMeret.Filtered := false;
    DM.CDSTextilMeret.Filter := 'meret_id='+IntToStr(id);
    DM.CDSTextilMeret.Filtered := true;
    DM.CDSTextilMeret.First;
    result := DM.CDSTextilMeret.FieldByName('meret_nev').AsString;
    DM.CDSTextilMeret.Filtered := false;
  end;

  function GetTextilMagassag(id: integer): string;
  begin
    result := '';
    DM.CDSTextilMagassag.Filtered := false;
    DM.CDSTextilMagassag.Filter := 'magassag_id='+IntToStr(id);
    DM.CDSTextilMagassag.Filtered := true;
    DM.CDSTextilMagassag.First;
    result := DM.CDSTextilMagassag.FieldByName('magassag_nev').AsString;
    DM.CDSTextilMagassag.Filtered := false;
  end;

  function GetTextilSzin(id, textilid: integer): string;
  begin
    result := '';
    DM.CDSTextilSzinek.Filtered := false;
    DM.CDSTextilSzinek.Filter := 'kpsz_szin_id='+IntToStr(id)+' and kpsz_kpt_id='+IntToStr(textilid);
    DM.CDSTextilSzinek.Filtered := true;
    DM.CDSTextilSzinek.First;
    result := DM.CDSTextilSzinek.FieldByName('szin_nev').AsString;
    DM.CDSTextilSzinek.Filtered := false;
  end;

  function GetTextilSuly(id: integer): double;
  begin
    result := 0;
    DM.CDSTextilek.Filtered := false;
    DM.CDSTextilek.Filter := 'kpt_id='+IntToStr(id);
    DM.CDSTextilek.Filtered := true;
    DM.CDSTextilek.First;
    result := DM.CDSTextilek.FieldByName('kpt_textsuly').AsFloat;
    DM.CDSTextilek.Filtered := false;
  end;

  function FindInDarabosXml: boolean;
  begin
    result := false;
    DM.CDSDarabosPuffer.First;
    while not DM.CDSDarabosPuffer.Eof do
      begin
        if
          (ADataSet.FieldByName('pbfej_uzem_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('uzemid').AsInteger) and
          (ADataSet.FieldByName('pbfej_partner_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('partnerid').AsInteger) and
          (ADataSet.FieldByName('pbtet_textil_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('textilid').AsInteger) and
          (ADataSet.FieldByName('pbtet_fajta_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('fajtaid').AsInteger) and
          (ADataSet.FieldByName('pbtet_allapot_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('allapotid').AsInteger) and
          (ADataSet.FieldByName('pbtet_meret_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('meretid').AsInteger) and
          (ADataSet.FieldByName('pbtet_magassag_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('magassagid').AsInteger) and
          (ADataSet.FieldByName('pbtet_szin_id').AsInteger = DM.CDSDarabosPuffer.FieldByName('szinid').AsInteger)
        then
          begin
            result := true;
            exit;
          end;
        DM.CDSDarabosPuffer.Next;
      end;
  end;

  function FindInChipPuffer: boolean;
  begin
    result := false;
    DM.CDSChipPuffer.First;
    while not DM.CDSChipPuffer.Eof do
      begin
        if
          (DM.CDSChipPuffer.FieldByName('chipkod').AsString = ADataSet.FieldByName('chipkod').AsString) and
          (DM.CDSChipPuffer.FieldByName('uzemid').AsString = ADataSet.FieldByName('uzem_id').AsString) and
          (DM.CDSChipPuffer.FieldByName('partnerid').AsString = ADataSet.FieldByName('partner_id').AsString)
        then
          begin
            result := true;
            exit;
          end;
        DM.CDSChipPuffer.Next;
      end;
  end;

  function GetPartnerNev(id: integer): string;
  begin
    result := '';
    DM.CDSPartnerek.First;
    while not DM.CDSPartnerek.Eof do
      begin
        if DM.CDSPartnerek.FieldByName('par_id').AsInteger = id then
          begin
            result := DM.CDSPartnerek.FieldByName('par_nev').AsString;
            exit;
          end;
        DM.CDSPartnerek.Next;
      end;
  end;


var
  editChipId: integer;
  lastDate: TDateTime;
  lcCurrentPath: string;
  i: integer;
begin
  ADataSet.Filtered := false;
  ADataSet.First;
  while not ADataSet.Eof do
    begin
      if AMoveDir = 'bevet' then
        begin
          if AChipes then
            begin
              (*
                chip-es bev�telez�s, chippuffer.xml
                  PBM - bev�telez�s,
                  PVM - visszav�telez�s
                  ? - rakt�rak k�z�tti mozg�s, bev�telez�s
              *)
              if FindInChipPuffer then
                begin
                  DM.CDSChipPuffer.Edit;
                  DM.CDSChipPuffer.FieldByName('chipkod').AsString := ADataSet.FieldByName('chipkod').AsString;
                  DM.CDSChipPuffer.FieldByName('uzemid').AsInteger := ADataSet.FieldByName('uzem_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('partnerid').AsInteger := ADataSet.FieldByName('partner_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('textilid').AsInteger := ADataSet.FieldByName('textil_kpt_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('textilnev').AsString := GetTextilNev(ADataSet.FieldByName('textil_kpt_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('fajtaid').AsInteger := ADataSet.FieldByName('fajta_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('fajtanev').AsString := GetTextilFajta(ADataSet.FieldByName('fajta_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('allapotid').AsInteger := ADataSet.FieldByName('allapot_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('allapotnev').AsString := GetTextilAllapot(ADataSet.FieldByName('allapot_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('meretid').AsInteger := ADataSet.FieldByName('meret_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('meretnev').AsString := GetTextilMeret(ADataSet.FieldByName('meret_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('magassagid').AsInteger := ADataSet.FieldByName('magassag_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('magassagnev').AsString := GetTextilMagassag(ADataSet.FieldByName('magassag_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('szinid').AsInteger := ADataSet.FieldByName('szin_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('szinnev').AsString := GetTextilSzin(ADataSet.FieldByName('szin_id').AsInteger, ADataSet.FieldByName('textil_kpt_id').AsInteger);
                  DM.CDSChipPuffer.Post;
                end
              else
                begin
                  DM.CDSChipPuffer.Insert;
                  DM.CDSChipPuffer.FieldByName('chipkod').AsString := ADataSet.FieldByName('chipkod').AsString;
                  DM.CDSChipPuffer.FieldByName('partnerid').AsInteger := ADataSet.FieldByName('partner_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('uzemid').AsInteger := ADataSet.FieldByName('uzem_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('osztalyid').AsInteger := GPufferRaktarOsztaly;
                  DM.CDSChipPuffer.FieldByName('textilid').AsInteger := ADataSet.FieldByName('textil_kpt_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('textilnev').AsString := GetTextilNev(ADataSet.FieldByName('textil_kpt_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('fajtaid').AsInteger := ADataSet.FieldByName('fajta_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('fajtanev').AsString := GetTextilFajta(ADataSet.FieldByName('fajta_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('allapotid').AsInteger := ADataSet.FieldByName('allapot_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('allapotnev').AsString := GetTextilAllapot(ADataSet.FieldByName('allapot_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('meretid').AsInteger := ADataSet.FieldByName('meret_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('meretnev').AsString := GetTextilMeret(ADataSet.FieldByName('meret_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('magassagid').AsInteger := ADataSet.FieldByName('magassag_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('magassagnev').AsString := GetTextilMagassag(ADataSet.FieldByName('magassag_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('szinid').AsInteger := ADataSet.FieldByName('szin_id').AsInteger;
                  DM.CDSChipPuffer.FieldByName('szinnev').AsString := GetTextilSzin(ADataSet.FieldByName('szin_id').AsInteger, ADataSet.FieldByName('textil_kpt_id').AsInteger);
                  DM.CDSChipPuffer.FieldByName('textilsuly').AsFloat := GetTextilSuly(ADataSet.FieldByName('textil_kpt_id').AsInteger);
                  DM.CDSChipPuffer.Post;
                end;
            end
          else
            begin
              (*
                db-os bev�telez�s, darabospuffer.xml
                  PB - bev�telez�s, (darabos-chip)
                  ? - rakt�rak k�z�tti mozg�s, bev�telez�s (darabos-chip)
              *)
              if ADataSet.FieldByName('pbtet_darabosdb').AsInteger > 0 then
                begin
                  if FindInDarabosXml then
                  begin
                    DM.CDSDarabosPuffer.Edit;
                    DM.CDSDarabosPuffer.FieldByName('darabosossz').AsInteger := DM.CDSDarabosPuffer.FieldByName('darabosossz').AsInteger + ADataSet.FieldByName('pbtet_darabosdb').AsInteger;
                    DM.CDSDarabosPuffer.Post;
                  end
                else
                  begin
                    DM.CDSDarabosPuffer.Insert;
                    DM.CDSDarabosPuffer.FieldByName('darabosossz').AsInteger := ADataSet.FieldByName('pbtet_darabosdb').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('uzemid').AsInteger := ADataSet.FieldByName('pbfej_uzem_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('partnerid').AsInteger := ADataSet.FieldByName('pbfej_partner_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('partnernev').AsString := GetPartnerNev(ADataSet.FieldByName('pbfej_partner_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('textilid').AsInteger := ADataSet.FieldByName('pbtet_textil_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('textilnev').AsString := GetTextilNev(ADataSet.FieldByName('pbtet_textil_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('fajtaid').AsInteger := ADataSet.FieldByName('pbtet_fajta_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('fajtanev').AsString := GetTextilFajta(ADataSet.FieldByName('pbtet_fajta_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('allapotid').AsInteger := ADataSet.FieldByName('pbtet_allapot_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('allapotnev').AsString := GetTextilAllapot(ADataSet.FieldByName('pbtet_allapot_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('meretid').AsInteger := ADataSet.FieldByName('pbtet_meret_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('meretnev').AsString := GetTextilMeret(ADataSet.FieldByName('pbtet_meret_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('magassagid').AsInteger := ADataSet.FieldByName('pbtet_magassag_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('magassagnev').AsString := GetTextilMagassag(ADataSet.FieldByName('pbtet_magassag_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('szinid').AsInteger := ADataSet.FieldByName('pbtet_szin_id').AsInteger;
                    DM.CDSDarabosPuffer.FieldByName('szinnev').AsString := GetTextilSzin(ADataSet.FieldByName('pbtet_szin_id').AsInteger, ADataSet.FieldByName('pbtet_textil_id').AsInteger);
                    DM.CDSDarabosPuffer.FieldByName('textilsuly').AsFloat := GetTextilSuly(ADataSet.FieldByName('pbtet_textil_id').AsInteger);
                    //DM.CDSDarabosPuffer.FieldByName('id').AsInteger := StrToInt(formatDateTime('YYYYMMDDHHNNSSZZZ', now()));
                    DM.CDSDarabosPuffer.Post;
                    Sleep(1);
                  end;
                end;
            end;
        end
      else if AMoveDir = 'kiad' then
        begin
          if AChipes then
            begin
              (*
                chip-es kiad�s, chippuffer.xml
                  PKM - kiad�s
                  ? - rakt�rak k�z�tti mozg�s, kiad�s
              *)
              if FindInChipPuffer then
                begin
                  DM.CDSChipPuffer.Delete;
                end;
            end
          else
            begin
              (*
                db-os kiad�s, darabospuffer.xml
                  PK - kiad�s
                  ? - rakt�rak k�z�tti mozg�s, kiad�s
              *)
              if FindInDarabosXml then
                begin
                  DM.CDSDarabosPuffer.Edit;
                  DM.CDSDarabosPuffer.FieldByName('darabosossz').AsInteger := DM.CDSDarabosPuffer.FieldByName('darabosossz').AsInteger - ADataSet.FieldByName('pbtet_darabosdb').AsInteger;
                  DM.CDSDarabosPuffer.Post;
                end;
            end;
        end;

      if AModifyChipXml then
        begin
          // M�dos�tani kell a param�tereket a Chip XML-ben is
          lastDate := EncodeDateTime(1900, 01, 01, 0, 0, 0, 0);
          DM.CDSChip.First;
          while not DM.CDSChip.Eof do
            begin
              if
                ((DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime > lastDate) OR (DM.CDSChip.FieldByName('cms_jelolesdatum').IsNull AND (lastDate < EncodeDateTime(1901, 01, 01, 0, 0, 0, 0) ))) and
                (DM.CDSChip.FieldByName('cms_vonalkod').AsString = ADataSet.FieldByName('vonalkod').AsString)
              then
                begin
                  lastDate := DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime;
                  editChipId := DM.CDSChip.FieldByName('cms_id').AsInteger;
                end;
              DM.CDSChip.Next;
            end;

          DM.CDSChip.First;
          while not DM.CDSChip.Eof do
            begin
              if (editChipId = DM.CDSChip.FieldByName('cms_id').AsInteger) then
                begin
                  DM.CDSChip.Edit;
                  DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime := Now();
                  DM.CDSChip.FieldByName('cms_chipkod').AsString := ADataSet.FieldByName('chipkod').AsString;
                  DM.CDSChip.FieldByName('cms_kptid').AsInteger := ADataSet.FieldByName('textil_kpt_id').AsInteger;
                  DM.CDSChip.FieldByName('cms_fajtaid').AsInteger := ADataSet.FieldByName('fajta_id').AsInteger;
                  DM.CDSChip.FieldByName('cms_allapotid').AsInteger := ADataSet.FieldByName('allapot_id').AsInteger;
                  DM.CDSChip.FieldByName('cms_meretid').AsInteger := ADataSet.FieldByName('meret_id').AsInteger;
                  DM.CDSChip.FieldByName('cms_magassagid').AsInteger := ADataSet.FieldByName('magassag_id').AsInteger;
                  DM.CDSChip.FieldByName('cms_szinid').AsInteger := ADataSet.FieldByName('szin_id').AsInteger;
                  DM.CDSChip.Post;
                end;
              DM.CDSChip.Next;
            end;
        end;

      if not AChipes then
        begin
          i := 0;
          DM.CDSDarabosPuffer.First;
          while not DM.CDSDarabosPuffer.Eof do
            begin
              DM.CDSDarabosPuffer.Edit;
              DM.CDSDarabosPuffer.FieldByName('id').AsInteger := i;
              DM.CDSDarabosPuffer.Post;
              DM.CDSDarabosPuffer.Next;
              i := i + 1;
            end;
        end;

      ADataSet.Next;
    end;
  lcCurrentPath := GetCurrentDir;
  SetCurrentDir(GRootPath);
  DataSetToXML(DM.CDSChipPuffer, 'chippuffer.xml');
  DataSetToXML(DM.CDSDarabosPuffer, 'darabospuffer.xml');
  DataSetToXML(DM.CDSChip, 'chip.xml');
  SetCurrentDir(lcCurrentPath);
end;

function TfgvUnit.GetVonalkodByChipkod(AChipKod:string): string;
var lcDateTime: TDateTime;
begin
  result := '';
  DM.CDSChip.First;
  lcDateTime := EncodeDateTime(1900, 01, 01, 0, 0, 0, 0);
  while not DM.CDSChip.Eof do
    begin
      if AChipKod = DM.CDSChip.FieldByName('cms_chipkod').AsString then
        begin
          if lcDateTime < DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime then
            begin
              lcDateTime := DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime;
              result := DM.CDSChip.FieldByName('cms_vonalkod').AsString;
            end;
        end;
      DM.CDSChip.Next;
    end;
end;

function TfgvUnit.GetMeretCsoportByTextilId(textil_id: integer; isSimple: boolean): integer;
begin
  if not isSimple then
    begin
      result := -1;
      DM.CDSTextilek.Filtered := false;
      DM.CDSTextilek.First;
      while not DM.CDSTextilek.Eof do
        begin
          if DM.CDSTextilek.FieldByName('kpt_id').AsInteger = textil_id then
            result := DM.CDSTextilek.FieldByName('kpt_meretcsoport').AsInteger;
          DM.CDSTextilek.Next;
        end;
    end
  else if (Assigned(frmPufferVisszavetel)) then
    begin
      result := -1;
      frmPufferVisszavetel.pubCDSMegnevezes.Filtered := false;
      frmPufferVisszavetel.pubCDSMegnevezes.First;
      while not frmPufferVisszavetel.pubCDSMegnevezes.Eof do
        begin
          if frmPufferVisszavetel.pubCDSMegnevezes.FieldByName('kpt_id').AsInteger = textil_id then
            result := frmPufferVisszavetel.pubCDSMegnevezes.FieldByName('kpt_meretcsoport').AsInteger;
          frmPufferVisszavetel.pubCDSMegnevezes.Next;
        end;
    end;

end;

end.
