unit uOsszerendel;

interface


uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, INIFiles, System.WideStrUtils, Data.DB,
  Vcl.Grids, Vcl.DBGrids, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, System.UITypes;

type
  TDataMars = class
  private
  public
    procedure open_com(Port,Param:string);
    procedure close_com;
    procedure Purge;
  end;

  TfrmOsszerendel = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    btnBezar: TButton;
    ilGombok: TImageList;
    gbVonalOlv: TGroupBox;
    gbChipOlv: TGroupBox;
    btnChipStart: TButton;
    btnChipEnd: TButton;
    tOlvasas: TTimer;
    Label1: TLabel;
    eChipKod: TEdit;
    eVonalkod: TEdit;
    btnOsszerendel: TButton;
    gbOsszLista: TGroupBox;
    dbgOsszerendel: TDBGrid;
    IdFTP1: TIdFTP;
    pMessage: TPanel;
    GroupBox1: TGroupBox;
    lUzenet: TLabel;
    Button1: TButton;
    gbValPartner: TGroupBox;
    lPartnernev: TLabel;    procedure btnBezarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure tOlvasasTimer(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOsszerendelClick(Sender: TObject);
    procedure eVonalkodKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    prUtvonal, prUtMent : String;
  public
    { Public declarations }
    portszam, portertek : String;
    procedure PanelShow(AShow : Boolean; ASzoveg : String);
  end;

var
  frmOsszerendel: TfrmOsszerendel;
  fileid: Thandle;
  DataM : TDataMars;
  atveteliranya : Char;
  xtime : String;
  pubbeolvkod : String[24];

implementation
uses uDM, uFgv, uHttps;
{$R *.dfm}

procedure TDataMars.open_com(Port,Param:string);
var
  Device,kiir : string;
  m : Dword;
  i : integer;
  kiirc : char;
  aDCB : DCB;
  CommTimeOuts : TCOMMTIMEOUTS;
begin
  Device:=format('\.\\%s', [Port]);
  fileid := CreateFile(PChar(Device), GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  If fileid = INVALID_HANDLE_VALUE then
    raise Exception.Create('TDataMars: CreateFile failed');
  SetupComm(fileid,1024,1024);  //fileid,1024,1024
  FillChar(aDCB,sizeof(DCB),0);
  Device:=format('%s:%s',[Port,Param]);
  BuildCommDCB(PChar(Device),aDCB);
  SetCommState(fileid,aDCB);
  FillChar(CommTimeOuts, sizeof(TCOMMTIMEOUTS), 0);
//  CommTimeouts.ReadIntervalTimeout:=1;     //en�lk�l is megy
  CommTimeouts.ReadTotalTimeoutMultiplier:=1;
  SetCommTimeouts(fileid, CommTimeOuts);
    //soros portra �r�s
  m:=0; //i:=0;
//  kiir :='*d'+Chr(10)+chr(13);
  for i := 1 to Length(kiir) do
    begin
      kiirc:=kiir[i];
      WriteFile(fileid,kiirc,sizeof(kiirc),m,nil);
    end;
end;

procedure TDataMars.close_com;
begin
  CloseHandle(fileid);
end;

procedure TDataMars.Purge;   // reset
begin
  PurgeComm(fileid,PURGE_TXCLEAR or PURGE_RXCLEAR);
end;

procedure TfrmOsszerendel.PanelShow(AShow : Boolean; ASzoveg: String);
begin
  if AShow then
    begin
      pMessage.Top:= (dbgOsszerendel.Height div 2) - (pMessage.Height div 2);
      pMessage.Left:= (dbgOsszerendel.Width div 2) - (pMessage.Width div 2);
      lUzenet.Caption := ASzoveg;
    end;
  pMessage.Visible := AShow;
end;

procedure TfrmOsszerendel.btnBezarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmOsszerendel.eVonalkodKeyPress(Sender: TObject; var Key: Char);
var
  lcVonalkod, lcChipKod, lcEmptyVonalkod, lcDolgozoNev, lcCikknev : TfgvUnit;
  lcVon, lcChip : Boolean;
begin
  lcVonalkod := TfgvUnit.Create;
  lcChipKod := TfgvUnit.Create;
  lcEmptyVonalkod := TfgvUnit.Create;
  lcDolgozoNev := TfgvUnit.Create;
  if Ord(Key) = VK_RETURN then
    begin
      lcVon := lcVonalkod.GetVonalkod(eVonalkod.Text);
      if (not lcVon) then
        begin
          DM.cdsOsszerendel.Append;
          DM.cdsOsszerendel.FieldByName('Chipkod').AsString := eChipKod.Text;
          DM.cdsOsszerendel.FieldByName('Vonalkod').AsString := eVonalkod.Text;
          DM.cdsOsszerendel.FieldByName('Datum').AsDateTime := now();
          DM.cdsOsszerendel.FieldByName('Dolgozonev').AsString := lcDolgozoNev.GetDolgozonev(eVonalkod.Text);
          DM.cdsOsszerendel.FieldByName('cikknev').AsString := lcDolgozoNev.GetCikknev(eVonalkod.Text);
          DM.cdsOsszerendel.FieldByName('Felhasznalo').AsInteger := GUserID;
          DM.cdsOsszerendel.Post;
        end
      else if not lcEmptyVonalkod.EmptyVonalkod(eVonalkod.Text) then
        begin
          MessageBox(handle,'Nincs ilyen vonalk�d az adatb�zisban!', 'Figyelmeztet�s!', MB_ICONWARNING);
          eVonalkod.Clear;
          eChipKod.Clear;
          exit;
        end
      else if (lcVonalkod.GetVonalkod(eVonalkod.Text)) then
        begin
//          MessageBox(handle,'Ehhez a vonalk�dhoz m�r rendeltek chipk�dot!', 'Hibajelz�s!', MB_ICONERROR);
          case MessageDlg('Ehhez a vonalk�dhoz m�r rendeltek chipk�dot! Biztos folytatja?', mtConfirmation, [mbYes, mbNo], 0) of
            mrNo:
              begin
                Abort;
              end;
            mrYes:
              begin
                DM.cdsOsszerendel.Append;
                DM.cdsOsszerendel.FieldByName('Chipkod').AsString := eChipKod.Text;
                DM.cdsOsszerendel.FieldByName('Vonalkod').AsString := eVonalkod.Text;
                DM.cdsOsszerendel.FieldByName('Datum').AsDateTime := now();
                DM.cdsOsszerendel.FieldByName('Dolgozonev').AsString := lcDolgozoNev.GetDolgozonev(eVonalkod.Text);
                DM.cdsOsszerendel.FieldByName('cikknev').AsString := lcDolgozoNev.GetCikknev(eVonalkod.Text);
                DM.cdsOsszerendel.FieldByName('Felhasznalo').AsInteger := GUserID;
                DM.cdsOsszerendel.Post;
              end;
          end;
        end;
      eVonalkod.Clear;
      eChipKod.Clear;
    end;
  gbOsszLista.Caption := '�sszerendelt lista';
  gbOsszLista.Caption := gbOsszLista.Caption + ' - darab: ' + IntToStr(DM.cdsOsszerendel.RecordCount);
end;

procedure TfrmOsszerendel.btnChipEndClick(Sender: TObject);
var jo : boolean;
begin
  case MessageDlg('Van m�g sz�moland� textil? ', mtConfirmation, [mbYes, mbNo], 0) of
      mrYes:
        begin
          jo:=False;
        end;
      mrNo:
        begin
          jo:=True;
        end;
  end;
  if jo then
    begin   {b}
      case MessageDlg('Biztos vagy benne?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            jo:=True;
          end;
        mrNo:
          begin
            jo:=False;
          end;
      end;
      if jo then
        begin  {c}
          tOlvasas.Enabled:=False;
          DataM.close_com;
          DataM.Purge;
          xtime:=TimeToStr(time);
          eChipKod.Clear;
          //chip_lerak;
          btnChipStart.Enabled:=True;
          btnChipEnd.Enabled := False;
          btnOsszerendel.Enabled := true;
        end;  {c}
    end;{b}
end;

procedure TfrmOsszerendel.btnChipStartClick(Sender: TObject);
begin
  if atveteliranya='K' then
    begin
      xtime:=TimeToStr(time);
    end;
  DataM.Purge ;
  DataM.open_com(portszam,portertek);   //('COM1,'19200,N,8,1')
  tOlvasas.Enabled:=True;
  btnChipStart.Enabled:=False;
  btnChipEnd.Enabled := true;
  btnOsszerendel.Enabled := false;
  eVonalkod.SetFocus;
end;

procedure TfrmOsszerendel.btnOsszerendelClick(Sender: TObject);
procedure RecordAppend(ASzallGen:Boolean;AFajlNev:String);
   begin
    DM.CDSTiszta.Append;
    DM.CDSTiszta.FieldByName('Chipkod').AsString := DM.cdsOsszerendel.FieldByName('Chipkod').AsString;
    DM.CDSTiszta.FieldByName('Vonalkod').AsString := DM.cdsOsszerendel.FieldByName('Vonalkod').AsString;
    DM.CDSTiszta.FieldByName('Datum').AsString := DM.cdsOsszerendel.FieldByName('Datum').AsString;
    DM.CDSTiszta.FieldByName('Uzem').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
    DM.CDSTiszta.FieldByName('Felhasznalo').AsInteger := DM.cdsOsszerendel.FieldByName('Felhasznalo').AsInteger;
    DM.CDSTiszta.FieldByName('Fajlnev').AsString := AFajlNev;
    DM.CDSTiszta.Post;
  end;

var
  lcFajlLista : TStringList;
 // I : Integer;
  lcUtvonal, lcMappa, lcMentFajlNev, lcIdopont, lcS, lcS_0, lcS_1, lcIPCim, lcIniUtvonal : String;
  lcEllenorzes, lcFajl, lcXmlMent : TfgvUnit;
//  lcSearchResult : TSearchRec;
  nw: TNetResource;
  errCode : DWord;
  lcXmlPost : THttpsUnit;
  lciniFile : TIniFile;
begin
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
    end;

  lcFajl := TfgvUnit.Create();
  GDate := now();
  lcMentFajlNev := lcFajl.FajlNev(GDate);
  lcS :=(formatDateTime('YYYY.MM.DD. HH:NN:SS',now()));

  //Ment�s...
  lcS_0 := 'ossze_' + copy(lcMentFajlNev,1,lcMentFajlNev.Length-1);
  lcMentFajlNev := lcS_0;


  lcS_1 := lcS_0;
  if lcS.Length = 20 then
    begin
      lcIdopont := lcIdopont + copy(lcS,13,2);
      lcIdopont := lcIdopont + copy(lcS,16,2);
      lcIdopont := lcIdopont + copy(lcS,19,2);
      lcS_0 := copy(lcS_0,1,lcS_0.Length) + lcIdopont + '.xml';
    end;

  PanelShow(true,'K�rem v�rjon...');
  Application.ProcessMessages;

  DM.cdsOsszerendel.First;
  DM.CDSTiszta.FieldDefs.Clear;
  DM.CDSTiszta.Active := false;
  DM.CDSTiszta.FieldDefs.Add('Chipkod',ftString,24);
  DM.CDSTiszta.FieldDefs.Add('Vonalkod',ftString,24);
  DM.CDSTiszta.FieldDefs.Add('Datum',ftDateTime);
  DM.CDSTiszta.FieldDefs.Add('Uzem',ftInteger);
  DM.CDSTiszta.FieldDefs.Add('Felhasznalo',ftInteger);
  DM.CDSTiszta.FieldDefs.Add('Fajlnev',ftString,45);
  DM.CDSTiszta.CreateDataSet;

  while not DM.cdsOsszerendel.Eof do
    begin
      RecordAppend(false,lcS_0);
      DM.cdsOsszerendel.Next;
    end;


  lcXmlMent := TfgvUnit.Create();
  lcXmlMent.DataSetToXML(DM.CDSTiszta, lcUtvonal + '\' +lcS_0);
  lcXmlMent.Free;
  DM.CDSTiszta.Active := false;


  lcS_1 := lcUtvonal + '\' + lcS_0;
{  try
    if not IdFTP1.Connected then
       begin
         lcFajlLista := TStringList.Create;
         IdFTP1.Port := 21;
         IdFTP1.Connect;
         if IdFTP1.Connected then
           begin
             IdFTP1.ChangeDir('rfidbtx/mav/ossze');
             IdFTP1.Put(lcS_1, lcS_0, false);
             IdFTP1.List(lcFajlLista,'',false);
//             IdFTP1.Get('tmp_20180815.xml', 'c:\development\DXE_10_2\tmr\Win32\Debug\torzs\torzs_12\tmp_20180815.xml', true, true);
             IdFTP1.Disconnect;
           end;
       end;
  except
    on E:Exception do
       begin
         MessageDlg('connection error!', mtError, [mbOK], 0);
       end;
   end;  }

  DM.cdsOsszerendel.First;
  while not DM.cdsOsszerendel.Eof do
    begin
      DM.CDSChip.First;
      while not DM.CDSChip.Eof do
        begin
          if DM.cdsOsszerendel.FieldByName('vonalkod').AsString = DM.CDSChip.FieldByName('cms_vonalkod').AsString then
            begin
              DM.CDSChip.Edit;
              DM.CDSChip.FieldByName('cms_chipkod').AsString := DM.cdsOsszerendel.FieldByName('chipkod').AsString;
              DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime := DM.cdsOsszerendel.FieldByName('Datum').AsDateTime;
              DM.CDSChip.Post;
            end;
          DM.CDSChip.Next;
        end;
      DM.cdsOsszerendel.Next;
    end;

//�sszerendel�s k�ld�s kezd�s...
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcIniUtvonal := lciniFile.ReadString('utvonal','path','');

  try
    lcXmlPost := THttpsUnit.Create();
    if lcXmlPost.PostHttps(lcS_1, lcIPCim, lcIniUtvonal,'osszerendel/' ) <> '' then
      MessageBox(handle,'Sikeres k�ld�s!', 'T�j�koztat�s!', MB_ICONINFORMATION)
    else
      MessageBox(handle,'Sikertelen k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
  except
    on E : Exception do
      begin
        MessageBox(handle,'Sikertelen k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
      end;
  end;
//�sszerendel�s k�ld�s v�ge...


  lcXmlMent := TfgvUnit.Create();
  lcUtvonal := GetCurrentDir;
  lcXmlMent.DataSetToXML(DM.CDSChip, lcUtvonal + '\chip.xml');
  lcXmlMent.Free;
  PanelShow(false,'');
  Application.ProcessMessages;
  MessageBox(handle, 'K�sz!', 'T�j�koztat�s!', MB_ICONINFORMATION);
  DM.cdsOsszerendel.EmptyDataSet;
  eVonalkod.SetFocus;
end;

procedure TfrmOsszerendel.Button1Click(Sender: TObject);
begin
//t�rl�s a cliensdatasetb�l.
{  DM.cdsOsszerendel.Filtered := false;
  DM.cdsOsszerendel.Filter := 'Vonalkod=''' + dbgOsszerendel.DataSource.DataSet.FieldByName('Vonalkod').AsString + '''';
  DM.cdsOsszerendel.Filtered := true;
  DM.cdsOsszerendel.Edit;
  if DM.cdsOsszerendel.RecordCount > 0 then
    DM.cdsOsszerendel.Delete;
  DM.cdsOsszerendel.Filtered := false;
  DM.cdsOsszerendel.Filter := '';
  DM.cdsOsszerendel.Filtered := true;  }

  if DM.cdsOsszerendel.Locate('Vonalkod',dbgOsszerendel.DataSource.DataSet.FieldByName('Vonalkod').AsString,[]) = true then
    begin
      DM.cdsOsszerendel.Delete;
    end;


{  DM.cdsOsszerendel.First;
  while not DM.cdsOsszerendel.Eof do
    begin
      if DM.cdsOsszerendel.FieldByName('Vonalkod').AsString = dbgOsszerendel.DataSource.DataSet.FieldByName('Vonalkod').AsString then
        begin
          DM.cdsOsszerendel.Delete;
          exit;
        end;
      DM.cdsOsszerendel.Next;
    end;}
  eVonalkod.SetFocus;
end;

procedure TfrmOsszerendel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetCurrentDir(prUtMent);
  Action := caFree;
end;

procedure TfrmOsszerendel.FormCreate(Sender: TObject);
var
  iniFile : TiniFile;
 // beallitasfile : TextFile;
  //portszam,portertek : string;
begin
  // Be�ll�t�sok kiolvals�sa a sz�vegf�jlb�l
  //men�ben a szennyes vagy tiszt�t letiltani
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  //  par:=IniFile.ReadString('kliens','Par',par);
  //  jel:=IniFile.ReadString('kliens','Jel',jel);
  portszam:=IniFile.ReadString('port','szam',portszam);
  portertek:=IniFile.ReadString('port','ertek',portertek);
  //  Label4.Caption:=IntToStr(IniFile.ReadInteger('kliens','chipossz',0));
  IniFile.Free;
  GOldal := '';
end;

procedure TfrmOsszerendel.FormShow(Sender: TObject);
begin
  eVonalkod.SetFocus;
  btnChipEnd.Enabled := false;
  btnOsszerendel.Enabled := false;
  DM.cdsOsszerendel.FieldDefs.Clear;
  DM.cdsOsszerendel.Active := false;
  DM.cdsOsszerendel.FieldDefs.Add('Vonalkod',ftString,24);
  DM.cdsOsszerendel.FieldDefs.Add('Chipkod',ftString,24);
  DM.cdsOsszerendel.FieldDefs.Add('Dolgozonev',ftString,100);
  DM.cdsOsszerendel.FieldDefs.Add('cikknev',ftString,30);
  DM.cdsOsszerendel.FieldDefs.Add('Datum',ftDateTime);
  DM.cdsOsszerendel.FieldDefs.Add('Felhasznalo',ftInteger);
  DM.cdsOsszerendel.FieldDefs.Add('Fajlnev',ftString,45);
  DM.cdsOsszerendel.FieldDefs.Items[0].Size := 15;
  DM.cdsOsszerendel.FieldDefs.Items[1].Size := 20;
  DM.cdsOsszerendel.FieldDefs.Items[2].Size := 30;
  DM.cdsOsszerendel.FieldDefs.Items[3].Size := 30;
  DM.cdsOsszerendel.CreateDataSet;
  DM.cdsOsszerendel.FieldByName('Chipkod').DisplayLabel := 'Chipk�d';
  DM.cdsOsszerendel.FieldByName('Vonalkod').DisplayLabel := 'Vonalk�d';
  DM.cdsOsszerendel.FieldByName('Datum').DisplayLabel := 'D�tum';
  DM.cdsOsszerendel.FieldByName('Dolgozonev').DisplayLabel := 'Dolgoz�n�v';
  DM.cdsOsszerendel.FieldByName('cikknev').DisplayLabel := 'Cikkn�v';
  DM.cdsOsszerendel.FieldByName('Felhasznalo').Visible := false;
  DM.cdsOsszerendel.FieldByName('Fajlnev').Visible := false;

  lPartnernev.Caption := '';
  DM.CDSPartnerek.First;
  while not DM.CDSPartnerek.Eof do
    begin
      if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
        begin
          lPartnernev.Caption := DM.CDSPartnerek.FieldByName('par_nev').AsString;
          exit;
        end;
      DM.CDSPartnerek.Next;
    end;
end;

procedure TfrmOsszerendel.tOlvasasTimer(Sender: TObject);
var
  m : DWORD;
  beolv : char;  //beolv1

begin
  m:= 0; beolv:=chr(42);   //hib�s beolvas�sok miatt
  tOlvasas.Enabled:=False;
  ReadFile(fileid, beolv, sizeof(1), m, nil);
  Application.ProcessMessages;
   if (beolv=Chr(10)) or (beolv=Chr(13)) then pubbeolvkod:='';  //ha entert olvas be null�z
  if (beolv<>'') and (beolv<>Chr(10)) and (beolv<>Chr(13)) and (beolv<>chr(42)) then
  begin
    Label1.Caption :=Label1.Caption + beolv;
    pubbeolvkod:=pubbeolvkod+beolv;
     if Length(pubbeolvkod)=16 then
    begin
      eChipKod.Text := pubbeolvkod;
      pubbeolvkod:='';
      Label1.Caption := '';
    end;
  end;
  tOlvasas.Enabled:=True;
end;

end.
