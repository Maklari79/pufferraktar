unit uPufferRaktarBevetelezes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ComCtrls, System.ImageList, Vcl.ImgList,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.Provider, Datasnap.DBClient, Vcl.Menus, System.DateUtils, System.UITypes;

type
  TfrmPufferRaktarBevetelezes = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    gbDatum: TGroupBox;
    mcDatum: TMonthCalendar;
    btnAdatok: TButton;
    btnVissza: TButton;
    ilSzennyes: TImageList;
    gbValPartner: TGroupBox;
    lValPartner: TLabel;
    lsInfo: TLabel;
    lSDatumVal: TLabel;
    gbSelectGrid: TGroupBox;
    dbGrid: TDBGrid;
    btnValtas: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnVisszaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mcDatumDblClick(Sender: TObject);
    procedure btnAdatokClick(Sender: TObject);
    procedure btnValtasClick(Sender: TObject);
    procedure dbGridDblClick(Sender: TObject);
    procedure dbGridCellClick(Column: TColumn);
  private
    { Private declarations }
    prUtvonal, prUtMent : String;
    prId, prFeladoUzem: integer;
  public
    { Public declarations }
  end;

var
  frmPufferRaktarBevetelezes: TfrmPufferRaktarBevetelezes;

implementation

uses uDM, uTMRMain, uFgv, uLogs, uPufferRaktarBevetelezesParameterek, uPufferKiadOsszerendel, uPufferVisszavetelezes,
      uPufferAttarolasKiadas, uPufferAttarolasBevet;
{$R *.dfm}

procedure TfrmPufferRaktarBevetelezes.btnAdatokClick(Sender: TObject);
var
  lcS : TfgvUnit;
begin
  // Majd ide kell berakni a f�jlnevet - gondolom
  //GTFajlNev := 'S'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
  if GSelectedForm = 'bevet' then
    begin
      Application.CreateForm(TfrmPufferRaktarBevetelezesParameterek, frmPufferRaktarBevetelezesParameterek);
      frmPufferRaktarBevetelezesParameterek.ShowModal();
    end
  else if GSelectedForm = 'kiad' then
    begin
      Application.CreateForm(TFrmPufferKiadOsszerendel, frmPufferKiadOsszerendel);
      frmPufferKiadOsszerendel.ShowModal();
    end
  else if GSelectedForm = 'visszavet' then
    begin
      Application.CreateForm(TfrmPufferVisszavetel, frmPufferVisszavetel);
      frmPufferVisszavetel.ShowModal();
    end
  else if GSelectedForm = 'attarolas' then
    begin
      if lValPartner.Caption = '' then
        begin
          if btnValtas.Tag = 0 then
            showMessage('V�lasszon c�l �zemet!')
          else
            showMessage('V�lasszon bizonylatot!');
          exit;
        end;
      if btnValtas.Tag = 0 then
        begin
          // Kiad�s
          Application.CreateForm(TfrmPufferAttarolasKiadas, frmPufferAttarolasKiadas);
          frmPufferAttarolasKiadas.lInfoString.Caption := 'Puffer rakt�r: '+ lValPartner.Caption + #13#10 + 'D�tum: ' + DateToStr(GDate);
          frmPufferAttarolasKiadas.pubCelUzem := prId;
          frmPufferAttarolasKiadas.ShowModal();
        end
      else
        begin
          // bev�telez�s
          Application.CreateForm(TfrmPufferAttarolasBevet, frmPufferAttarolasBevet);
          frmPufferAttarolasBevet.lInfoString.Caption := 'Bizonylat: '+ lValPartner.Caption + #13#10 + 'D�tum: ' + DateToStr(GDate);
          frmPufferAttarolasBevet.pubSzfejId := prId;
          frmPufferAttarolasBevet.pubEllenSorszam := lValPartner.Caption;
          frmPufferAttarolasBevet.pubFeladoUzem := prFeladoUzem;
          frmPufferAttarolasBevet.ShowModal();
        end;
    end;
  Close;
end;

procedure TfrmPufferRaktarBevetelezes.btnValtasClick(Sender: TObject);
begin
  if btnValtas.Tag = 0 then
    begin
      DM.CDSPKKSzamok.Filtered := false;
      DM.CDSPKKSzamok.Filter := 'prszfej_partner_id='+IntToStr(GPartner);
      DM.CDSPKKSzamok.Filtered := true;
      dbGrid.DataSource := DM.DSPKKSzamok;
      dbGrid.Columns.Clear;
      dbGrid.Columns.Add;
      dbGrid.Columns[0].FieldName := 'prszfej_id';
      dbGrid.Columns[0].Title.Caption := 'prszfej_id';
      dbGrid.Columns[0].Visible := false;
      dbGrid.Columns.Add;
      dbGrid.Columns[1].FieldName := 'uzem_nev';
      dbGrid.Columns[1].Title.Caption := 'Felad�';
      dbGrid.Columns[1].Width := 100;
      dbGrid.Columns.Add;
      dbGrid.Columns[2].FieldName := 'prszfej_szallszam';
      dbGrid.Columns[2].Title.Caption := 'Bizonylatsz�m';
      dbGrid.Columns[2].Width := 250;
      dbGrid.Columns.Add;
      dbGrid.Columns[3].FieldName := 'prszfej_uzem_id';
      dbGrid.Columns[3].Visible := false;
      btnValtas.Tag := 1;
      btnValtas.Caption := 'V�lt�s kiad�sra';
      gbSelectGrid.Caption := 'Be�rkez� bizonylatok';
      btnValtas.ImageIndex := 4;
      lValPartner.Caption := '';
    end
  else
    begin
      DM.CDSUzemPartner.Filtered := false;
      DM.CDSUzemPartner.Filter := 'uzem_id <> '+DM.CDSUzem.FieldByName('uzem_id').AsString +' AND partner_id='+IntToStr(GPartner);
      DM.CDSUzemPartner.Filtered := true;
      dbGrid.DataSource := DM.DSUzemPartner;
      dbGrid.Columns.Clear;
      dbGrid.Columns.Add;
      dbGrid.Columns[0].FieldName := 'uzem_nev';
      dbGrid.Columns[0].Title.Caption := 'Puffer rakt�r';
      dbGrid.Columns.Add;
      dbGrid.Columns[1].FieldName := 'partner_id';
      dbGrid.Columns[1].Visible := false;
      btnValtas.Tag := 0;
      btnValtas.Caption := 'V�lt�s bev�telez�sre';
      gbSelectGrid.Caption := 'Puffer rakt�rak';
      btnValtas.ImageIndex := 3;
      lValPartner.Caption := '';
    end;
end;

procedure TfrmPufferRaktarBevetelezes.btnVisszaClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmPufferRaktarBevetelezes.dbGridCellClick(Column: TColumn);
begin
  dbGrid.SelectedRows.CurrentRowSelected := True;
end;

procedure TfrmPufferRaktarBevetelezes.dbGridDblClick(Sender: TObject);
begin
  if dbGrid.SelectedRows.Count > 0 then
    begin
      if btnValtas.Tag = 0 then
        begin
          lValPartner.Caption := DM.CDSUzemPartner.FieldByName('uzem_nev').AsString;
          prId := DM.CDSUzemPartner.FieldByName('uzem_id').AsInteger;
        end
      else
        begin
          lValPartner.Caption := DM.CDSPKKSzamok.FieldByName('prszfej_szallszam').AsString;
          prId := DM.CDSPKKSzamok.FieldByName('prszfej_id').AsInteger;
          prFeladoUzem := DM.CDSPKKSzamok.FieldByName('prszfej_uzem_id').AsInteger;
        end;
    end
end;

procedure TfrmPufferRaktarBevetelezes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmPufferRaktarBevetelezes.FormShow(Sender: TObject);
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;
  lcLogs : TlogsUnit;
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;
  DM.CDSChip.Filtered := false;
  DM.CDSChip.Filter := '';
  DM.CDSChip.Filtered := true;
  btnValtas.Tag := 0;
  width := gbDatum.Width+30;
  if GSelectedForm = 'bevet' then
    begin
      Caption := 'Pufferrakt�r bev�telez�s';
    end
  else if GSelectedForm = 'kiad' then
    begin
      Caption := 'Pufferrakt�r kiad�s';
    end
  else if GSelectedForm = 'visszavet' then
    begin
      Caption := 'Pufferrakt�r visszav�telez�s';
    end
  else if GSelectedForm = 'attarolas' then
    begin
      Caption := 'Pufferrakt�rak k�z�tti �tt�rol�s';
      width := gbDatum.Width+gbSelectGrid.Width+30;
      gbSelectGrid.Visible := true;
      btnValtas.Visible := true;
      DM.CDSUzemPartner.Filtered := false;
      DM.CDSUzemPartner.Filter := 'uzem_id <> '+DM.CDSUzem.FieldByName('uzem_id').AsString +' AND partner_id='+IntToStr(GPartner);
      DM.CDSUzemPartner.Filtered := true;
      dbGrid.DataSource := DM.DSUzemPartner;
      dbGrid.Columns.Clear;
      dbGrid.Columns.Add;
      dbGrid.Columns[0].FieldName := 'uzem_nev';
      dbGrid.Columns[0].Title.Caption := 'Puffer rakt�r';
      dbGrid.Columns.Add;
      dbGrid.Columns[1].FieldName := 'partner_id';
      dbGrid.Columns[1].Visible := false;
    end;
  lcLogs := TlogsUnit.Create();
  lcLogs.LogFajlWrite(' - Program - frmPufferRaktarBevetelezes megnyit�sa! ');
  lcLogs.Free;
  mcDatum.Date := now();
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
      DM.CDSNaptar.LoadFromFile(lcUtvonal + '\naptar.xml');
      DM.CDSNaptar.LogChanges := false;
      if FileExists(lcUtvonal+'\textil_szinek.xml') then
        begin
          DM.CDSTextilSzinek.LoadFromFile(lcUtvonal + '\textil_szinek.xml');
          DM.CDSTextilSzinek.Open;
          DM.CDSTextilSzinek.EnableControls;
          DM.CDSTextilSzinek.LogChanges := false;
        end
      else
        begin
          if DM.CDSTextilSzinek.Active then
            begin
              DM.CDSTextilSzinek.DisableControls;
              DM.CDSTextilSzinek.EmptyDataSet;
              DM.CDSTextilSzinek.Close;
            end;
        end;
      if FileExists(lcUtvonal+'\textil.xml') then
        begin
          DM.CDSTextilek.LoadFromFile(lcUtvonal+'\textil.xml');
        end;
      {DM.CDSOsztalyok.LoadFromFile(lcUtvonal+'\osztaly.xml');
      DM.CDSOsztalyok.LogChanges := false;
      DM.CDSOsztalyok.FieldByName('kpo_id').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_partner').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_osztaly').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_kod').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_csoport').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_uzem').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastuser').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastup').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_delete').Visible := false;
      DM.CDSOsztalyok.FieldByName('oszt_nev').DisplayLabel := 'Oszt�ly n�v';}
      btnAdatok.Enabled := false;
      mcDatum.Enabled := true;
      mcDatum.SetFocus;
    end
  else
    begin
      MessageBox(handle, 'Nincs t�rzs adata ennek a partnernek!', 'Hiba!', MB_ICONERROR);
      btnAdatok.Enabled := false;
    end;

  lValPartner.Caption := '';
  if GSelectedForm <> 'attarolas' then
    begin
      DM.CDSPartnerek.First;
      while not DM.CDSPartnerek.Eof do
        begin
          if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
            begin
              lValPartner.Caption := DM.CDSPartnerek.FieldByName('par_nev').AsString;
              break;
            end;
          DM.CDSPartnerek.Next;
        end;
    end;
end;

procedure TfrmPufferRaktarBevetelezes.mcDatumDblClick(Sender: TObject);
var
  lcDatum : TDate;
  lcKorhaz, lcFajlGenNev, lcMappa, lcDatumEll : TfgvUnit;
  lcOsztalyID : Integer;
  lcOsszFajlNev, lcUtvonal : String;
  lcSearchResult : TSearchRec;
begin
  lcDatum := now();
  GDate := mcDatum.Date;
  lcDatumEll := TfgvUnit.Create();
  if lcDatumEll.GetNaptar(GDate) then
    begin
      MessageBox(handle, 'Ezen a napon nem dolgozunk!', 'Figyelmeztet�s!', MB_ICONWARNING);
      exit;
    end
  else
    begin
    //  gbSzallitoAdat.Enabled := true;
      btnAdatok.Enabled := true;
     // eSzallSorszam.SetFocus;
      lSDatumVal.Caption := 'V�lasztott d�tum: ' + DateToStr(mcDatum.Date);
    end;
 { if CompareDate(mcDatum.Date,lcDatum) <> 0  then
    begin
      MessageBox(handle, 'Nem lehet visszad�tumozni!', 'Figyelmeztet�s!', MB_ICONWARNING);
      mcDatum.Date := now();
      mcDatum.SetFocus;
    end
  else if CompareDate(mcDatum.Date,lcDatum) = 0 then
    begin
      //Ha l�tezik az �sszes�tett f�jl akkor m�r nem lehet tov�bb szennyest r�gz�teni arra a napra.
      //�tvonalat be kell �ll�tani.
      lcKorhaz := TfgvUnit.Create();
      lcOsztalyID := lcKorhaz.SetOsztalyID('K�rh�z');
      lcFajlGenNev := TfgvUnit.Create();
      //mappa kell...
      lcMappa := TfgvUnit.Create();
      lcOsszFajlNev := 'O' + lcFajlGenNev.FajlNev_2(GDate, lcOsztalyID);
      lcUtvonal := (prUtvonal + '\adat_'+ IntToStr(GPartner) + '\' + lcMappa.Mappa(mcDatum.Date) + '\' +lcOsszFajlNev+'.xml');
      if FindFirst(lcUtvonal, faAnyFile, lcSearchResult) = 0 then
        begin
          MessageBox(handle, 'Nem lehet sz�ll�t�t r�gz�teni! Mert m�r l�tezik az �sszes�tett sz�ll�t�!', 'Figyelmeztet�s!', MB_ICONWARNING);
          Close;
        end
      else
        begin
          gbSzallitoAdat.Enabled := true;
          btnAdatok.Enabled := true;
          eSzallSorszam.SetFocus;
        end;
    end; }
end;

end.
