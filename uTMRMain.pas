unit uTMRMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Menus, Vcl.ComCtrls,
  Vcl.ExtCtrls, System.ImageList, Vcl.ImgList, INIFiles, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.UITypes,
  Datasnap.DBClient, Datasnap.Provider, ActiveX, ComObj, shellapi, TlHelp32;

type
  TfrmTMRMain = class(TForm)
    StatusBar1: TStatusBar;
    ilGomblist: TImageList;
    CategoryPanelGroup1: TCategoryPanelGroup;
    cpMenu: TCategoryPanel;
    btnSzennyes: TButton;
    btnKilep: TButton;
    btnPartnerSelect: TButton;
    DataSource1: TDataSource;
    FDQuery1: TFDQuery;
    btnBeallit: TButton;
    Timer1: TTimer;
    btnOsszesit: TButton;
    btnTiszta: TButton;
    btnOsszerend: TButton;
    btnOsszLista: TButton;
    btnPufferBevetBtn: TButton;
    Button2: TButton;
    Button3: TButton;
    btnPufferKiadOsszerendel: TButton;
    btnPufferVisszavet: TButton;
    btnPufferAttarolas: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSzennyesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnPartnerSelectClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnBeallitClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure btnOsszesitClick(Sender: TObject);
    procedure btnosszerendClick(Sender: TObject);
    procedure btnOsszListaClick(Sender: TObject);
    procedure btnTisztaClick(Sender: TObject);
    procedure btnKilepClick(Sender: TObject);
    procedure btnPufferBevetBtnClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnPufferKiadOsszerendelClick(Sender: TObject);
    procedure btnPufferVisszavetClick(Sender: TObject);
    procedure btnPufferAttarolasClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    function CreateModal(ModalForm: TFormClass): Integer;
  end;

var
  frmTMRMain: TfrmTMRMain;

implementation

uses uSzennyes, UDM, uKuldes, uPartnerSelect, uFgv, uBeallitasok, uHttps,
     uSzallitoOssz, uOsszerendel, uOsszeLista, uTiszta, uLogs, uPufferRaktarBevetelezes,
     uPufferRaktarBevetelezesParameterek, uPufferKiadOsszerendel;

{$R *.dfm}

function KillTask(ExeFileName: string): Integer;
const PROCESS_TERMINATE = $0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
      Result := Integer(TerminateProcess(
                        OpenProcess(PROCESS_TERMINATE,
                                    BOOL(0),
                                    FProcessEntry32.th32ProcessID),
                                    0));
     ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;


procedure TfrmTMRMain.btnPufferAttarolasClick(Sender: TObject);
begin
  GSelectedForm := 'attarolas';
  CreateModal(TfrmPufferRaktarBevetelezes);
end;

procedure TfrmTMRMain.btnBeallitClick(Sender: TObject);
begin
  CreateModal(TfrmBeallitasok);
end;

procedure TfrmTMRMain.btnKilepClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmTMRMain.btnosszerendClick(Sender: TObject);
begin
  GOldal := '';
  CreateModal(TfrmOsszerendel);
end;

procedure TfrmTMRMain.btnOsszesitClick(Sender: TObject);
begin
  GOlTipus := 'K';
  CreateModal(TfrmSzallitoOssz);
end;

procedure TfrmTMRMain.btnOsszListaClick(Sender: TObject);
begin
  //�sszerendel�s lista megnyit�sa.
  btnOsszLista.Enabled := false;
  CreateModal(TfrmOsszeLista);
end;

procedure TfrmTMRMain.btnPartnerSelectClick(Sender: TObject);
begin
  CreateModal(TfrmPartnerSelect);
end;

procedure TfrmTMRMain.btnPufferBevetBtnClick(Sender: TObject);
begin
  GSelectedForm := 'bevet';
  CreateModal(TfrmPufferRaktarBevetelezes);
end;

procedure TfrmTMRMain.btnPufferKiadOsszerendelClick(Sender: TObject);
var lcCurrPath: string;
    lcSearchRes: TSearchRec;
    lcDS: TFDMemTable;
    lcPartner: integer;
    lcPartnerNev: string;
begin
  lcCurrPath := GetCurrentDir;
  SetCurrentDir(GRootPath);
  try
    if FindFirst('tmp_osszerendel.xml', faAnyFile, lcSearchRes) = 0 then
      begin
        try
          lcDS := TFDMemTable.Create(Application);
          lcDS.LoadFromFile('tmp_osszerendel.xml');
          lcPartner := lcDS.FieldByName('partner').AsInteger;
          if lcPartner <> GPartner then
            begin
              while not DM.CDSPartnerek.Eof do
              begin
                if DM.CDSPartnerek.FieldByName('par_id').AsInteger = lcPartner then
                  begin
                    lcPartnerNev := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                    break;
                  end;
                DM.CDSPartnerek.Next;
              end;
              ShowMessage('M�r van egy megkezdett kiad�s a(z) ' + lcPartnerNev + ' partnern�l. V�ltson a partnerre �s fejezze be a kiad�st.');
            end
          else
            begin
              frmPufferKiadOsszerendel:= TfrmPufferKiadOsszerendel.Create(Application);
              frmPufferKiadOsszerendel.Hide;
              frmPufferKiadOsszerendel.FDMemTableOsszerendel.LoadFromFile(GRootPath + '\tmp_osszerendel.xml');
              SetCurrentDir(lcCurrPath);
              frmPufferKiadOsszerendel.ShowModal;
            end;
        finally
          lcDS.Free;
        end;
      end
    else
      begin
        SetCurrentDir(lcCurrPath);
        GSelectedForm := 'kiad';
        CreateModal(TfrmPufferRaktarBevetelezes);
      end;
  finally
    SetCurrentDir(lcCurrPath);
  end;

//  Application.CreateForm(TFrmPufferKiadOsszerendel, frmPufferKiadOsszerendel);
//  frmPufferKiadOsszerendel.ShowModal();
end;

procedure TfrmTMRMain.btnPufferVisszavetClick(Sender: TObject);
begin
  GSelectedForm := 'visszavet';
  CreateModal(TfrmPufferRaktarBevetelezes);
end;

procedure TfrmTMRMain.btnSzennyesClick(Sender: TObject);
begin
  GOldal := 'S';
  GOlTipus := 'O';
  CreateModal(TfrmSzennyes);
end;

procedure TfrmTMRMain.btnTisztaClick(Sender: TObject);
begin
  GOldal := 'T';
  GOlTipus := 'O';
  CreateModal(TfrmTiszta);
end;

procedure TfrmTMRMain.Button1Click(Sender: TObject);
var
  lcDataSetToXML : TfgvUnit;
begin
  lcDataSetToXML := TfgvUnit.Create();
  FDQuery1.Active := false;
  FDQuery1.Active := true;
//  lcDataSetToXML.DataSetToXML(FDQuery1, 'parameterek.xml');
//  lcDataSetToXML.DataSetToXML(FDQuery1, 'uzem.xml');
//  lcDataSetToXML.DataSetToXML(FDQuery1, 'chip.xml');
//  lcDataSetToXML.DataSetToXML(FDQuery1, 'partnerek.xml');
//  lcDataSetToXML.DataSetToXML(FDQuery1, 'felhasznalo.xml');
//  lcDataSetToXML.DataSetToXML(FDQuery1, 'sorszam.xml');
end;

procedure TfrmTMRMain.Button2Click(Sender: TObject);
begin
  ShowMessage(inttostr(dm.CDSOsztalyok.FieldByName('kpo_id').AsInteger));
end;

function TfrmTMRMain.CreateModal(ModalForm: TFormClass): Integer;
begin
  with ModalForm.Create(Self) do
  try
    FormStyle := fsNormal;
    WindowState := wsNormal;
    Hide;
    Result := ShowModal;
  finally
    Free;
  end;
end;

procedure TfrmTMRMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  lcLogs : TlogsUnit;
begin
  case MessageDlg('Biztos ki akar l�pni a programb�l? ', mtConfirmation, [mbYes, mbNo], 0) of
     mrNo:
       begin
         Abort;
       end;
     mrYes:
       begin
         lcLogs := TlogsUnit.Create();
         lcLogs.LogFajlWrite(' - Program - Bez�r�s! ');
         lcLogs.Free;
         Application.Terminate;
       end;
  end;
end;

procedure TfrmTMRMain.FormShow(Sender: TObject);
begin
  KillTask('serredsup.exe');
  StatusBar1.Panels[2].Text := 'verzi� 0.7.4. ' + 'build 51';
  if GVer = 'TH' then
    begin
      btnSzennyes.Enabled := true;
      btnOsszesit.Enabled := true;
      btnTiszta.Enabled := true;
      btnPartnerSelect.Enabled := false;
    end
  else if GVer = 'RA' then
    begin
      btnSzennyes.Enabled := false;
      btnOsszesit.Enabled := false;
      btnTiszta.Enabled := false;
      btnPartnerSelect.Enabled := true;
      btnOsszerend.Enabled := false;
      btnOsszLista.Enabled := false;
    end;
  if GInternet then
    begin
      StatusBar1.Panels[0].Text := 'Internet kapcsolat rendben!';
    end
  else
    begin
      StatusBar1.Panels[0].Text := 'Nincs internet kapcsolat!';
    end;
//  DM.tInternet.Enabled := True;
end;

procedure TfrmTMRMain.Timer1Timer(Sender: TObject);
begin
  if GInternet then
    begin
      StatusBar1.Panels[0].Text := 'Internet kapcsolat rendben!';
    end
  else
    begin
      StatusBar1.Panels[0].Text := 'Nincs internet kapcsolat!';
    end;
end;

end.
