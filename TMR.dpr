program TMR;

uses
  Vcl.Forms,
  uTMRMain in 'uTMRMain.pas' {frmTMRMain},
  uPufferRaktarBevetelezes in 'uPufferRaktarBevetelezes.pas' {frmPufferRaktarBevetelezes},
  uDm in 'uDm.pas' {DM: TDataModule},
  uSzennyesUj in 'uSzennyesUj.pas' {frmSzennyesUj},
  uFgv in 'uFgv.pas',
  uPartnerSelect in 'uPartnerSelect.pas' {frmPartnerSelect},
  uBeallitasok in 'uBeallitasok.pas' {frmBeallitasok},
  uHttps in 'uHttps.pas',
  uSplash in 'uSplash.pas' {frmSplash},
  uSzallitoOssz in 'uSzallitoOssz.pas' {frmSzallitoOssz},
  uTiszta in 'uTiszta.pas' {frmTiszta},
  uOsszerendel in 'uOsszerendel.pas' {frmOsszerendel},
  Vcl.Themes,
  Vcl.Styles,
  uChipBeolvas in 'uChipBeolvas.pas' {frmChipBeolvas},
  uOsszeLista in 'uOsszeLista.pas' {frmOsszeLista},
  uTisztaSelect in 'uTisztaSelect.pas' {frmTisztaSelect},
  uLogs in 'uLogs.pas',
  uSzennyes in 'uSzennyes.pas' {frmSzennyes},
  uSerialReader in 'uSerialReader.pas',
  uPufferRaktarBevetelezesParameterek in 'uPufferRaktarBevetelezesParameterek.pas' {frmPufferRaktarBevetelezesParameterek},
  Comport in 'Comport.pas',
  uPufferKiadOsszerendel in 'uPufferKiadOsszerendel.pas' {frmPufferKiadOsszerendel},
  uPufferVisszavetelezes in 'uPufferVisszavetelezes.pas' {frmPufferVisszavetel},
  uPufferAttarolasKiadas in 'uPufferAttarolasKiadas.pas' {frmPufferAttarolasKiadas},
  uPufferAttarolasBevet in 'uPufferAttarolasBevet.pas' {frmPufferAttarolasBevet};

{$R *.res}


begin
  Application.Initialize;
  frmSplash := TfrmSplash.Create(Application);
  try
    Application.CreateForm(TDM, DM);
    frmSplash.MakeSplash;
    Application.MainFormOnTaskbar := True;
    Application.Title := 'Textil Mos�si Rendszer';
    Application.CreateForm(TfrmTMRMain, frmTMRMain);
  finally
    frmSplash.Free;
    frmSplash := nil;
  end;
  Application.Run;
end.
