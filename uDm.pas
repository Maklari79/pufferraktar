unit uDM;

interface

uses
  System.SysUtils, System.Classes, Datasnap.Provider, Data.DB, Datasnap.DBClient,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Stan.StorageXML, FireDAC.DApt, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, INIFiles, WinInet, Vcl.Forms,
  Vcl.ExtCtrls, IdRawBase, IdRawClient, IdIcmpClient, IdAntiFreezeBase,
  IdAntiFreeze, frxClass, frxDBSet, System.Hash, frxExportPDF,
  IdExplicitTLSClientServerBase, IdFTP, System.UITypes, Vcl.Dialogs,
  System.IOUtils, Windows, System.DateUtils, System.Variants;

type
  TURLs = Record
    pufBevRakXMLUpload: string;
    pufBevRakXMLUpload_PBM: string;
    pufKiadRakXMLUpload: string;
    pufMoveRakXMLUpload: string;
    pufOsszerendXMLUpload: string;
    pufVisszaXMLUpload: string;
  End;

type
  TPufferRaktarMozgasnemek = Record
    PB: integer;
    PK: integer;
    PV: integer;
    PKK: integer;
    PKB: integer;
    PTK: integer;
  End;

type
  TDM = class(TDataModule)
    ConnLogotex: TFDConnection;
    pMySQLDLink: TFDPhysMySQLDriverLink;
    CDSTextilek: TClientDataSet;
    DSTextilek: TDataSource;
    DSTextilTable: TDataSource;
    CDSSzallMent: TClientDataSet;
    DSPSzallMent: TDataSetProvider;
    FDStanStorageXMLLink1: TFDStanStorageXMLLink;
    CDSMozgnem: TClientDataSet;
    FDMemTextilek: TFDMemTable;
    FDMemTextilekszfej_partner: TIntegerField;
    FDMemTextilekszfej_uzem: TIntegerField;
    FDMemTextilekszfej_mozgid: TIntegerField;
    FDMemTextilekszfej_osztid: TIntegerField;
    FDMemTextilekszfej_osszsuly: TFloatField;
    FDMemTextilekszfej_lastuser: TIntegerField;
    FDMemTextilekszfej_lastup: TDateTimeField;
    FDMemTextilekszfej_delete: TIntegerField;
    FDMemTextilekszfej_createuser: TIntegerField;
    FDMemTextilekszfej_createdate: TDateTimeField;
    FDMemTextilekszfej_szallszam: TStringField;
    FDMemTextilekszfej_kijelol: TIntegerField;
    FDMemTextilekszfej_mertsuly: TFloatField;
    FDMemTextileksztet_osztid: TIntegerField;
    FDMemTextileksztet_osztsuly: TFloatField;
    FDMemTextileksztet_textilid: TIntegerField;
    FDMemTextileksztet_textilnev: TStringField;
    FDMemTextileksztet_textildb: TIntegerField;
    FDMemTextileksztet_textilsuly: TFloatField;
    FDMemTextileksztet_lastuser: TIntegerField;
    FDMemTextileksztet_lastup: TDateTimeField;
    FDMemTextileksztet_delete: TIntegerField;
    FDMemTextileksztet_createuser: TIntegerField;
    FDMemTextileksztet_createdate: TDateTimeField;
    FDMemTextileksztet_fajlnev: TStringField;
    FDMemTextileksztet_recsorsz: TIntegerField;
    IdHTTP1: TIdHTTP;
    handlerSSL: TIdSSLIOHandlerSocketOpenSSL;
    CDSOsztaly: TClientDataSet;
    tInternet: TTimer;
    IdIcmpClient1: TIdIcmpClient;
    IdAntiFreeze1: TIdAntiFreeze;
    CDSPartnerek: TClientDataSet;
    CDSXmlPost: TClientDataSet;
    DSPXmlPost: TDataSetProvider;
    CDSOsztalyok: TClientDataSet;
    DSOsztalyok: TDataSource;
    CDSMozg: TClientDataSet;
    frxDBDataset1: TfrxDBDataset;
    frxRepSzall: TfrxReport;
    CDSFelhasz: TClientDataSet;
    CDSUzem: TClientDataSet;
    CDSLoadXml: TClientDataSet;
    frxPDFExport1: TfrxPDFExport;
    FDMemTextileksztet_javitott: TSmallintField;
    FDMemTextileksztet_bekeszit: TIntegerField;
    CDSParameterek: TClientDataSet;
    CDSTiszta: TClientDataSet;
    DSPTiszta: TDataSetProvider;
    DSPParam: TDataSetProvider;
    CDSNaptar: TClientDataSet;
    cdsOsszerendel: TClientDataSet;
    dsOsszerendel: TDataSource;
    CDSChip: TClientDataSet;
    IdFTP1: TIdFTP;
    CDSCikkTorzs: TClientDataSet;
    DSChipOlvas: TDataSource;
    frxOlvasChip: TfrxReport;
    frxDBDSOlvasChip: TfrxDBDataset;
    FDMemChipOlvas: TFDMemTable;
    FDMemChipOlvasVonalkod: TStringField;
    FDMemChipOlvasChipkod: TStringField;
    FDMemChipOlvasHelyszinnev: TStringField;
    FDMemChipOlvasDolgozonev: TStringField;
    FDMemChipOlvascikknev: TStringField;
    FDMemChipOlvasDatum: TDateTimeField;
    cdsBTXMozgas: TClientDataSet;
    cdsBTXMozgaschm_chipkod: TStringField;
    cdsBTXMozgaschm_vonalkod: TStringField;
    cdsBTXMozgaschm_mozgastipus: TStringField;
    cdsBTXMozgaschm_telephelykod: TStringField;
    cdsBTXMozgaschm_kivezetesoka: TStringField;
    cdsBTXMozgaschm_datum: TDateTimeField;
    cdsBTXMozgaschm_felhasznalo: TStringField;
    FDMemChipOlvasMozgasTipus: TStringField;
    FDMemChipOlvasTelephelykod: TStringField;
    FDMemChipOlvasKivezetesoka: TStringField;
    FDMemChipOlvasBizonylat: TStringField;
    FDMemChipOlvasMosoda: TStringField;
    CDSPartSelect: TClientDataSet;
    DSPartSelect: TDataSource;
    CDSChipEllenorzes: TClientDataSet;
    FDMemChipOlvasSzekreny: TStringField;
    CDSSzallSorszam: TClientDataSet;
    FDMemChipOlvasFajlnev: TStringField;
    IdHTTP2: TIdHTTP;
    SSL2: TIdSSLIOHandlerSocketOpenSSL;
    FDMemChipOlvasFelhasznalo: TIntegerField;
    FDMemTextilekszfej_kiszdatum: TDateField;
    CDSXMLMentes: TClientDataSet;
    FDMemTextilekszfej_lezart: TIntegerField;
    CDSTSzallLista: TClientDataSet;
    CDSTSzallTetelek: TClientDataSet;
    DSTSzallLista: TDataSource;
    DSTSzallTetelek: TDataSource;
    FDMemTextilekszfej_id: TIntegerField;
    CDSTisztaEllenorzes: TClientDataSet;
    CDSBetoltTiszta: TClientDataSet;
    DSTextilSzinek: TDataSource;
    CDSTextilSzinek: TClientDataSet;
    DSTextilAllapot: TDataSource;
    CDSTextilAllapot: TClientDataSet;
    DSTextilMagassag: TDataSource;
    CDSTextilMagassag: TClientDataSet;
    DSTextilFajta: TDataSource;
    CDSTextilFajta: TClientDataSet;
    DSTextilMeret: TDataSource;
    CDSTextilMeret: TClientDataSet;
    FDMemTablePufferRakBevet: TFDMemTable;
    DSPufferRaktarBevet: TDataSource;
    FDMemTablePufferRakBevetpbfej_uzem_id: TIntegerField;
    FDMemTablePufferRakBevetpbfej_felhasznalo_id: TIntegerField;
    FDMemTablePufferRakBevetpbfej_create_date: TDateTimeField;
    FDMemTablePufferRakBevetpbfej_create_user: TIntegerField;
    FDMemTablePufferRakBevetpbfej_lastupdate_date: TDateTimeField;
    FDMemTablePufferRakBevetpbfej_lastupdate_user: TIntegerField;
    FDMemTablePufferRakBevetpbtet_darabszam: TIntegerField;
    FDMemTablePufferRakBevetpbtet_chip_kod: TStringField;
    FDMemTablePufferRakBevetpbtet_fajta_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_allapot_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_meret_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_magassag_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_szin_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_mozgas_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_create_date: TDateTimeField;
    FDMemTablePufferRakBevetpbtet_create_user: TIntegerField;
    FDMemTablePufferRakBevetpbtet_lastupdate_date: TDateTimeField;
    FDMemTablePufferRakBevetpbtet_lastupdate_user: TIntegerField;
    FDMemTablePufferRakBevetpbfej_partner_id: TIntegerField;
    FDMemTablePufferRakBevettmp_bevet_group: TIntegerField;
    FDMemTablePufferRaktarSynchs: TFDMemTable;
    FDMemTablePufferRaktarSynchschip_kod: TStringField;
    FDMemTablePufferRakBevetfajtaText: TStringField;
    FDMemTablePufferRakBevetallapotText: TStringField;
    FDMemTablePufferRakBevetszinText: TStringField;
    FDMemTablePufferRakBevetmeretText: TStringField;
    FDMemTablePufferRakBevetmagassagText: TStringField;
    FDMemTablePufferRakBevetpbfej_selected_date: TDateTimeField;
    FDMemTablePufferRakBevetpbfej_sorszam: TStringField;
    FDMemTablePufferRakBevetpbfej_id: TIntegerField;
    FDMemTablePufferRakBevetpbtet_megnevezes: TStringField;
    FDMemTablePufferRakBevetmegnevezesText: TStringField;
    FDMemTablePufferRakBevetpbfej_megjegyzes: TStringField;
    FDMemTablePufferRakBevetpbtet_megjegyzes: TStringField;
    FDMemTablePufferRakBevetpbfej_osztaly_id: TIntegerField;
    CDSChipPuffer: TClientDataSet;
    n: TStringField;
    CDSChipPufferfajtaid: TIntegerField;
    CDSChipPufferuzemid: TIntegerField;
    CDSChipPufferpartnerid: TIntegerField;
    CDSChipPufferosztalyid: TIntegerField;
    CDSChipPufferchipkod: TStringField;
    CDSChipPuffertextilid: TIntegerField;
    CDSChipPuffertextilnev: TStringField;
    CDSChipPufferallapotid: TIntegerField;
    CDSChipPufferallapotnev: TStringField;
    CDSChipPuffermeretid: TIntegerField;
    CDSChipPuffermeretnev: TStringField;
    CDSChipPuffermagassagid: TIntegerField;
    CDSChipPuffermagassagnev: TStringField;
    CDSChipPufferszinid: TIntegerField;
    CDSChipPufferszinnev: TStringField;
    CDSDarabosPuffer: TClientDataSet;
    CDSDarabosPufferfajtaid: TIntegerField;
    CDSDarabosPufferfajtanev: TStringField;
    CDSDarabosPufferuzemid: TIntegerField;
    CDSDarabosPufferpartnerid: TIntegerField;
    CDSDarabosPuffertextilid: TIntegerField;
    CDSDarabosPuffertextilnev: TStringField;
    CDSDarabosPufferallapotid: TIntegerField;
    CDSDarabosPuffermeretid: TIntegerField;
    CDSDarabosPuffermagassagid: TIntegerField;
    CDSDarabosPufferszinid: TIntegerField;
    CDSDarabosPufferdarabosossz: TIntegerField;
    CDSDarabosPufferallapotnev: TStringField;
    CDSDarabosPuffermeretnev: TStringField;
    CDSDarabosPuffermagassagnev: TStringField;
    CDSDarabosPufferszinnev: TStringField;
    FDMemTablePufferKiadSynchs: TFDMemTable;
    StringField1: TStringField;
    CDSChipPuffertextilsuly: TFloatField;
    CDSDarabosPuffertextilsuly: TFloatField;
    CDSKivetelezesOka: TClientDataSet;
    DSKivetelezesOka: TDataSource;
    CDSUzemek: TClientDataSet;
    DSUzemek: TDataSource;
    DSPKKSzamok: TDataSource;
    CDSPKKSzamok: TClientDataSet;
    CDSPKKSzallitok: TClientDataSet;
    DSPKKSzallitok: TDataSource;
    CDSPKKChipmozgas: TClientDataSet;
    DSPKKChipmozgas: TDataSource;
    FDMemTablePufferVisszavetSynchs: TFDMemTable;
    StringField2: TStringField;
    CDSDarabosPufferpartnernev: TStringField;
    CDSUzemPartner: TClientDataSet;
    DSUzemPartner: TDataSource;
    CDSDarabosPufferid: TIntegerField;
    CDSRFID: TClientDataSet;
    procedure cds(Sender: TObject);
    procedure tInternetTimer(Sender: TObject);
    procedure FDMemTablePufferRakBevetpbtet_darabszamChange(Sender: TField);
    procedure FDMemTablePufferRakBevetAfterPost(DataSet: TDataSet);
    procedure DSTextilekDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
     prUtvonal, prUtMent : String;
     function EnDeCrypt(const Value : AnsiString) : AnsiString;
  public
    { Public declarations }
    pubiniFile : TIniFile;
    function Szinkronizalas(AMit : String) : String;
    function InternetEll : Boolean;
    function Szinkronizalas_2(AMit: String) : String;
    function Szinkroniz�l�s_3(AMit: String) : String;
    function GetFtp : String;
    function CikkTorzsFeldolgozas(AFile: String) : Boolean;
    function Szinkronizalas_puffer_raktar(AMit: string; APartner: integer) : string;
    function RFIDSzinkronizalas(ARFIDGet: String; AMit: String): String;
    procedure LoadSynchChip;
  end;

  TLogin = (lOk, lExpired, lInvalid, lNoDefStorage);

var
  DM: TDM;
  GOsztalyID, GPartner, GUserID : Integer;
  GPartnerNev: String;
  GMertSuly : Double;
  GOldal, GOlTipus, GUserNev, GUserPassw : String;
  GDate : TDate;
  GVer, GSzallSorsz, GOsztNev, GFajlNev, GTFajlNev : String;
  GInternet, GEnable, GTobbSzallito : Boolean;
  GIp, GUtvonal : String;
  GMappa : String; {Tiszta sz�ll�t�k let�lt�s�hez, hogy melyik mapp�ba kell menteni a tiszta sz�ll�tokat.}
  GNincsTiszta : Boolean; {Ha nincs tiszta f�jl, akkor ez igaz.}
  GDatasetValtas : Boolean; {Kell-e f�jl bet�lt�skor kliens datasetet v�ltani.}
  GWHazUrls: TURLs;
  GPufferRaktarMozgasnemek: TPufferRaktarMozgasnemek;
  GPufferRaktarOsztaly: integer;
  GRootPath: string;
  GSelectedForm: string; // Bev�telez�s / Kiad�s-t eld�nt� glob�lis v�ltoz�
 // GSorszamKell : Boolean; {Ha tiszta f�jlt let�ltik nem kell sorsz�mot gener�lni.}


implementation

uses uHttps, uTMRMain, uFgv, uPufferRaktarBevetelezesParameterek,
     uPufferVisszavetelezes, uLogs;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TDM.CikkTorzsFeldolgozas(AFile: string) : Boolean;
begin
  CDSCikkTorzs.LoadFromFile(AFile);
  CDSCikkTorzs.LogChanges := false;
  CDSCikkTorzs.First;
  while not CDSCikkTorzs.Eof do
    begin
      CDSChip.First;
      CDSChip.Filtered := false;
      CDSChip.Filter := 'cms_vonalkod =' + CDSCikkTorzs.FieldByName('cms_vonalkod').AsString;
      if (CDSChip.RecordCount > 0) and (CDSChip.FieldByName('cms_moddatum').AsDateTime < CDSCikkTorzs.FieldByName('cms_moddatum').AsDateTime) then
        begin
          CDSChip.Edit;
          CDSChip.FieldByName('cms_chipkod').AsString := CDSCikkTorzs.FieldByName('cms_chipkod').AsString;
          CDSChip.FieldByName('cms_partnerkod').AsString := CDSCikkTorzs.FieldByName('cms_partnerkod').AsString;
          CDSChip.FieldByName('cms_partnernev').AsString := CDSCikkTorzs.FieldByName('cms_partnernev').AsString;
          CDSChip.FieldByName('cms_telephelykod').AsString := CDSCikkTorzs.FieldByName('cms_telephelykod').AsString;
          CDSChip.FieldByName('cms_telephelynev').AsString := CDSCikkTorzs.FieldByName('cms_telephelynev').AsString;
          CDSChip.FieldByName('cms_telephelycim').AsString := CDSCikkTorzs.FieldByName('cms_telephelycim').AsString;
          CDSChip.FieldByName('cms_helyszinnev').AsString := CDSCikkTorzs.FieldByName('cms_helyszinnev').AsString;
          CDSChip.FieldByName('cms_ruhaid').AsInteger := CDSCikkTorzs.FieldByName('cms_ruhaid').AsInteger;
          CDSChip.FieldByName('cms_statusz').AsString := CDSCikkTorzs.FieldByName('cms_statusz').AsString;
          CDSChip.FieldByName('cms_cikkszam').AsString := CDSCikkTorzs.FieldByName('cms_cikkszam').AsString;
          CDSChip.FieldByName('cms_cikknev').AsString := CDSCikkTorzs.FieldByName('cms_cikknev').AsString;
          CDSChip.FieldByName('cms_meret').AsString := CDSCikkTorzs.FieldByName('cms_meret').AsString;
          CDSChip.FieldByName('cms_szekrenynev').AsString := CDSCikkTorzs.FieldByName('cms_szekrenynev').AsString;
          CDSChip.FieldByName('cms_dolgozokod').AsString := CDSCikkTorzs.FieldByName('cms_dolgozokod').AsString;
          CDSChip.FieldByName('cms_dolgozonev').AsString := CDSCikkTorzs.FieldByName('cms_dolgozonev').AsString;
          CDSChip.FieldByName('cms_garnitsorsz').AsInteger := CDSCikkTorzs.FieldByName('cms_garnitsorsz').AsInteger;
          CDSChip.FieldByName('cms_cimkedatum').AsDateTime := CDSCikkTorzs.FieldByName('cms_cimkedatum').AsDateTime;
          CDSChip.FieldByName('cms_kihelydatum').AsDateTime := CDSCikkTorzs.FieldByName('cms_kihelydatum').AsDateTime;
          CDSChip.FieldByName('cms_kivezdatum').AsDateTime := CDSCikkTorzs.FieldByName('cms_kivezdatum').AsDateTime;
          CDSChip.FieldByName('cms_selejtdatum').AsDateTime := CDSCikkTorzs.FieldByName('cms_selejtdatum').AsDateTime;
          CDSChip.FieldByName('cms_elveteldatum').AsDateTime := CDSCikkTorzs.FieldByName('cms_elveteldatum').AsDateTime;
          CDSChip.FieldByName('cms_moddatum').AsDateTime := CDSCikkTorzs.FieldByName('cms_moddatum').AsDateTime;
          CDSChip.FieldByName('cms_btxid').AsInteger := CDSCikkTorzs.FieldByName('cms_btxid').AsInteger;
          CDSChip.Post;
          break;
        end;
      CDSCikkTorzs.Next;
    end;
end;

procedure TDM.DSTextilekDataChange(Sender: TObject; Field: TField);
var lcFgv: TfgvUnit;
    lcMeretCsoport: integer;
begin
  lcFgv := TfgvUnit.Create;
  if GSelectedForm = 'bevet' then
    begin
      if (Assigned(frmPufferRaktarBevetelezesParameterek)) and (not VarIsNull(frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxMegnevezes.KeyValue)) then
        begin
          frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.Enabled := true;
          CDSTextilSzinek.Filtered := false;
          CDSTextilSzinek.Filter := 'kpsz_kpt_id = '+string(frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxMegnevezes.KeyValue);
          CDSTextilSzinek.Filtered := true;
          //if lcFgv.FilteredRecordCount(CDSTextilSzinek) = 0 then
          if CDSTextilSzinek.RecordCount = 0 then
            begin
              frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.Enabled := false;
              frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.KeyValue := null;
            end;

        end
      else if (Assigned(frmPufferRaktarBevetelezesParameterek)) and (VarIsNull(frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxMegnevezes.KeyValue)) then
        begin
          frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.Enabled := false;
          frmPufferRaktarBevetelezesParameterek.DBLookupComboBoxTextilSzin.KeyValue := null;
        end;
    end
  else if GSelectedForm = 'visszavet' then
    begin
      if (Assigned(frmPufferVisszavetel)) and (not VarIsNull(frmPufferVisszavetel.DBLookupComboBoxMegnevezes.KeyValue)) then
        begin
          //frmPufferVisszavetel.DBLookupComboBoxTextilSzin.Enabled := true;
          CDSTextilSzinek.Filtered := false;
          CDSTextilSzinek.Filter := 'kpsz_kpt_id = '+string(frmPufferVisszavetel.DBLookupComboBoxMegnevezes.KeyValue);
          CDSTextilSzinek.Filtered := true;
          //if lcFgv.FilteredRecordCount(CDSTextilSzinek) = 0 then
          if CDSTextilSzinek.RecordCount = 0 then
            begin
              frmPufferVisszavetel.DBLookupComboBoxTextilSzin.Enabled := false;
              frmPufferVisszavetel.DBLookupComboBoxTextilSzin.KeyValue := null;
            end
          else
            begin
              if frmPufferVisszavetel.pubInEdit then
                frmPufferVisszavetel.DBLookupComboBoxTextilSzin.Enabled := true;
            end;
         // Kiemeltem, mert �gy mindk�t m�retcsoportot bet�lti. by Zoli - 2020.05.19.
         // lcMeretCsoport := lcFgv.GetMeretCsoportByTextilId(frmPufferVisszavetel.DBLookupComboBoxMegnevezes.KeyValue, true);

         // CDSTextilMeret.Filtered := false;
         // CDSTextilMeret.Filter := 'meret_csoport ='+IntToStr(lcMeretCsoport);
         // CDSTextilMeret.Filtered := true;
        end
      else if (Assigned(frmPufferVisszavetel)) and (VarIsNull(frmPufferVisszavetel.DBLookupComboBoxMegnevezes.KeyValue)) then
        begin
          frmPufferVisszavetel.DBLookupComboBoxTextilSzin.Enabled := false;
          frmPufferVisszavetel.DBLookupComboBoxTextilSzin.KeyValue := null;
        end;
    end;
  lcFgv.Free;
end;

function TDM.EnDeCrypt(const Value : AnsiString) : AnsiString;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := AnsiChar(not(ord(AnsiChar(Value[CharIndex]))));
end;

procedure TDM.FDMemTablePufferRakBevetAfterPost(DataSet: TDataSet);
begin
  frmPufferRaktarBevetelezesParameterek.RedrawOsszPanel;
end;

procedure TDM.FDMemTablePufferRakBevetpbtet_darabszamChange(Sender: TField);
begin
  // Nincs negat�v mennyis�gre lehet�s�g a puffer rakt�r bev�telez�sn�l.
  if (Sender.Value < 0) AND NOT (VarIsNull(Sender.Value)) then
    begin
      Sender.Value := 0;
    end;
end;

procedure TDM.cds(Sender: TObject);
var
  lcMyDate : TDateTime;
  lcDatum, lcJelszo : String;

begin
  ConnLogotex.Connected := false;
  lcmyDate := Now();
  DateTimeToString(lcDatum, 'hhmmsszzz', lcmyDate);
  pubiniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  GVer := pubiniFile.ReadString('prog','ver','');
  GUserID := pubiniFile.ReadInteger('user','id',0);
  GUserNev := pubiniFile.ReadString('felhasznalo','felh','');
  lcJelszo := pubiniFile.ReadString('jelszo','passw','');
  GIp := pubiniFile.ReadString('serverip','ip','');
  GUtvonal := pubiniFile.ReadString('utvonal','path','');
  GUserPassw := EnDeCrypt(lcJelszo);
  if GVer = 'TH' then
    begin
      prUtvonal := GetCurrentDir+ '\adat\';
      prUtMent := GetCurrentDir;
      DM.CDSPartnerek.LoadFromFile('partnerek.xml');
      DM.CDSPartnerek.LogChanges := false;
      GPartner := DM.CDSPartnerek.FieldByName('par_id').AsInteger;
//      GPartner := DM.CDSTextilek.FieldByName('par_id').AsInteger;
    end
  else if GVer = 'RA' then
    begin
      prUtvonal := GetCurrentDir+ '\adat\';
      prUtMent := GetCurrentDir;
      CDSPartnerek.LoadFromFile('partnerek.xml');
      CDSPartnerek.LogChanges := false;
    end;
//  CDSFelhasz.LoadFromFile('felhasznalo.xml');
//  CDSFelhasz.LogChanges := false;
//  CDSUzem.LoadFromFile('uzem.xml');
//  CDSUzem.LogChanges := false;
//  CDSParameterek.LoadFromFile('parameterek.xml');
//  CDSParameterek.LogChanges := false;
//  DM.CDSMozg.LoadFromFile('mozgasnem.xml');
//  DM.CDSMozg.LogChanges := false;
//  DM.CDSMozgnem.LoadFromFile('mozgasnem.xml');
//  DM.CDSMozgnem.LogChanges := false;
//  CDSChip.LoadFromFile('chip.xml');
//  CDSChip.LogChanges := false;
  pubiniFile.Free;
//
  GWHazUrls.pufBevRakXMLUpload := 'pufferszallitolevel/';
  GWHazUrls.pufBevRakXMLUpload_PBM := 'pufferchipmozgas/';
  GWHazUrls.pufKiadRakXMLUpload := 'pufkiadrak/';
  GWHazUrls.pufMoveRakXMLUpload := 'pufmoverak/';
  GWHazUrls.pufOsszerendXMLUpload := 'osszerendel/';
  GWHazUrls.pufVisszaXMLUpload := 'visszavet/';

{
  GPufferRaktarMozgasnemek.PB := 15;
  GPufferRaktarMozgasnemek.PK := 16;
  GPufferRaktarMozgasnemek.PV := 17;
  GPufferRaktarMozgasnemek.PKK := 18;
  GPufferRaktarMozgasnemek.PKB := 19;
  GPufferRaktarMozgasnemek.PTK := 20;
}
end;

function TDM.InternetEll : Boolean;
var
  lcXmlSave : THttpsUnit;
  lcValasz : String;
begin
  try
    { WHAZ DEBUG }
//    result := false;
//    exit;
    { WHAZ DEBUG }
    lcXmlSave := THttpsUnit.Create();
    //lcValasz := lcXmlSave.GetHttps('192.168.22.16','/emer/v1/','hello/',GUserID);
    lcValasz := lcXmlSave.GetHttps(GIp,GUtvonal,'hello/',GUserID);
    if lcValasz = 'Hello, ' + IntToStr(GUserID) then
      begin
        Result := true;
      end;
  except
    Result := False;
    Exit;
  end;
{  try
    IdIcmpClient1.Host := '192.168.22.16';
    IdIcmpClient1.Ping;
  except
    Result := False;
    Exit;
  end;
  if IdIcmpClient1.ReplyStatus.ReplyStatusType = rsEcho then
    begin
      result := true;
      GInternet := true;
    end
  else if IdIcmpClient1.ReplyStatus.ReplyStatusType = rsError then
    begin
      result := false;
      GInternet := false;
    end
  else if IdIcmpClient1.ReplyStatus.ReplyStatusType = rsTimeOut then
    begin
      result := false;
      GInternet := false;
    end
  else
    begin
      result := false;
      GInternet := false;
    end;}
end;

function TDM.RFIDSzinkronizalas(ARFIDGet: String; AMit: String): String;
var
  lciniFile : TIniFile;
  lcIPCim, lcUtvonal, lcRFIDdb : String;
  lcRFIDMax, I : Integer;
  lcXmlSave : THttpsUnit;
  lcXml : TfgvUnit;
  lcLogs : TlogsUnit;
begin
  sleep(500);
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcUtvonal := lciniFile.ReadString('utvonal','path','');
  try
    CDSOsztaly.Active := false;
    lcLogs := TlogsUnit.Create();
    If ARFIDGet = 'RFIDCount/' then
      begin
        lcXmlSave := THttpsUnit.Create();
        lcXml := TfgvUnit.Create();
        lcRFIDdb := lcXmlSave.GetRFIDMaxSzam(lcIPCim,lcUtvonal,ARFIDGet,0);
        lcLogs.LogFajlWrite(' - RFIDCount -> ' + lcRFIDdb);
        lcRFIDMax := strtoint(lcRFIDdb);
        lcXml.Free;
        lcXmlSave.Free;
      end;
    if lcRFIDMax <> 0 then
      begin
        lcRFIDMax := lcRFIDMax div 10000;
        for I := 0 to lcRFIDMax+1 do
          begin
            lcXmlSave := THttpsUnit.Create();
            lcXml := TfgvUnit.Create();
            CDSRFID.Active := false;
            CDSRFID.XMLData.Empty;
            lcLogs.LogFajlWrite(' - RFIDCount Limit kezd��rt�k -> ' + (intToStr(I*10000)));
            CDSRFID.XMLData := lcXmlSave.GetRFIDMaxSzam(lcIPCim,lcUtvonal,AMit,I*10000);
            CDSRFID.Active := true;
            CDSOsztaly.AppendData(CDSRFID.Data,true);
            lcXml.Free;
            lcXmlSave.Free;
          end;
      end;

    lcUtvonal := GetCurrentDir;
    CDSOsztaly.Active := true;
    lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\chip.xml');
    result := 'Sikeres!';
   except
     on E : Exception do
       begin
        //raise Exception.Create(Exception(ExceptObject).Message);
        CDSOsztaly.Active := false;
        CDSRFID.Active := false;
        result := 'Sikertelen!';
        end;
   end;
  lcLogs.Free;
  CDSOsztaly.Active := false;
  CDSRFID.Close;
end;

function TDM.GetFtp : String;
var
  lcFajlLista, lcLogFile : TStringList;
  I, K : Integer;
  lcXml : TfgvUnit;
  LogFile : TextFile;
  lcUt, lcSor, lcMasol : String;
begin
  try
    if not IdFTP1.Connected then
       begin
         lcFajlLista := TStringList.Create;
         lcLogFile := TStringList.Create;
         lcUt := GetCurrentDir;
         IdFTP1.Connect;
         if IdFTP1.Connected then
           begin
             AssignFile(LogFile, lcUt+'\ftplog.txt');
             Reset(LogFile);
             while not eof(LogFile) do
               begin
                 Readln(LogFile, lcMasol);
                 lcLogFile.Add(lcMasol);
               end;
             CloseFile(LogFile);
             IdFTP1.ChangeDir('rfidbtx/mav/cikktorzs');
             IdFTP1.List(lcFajlLista,'',false);
             lcMasol := '';
             for I := 2 to lcFajlLista.Count-1 do
               begin
                 for K := 0 to lcLogFile.Count-1 do
                   begin
                     lcMasol := copy(lcLogFile[K],31,7);
                     lcSor := copy(lcLogFile[K],1,29);
                     if ((lcMasol <> 'Sikeres') and (lcSor <> lcFajlLista[I])) then
                    // if (lcSor <> lcFajlLista[I]) then
                       begin
                         IdFTP1.Get(lcFajlLista[I], 'c:\development\DXE_10_2\tmr\Win32\Debug\'+lcFajlLista[I], true, true);
                         CikkTorzsFeldolgozas('c:\development\DXE_10_2\tmr\Win32\Debug\'+lcFajlLista[I]);
                         AssignFile(LogFile, lcUt+'\ftplog.txt');
                         Append(LogFile);
                         WriteLn(LogFile, lcFajlLista[I] + ' Sikeres ' + FormatDateTime('YYYY.MM.DD HH:NN:SS',now()));
                         CloseFile(LogFile);
                         Deletefile(PWideChar(lcUt+lcFajlLista[I]));
                         break;
                       end;
                   end;
               end;
             IdFTP1.Disconnect;
             Result := 'Sikeres!';
           end;
       end;
  except
    on E:Exception do
       begin
         Result := 'Sekertelen!';
         AssignFile(LogFile, lcUt+'\ftplog.txt');
         Append(LogFile);
         WriteLn(LogFile, lcFajlLista[I] + ' Sikertelen ' + FormatDateTime('YYYY.MM.DD HH:NN:SS',now()));
         CloseFile(LogFile);
         MessageDlg('Let�lt�s sikertelen!', mtError, [mbOK], 0);
       end;
  end;
  lcXml := TfgvUnit.Create();
  lcXml.DataSetToXML(CDSChip, 'chip.xml');
  lcXml.Free;
end;

//egyenl�re nem kell....
function TDM.Szinkroniz�l�s_3(AMit: String) : String;
var
  lciniFile : TIniFile;
  lcIPCim, lcUtvonal : String;
  lcXmlSave : THttpsUnit;
  lcXml : TfgvUnit;
begin
  sleep(500);
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcUtvonal := lciniFile.ReadString('utvonal','path','');
  CDSOsztaly.XMLData.Empty;
  CDSPartnerek.First;
  try
    if AMit = 'parameterpartner/' then
      begin
        while not CDSPartnerek.Eof do
          begin
            lcXmlSave := THttpsUnit.Create();
            lcXml := TfgvUnit.Create();
            CDSOsztaly.Active := false;
            CDSOsztaly.XMLData := lcXmlSave.GetHttps(lcIPCim,lcUtvonal,AMit, CDSPartnerek.FieldByName('par_id').AsInteger);
            CDSOsztaly.Active := true;
            lcUtvonal := GetCurrentDir;
            lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\parameterek.xml');
            result := 'Sikeres!';
            CDSPartnerek.Next;
          end;
      end;
  except
     on E : Exception do
       begin
         result := 'Sikertelen!';
       end;
   end;
end;

function TDM.Szinkronizalas_2(AMit: string) : String;
var
  lciniFile : TIniFile;
  lcIPCim, lcUtvonal : String;
  lcXmlSave : THttpsUnit;
  lcXml : TfgvUnit;
  lcRequestPath: string;
begin
  sleep(500);
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcRequestPath := lciniFile.ReadString('utvonal','path','');
  try
    lcXmlSave := THttpsUnit.Create();
    lcXml := TfgvUnit.Create();
    CDSOsztaly.Active := false;
    CDSOsztaly.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
    CDSOsztaly.Active := true;
    lcUtvonal := GetCurrentDir;
    if AMit = 'partnerek/' then
      begin
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\partnerek2.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'mozgasnem/' then
      begin
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\mozgasnem.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'mozgsorszam/' then
      begin
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\sorszam.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'chippartner/' then
      begin
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\chip.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'meretek/' then
      begin
        CDSTextilMeret.Active := false;
        CDSTextilMeret.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSTextilMeret.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSTextilMeret, lcUtvonal+'\textil_meret.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'magassag/' then
      begin
        CDSTextilMagassag.Active := false;
        CDSTextilMagassag.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSTextilMagassag.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSTextilMagassag, lcUtvonal+'\textil_magassag.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'textilallapot/' then
      begin
        CDSTextilAllapot.Active := false;
        CDSTextilAllapot.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSTextilAllapot.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSTextilAllapot, lcUtvonal+'\textil_allapot.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'textilfajta/' then
      begin
        CDSTextilFajta.Active := false;
        CDSTextilFajta.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSTextilFajta.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSTextilFajta, lcUtvonal+'\textil_fajta.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'chippuffer/' then
      begin
        CDSChipPuffer.Active := false;
        CDSChipPuffer.XMLData := lcXmlSave.GetHttps_2(lcIPCim, lcRequestPath,AMit);
        CDSChipPuffer.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSChipPuffer, lcUtvonal+'\chippuffer.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'prdarabosdb/' then
      begin
        CDSDarabosPuffer.Active := false;
        CDSDarabosPuffer.XMLData := lcXmlSave.GetHttps_2(lcIPCim, lcRequestPath,AMit);
        CDSDarabosPuffer.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSDarabosPuffer, lcUtvonal+'\darabospuffer.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'kivezetesoka/' then
      begin
        CDSKivetelezesOka.Active := false;
        CDSKivetelezesOka.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSKivetelezesOka.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSKivetelezesOka, lcUtvonal+'\kivetelezesoka.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'uzemek/' then
      begin
        CDSUzemek.Active := false;
        CDSUzemek.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSUzemek.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSUzemek, lcUtvonal+'\uzemek.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'pkkszamok/' then
      begin
        CDSPKKSzamok.Active := false;
        CDSPKKSzamok.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSPKKSzamok.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSPKKSzamok, lcUtvonal+'\pkkszamok.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'pkkszallitok/' then
      begin
        CDSPKKSzallitok.Active := false;
        CDSPKKSzallitok.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSPKKSzallitok.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSPKKSzallitok, lcUtvonal+'\pkkszallitok.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'pkkchipmozgas/' then
      begin
        CDSPKKChipmozgas.Active := false;
        CDSPKKChipmozgas.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSPKKChipmozgas.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSPKKChipmozgas, lcUtvonal+'\pkkchipmozgas.xml');
        result := 'Sikeres!';
      end;
    if AMit = 'uzemekpartnerei/' then
      begin
        CDSUzemPartner.Active := false;
        CDSUzemPartner.XMLData := lcXmlSave.GetHttps_2(lcIPCim,lcRequestPath,AMit);
        CDSUzemPartner.Active := true;
        lcUtvonal := GetCurrentDir;
        lcXml.DataSetToXML(CDSUzemPartner, lcUtvonal+'\uzem_partner.xml');
        result := 'Sikeres!';
      end;

  except
     on E : Exception do
       begin
        //raise Exception.Create(Exception(ExceptObject).Message);
        result := 'Sikertelen!';

        end;
   end;
end;

function TDM.Szinkronizalas(AMit: string) : String;
var
  lcXmlSave, lcXmlPost, lcTiszteGet, lcTisztaPut : THttpsUnit;
  lcIPCim, lcUtvonal, lcTabla, lcMappa, lcServerUt, lcFajl, lcEredmeny, lcUtvonalMent : String;
  lciniFile : TIniFile;
  lcXml, lcEllenorzes, lcS, lcDataSetToXML, lcPartnerMappa, lcMappaCreate : TfgvUnit;
  lcSearchResult : TSearchRec;
  lcDatumSzinkron : TDate;
  lcI, lcFajlTalalt : Integer;
  lcResponse, lcRespFajln, lcSzallFej  : String;
begin
  sleep(500);
  lcUtvonalMent := GetCurrentDir;
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcServerUt := lciniFile.ReadString('utvonal','path','');
  try
    lcXmlSave := THttpsUnit.Create();
    lcXml := TfgvUnit.Create();
//    CDSOsztaly.Active := false;
//    CDSOsztaly.XMLData := lcXmlSave.GetHttps(lcIPCim,lcUtvonal,AMit,GPartner);
//    CDSOsztaly.Active := true;
    lcUtvonal := GetCurrentDir + '\torzs\';
    lcMappa := 'torzs_' + IntToStr(GPartner);
    if AMit = 'osztalyokpartner/' then
      begin
        CDSOsztaly.Active := false;
        CDSOsztaly.XMLData := lcXmlSave.GetHttps(lcIPCim,lcServerUt,AMit,GPartner);
        CDSOsztaly.Active := true;
        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
            lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\osztaly.xml');
            result := 'Sikeres!';
          end
        else
          begin
             if CreateDir(GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner)) then
               begin
                 lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+lcMappa+'\osztaly.xml');
                 result := 'Sikeres!';
               end
             else
               begin
                 result := 'Sikertelen';
               end;
          end;
        lcEllenorzes.Free;
      end
    else if AMit = 'textilszinek/' then
      begin
        CDSTextilSzinek.Active := false;
        CDSTextilSzinek.XMLData := lcXmlSave.GetHttps(lcIPCim,lcServerUt,AMit,GPartner);
        CDSTextilSzinek.Active := true;

        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
            lcXml.DataSetToXML(CDSTextilSzinek, lcUtvonal+'\textil_szinek.xml');
            result := 'Sikeres!';
           end
        else
          begin
            if CreateDir(GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner)) then
              begin
                lcXml.DataSetToXML(CDSTextilSzinek, lcUtvonal+lcMappa+'\textil_szinek.xml');
                result := 'Sikeres!';
              end
            else
              begin
                result := 'Sikertelen!';
              end;
          end;
        lcEllenorzes.Free;
      end
    else if AMit = 'textilekpartner/' then
      begin
        CDSOsztaly.Active := false;
        CDSOsztaly.XMLData := lcXmlSave.GetHttps(lcIPCim,lcServerUt,AMit,GPartner);
        CDSOsztaly.Active := true;

        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
            lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\textil.xml');
            result := 'Sikeres!';
           end
        else
          begin
            if CreateDir(GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner)) then
              begin
                lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+lcMappa+'\textil.xml');
                result := 'Sikeres!';
              end
            else
              begin
                result := 'Sikertelen!';
              end;
          end;
        lcEllenorzes.Free;
      end
    else if Amit = 'naptarpartner/' then
      begin
        CDSOsztaly.Active := false;
        CDSOsztaly.XMLData := lcXmlSave.GetHttps(lcIPCim,lcServerUt,AMit,GPartner);
        CDSOsztaly.Active := true;

        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
            lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+'\naptar.xml');
            result := 'Sikeres!';
          end
        else
          begin
            if CreateDir(GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner)) then
              begin
                lcXml.DataSetToXML(CDSOsztaly, lcUtvonal+lcMappa+'\naptar.xml');
                result := 'Sikeres!';
              end
            else
              begin
                result := 'Sikertelen!';
              end;
          end;
        lcEllenorzes.Free;
      end
    else if Amit = 'feltoltottfajl/osszerendeles' then   //�sszerndel�s
      begin
        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
            SetCurrentDir(lcUtvonal);
            lcS := TfgvUnit.Create;
            for lcI := -10 to 0 do
              begin
                lcDatumSzinkron := incDay(now(),lcI);
                lcFajl := lcUtvonal+ '\' + 'ossze_'+lcS.Mappa(lcDatumSzinkron)+'*' + '.xml';
                lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
                While lcFajlTalalt = 0 do
                  begin
                    //Ha tal�lunk a vizsg�lt napon f�jl, akkor lek�rdezz�k, hogy fel van-e t�ltve.
                    lcEredmeny := lcXmlSave.GetHttpsFajlok(lcIPCim,lcServerUt,AMit,lcSearchResult.Name,GUserID);
                    lcEredmeny := Copy(lcEredmeny, lcEredmeny.Length, lcEredmeny.Length);
                    if lcEredmeny <> '' then
                      begin
                        if (StrToInt(lcEredmeny)) = 0 then
                          begin
                            try
                              lcXmlPost := THttpsUnit.Create();
                              if lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'osszerendel/' ) <> '' then
                                begin
                                  result := 'Sikeres!';
                                end
                              else
                                begin
                                  result := 'Sikertelen!';
                                end;
                            except
                              on E : Exception do
                               begin
                                 result := 'Hiba �sszerendel�s!';
                               end;
                            end;
                          end;
                      end;
                    lcFajlTalalt := FindNext(lcSearchResult);
                  end;
              end;
            SetCurrentDir(lcUtvonalMent);
            result := 'Sikeres!';
          end
        else
          begin
            result := 'Sikertelen!';
          end;
        lcEllenorzes.Free;
      end
    else if Amit = 'feltoltottfajl/mozgas' then //Sz�ll�t�lev�l
      begin
        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\adat\' + 'adat_' + IntToStr(GPartner);
            SetCurrentDir(lcUtvonal);
            lcS := TfgvUnit.Create;
            for lcI := -10 to 0 do
              begin
                lcDatumSzinkron := incDay(now(),lcI);
                lcFajl := lcUtvonal+ '\' +lcS.Mappa(lcDatumSzinkron)+ '\' + 'S'+lcS.Mappa(lcDatumSzinkron)+'*' + '.xml';
                SetCurrentDir(lcUtvonal+'\' +lcS.Mappa(lcDatumSzinkron)+ '\');
                lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
                While lcFajlTalalt = 0 do
                  begin
                    //Ha tal�lunk a vizsg�lt napon f�jl, akkor lek�rdezz�k, hogy fel van-e t�ltve.
                    lcEredmeny := lcXmlSave.GetHttpsFajlok(lcIPCim,lcServerUt,AMit,lcSearchResult.Name,GUserID);
                    lcEredmeny := Copy(lcEredmeny, lcEredmeny.Length, lcEredmeny.Length);
                    if lcEredmeny <> '' then
                      begin
                        if (StrToInt(lcEredmeny)) = 0 then
                          begin
                            try
                              lcXmlPost := THttpsUnit.Create();
                              lcSzallFej := lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'szallitolevel/' );
                              if lcSzallFej <> '' then
                                begin
                                 //Bet�ltj�k a f�jlt egy kliensdatasetbe.
                                  DM.CDSTisztaEllenorzes.LoadFromFile(lcSearchResult.Name);
                                  DM.CDSTisztaEllenorzes.LogChanges := false;
                                  DM.CDSTisztaEllenorzes.First;
                                  while not DM.CDSTisztaEllenorzes.Eof do
                                    begin
                                      DM.CDSTisztaEllenorzes.Edit;
                                      DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger := StrToInt(lcSzallFej);
                                      DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger := 1;
                                      DM.CDSTisztaEllenorzes.Post;
                                      DM.CDSTisztaEllenorzes.Next;
                                    end;
                                  lcXml.DataSetToXML(DM.CDSTisztaEllenorzes,lcSearchResult.Name);
                                  result := 'Sikeres!';
                                end
                              else
                                begin
                                  result := 'Sikertelen!';
                                end;
                            except
                              on E : Exception do
                               begin
                                 result := 'Hiba szennyes sz�ll�t�lev�l!';
                               end;
                            end;
                          end;
                      end;
                    lcFajlTalalt := FindNext(lcSearchResult);
                  end;
              end;
            for lcI := -10 to 10 do
              begin
                lcDatumSzinkron := incDay(now(),lcI);
                lcFajl := lcUtvonal+ '\' +lcS.Mappa(lcDatumSzinkron)+ '\' + 'T'+lcS.Mappa(lcDatumSzinkron)+'*' + '.xml';
                SetCurrentDir(lcUtvonal+'\' +lcS.Mappa(lcDatumSzinkron)+ '\');
                lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
                While lcFajlTalalt = 0 do
                  begin
                    //Bet�ltj�k a f�jlt egy kliensdatasetbe.
                    DM.CDSTisztaEllenorzes.LoadFromFile(lcSearchResult.Name);
                    DM.CDSTisztaEllenorzes.LogChanges := false;
                    DM.CDSTisztaEllenorzes.First;
                    DM.CDSParameterek.First;
                    while not DM.CDSParameterek.Eof do
                      begin
                        if DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner then
                          begin
                            break;
                          end;
                        DM.CDSParameterek.Next;
                      end;
                    //Megvizsg�ljuk van-e id-ink.
                    if //(not (DM.CDSTisztaEllenorzes.FieldByName('szfej_id').IsNull) and (DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger = 1)) OR
                     ((DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger <> 0 ) and (DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger = 1)) then
                      begin
                        //Ha van id, akkor lek�rdezz�k fel van-e t�ltve.   GetTisztaSzallito
                        lcResponse := '';
                        lcTiszteGet := THttpsUnit.Create();
                        lcResponse := lcTiszteGet.GetTisztaSzallito(lcIPCim,lcServerUt,'tisztaszfajl',DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger);
                        lcRespFajln := copy(lcResponse,(pos(':',lcResponse)+1),lcResponse.Length);
                        if (copy(lcResponse,1,1) <> '0') and (lcRespFajln = '0') then
                          begin
                            //Tiszta update;
                            try
                              lcTisztaPut := THttpsUnit.Create();
                              if lcTisztaPut.TisztaPutHttps(lcIPCim, lcServerUt,'tisztaszlev', DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger, DM.CDSTisztaEllenorzes.XMLData) <> '' then
                                begin
                                  result := 'Sikeres!';
                                end
                              else
                                begin
                                  result := 'Sikertelen!';
                                end;
                            except
                              on E : Exception do
                               begin
                                 result := 'Hiba Tiszta sz�ll�t�lev�l update!';
                               end;
                            end;
                          end;
                      end
                    else if //((DM.CDSTisztaEllenorzes.FieldByName('szfej_id').IsNull) and (DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger = 1)) OR
                     ((DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger = 0 ) and (DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger = 1)) then
                      begin
                        try
                          //Nincs id a f�jlban, post!
                          lcXmlPost := THttpsUnit.Create();
                          lcRespFajln := ''; //Ebbe ker�l a szfej_id;
                          lcRespFajln := lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'szallitolevel/' );
                          if lcRespFajln <> '' then
                            begin
                              while not DM.CDSTisztaEllenorzes.Eof do
                                begin
                                  DM.CDSTisztaEllenorzes.Edit;
                                  DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger := StrToInt(lcRespFajln);
                                  DM.CDSTisztaEllenorzes.Post;
                                  DM.CDSTisztaEllenorzes.Next;
                                end;
                              lcXml.DataSetToXML(DM.CDSTisztaEllenorzes,lcSearchResult.Name);
                              result := 'Sikeres!';
                            end
                         else if lcRespFajln = '0' then
                            begin
                              result := 'Sikertelen!';
                             end;
                        except
                          on E : Exception do
                           begin
                             result := 'Hiba Tiszta sz�ll�t�lev�l!';
                           end;
                        end;
                      end
                     else if //((DM.CDSTisztaEllenorzes.FieldByName('szfej_id').IsNull) and (DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger = 1)) OR
                     ((DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger = 0 ) and (DM.CDSTisztaEllenorzes.FieldByName('szfej_lezart').AsInteger = 0) and
                       (DM.CDSParameterek.FieldByName('prm_tisztagenpart').AsString = 'Automatikus')) then
                      begin
                        try
                          //Nincs id a f�jlban, post!
                          lcXmlPost := THttpsUnit.Create();
                          lcRespFajln := ''; //Ebbe ker�l a szfej_id;
                          lcRespFajln := lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'szallitolevel/' );
                          if lcRespFajln <> '' then
                            begin
                              while not DM.CDSTisztaEllenorzes.Eof do
                                begin
                                  DM.CDSTisztaEllenorzes.Edit;
                                  DM.CDSTisztaEllenorzes.FieldByName('szfej_id').AsInteger := StrToInt(lcRespFajln);
                                  DM.CDSTisztaEllenorzes.Post;
                                  DM.CDSTisztaEllenorzes.Next;
                                end;
                              lcXml.DataSetToXML(DM.CDSTisztaEllenorzes,lcSearchResult.Name);
                              result := 'Sikeres!';
                            end
                         else if lcRespFajln = '0' then
                            begin
                              result := 'Sikertelen!';
                             end;
                        except
                          on E : Exception do
                           begin
                             result := 'Hiba Tiszta sz�ll�t�lev�l!';
                           end;
                        end;
                      end;

                    //Ha tal�lunk a vizsg�lt napon f�jl, akkor lek�rdezz�k, hogy fel van-e t�ltve.
             {       lcEredmeny := lcXmlSave.GetHttpsFajlok(lcIPCim,lcServerUt,AMit,lcSearchResult.Name,GUserID);
                    lcEredmeny := Copy(lcEredmeny, lcEredmeny.Length, lcEredmeny.Length);
                   // ShowMessage(lcSearchResult.Name + ' - ' + lcEredmeny);
                    if lcEredmeny <> '' then
                      begin
                        if (StrToInt(lcEredmeny)) = 0 then
                          begin
                            try
                              lcXmlPost := THttpsUnit.Create();
                              if lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'szallitolevel/' ) <> '' then
                                begin
                                  result := 'Sikeres!';
                                end
                              else
                                begin
                                  result := 'Sikertelen!';
                                end;
                            except
                              on E : Exception do
                               begin
                                 result := 'Hiba Tiszta sz�ll�t�lev�l!';
                               end;
                            end;
                          end;
                      end;      }
                    lcFajlTalalt := FindNext(lcSearchResult);
                  end;
              end;

            SetCurrentDir(lcUtvonalMent);
            result := 'Sikeres!';
          end
        else
          begin
            result := 'Sikertelen!';
          end;
        lcEllenorzes.Free;
      end
    else if Amit = 'feltoltottfajl/chipmozgasell' then //Sz�ll�t�lev�l
      begin
        lcEllenorzes := TfgvUnit.Create();
        if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
          begin
            lcUtvonal := GetCurrentDir + '\adat\' + 'adat_' + IntToStr(GPartner);
            SetCurrentDir(lcUtvonal);
            lcS := TfgvUnit.Create;
            for lcI := -10 to 0 do
              begin
                lcDatumSzinkron := incDay(now(),lcI);
                lcFajl := lcUtvonal+ '\' +lcS.Mappa(lcDatumSzinkron)+ '\' + 'SM'+lcS.Mappa(lcDatumSzinkron)+'*' + '.xml';
                SetCurrentDir(lcUtvonal+'\' +lcS.Mappa(lcDatumSzinkron)+ '\');
                lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
                While lcFajlTalalt = 0 do
                  begin
                    //Ha tal�lunk a vizsg�lt napon f�jl, akkor lek�rdezz�k, hogy fel van-e t�ltve.
                    lcEredmeny := lcXmlSave.GetHttpsFajlok(lcIPCim,lcServerUt,AMit,lcSearchResult.Name,GUserID);
                    lcEredmeny := Copy(lcEredmeny, lcEredmeny.Length, lcEredmeny.Length);
                    if lcEredmeny <> '' then
                      begin
                        if (StrToInt(lcEredmeny)) = 0 then
                          begin
                            try
                              lcXmlPost := THttpsUnit.Create();
                              if lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'chipmozgas/' ) <> '' then
                                begin
                                  result := 'Sikeres!';
                                end
                              else
                                begin
                                  result := 'Sikertelen!';
                                end;
                            except
                              on E : Exception do
                               begin
                                 result := 'Hiba �sszerendel�s!';
                               end;
                            end;
                          end;
                      end;
                    lcFajlTalalt := FindNext(lcSearchResult);
                  end;
              end;
            for lcI := -10 to 10 do
              begin
                lcDatumSzinkron := incDay(now(),lcI);
                lcFajl := lcUtvonal+ '\' +lcS.Mappa(lcDatumSzinkron)+ '\' + 'TM'+lcS.Mappa(lcDatumSzinkron)+'*' + '.xml';
                SetCurrentDir(lcUtvonal+'\' +lcS.Mappa(lcDatumSzinkron)+ '\');
                lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
                While lcFajlTalalt = 0 do
                  begin
                    //Ha tal�lunk a vizsg�lt napon f�jl, akkor lek�rdezz�k, hogy fel van-e t�ltve.
                    lcEredmeny := lcXmlSave.GetHttpsFajlok(lcIPCim,lcServerUt,AMit,lcSearchResult.Name,GUserID);
                    lcEredmeny := Copy(lcEredmeny, lcEredmeny.Length, lcEredmeny.Length);
                    if lcEredmeny <> '' then
                      begin
                        if (StrToInt(lcEredmeny)) = 0 then
                          begin
                            try
                              lcXmlPost := THttpsUnit.Create();
                              if lcXmlPost.PostHttps(lcSearchResult.Name, lcIPCim, lcServerUt,'chipmozgas/' ) <> '' then
                                begin
                                  result := 'Sikeres!';
                                end
                              else
                                begin
                                  result := 'Sikertelen!';
                                end;
                            except
                              on E : Exception do
                               begin
                                 result := 'Hiba tiszta mozg�s!';
                               end;
                            end;
                          end;
                      end;
                    lcFajlTalalt := FindNext(lcSearchResult);
                  end;
              end;
            SetCurrentDir(lcUtvonalMent);
            result := 'Sikeres!';
          end
        else
          begin
            result := 'Sikertelen!';
          end;
        lcEllenorzes.Free;
      end;
  except
     on E : Exception do
       begin
//        raise Exception.Create(Exception(ExceptObject).Message);
          SetCurrentDir(lcUtvonalMent);
          result := 'Sikertelen!';
        end;
   end;
  lcXml.Free;
  SetCurrentDir(lcUtvonalMent);
end;

function TDM.Szinkronizalas_puffer_raktar(AMit: string; APartner: integer): string;

  function FindInFile(AFileName, AHaystack: string): boolean;
  var lcSl: TStringList;
      i: integer;
  begin
    result := false;
    lcSl := TStringList.Create;
    lcSl.LoadFromFile(AFileName);
    for i := 0 to lcSl.Count-1 do
      begin
        if pos(AHaystack, lcSl[i]) <> 0 then
          begin
            result := true;
            exit;
          end;
      end;
  end;

var lcFgv: TfgvUnit;
    lcIniFile : TIniFile;
    lcIPCim, lcUtvonal : String;
    lcAdatMappa: string;
    lcTorzsMappa: string;
    lcDatumSzinkron: TDate;
    lcFajl: String;
    lcFajlTalalat, lcMappaTalalat: integer;
    lcSearchResult, lcDirSearchResult: TSearchRec;
    lcHttpsRequest: THttpsUnit;
    lcHttpsResult: string;
    lcDataset: TClientDataSet;
    lcCurrentDir: string;
    i: integer;
    lcTmpId: string;
    lcPufferPartnerMappa: string;
begin
  lcFgv := TfgvUnit.Create;
  lcDataset := TClientDataSet.Create(Application);
  lcHttpsRequest := THttpsUnit.Create;
  lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcUtvonal := lciniFile.ReadString('utvonal','path','');
  lcCurrentDir := GetCurrentDir;
  lcAdatMappa := GetCurrentDir + '\adat\adat_'+IntToStr(APartner);
  lcTorzsMappa := GetCurrentDir + '\torzs\torzs_'+IntToStr(APartner);
  lcPufferPartnerMappa := GetCurrentDir + '\adat\';
  if DirectoryExists(lcAdatMappa) then
    begin
      setCurrentDir(lcAdatMappa);
    end
  else
    begin
      result := 'Sikeres!';
      exit;
    end;
  try
    if (AMit = GWHazUrls.pufBevRakXMLUpload) then
      begin
        for i := -10 to 0 do
          begin
            lcDatumSzinkron := incDay(now(), i);
            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PB'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'pbfej_id="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, AMit);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcTmpId := lcHttpsResult;
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('pbfej_id').AsInteger := StrToInt(lcHttpsResult);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PBM'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('uploaded').AsInteger := 1;
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

          end;
      end
    else if (AMit = GWHazUrls.pufKiadRakXMLUpload) then
      begin
        for i := -10 to 0 do
          begin
            lcDatumSzinkron := incDay(now(), i);
            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PK'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'pbfej_id="-1"') then
                  begin
                     lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcTmpId := lcHttpsResult;
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('pbfej_id').AsInteger := StrToInt(lcHttpsResult);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKM'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('uploaded').AsInteger := StrToInt(lcTmpId);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKT'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'szfej_id="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, 'szallitolevel/');
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('szfej_id').AsInteger := StrToInt(lcHttpsResult);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKTM'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, 'chipmozgas/');
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('uploaded').AsInteger := 1;
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            setCurrentDir(lcCurrentDir);
            setCurrentDir(lcTorzsMappa);
            lcFajl := lcTorzsMappa + '\' + 'p_ossze_*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                    begin
                      lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufOsszerendXMLUpload);
                      if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                        begin
                          lcDataset.LoadFromFile(lcSearchResult.Name);
                          lcDataset.LogChanges := false;
                          lcDataset.First;
                          while not lcDataset.Eof do
                            begin
                              lcDataset.Edit;
                              lcDataset.FieldByName('uploaded').AsInteger := 1;
                              lcDataset.Post;
                              lcDataset.Next;
                            end;
                          lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                          RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                          Result := 'Sikeres';
                        end;
                    end
                  else
                    begin
                      RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                    end;
                lcFajlTalalat := FindNext(lcSearchResult);
              end;
          end;
      end
    else if (AMit = GWHazUrls.pufVisszaXMLUpload) then
      begin
        for i := -10 to 0 do
          begin
            lcDatumSzinkron := incDay(now(), i);
            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PV'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'pbfej_id="-1"') then
                  begin
                   // lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, AMit);
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcTmpId := lcHttpsResult;
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('pbfej_id').AsInteger := StrToInt(lcHttpsResult);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PVM'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('uploaded').AsInteger := 1;
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

          end;
      end
    else if (AMit = GWHazUrls.pufMoveRakXMLUpload) then
      begin
        for i := -10 to 0 do
          begin
            lcDatumSzinkron := incDay(now(), i);
            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKK'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'pbfej_id="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcTmpId := lcHttpsResult;
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('pbfej_id').AsInteger := StrToInt(lcHttpsResult);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKKM'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('uploaded').AsInteger := 1;
                            if lcTmpId <> '' then
                              lcDataset.FieldByName('prszall_fej_id').AsInteger := StrToInt(lcTmpId);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

          lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKB'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'pbfej_id="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcTmpId := lcHttpsResult;
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('pbfej_id').AsInteger := StrToInt(lcHttpsResult);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

            lcFajl := lcAdatMappa + '\' +Trim(lcFgv.Mappa(lcDatumSzinkron))+ '\' + 'PKBM'+Trim(lcFgv.Mappa(lcDatumSzinkron))+'*' + '.xml';
            lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcSearchResult);
            While lcFajlTalalat = 0 do
              begin
                setCurrentDir(lcAdatMappa + '\' + Trim(lcFgv.Mappa(lcDatumSzinkron)));
                if FindInFile(lcSearchResult.Name, 'uploaded="-1"') then
                  begin
                    lcHttpsResult := lcHttpsRequest.PostHttps(lcSearchResult.Name, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                    if (lcHttpsResult <> '') and lcFgv.IsNumber(lcHttpsResult) then
                      begin
                        lcDataset.LoadFromFile(lcSearchResult.Name);
                        lcDataset.LogChanges := false;
                        lcDataset.First;
                        while not lcDataset.Eof do
                          begin
                            lcDataset.Edit;
                            lcDataset.FieldByName('uploaded').AsInteger := 1;
                            if lcTmpId <> '' then
                              lcDataset.FieldByName('prszall_fej_id').AsInteger := StrToInt(lcTmpId);
                            lcDataset.Post;
                            lcDataset.Next;
                          end;
                        lcFgv.DataSetToXML(lcDataset, lcSearchResult.Name);
                        RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                        Result := 'Sikeres';
                      end;
                  end
                else
                  begin
                    RenameFile(lcSearchResult.Name, '_uploaded_'+lcSearchResult.Name);
                  end;

                setCurrentDir(lcAdatMappa);
                lcFajlTalalat := FindNext(lcSearchResult);
              end;

          end;
      end;
  except
    on E: Exception do
      begin
        result := 'Sikertelen';
      end;
  end;
  setCurrentDir(lcCurrentDir);
  lcDataset.Destroy;
  lcHttpsRequest.Destroy;
  lcFgv.Destroy;
  result := '';
end;

procedure TDM.LoadSynchChip;

  function FindInFile(AFileName, AHaystack: string): boolean;
  var lcSl: TStringList;
      i: integer;
  begin
    result := false;
    lcSl := TStringList.Create;
    lcSl.LoadFromFile(AFileName);
    for i := 0 to lcSl.Count-1 do
      begin
        if pos(AHaystack, lcSl[i]) <> 0 then
          begin
            result := true;
            exit;
          end;
      end;
  end;

var lcFajl: string;
    lcFajlTalalat: integer;
    lcMappa: string;
    lcMappaTalalat: integer;
    lcDirSearchRes, lcFileSearchRes: TSearchRec;
    lcAdatMappa: string;
    lcCurrentDir: string;
    lcSzinkronDatum: TDate;
    lcFgv: TfgvUnit;
    i: integer;
    lcDataset: TClientDataSet;
begin
  lcFgv := TfgvUnit.Create;
  lcDataset := TClientDataSet.Create(Application);
  lcCurrentDir := GetCurrentDir;
  lcAdatMappa := GetCurrentDir + '\adat\';
  FDMemTablePufferRaktarSynchs.EmptyDataSet;
  FDMemTablePufferRaktarSynchs.Close;
  FDMemTablePufferRaktarSynchs.Open;
  if DirectoryExists(lcAdatMappa) then
    begin
      setCurrentDir(lcAdatMappa);
      lcMappa := 'adat_*';
      lcMappaTalalat := FindFirst(lcMappa, faDirectory, lcDirSearchRes);
      while lcMappaTalalat = 0 do
        begin
          SetCurrentDir(GetCurrentDir + '\' + lcDirSearchRes.Name);
          for i := -10 to 0 do
            begin
              lcSzinkronDatum := incDay(now(), i);
              if DirectoryExists(Trim(lcFgv.Mappa(lcSzinkronDatum))) then
                begin
                  SetCurrentDir(GetCurrentDir + '\' + Trim(lcFgv.Mappa(lcSzinkronDatum)));
                  // Bev�telez�sek
                  lcFajl := lcAdatMappa + lcDirSearchRes.Name +'\'+ Trim(lcFgv.Mappa(lcSzinkronDatum)) + '\PBM'+Trim(lcFgv.Mappa(lcSzinkronDatum))+'*.xml';
                  lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcFileSearchRes);
                  while lcFajlTalalat = 0 do
                    begin
                      if FindInFile(lcFileSearchRes.Name, 'prszall_fej_id="-1"') then
                        begin
                          lcDataset.Close;
                          lcDataset.LoadFromFile(lcFileSearchRes.Name);
                          lcDataset.LogChanges := false;
                          lcDataset.First;
                          while not lcDataset.eof do
                            begin
                              if lcDataset.FieldByName('chipkod').AsString <> '' then
                                begin
                                  FDMemTablePufferRaktarSynchs.Insert;
                                  FDMemTablePufferRaktarSynchs.FieldByName('chip_kod').AsString := lcDataset.FieldByName('chipkod').AsString;
                                  FDMemTablePufferRaktarSynchs.Post;
                                end;
                              lcDataset.Next;
                            end;
                        end;
                      lcFajlTalalat := FindNext(lcFileSearchRes);
                    end;
                  // Kiad�sok
                  lcFajl := lcAdatMappa + lcDirSearchRes.Name +'\'+ Trim(lcFgv.Mappa(lcSzinkronDatum)) + '\PKM'+Trim(lcFgv.Mappa(lcSzinkronDatum))+'*.xml';
                  lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcFileSearchRes);
                  while lcFajlTalalat = 0 do
                    begin
                      if FindInFile(lcFileSearchRes.Name, 'prszall_fej_id="-1"') then
                        begin
                          lcDataset.Close;
                          lcDataset.LoadFromFile(lcFileSearchRes.Name);
                          lcDataset.LogChanges := false;
                          lcDataset.First;
                          while not lcDataset.eof do
                            begin
                              if lcDataset.FieldByName('chipkod').AsString <> '' then
                                begin
                                  FDMemTablePufferKiadSynchs.Insert;
                                  FDMemTablePufferKiadSynchs.FieldByName('chip_kod').AsString := lcDataset.FieldByName('chipkod').AsString;
                                  FDMemTablePufferKiadSynchs.Post;
                                end;
                              lcDataset.Next;
                            end;
                        end;
                      lcFajlTalalat := FindNext(lcFileSearchRes);
                    end;
                  // Visszav�telez�sek
                  lcFajl := lcAdatMappa + lcDirSearchRes.Name +'\'+ Trim(lcFgv.Mappa(lcSzinkronDatum)) + '\PVM'+Trim(lcFgv.Mappa(lcSzinkronDatum))+'*.xml';
                  lcFajlTalalat := FindFirst(lcFajl, faAnyFile, lcFileSearchRes);
                  while lcFajlTalalat = 0 do
                    begin
                      if FindInFile(lcFileSearchRes.Name, 'prszall_fej_id="-1"') then
                        begin
                          lcDataset.Close;
                          lcDataset.LoadFromFile(lcFileSearchRes.Name);
                          lcDataset.LogChanges := false;
                          lcDataset.First;
                          while not lcDataset.eof do
                            begin
                              if lcDataset.FieldByName('chipkod').AsString <> '' then
                                begin
                                  FDMemTablePufferVisszavetSynchs.Insert;
                                  FDMemTablePufferVisszavetSynchs.FieldByName('chip_kod').AsString := lcDataset.FieldByName('chipkod').AsString;
                                  FDMemTablePufferVisszavetSynchs.Post;
                                end;
                              lcDataset.Next;
                            end;
                        end;
                      lcFajlTalalat := FindNext(lcFileSearchRes);
                    end;
                end;
            end;

          SetCurrentDir(lcAdatMappa);
          lcMappaTalalat := FindNext(lcDirSearchRes);
        end;
    end;
  SetCurrentDir(lcCurrentDir);
  lcDataset.Destroy;
  lcFgv.Destroy;
end;

procedure TDM.tInternetTimer(Sender: TObject);
begin
 if InternetEll then
    begin
      GInternet := true;
    end
  else
    begin
      GInternet := false;
    end;
end;

end.
