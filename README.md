delphi http haszn�lata: https://pythonhosted.org/TGWebServices/examples/delphi.html
---
http://docwiki.embarcadero.com/CodeExamples/Tokyo/en/HTTP_Get_(Delphi)
---

### Textil lek�rdez�se sql ###

~~~~
SELECT kpt_id, kpt_textilId, kpt_partnerId, kpt_textilkod, kpt_textsuly, 
	   kpt_texcsop, kpt_texora, kpt_texdbkg, kpt_texberelt, kpt_tersuly, kpt_term, 
	   kpt_texchip, kpt_texar, kpt_texallv, kpt_texmos, kpt_keszlet, kpt_lastuser, 
	   kpt_lastup, kpt_delete, kpt_uzem, kpt_smegnev, kpt_szamlaegyseg, kpt_szamlazando 
	FROM kap_par_textil 
	WHERE kpt_delete=0 AND kpt_partnerId=?
~~~~
---	

## Milyen xml f�jlokat kell partnerenk�nt l�trehozni a telephelyre vagy a rakt�ri g�pre ##

1. felhasznalo.xml
2. mozgasnem.xml
3. parameterek.xml
4. partnerek.xml
5. uzem.xml

## Tiszta gener�l�s partnern�l ##
- Nincs
- Manu�lis
- Automatikus

## Tiszta gener�l�s �zemben ##
- Nincs
- Manu�lis
- Automatikus

## Tiszta gener�l�s tipusa ##
- Oszt�lyos
- K�rh�zi

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact