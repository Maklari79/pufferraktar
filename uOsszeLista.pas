unit uOsszeLista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Vcl.ComCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Datasnap.DBClient, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfrmOsszeLista = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    btnBezar: TButton;
    iListgomb: TImageList;
    gbOpciok: TGroupBox;
    gbOsszeList: TGroupBox;
    DBGrid1: TDBGrid;
    cdsOsszList: TClientDataSet;
    dsOsszList: TDataSource;
    dbgFajlok: TDBGrid;
    dsFajl: TDataSource;
    fdmFajl: TFDMemTable;
    fdmFajlFajlNev: TStringField;
    cdsOsszeLoad: TClientDataSet;
    procedure FormShow(Sender: TObject);
    procedure btnBezarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgFajlokCellClick(Column: TColumn);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOsszeLista: TfrmOsszeLista;

implementation

uses uFgv, uDM, uTMRMain;

{$R *.dfm}

procedure TfrmOsszeLista.btnBezarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmOsszeLista.dbgFajlokCellClick(Column: TColumn);
var
  lcUtvonal, lcUtMentes, lcMappa : String;
  lcEllenorzes : TfgvUnit;
  lcSearchResult : TSearchRec;
begin
  //Kiv�lasztott f�jl megnyit�sa a gridben.
  lcUtMentes := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
    end;
  SetCurrentDir(lcUtvonal);

  cdsOsszList.FieldDefs.Clear;
  cdsOsszList.Active := false;
  cdsOsszList.FieldDefs.Add('Vonalkod',ftString,24);
  cdsOsszList.FieldDefs.Add('Chipkod',ftString,24);
  cdsOsszList.FieldDefs.Add('Dolgozonev',ftString,100);
  cdsOsszList.FieldDefs.Add('cikknev',ftString,30);
  cdsOsszList.FieldDefs.Add('Datum',ftDateTime);
  cdsOsszList.FieldDefs.Items[0].Size := 15;
  cdsOsszList.FieldDefs.Items[1].Size := 20;
  cdsOsszList.FieldDefs.Items[2].Size := 30;
  cdsOsszList.FieldDefs.Items[3].Size := 15;
  cdsOsszList.CreateDataSet;
  cdsOsszList.FieldByName('Chipkod').DisplayLabel := 'Chipk�d';
  cdsOsszList.FieldByName('Vonalkod').DisplayLabel := 'Vonalk�d';
  cdsOsszList.FieldByName('Datum').DisplayLabel := 'D�tum';
  cdsOsszList.FieldByName('Dolgozonev').DisplayLabel := 'Dolgoz�n�v';
  cdsOsszList.FieldByName('cikknev').DisplayLabel := 'Cikkn�v';

  if FindFirst(dbgFajlok.DataSource.DataSet.FieldByName('FajlNev').AsString, faAnyFile, lcSearchResult) = 0 then
    begin
      cdsOsszeLoad.LoadFromFile(dbgFajlok.DataSource.DataSet.FieldByName('FajlNev').AsString);
      cdsOsszeLoad.LogChanges := false;
      cdsOsszeLoad.First;
      cdsOsszList.Edit;
      while not cdsOsszeLoad.Eof do
        begin
          cdsOsszList.Append;
          cdsOsszList.FieldByName('Chipkod').AsString := cdsOsszeLoad.FieldByName('Chipkod').AsString;
          cdsOsszList.FieldByName('Vonalkod').AsString := cdsOsszeLoad.FieldByName('Vonalkod').AsString;
          cdsOsszList.FieldByName('Datum').AsDateTime := cdsOsszeLoad.FieldByName('Datum').AsDateTime;
          cdsOsszList.Post;
          cdsOsszeLoad.Next;
        end;
    end;

  DM.CDSChip.First;
  cdsOsszList.First;
  while not cdsOsszList.Eof do
    begin
      DM.CDSChip.Filtered := false;
      DM.CDSChip.Filter := 'cms_vonalkod='''+ cdsOsszList.FieldByName('Vonalkod').AsString+'''';
      DM.CDSChip.Filtered := true;
      if DM.CDSChip.RecordCount > 0 then
        begin
          cdsOsszList.Edit;
          cdsOsszList.FieldByName('Dolgozonev').AsString := DM.CDSChip.FieldByName('cms_dolgozonev').AsString;
          cdsOsszList.FieldByName('cikknev').AsString := DM.CDSChip.FieldByName('cms_cikknev').AsString;
          cdsOsszList.Post;
        end;
      cdsOsszList.Next;
      DM.CDSChip.Filtered := false;
      DM.CDSChip.Filter := '';
      DM.CDSChip.Filtered := true;
    end;
  gbOsszeList.Caption := '�sszerendel�s lista ';
  gbOsszeList.Caption :=   gbOsszeList.Caption + ' darab sz�m: '  + IntToStr(cdsOsszList.RecordCount);

  SetCurrentDir(lcUtMentes);
end;

procedure TfrmOsszeLista.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmTMRMain.btnOsszLista.Enabled := true;
  Action := caFree;
end;

procedure TfrmOsszeLista.FormShow(Sender: TObject);
var
  lcUtvonal, lcUtMentes, lcMappa : String;
  lcEllenorzes : TfgvUnit;
  lcSearchResult : TSearchRec;
begin
  lcUtMentes := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
    end;
  SetCurrentDir(lcUtvonal);
  fdmFajl.Open;

  if FindFirst('ossze_*.xml', faAnyFile, lcSearchResult) = 0 then
    repeat
      fdmFajl.Append;
      fdmFajl.FieldByName('FajlNev').AsString := lcSearchResult.Name;
      fdmFajl.Post;
    until FindNext(lcSearchResult) <> 0;

  SetCurrentDir(lcUtMentes);
end;

end.
