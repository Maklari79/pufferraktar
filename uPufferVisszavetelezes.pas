unit uPufferVisszavetelezes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, System.ImageList, Vcl.ImgList,
  Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Vcl.ComCtrls, INIFiles,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient, frxClass, frxDBSet;

type
  TIntegerArray = array of integer;

type
  TfrmPufferVisszavetel = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    gbChipList: TGroupBox;
    mChipList: TMemo;
    gbInfo: TGroupBox;
    lInfoString: TLabel;
    gbTextilList: TGroupBox;
    dbgTextilList: TDBGrid;
    pAlso: TPanel;
    btnChipStart: TButton;
    btnChipEnd: TButton;
    gbOsszLista: TGroupBox;
    ScrollBox1: TScrollBox;
    samplePanel: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    GroupBox1: TGroupBox;
    iPufferVisszavet: TImageList;
    Button2: TButton;
    Button4: TButton;
    FDMemTableVisszavetelezes: TFDMemTable;
    DSVisszavetelezes: TDataSource;
    Button5: TButton;
    FDMemTableVisszavetelezeschipkod: TStringField;
    FDMemTableVisszavetelezesvonalkod: TStringField;
    FDMemTableVisszavetelezestextilid: TIntegerField;
    FDMemTableVisszavetelezestextilnev: TStringField;
    FDMemTableVisszavetelezesfajtaid: TIntegerField;
    FDMemTableVisszavetelezesfajtanev: TStringField;
    FDMemTableVisszavetelezesallapotid: TIntegerField;
    FDMemTableVisszavetelezesallapotnev: TStringField;
    FDMemTableVisszavetelezesmeretid: TIntegerField;
    FDMemTableVisszavetelezesmeretnev: TStringField;
    FDMemTableVisszavetelezesmagassagid: TIntegerField;
    FDMemTableVisszavetelezesmagassagnev: TStringField;
    FDMemTableVisszavetelezesszinid: TIntegerField;
    FDMemTableVisszavetelezesszinnev: TStringField;
    FDMemTableVisszavetelezesdatum: TDateTimeField;
    FDMemTableVisszavetelezesmegjegyzes: TStringField;
    FDMemTableVisszavetelezeskivetelezesokid: TIntegerField;
    FDMemTableVisszavetelezeskivetelezesokanev: TStringField;
    FDMemTablePVXML: TFDMemTable;
    FDMemTablePVXMLpbfej_id: TIntegerField;
    FDMemTablePVXMLpbfej_uzem_id: TIntegerField;
    FDMemTablePVXMLpbfej_partner_id: TIntegerField;
    FDMemTablePVXMLpbfej_osztaly_id: TIntegerField;
    FDMemTablePVXMLpbfej_sorszam: TStringField;
    FDMemTablePVXMLpbfej_mozgas_id: TIntegerField;
    FDMemTablePVXMLpbfej_felhasznalo_id: TIntegerField;
    FDMemTablePVXMLpbfej_megjegyzes: TStringField;
    FDMemTablePVXMLpbfej_create_date: TDateTimeField;
    FDMemTablePVXMLpbfej_create_user: TIntegerField;
    FDMemTablePVXMLpbfej_lastupdate_date: TDateTimeField;
    FDMemTablePVXMLpbfej_lastupdate_user: TIntegerField;
    FDMemTablePVXMLpbfej_selected_date: TDateTimeField;
    FDMemTablePVXMLpbtet_darabszam: TIntegerField;
    FDMemTablePVXMLpbtet_fajta_id: TIntegerField;
    FDMemTablePVXMLpbtet_allapot_id: TIntegerField;
    FDMemTablePVXMLpbtet_meret_id: TIntegerField;
    FDMemTablePVXMLpbtet_magassag_id: TIntegerField;
    FDMemTablePVXMLpbtet_szin_id: TIntegerField;
    FDMemTablePVXMLpbtet_textil_id: TIntegerField;
    FDMemTablePVXMLpbtet_megjegyzes: TStringField;
    FDMemTablePVXMLpbtet_create_date: TDateTimeField;
    FDMemTablePVXMLpbtet_create_user: TIntegerField;
    FDMemTablePVXMLpbtet_lastupdate_date: TDateTimeField;
    FDMemTablePVXMLpbtet_lastupdate_user: TIntegerField;
    FDMemTablePVXMLpbtet_darabosdb: TIntegerField;
    FDMemTablePVXMLpbtet_chiphez_rendelve: TIntegerField;
    FDMemTablePVXMLpbtet_fajlnev: TStringField;
    FDMemTablePVXMLallapotText: TStringField;
    FDMemTablePVXMLfajtaText: TStringField;
    FDMemTablePVXMLszinText: TStringField;
    FDMemTablePVXMLmeretText: TStringField;
    FDMemTablePVXMLmagassagText: TStringField;
    FDMemTablePVXMLmegnevezesText: TStringField;
    FDMemTablePVMXML: TFDMemTable;
    FDMemTablePVMXMLsorszam: TStringField;
    FDMemTablePVMXMLpartner_id: TIntegerField;
    FDMemTablePVMXMLuzem_id: TIntegerField;
    FDMemTablePVMXMLosztaly_id: TIntegerField;
    FDMemTablePVMXMLprszall_fej_id: TIntegerField;
    FDMemTablePVMXMLvonalkod: TStringField;
    FDMemTablePVMXMLchipkod: TStringField;
    FDMemTablePVMXMLmozgas_id: TIntegerField;
    FDMemTablePVMXMLtextil_kpt_id: TIntegerField;
    FDMemTablePVMXMLtextil: TStringField;
    FDMemTablePVMXMLfajta_id: TIntegerField;
    FDMemTablePVMXMLfajta: TStringField;
    FDMemTablePVMXMLallapot_id: TIntegerField;
    FDMemTablePVMXMLallapot: TStringField;
    FDMemTablePVMXMLmeret_id: TIntegerField;
    FDMemTablePVMXMLmeret: TStringField;
    FDMemTablePVMXMLmagassag_id: TIntegerField;
    FDMemTablePVMXMLmagassag: TStringField;
    FDMemTablePVMXMLszin_id: TIntegerField;
    FDMemTablePVMXMLszin: TStringField;
    FDMemTablePVMXMLdatum: TDateTimeField;
    FDMemTablePVMXMLfelhasznalo_id: TIntegerField;
    FDMemTablePVMXMLfajlnev: TStringField;
    FDMemTablePVMXMLDarabosbol: TBooleanField;
    FDMemTablePVMXMLkivezetesoka: TIntegerField;
    FDMemTablePVMXMLkivezetesokanev: TStringField;
    frxReportPV: TfrxReport;
    frxDBDatasetPV: TfrxDBDataset;
    frxReportPVM: TfrxReport;
    frxDBDatasetPVM: TfrxDBDataset;
    FDMemTablePVMXMLuploaded: TIntegerField;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    Label22: TLabel;
    DBLookupComboBoxMegnevezes: TDBLookupComboBox;
    Panel13: TPanel;
    DBLookupComboBoxTextilFajta: TDBLookupComboBox;
    Label1: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    DBLookupComboBoxTextilAllapot: TDBLookupComboBox;
    Panel4: TPanel;
    Label4: TLabel;
    DBLookupComboBoxTextilMeret: TDBLookupComboBox;
    Panel5: TPanel;
    Label5: TLabel;
    DBLookupComboTextilMagassag: TDBLookupComboBox;
    Panel6: TPanel;
    Label3: TLabel;
    DBLookupComboBoxTextilSzin: TDBLookupComboBox;
    Panel7: TPanel;
    Label6: TLabel;
    DBLookupComboBoxKivetelezesOka: TDBLookupComboBox;
    Panel8: TPanel;
    Label7: TLabel;
    DBMemoTetelMegjegyzes: TMemo;
    Button1: TButton;
    Button3: TButton;
    Panel9: TPanel;
    Label8: TLabel;
    Label21: TLabel;
    Label25: TLabel;
    Label27: TLabel;
    Label29: TLabel;
    DBLookupComboBoxMegnevezes_t: TDBLookupComboBox;
    Panel10: TPanel;
    DBLookupComboBoxTextilFajta_t: TDBLookupComboBox;
    Panel11: TPanel;
    DBLookupComboBoxTextilAllapot_t: TDBLookupComboBox;
    Panel12: TPanel;
    DBLookupComboTextilMagassag_t: TDBLookupComboBox;
    Panel15: TPanel;
    DBLookupComboBoxKivetelezesOka_t: TDBLookupComboBox;
    Panel17: TPanel;
    DBMemoTetelMegjegyzes_t: TMemo;
    Button6: TButton;
    Button7: TButton;
    DSTextilek_t: TDataSource;
    DSTextilFajta_t: TDataSource;
    DSTextilAllapot_t: TDataSource;
    DSTextilMagassag_t: TDataSource;
    DSKivetelezesOka_t: TDataSource;
    RadioMegjegyzes: TRadioGroup;
    FDMemTableVisszavetelezesselect: TBooleanField;
    CheckAllBtn: TButton;
    UncheckAllButton: TButton;
    Label28: TLabel;
    DBLookupComboBoxTextilMeret_t: TDBLookupComboBox;
    Panel16: TPanel;
    Label26: TLabel;
    DBLookupComboBoxTextilSzin_t: TDBLookupComboBox;
    Panel14: TPanel;
    DSTextilMeret_t: TDataSource;
    DSTextilSzinek_t: TDataSource;
    FDMemTablePVMXMLmegjegyzes: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Panel13Click(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure Panel5Click(Sender: TObject);
    procedure Panel6Click(Sender: TObject);
    procedure Panel7Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
    procedure dbgTextilListCellClick(Column: TColumn);
    procedure dbgTextilListDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RedrawOsszPanel;
    procedure DSVisszavetelezesDataChange(Sender: TObject; Field: TField);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure CheckAllBtnClick(Sender: TObject);
    procedure UncheckAllButtonClick(Sender: TObject);
    function GetSelectedRows(): integer;
    function ChangesAllowed(kpt_id, meret_id, szin_id: integer): boolean;
    procedure Button7Click(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure RadioMegjegyzesClick(Sender: TObject);
    procedure DSTextilek_tDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    prSelectedRow: integer;
    prCloseOnSave: boolean;
    prPanels: array of TComponent;
    prNextPanel: integer;
    prCDSFajta, prCDSAllapot,
    prCDSMagassag, prCDSKivetelezes, prCDSMeret,
    prCDSSzin: TClientDataset;
  public
    { Public declarations }
    pubSelectedIds: TIntegerArray;
    pubSameSelected: boolean;
    pubInEdit: boolean;
    pubCDSMegnevezes: TClientDataSet;
    procedure pubHandleRFID(rfid: string);
  end;

var
  frmPufferVisszavetel: TfrmPufferVisszavetel;

implementation

  uses uDm, uFgv, uHttps, uSerialReader;

{$R *.dfm}

var SerialReader: TSerialReader;
    prKivalasztottTetel: TPufferRaktarTextil;

function SetSelectedIds(AValue: integer; remove: boolean): TIntegerArray;
var i, len: integer;
    del: boolean;
begin
  result := frmPufferVisszavetel.pubSelectedIds;
  if (not remove) then
    begin
      setLength(result, length(result)+1);
      result[length(result)-1] := AValue;
    end
  else
    begin
      len := length(result);
      if len = 0 then
        begin
          exit;
        end;

      for i := 0 to len-2 do
        begin
          if (result[i] = AValue) AND (not del) then
            begin
              del := true;
            end;
          if del then
            begin
              result[i] := result[i+1];
            end;
        end;
      SetLength(result, len-1);
    end;
  frmPufferVisszavetel.pubSameSelected := true;
  if length(result) > 1 then
    begin
      for i := 1 to length(result)-1 do
        begin
          if (result[i-1] <> result[i]) then
            begin
              frmPufferVisszavetel.pubSameSelected := false;
            end;
        end;
    end;
end;

function CreateCustomDataset(AClientDataset: TClientDataSet; AIDField, AMegnevezesField: string): TClientDataset;
var i: integer;
begin
  result := TClientDataset.Create(Application);
  for i:= 0 to AClientDataset.FieldCount - 1 do
    begin
      if AClientDataset.Fields[i] is TAutoIncField then
        result.FieldDefs.Add(AClientDataset.Fields[i].FieldName, ftInteger, AClientDataset.Fields[i].Size)
      else
        result.FieldDefs.Add(AClientDataset.Fields[i].FieldName, AClientDataset.Fields[i].DataType, AClientDataset.Fields[i].Size);
    end;

  result.CreateDataSet;
  result.Active := true;
  result.Insert;
  result.FieldByName(AIDField).AsInteger := -2;
  result.FieldByName(AMegnevezesField).AsString := 'Nem v�ltozik';
  result.Post;
  result.Insert;
  result.FieldByName(AIDField).AsInteger := -1;
  result.FieldByName(AMegnevezesField).AsString := '�sszes t�rl�se';
  result.Post;
  AClientDataset.First;
  while not (AClientDataset.Eof) do
    begin
      result.Insert;
      result.CopyFields(AClientDataset);
      result.Post;
      AClientDataset.Next;
    end;
  result.IndexFieldNames := AIDField;
end;

{ Component Copy }
function CopyComponent(Component,AParent: TComponent; NewComponentName: String): TComponent;
var
 Stream: TMemoryStream;
 S: String;
begin
 Result := TComponentClass(Component.ClassType).Create(Component.Owner);
 S := Component.Name;
 Component.Name := NewComponentName;
 Stream := TMemoryStream.Create;
 try
   Stream.WriteComponent(Component);
   Component.Name := S;
   Stream.Seek(0, soFromBeginning);
   TWinControl(AParent).InsertControl(TControl(Result));
   Stream.ReadComponent(Result);
 finally
   Stream.Free;
 end;
end;
// AComponent: the original component
// AOwner, AParent: the owner and parent of the new copy
// returns the new component

function CopyComponents(AComponent, AOwner, AParent: TComponent;Index:Integer): TComponent;
var
  i : integer;
  NewComponent: TComponent;
begin
  NewComponent := CopyComponent(AComponent,AParent,AComponent.Name+IntToStr(Index))as TComponentClass(AComponent.ClassType);{}
 // now, search for subcomponents
 for i:=0 to AComponent.Owner.ComponentCount-1 do
   if TWinControl(AComponent.Owner.Components[i]).Parent=AComponent then
     // and copy them too
     CopyComponents(AComponent.Owner.Components[i], AOwner, NewComponent,Index);
  Result:=NewComponent;
end;

procedure SetPanelLabels(AComponent: TComponent; AFajta, AAllapot, ASzin, AMeret, AMagassag, AMegnev: string; AMennyiseg: integer);
var i: integer;
begin
  for i := 0 to AComponent.Owner.ComponentCount-1 do
    begin
      if (AComponent.Owner.Components[i].ClassType = TLabel) and (TWinControl(AComponent.Owner.Components[i]).Parent = AComponent) then
        begin
          if TLabel(AComponent.Owner.Components[i]).Caption = 'lFajta' then
            TLabel(AComponent.Owner.Components[i]).Caption := AFajta
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'l�llapot' then
            TLabel(AComponent.Owner.Components[i]).Caption := AAllapot
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lSz�n' then
            TLabel(AComponent.Owner.Components[i]).Caption := ASzin
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lM�' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMeret
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMa' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMagassag
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lDarab' then
            TLabel(AComponent.Owner.Components[i]).Caption := IntToStr(AMennyiseg)
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMegnev' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMegnev
        end;
    end;
end;

{ End of Component Copy }

function GetTextilNev(id: integer): string;
begin
  result := '';
  DM.CDSTextilek.Filtered := false;
  DM.CDSTextilek.Filter := 'kpt_id='+IntToStr(id);
  DM.CDSTextilek.Filtered := true;
  DM.CDSTextilek.First;
  result := DM.CDSTextilek.FieldByName('kpt_smegnev').AsString;
  DM.CDSTextilek.Filtered := false;
end;

function GetTextilFajta(id: integer): string;
begin
  result := '';
  DM.CDSTextilFajta.Filtered := false;
  DM.CDSTextilFajta.Filter := 'fajta_id='+IntToStr(id);
  DM.CDSTextilFajta.Filtered := true;
  DM.CDSTextilFajta.First;
  result := DM.CDSTextilFajta.FieldByName('fajta_nev').AsString;
  DM.CDSTextilFajta.Filtered := false;
end;

function GetTextilAllapot(id: integer): string;
begin
  result := '';
  DM.CDSTextilAllapot.Filtered := false;
  DM.CDSTextilAllapot.Filter := 'allapot_id='+IntToStr(id);
  DM.CDSTextilAllapot.Filtered := true;
  DM.CDSTextilAllapot.First;
  result := DM.CDSTextilAllapot.FieldByName('allapot_nev').AsString;
  DM.CDSTextilAllapot.Filtered := false;
end;

function GetTextilMeret(id: integer): string;
begin
  result := '';
  DM.CDSTextilMeret.Filtered := false;
  DM.CDSTextilMeret.Filter := 'meret_id='+IntToStr(id);
  DM.CDSTextilMeret.Filtered := true;
  DM.CDSTextilMeret.First;
  result := DM.CDSTextilMeret.FieldByName('meret_nev').AsString;
  DM.CDSTextilMeret.Filtered := false;
end;

function GetTextilMagassag(id: integer): string;
begin
  result := '';
  DM.CDSTextilMagassag.Filtered := false;
  DM.CDSTextilMagassag.Filter := 'magassag_id='+IntToStr(id);
  DM.CDSTextilMagassag.Filtered := true;
  DM.CDSTextilMagassag.First;
  result := DM.CDSTextilMagassag.FieldByName('magassag_nev').AsString;
  DM.CDSTextilMagassag.Filtered := false;
end;

function GetTextilSzin(id, textilid: integer): string;
begin
  result := '';
  DM.CDSTextilSzinek.Filtered := false;
  DM.CDSTextilSzinek.Filter := 'kpsz_szin_id='+IntToStr(id)+' and kpsz_kpt_id='+IntToStr(textilid);
  DM.CDSTextilSzinek.Filtered := true;
  DM.CDSTextilSzinek.First;
  result := DM.CDSTextilSzinek.FieldByName('szin_nev').AsString;
  DM.CDSTextilSzinek.Filtered := false;
end;


function TfrmPufferVisszavetel.GetSelectedRows(): integer;
begin
  result := prSelectedRow;
end;

procedure TfrmPufferVisszavetel.pubHandleRFID(rfid: string);
var lcLastDate: TDateTime;
begin
  if pubInEdit then
    begin
      mChipList.Lines.Add('A '+rfid+' nem lett hozz�adva a visszav�telez�shez, mert egy vagy t�bb t�tel szerkeszt�s alatt �ll.');
      exit;
    end;

  FDMemTableVisszavetelezes.First;
  while not FDMemTableVisszavetelezes.Eof do
    begin
      if FDMemTableVisszavetelezes.FieldByName('chipkod').AsString = rfid then
        exit;
      FDMemTableVisszavetelezes.Next;
    end;
  // Chip ellen�rz�se a chip-ek XML-ben (legyen benne)
  // Ne szerepeljen a bev�telez�sek k�z�tt
  DM.CDSChipPuffer.Filtered := false;
  DM.CDSChipPuffer.Filter := 'chipkod='''+rfid+'''';
  DM.CDSChipPuffer.Filtered := true;
  if DM.CDSChipPuffer.RecordCount > 0 then
    begin
      DM.CDSChipPuffer.Filtered := false;
      mChipList.Lines.Add('A '+rfid+' chip jelenleg a pufferrakt�rban tal�lhat�!');
      exit;
    end
  else
    DM.CDSChipPuffer.Filtered := false;

  DM.CDSChip.Filtered := false;
  DM.CDSChip.Filter := 'cms_chipkod='''+rfid+'''';
  DM.CDSChip.Filtered := true;
  if DM.CDSChip.RecordCount > 0 then
    begin
      DM.CDSChip.First;
      lcLastDate := DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime;
      prKivalasztottTetel.chipkod := DM.CDSChip.FieldByName('cms_chipkod').AsString;
      prKivalasztottTetel.vonalkod := DM.CDSChip.FieldByName('cms_vonalkod').AsString;
      prKivalasztottTetel.textilid := DM.CDSChip.FieldByName('cms_kptid').AsInteger;
      prKivalasztottTetel.textilnev := GetTextilNev(DM.CDSChip.FieldByName('cms_kptid').AsInteger);
      prKivalasztottTetel.fajtaid := DM.CDSChip.FieldByName('cms_fajtaid').AsInteger;
      prKivalasztottTetel.fajtanev := GetTextilFajta(DM.CDSChip.FieldByName('cms_fajtaid').AsInteger);
      prKivalasztottTetel.allapotid := DM.CDSChip.FieldByName('cms_allapotid').AsInteger;
      prKivalasztottTetel.allapotnev := GetTextilAllapot(DM.CDSChip.FieldByName('cms_allapotid').AsInteger);
      prKivalasztottTetel.meretid := DM.CDSChip.FieldByName('cms_meretid').AsInteger;
      prKivalasztottTetel.meretnev := GetTextilMeret(DM.CDSChip.FieldByName('cms_meretid').AsInteger);
      prKivalasztottTetel.magassagid := DM.CDSChip.FieldByName('cms_magassagid').AsInteger;
      prKivalasztottTetel.magassagnev := GetTextilMagassag(DM.CDSChip.FieldByName('cms_magassagid').AsInteger);
      prKivalasztottTetel.szinid := DM.CDSChip.FieldByName('cms_szinid').AsInteger;
      prKivalasztottTetel.szinnev := GetTextilSzin(DM.CDSChip.FieldByName('cms_szinid').AsInteger, DM.CDSChip.FieldByName('cms_kptid').AsInteger);
      prKivalasztottTetel.id := DM.CDSChip.FieldByName('cms_id').AsInteger;
      while not DM.CDSChip.Eof do
        begin
          if lcLastDate < DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime then
            begin
              lcLastDate := DM.CDSChip.FieldByName('cms_jelolesdatum').AsDateTime;
              prKivalasztottTetel.chipkod := DM.CDSChip.FieldByName('cms_chipkod').AsString;
              prKivalasztottTetel.vonalkod := DM.CDSChip.FieldByName('cms_vonalkod').AsString;
              prKivalasztottTetel.textilid := DM.CDSChip.FieldByName('cms_kptid').AsInteger;
              prKivalasztottTetel.textilnev := GetTextilNev(DM.CDSChip.FieldByName('cms_kptid').AsInteger);
              prKivalasztottTetel.fajtaid := DM.CDSChip.FieldByName('cms_fajtaid').AsInteger;
              prKivalasztottTetel.fajtanev := GetTextilFajta(DM.CDSChip.FieldByName('cms_fajtaid').AsInteger);
              prKivalasztottTetel.allapotid := DM.CDSChip.FieldByName('cms_allapotid').AsInteger;
              prKivalasztottTetel.allapotnev := GetTextilAllapot(DM.CDSChip.FieldByName('cms_allapotid').AsInteger);
              prKivalasztottTetel.meretid := DM.CDSChip.FieldByName('cms_meretid').AsInteger;
              prKivalasztottTetel.meretnev := GetTextilMeret(DM.CDSChip.FieldByName('cms_meretid').AsInteger);
              prKivalasztottTetel.magassagid := DM.CDSChip.FieldByName('cms_magassagid').AsInteger;
              prKivalasztottTetel.magassagnev := GetTextilMagassag(DM.CDSChip.FieldByName('cms_magassagid').AsInteger);
              prKivalasztottTetel.szinid := DM.CDSChip.FieldByName('cms_szinid').AsInteger;
              prKivalasztottTetel.szinnev := GetTextilSzin(DM.CDSChip.FieldByName('cms_szinid').AsInteger, DM.CDSChip.FieldByName('cms_kptid').AsInteger);
              prKivalasztottTetel.id := DM.CDSChip.FieldByName('cms_id').AsInteger;
            end;
          DM.CDSChip.Next;
        end;

      FDMemTableVisszavetelezes.Insert;
      FDMemTableVisszavetelezes.FieldByName('chipkod').AsString := prKivalasztottTetel.chipkod;
      FDMemTableVisszavetelezes.FieldByName('vonalkod').AsString := prKivalasztottTetel.vonalkod;
      FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger := prKivalasztottTetel.textilid;
      FDMemTableVisszavetelezes.FieldByName('textilnev').AsString := prKivalasztottTetel.textilnev;
      FDMemTableVisszavetelezes.FieldByName('fajtaid').AsInteger := prKivalasztottTetel.fajtaid;
      FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString := prKivalasztottTetel.fajtanev;
      FDMemTableVisszavetelezes.FieldByName('allapotid').AsInteger := prKivalasztottTetel.allapotid;
      FDMemTableVisszavetelezes.FieldByName('allapotnev').AsString := prKivalasztottTetel.allapotnev;
      FDMemTableVisszavetelezes.FieldByName('meretid').AsInteger := prKivalasztottTetel.meretid;
      FDMemTableVisszavetelezes.FieldByName('meretnev').AsString := prKivalasztottTetel.meretnev;
      FDMemTableVisszavetelezes.FieldByName('magassagid').AsInteger := prKivalasztottTetel.magassagid;
      FDMemTableVisszavetelezes.FieldByName('magassagnev').AsString := prKivalasztottTetel.magassagnev;
      FDMemTableVisszavetelezes.FieldByName('szinid').AsInteger := prKivalasztottTetel.szinid;
      FDMemTableVisszavetelezes.FieldByName('szinnev').AsString := prKivalasztottTetel.szinnev;
      FDMemTableVisszavetelezes.FieldByName('datum').AsDateTime := now();
      FDMemTableVisszavetelezes.FieldByName('select').AsBoolean := false;
      FDMemTableVisszavetelezes.Post;
      mChipList.Lines.Add('A '+rfid+' hozz�adva a visszav�telez�shez.');
      RedrawOsszPanel;
    end
  else
    begin
      mChipList.Lines.Add('A '+rfid+' chipk�d nem tal�lhat� az adatb�zisban!');
    end;
  DM.CDSChip.Filtered := false;
end;

procedure TfrmPufferVisszavetel.btnChipEndClick(Sender: TObject);
begin
  btnChipStart.Enabled := true;
  btnChipEnd.Enabled := false;
  SerialReader.Stop;
end;

procedure TfrmPufferVisszavetel.btnChipStartClick(Sender: TObject);
begin
  btnChipStart.Enabled := False;
  btnChipEnd.Enabled := true;
  SerialReader.Execute;
end;

procedure TfrmPufferVisszavetel.Button1Click(Sender: TObject);
begin
  // Kiv�lasztott t�tel szerkeszt�se / Ment�se
  if GetSelectedRows() = 0 then
    begin
      ShowMessage('Nincs kijel�lt t�tel a list�n');
      exit;
    end;
  if Button1.Tag = 0 then
    begin
      dbgTextilList.Enabled := false;
      CheckAllBtn.Enabled := false;
      UncheckAllButton.Enabled := false;
      pubInEdit := true;
      DBLookupComboBoxMegnevezes.Enabled := true;
      Panel13.Enabled := true;
      DBLookupComboBoxTextilFajta.Enabled := True;
      Panel3.Enabled := true;
      DBLookupComboBoxTextilAllapot.Enabled := true;
      Panel4.Enabled := true;
      DBLookupComboBoxTextilMeret.Enabled := true;
      Panel5.Enabled := true;
      DBLookupComboTextilMagassag.Enabled := true;
      Panel6.Enabled := true;
      DBLookupComboBoxTextilSzin.Enabled := true;
      Panel7.Enabled := true;
      DBLookupComboBoxKivetelezesOka.Enabled := true;
      Panel8.Enabled := true;
      DBMemoTetelMegjegyzes.Enabled := true;
      button1.Tag := 1;
      button1.Caption := 'V�ltoztat�sok ment�se';
      button3.Enabled := true;
      Button5.Enabled := false;
    end
  else
    begin
      if (DBLookupComboBoxTextilFajta.Text = '') OR
         (DBLookupComboBoxTextilMeret.Text = '') OR
         (DBLookupComboBoxMegnevezes.Text = '') OR
         (DBLookupComboBoxKivetelezesOka.Text = '')
      then
        begin
          ShowMessage('A textil megnevez�se, fajt�ja, m�rete �s a visszav�telez�s oka mez�ket ki kell t�lteni!');
          exit;
        end;
      FDMemTableVisszavetelezes.First;
      while not FDMemTableVisszavetelezes.Eof do
        if FDMemTableVisszavetelezes.FieldByName('select').AsBoolean = false then
          begin
            FDMemTableVisszavetelezes.Next;
          end
        else
          break;

      if (DBLookupComboBoxMegnevezes.KeyValue > 0) then
        begin
          pubSelectedIds := SetSelectedIds(FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger, true);
          pubSelectedIds := SetSelectedIds(DBLookupComboBoxMegnevezes.KeyValue, false);
        end;

      FDMemTableVisszavetelezes.Edit;
      FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant := DBLookupComboBoxMegnevezes.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('textilnev').AsVariant := DBLookupComboBoxMegnevezes.Text;
      FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant := DBLookupComboBoxTextilFajta.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('fajtanev').AsVariant := DBLookupComboBoxTextilFajta.Text;
      FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant := DBLookupComboBoxTextilAllapot.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('allapotnev').AsVariant := DBLookupComboBoxTextilAllapot.Text;
      FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant := DBLookupComboBoxTextilMeret.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('meretnev').AsVariant := DBLookupComboBoxTextilMeret.Text;
      FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant := DBLookupComboTextilMagassag.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('magassagnev').AsVariant := DBLookupComboTextilMagassag.Text;
      FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant := DBLookupComboBoxTextilSzin.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('szinnev').AsVariant := DBLookupComboBoxTextilSzin.Text;
      FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsVariant := DBLookupComboBoxKivetelezesOka.KeyValue;
      FDMemTableVisszavetelezes.FieldByName('kivezetesokanev').AsVariant := DBLookupComboBoxKivetelezesOka.Text;
      FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsVariant := DBMemoTetelMegjegyzes.Text;
      FDMemTableVisszavetelezes.Post;
      RedrawOsszPanel;
      DBLookupComboBoxMegnevezes.Enabled := false;
      Panel13.Enabled := false;
      DBLookupComboBoxTextilFajta.Enabled := false;
      Panel3.Enabled := false;
      DBLookupComboBoxTextilAllapot.Enabled := false;
      Panel4.Enabled := false;
      DBLookupComboBoxTextilMeret.Enabled := false;
      Panel5.Enabled := false;
      DBLookupComboTextilMagassag.Enabled := false;
      Panel6.Enabled := false;
      DBLookupComboBoxTextilSzin.Enabled := false;
      Panel7.Enabled := false;
      DBLookupComboBoxKivetelezesOka.Enabled := false;
      Panel8.Enabled := false;
      DBMemoTetelMegjegyzes.Enabled := false;
      pubInEdit := false;
      dbgTextilList.Enabled := true;
      CheckAllBtn.Enabled := true;
      UncheckAllButton.Enabled := true;
      button1.Tag := 0;
      button1.Caption := 'Szerkeszt�s';
      button3.Enabled := false;
      Button5.Enabled := true;
    end;
end;

procedure TfrmPufferVisszavetel.Button2Click(Sender: TObject);

  function FindInOsszesitesPK: boolean;
  begin
    result := false;
    FDMemTablePVXML.First;
    while not FDMemTablePVXML.Eof do
      begin
        if
          (FDMemTablePVXML.FieldByName('megnevezesText').AsString = FDMemTableVisszavetelezes.FieldByName('textilnev').AsString) and
          (FDMemTablePVXML.FieldByName('fajtaText').AsString = FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString) and
          (FDMemTablePVXML.FieldByName('allapotText').AsString = FDMemTableVisszavetelezes.FieldByName('allapotnev').AsString) and
          (FDMemTablePVXML.FieldByName('meretText').AsString = FDMemTableVisszavetelezes.FieldByName('meretnev').AsString) and
          (FDMemTablePVXML.FieldByName('magassagText').AsString = FDMemTableVisszavetelezes.FieldByName('magassagnev').AsString) and
          (FDMemTablePVXML.FieldByName('szinText').AsString = FDMemTableVisszavetelezes.FieldByName('szinnev').AsString)
        then
          begin
            result := true;
            exit;
          end;
        FDMemTablePVXML.Next;
      end;
  end;

  function prGetMozgasnemNev(A_mozgId: Integer): String;
  Begin
    Result:= '';
    DM.CDSMozgnem.First;
    while not DM.CDSMozgnem.Eof do
      begin
        if DM.CDSMozgnem.FieldByName('moz_id').AsInteger = A_mozgId then
          Begin
            Result:= DM.CDSMozgnem.FieldByName('moz_kod').asString;
            exit;
          End;
        DM.CDSMozgnem.Next;
      end;
  End;

  procedure ChangeFieldValueInCDS(ADataSet: TClientDataSet; AFilePath, AFieldName: String; AValue: Variant; rename: boolean);
  var lcFgv: TFgvUnit;
  begin
    lcFgv := TfgvUnit.Create;
    ADataSet.LoadFromFile(AFilePath);
    ADataSet.LogChanges := false;
    ADataSet.First;
    while not ADataSet.Eof do
      begin
        ADataSet.Edit;
        ADataSet.FieldByName(AFieldName).AsVariant := AValue;
        ADataSet.Post;
        ADataSet.Next;
      end;
    lcFgv.DataSetToXML(ADataSet, AFilePath);
    if rename then
      RenameFile(AFilePath, '_uploaded_'+AFilePath);
    lcFgv.Free;
  end;

var
  lcFgv: TfgvUnit;
  lcMost: TDateTime;
  lcRootDir: string;
  lcAdatMappa: string;
  lcAdatUtvonal: string;
  lcSearchResult: TSearchRec;
  lcHttpRequest: THttpsUnit;
  lcHttpResponse: string;
  lcIniFile: TIniFile;
  lcIPCim, lcUtvonal: string;
  lcDataset: TClientDataSet;
  lcPVFajlNev: String;
  lcPVMFajlNev: String;
  lcSorszam: String;
  lcTmpId: string;
  lcOsztalyId: Integer;
  lcOsztalyNev: String;
  i: integer;
begin
  // Visszav�telez�s lez�r�sa
  FDMemTableVisszavetelezes.First;
  while not FDMemTableVisszavetelezes.Eof do
    begin
      if (FDMemTableVisszavetelezes.FieldByName('textilnev').AsString = '') OR
         (FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString = '') OR
         (FDMemTableVisszavetelezes.FieldByName('meretnev').AsString = '') OR
         (FDMemTableVisszavetelezes.FieldByName('kivezetesokanev').AsString = '') then
        begin
          ShowMessage('A visszav�telez�st nem lehet lez�rni am�g van hi�nyos param�terekkel rendelkez� textilia a visszav�telez�s list�n!');
          exit;
        end;
      FDMemTableVisszavetelezes.Next;
    end;
  case MessageDlg('Biztosan lez�rja a visszav�telez�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        // Create-k
        lcFgv := TfgvUnit.Create;
        lcHttpRequest := THttpsUnit.Create();
        lcDataset := TClientDataSet.Create(Application);
        lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
        lcIPCim := lciniFile.ReadString('serverip','ip','');
        lcUtvonal := lciniFile.ReadString('utvonal','path','');

        //K�vetkez� sorsz�m a lok�lis f�jlb�l. Nem k�ldj�k el a v�ltoztat�st a szervernek!
        lcSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PV', GPartner, GUserID, true, false);

        FDMemTablePVXML.Close;
        FDMemTablePVXML.Open;

        FDMemTablePVMXML.Close;
        FDMemTablePVMXML.Open;

        lcMost := now();

        // �tvonalak, f�jlok
        lcRootDir := GRootPath;
        lcAdatMappa := GetCurrentDir + '\adat\';
        SetCurrentDir(lcAdatMappa);

        if not DirectoryExists(lcFgv.PartnerMappa(GPartner)) then
          CreateDir(lcFgv.PartnerMappa(GPartner));

        lcAdatUtvonal := lcAdatMappa + lcFgv.PartnerMappa(GPartner);
        SetCurrentDir(lcAdatUtvonal);

        if not DirectoryExists(Trim(lcFgv.Mappa(lcMost))) then
          CreateDir(Trim(lcFgv.Mappa(lcMost)));

        lcAdatUtvonal := lcAdatUtvonal + '\' + Trim(lcFgv.Mappa(lcMost));

        GOldal := 'PV';
        lcPVFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';

        GOldal := 'PVM';
        lcPVMFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        i := 0;
        try
          // Adapterek felt�lt�se
          FDMemTableVisszavetelezes.Filtered := false;
          FDMemTableVisszavetelezes.First;
          while not FDMemTableVisszavetelezes.Eof do
            begin
              // PV.XML
              if not FindInOsszesitesPK then
                begin
                  FDMemTablePVXML.Insert;
                  FDMemTablePVXML.FieldByName('fajtaText').AsString := FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString;
                  FDMemTablePVXML.FieldByName('allapotText').AsString := FDMemTableVisszavetelezes.FieldByName('allapotnev').AsString;
                  FDMemTablePVXML.FieldByName('meretText').AsString := FDMemTableVisszavetelezes.FieldByName('meretnev').AsString;
                  FDMemTablePVXML.FieldByName('magassagText').AsString := FDMemTableVisszavetelezes.FieldByName('magassagnev').AsString;
                  FDMemTablePVXML.FieldByName('szinText').AsString := FDMemTableVisszavetelezes.FieldByName('szinnev').AsString;
                  FDMemTablePVXML.FieldByName('megnevezesText').AsString := FDMemTableVisszavetelezes.FieldByName('textilnev').AsString;

                  FDMemTablePVXML.FieldByName('pbfej_id').AsInteger := -1;
                  FDMemTablePVXML.FieldByName('pbfej_sorszam').AsString := lcSorszam;
                  FDMemTablePVXML.FieldByName('pbfej_uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePVXML.FieldByName('pbfej_partner_id').AsInteger := GPartner;
                  FDMemTablePVXML.FieldByName('pbfej_osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePVXML.FieldByName('pbfej_mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PV;
                  FDMemTablePVXML.FieldByName('pbfej_felhasznalo_id').AsInteger := GUserID;
                  FDMemTablePVXML.FieldByName('pbfej_megjegyzes').AsString := '';
                  FDMemTablePVXML.FieldByName('pbfej_create_date').AsDateTime := lcMost;
                  FDMemTablePVXML.FieldByName('pbfej_create_user').AsInteger := GUserID;
                  FDMemTablePVXML.FieldByName('pbfej_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePVXML.FieldByName('pbfej_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePVXML.FieldByName('pbfej_selected_date').AsDateTime := GDate;
                  FDMemTablePVXML.FieldByName('pbtet_darabszam').AsInteger := 1;
                  FDMemTablePVXML.FieldByName('pbtet_fajta_id').AsInteger := FDMemTableVisszavetelezes.FieldByName('fajtaid').AsInteger;
                  FDMemTablePVXML.FieldByName('pbtet_allapot_id').AsInteger := FDMemTableVisszavetelezes.FieldByName('allapotid').AsInteger;
                  FDMemTablePVXML.FieldByName('pbtet_textil_id').AsInteger := FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger;
                  FDMemTablePVXML.FieldByName('pbtet_meret_id').AsInteger := FDMemTableVisszavetelezes.FieldByName('meretid').AsInteger;
                  FDMemTablePVXML.FieldByName('pbtet_magassag_id').AsInteger := FDMemTableVisszavetelezes.FieldByName('magassagid').AsInteger;
                  FDMemTablePVXML.FieldByName('pbtet_szin_id').AsInteger := FDMemTableVisszavetelezes.FieldByName('szinid').AsInteger;
                  FDMemTablePVXML.FieldByName('pbtet_megjegyzes').AsString := '';
                  FDMemTablePVXML.FieldByName('pbtet_create_date').AsDateTime := lcMost;
                  FDMemTablePVXML.FieldByName('pbtet_create_user').AsInteger := GUserID;
                  FDMemTablePVXML.FieldByName('pbtet_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePVXML.FieldByName('pbtet_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePVXML.FieldByName('pbtet_fajlnev').AsString := lcPVFajlNev;
                  FDMemTablePVXML.FieldByName('pbtet_chiphez_rendelve').AsInteger := 0;
                  FDMemTablePVXML.FieldByName('pbtet_darabosdb').AsInteger := 0;
                  FDMemTablePVXML.Post;
                end
              else
                begin
                  // PV
                  FDMemTablePVXML.Edit;
                  FDMemTablePVXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTablePVXML.FieldByName('pbtet_darabszam').AsInteger + 1;
                  FDMemTablePVXML.Post;
                end;

              // PVM.xml
              FDMemTablePVMXML.Insert;
              FDMemTablePVMXML.FieldByName('sorszam').AsString := lcSorszam;
              FDMemTablePVMXML.FieldByName('partner_id').AsInteger := GPartner;
              FDMemTablePVMXML.FieldByName('uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
              FDMemTablePVMXML.FieldByName('osztaly_id').AsInteger := GPufferRaktarOsztaly;
              FDMemTablePVMXML.FieldByName('prszall_fej_id').AsInteger := -1;
              FDMemTablePVMXML.FieldByName('vonalkod').AsString := FDMemTableVisszavetelezes.FieldByName('Vonalkod').AsString;
              FDMemTablePVMXML.FieldByName('chipkod').AsString := FDMemTableVisszavetelezes.FieldByName('Chipkod').AsString;
              FDMemTablePVMXML.FieldByName('mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PV;
              FDMemTablePVMXML.FieldByName('textil_kpt_id').AsVariant := FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant;
              FDMemTablePVMXML.FieldByName('textil').AsString := FDMemTableVisszavetelezes.FieldByName('textilnev').AsString;
              FDMemTablePVMXML.FieldByName('fajta_id').AsVariant := FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant;
              FDMemTablePVMXML.FieldByName('fajta').AsString := FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString;
              FDMemTablePVMXML.FieldByName('allapot_id').AsVariant := FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant;
              FDMemTablePVMXML.FieldByName('allapot').AsString := FDMemTableVisszavetelezes.FieldByName('allapotnev').AsString;
              FDMemTablePVMXML.FieldByName('meret_id').AsVariant := FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant;
              FDMemTablePVMXML.FieldByName('meret').AsString := FDMemTableVisszavetelezes.FieldByName('meretnev').AsString;
              FDMemTablePVMXML.FieldByName('magassag_id').AsVariant := FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant;
              FDMemTablePVMXML.FieldByName('magassag').AsString := FDMemTableVisszavetelezes.FieldByName('magassagnev').AsString;
              FDMemTablePVMXML.FieldByName('szin_id').AsVariant := FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant;
              FDMemTablePVMXML.FieldByName('szin').AsString := FDMemTableVisszavetelezes.FieldByName('szinnev').AsString;
              FDMemTablePVMXML.FieldByName('datum').AsDateTime := FDMemTableVisszavetelezes.FieldByName('Datum').AsDateTime;
              FDMemTablePVMXML.FieldByName('megjegyzes').AsString := FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsString;
              FDMemTablePVMXML.FieldByName('felhasznalo_id').AsInteger := GUserId;
              FDMemTablePVMXML.FieldByName('fajlnev').AsString := lcPVMFajlNev;
              FDMemTablePVMXML.FieldByName('kivezetesoka').AsInteger := FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsInteger;
              FDMemTablePVMXML.FieldByName('kivezetesokanev').AsString := FDMemTableVisszavetelezes.FieldByName('kivezetesokanev').AsString;
              FDMemTablePVMXML.FieldByName('uploaded').AsInteger := -1;
              FDMemTablePVMXML.Post;

              FDMemTableVisszavetelezes.Next;
            end;

          DM.CDSUzem.First;
          TfrxMemoView(frxReportPV.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportPV.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPV.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPV.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPV.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPV.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
//          TfrxMemoView(frxReportPV.FindObject('frxDBDSSzallszfej_createdate')).Text := DateToStr(lcMost);
          TfrxMemoView(frxReportPV.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PV);

          TfrxMemoView(frxReportPVM.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportPVM.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPVM.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPVM.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPVM.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPVM.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPVM.FindObject('frxDBDSSzallszfej_szallszam')).Text := lcSorszam;
          TfrxMemoView(frxReportPVM.FindObject('frxDBDSSzallszfej_szallszam1')).Text := lcSorszam;
 //         TfrxMemoView(frxReportPVM.FindObject('frxDBDSSzallszfej_createdate')).Text := DateToStr(lcMost);
          TfrxMemoView(frxReportPVM.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PV);

          DM.CDSFelhasz.First;
          while not DM.CDSFelhasz.Eof do
            begin
              if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
                begin
                  TfrxMemoView(frxReportPV.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  TfrxMemoView(frxReportPVM.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                end;
              DM.CDSFelhasz.Next;
            end;

          while not DM.CDSPartnerek.Eof do
            begin
              if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
                begin
                  TfrxMemoView(frxReportPV.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                  TfrxMemoView(frxReportPVM.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                end;
              DM.CDSPartnerek.Next;
            end;
          SetCurrentDir(lcAdatUtvonal);
          FDMemTablePVXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPVFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPV.PrepareReport(true);
          frxReportPV.Export(DM.frxPDFExport1);

          frxReportPV.PrintOptions.Copies := 3;
          frxReportPV.PrintOptions.Collate := True;
          frxReportPV.PrepareReport(true);
          frxReportPV.ShowPreparedReport;

          // A Megn�velt sorsz�m felk�ld�se a szervernek
          SetCurrentDir(lcRootDir);
          lcSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PV', GPartner, GUserID, false, true);
          SetCurrentDir(lcAdatUtvonal);

          lcFgv.DataSetToXML(TDataSet(FDMemTablePVXML), lcPVFajlNev);

          FDMemTablePVMXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPVMFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPVM.PrepareReport(true);
          frxReportPVM.Export(DM.frxPDFExport1);

          frxReportPVM.PrintOptions.Copies := 3;
          frxReportPVM.PrintOptions.Collate := True;
          frxReportPVM.PrepareReport(true);
          frxReportPVM.ShowPreparedReport;

          lcFgv.DataSetToXML(TDataSet(FDMemTablePVMXML), lcPVMFajlNev);

          lcFgv.SetPufferRaktar('bevet', false, false, TClientDataSet(FDMemTablePVXML));
          lcFgv.SetPufferRaktar('bevet', true, true, TClientDataSet(FDMemTablePVMXML));

          lcHttpResponse := '';
          // Az�rt pufBevRakXMLUpload-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s, nincs r� k�l�n request.
          lcHttpResponse := lcHttpRequest.PostHttps(lcPVFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
          if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
            begin
              lcTmpId := lcHttpResponse;
              ChangeFieldValueInCDS(lcDataset, lcPVFajlNev, 'pbfej_id', lcTmpId, true);
              lcHttpResponse := '';
              // Az�rt pufBevRakXMLUpload_PBM-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s chip mozg�sok, nincs r� k�l�n request.
              ChangeFieldValueInCDS(lcDataset, lcPVMFajlNev, 'prszall_fej_id', lcTmpId, false);
              lcHttpResponse := lcHttpRequest.PostHttps(lcPVMFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
              if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                begin
                  ChangeFieldValueInCDS(lcDataset, lcPVMFajlNev, 'uploaded', lcTmpId, true);
                end
              else
                begin
                  showmessage('A felt�lt�s nem lehets�ges. (PVMxxxx.xml szakasz) - ' + lcHttpResponse);
                end;
            end
          else
            showmessage('A felt�lt�s nem lehets�ges. (PVxxxx.xml szakasz) - ' + lcHttpResponse);

        finally
          SetCurrentDir(lcRootDir);
        end;
        // Destroy-ok
        lcFgv.Destroy;
        lcHttpRequest.Destroy;
        lcDataset.Destroy;
        lcIniFile.Destroy;
        prCloseOnSave := true;
        Close;
      end;
  end;
end;

procedure TfrmPufferVisszavetel.Button3Click(Sender: TObject);
begin
  // Szerkeszt�s megszak�t�sa �s elvet�se
  dbgTextilList.SelectedRows.CurrentRowSelected := True;
  if GetSelectedRows() > 0 then
    begin
      DBLookupComboBoxTextilFajta.KeyValue := FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant;
      DBLookupComboBoxTextilAllapot.KeyValue := FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant;
      DBLookupComboBoxTextilMeret.KeyValue := FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant;
      DBLookupComboTextilMagassag.KeyValue := FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant;
      DBLookupComboBoxMegnevezes.KeyValue := FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant;
      DBLookupComboBoxKivetelezesOka.KeyValue := FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsVariant;
      DM.DSTextilek.OnDataChange(nil, nil);
      DBLookupComboBoxTextilSzin.KeyValue := FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant;
      DBMemoTetelMegjegyzes.Lines.Clear;
      DBMemoTetelMegjegyzes.Text := FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsString;
    end;
  DBLookupComboBoxMegnevezes.Enabled := false;
  Panel13.Enabled := false;
  DBLookupComboBoxTextilFajta.Enabled := false;
  Panel3.Enabled := false;
  DBLookupComboBoxTextilAllapot.Enabled := false;
  Panel4.Enabled := false;
  DBLookupComboBoxTextilMeret.Enabled := false;
  Panel5.Enabled := false;
  DBLookupComboTextilMagassag.Enabled := false;
  Panel6.Enabled := false;
  DBLookupComboBoxTextilSzin.Enabled := false;
  Panel7.Enabled := false;
  DBLookupComboBoxKivetelezesOka.Enabled := false;
  Panel8.Enabled := false;
  pubInEdit := false;
  dbgTextilList.Enabled := true;
  CheckAllBtn.Enabled := true;
  UncheckAllButton.Enabled := true;
  button1.Tag := 0;
  button1.Caption := 'Szerkeszt�s';
  button3.Enabled := false;
  Button5.Enabled := false;
end;

procedure TfrmPufferVisszavetel.Button4Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPufferVisszavetel.Button5Click(Sender: TObject);
begin
  case MessageDlg('Biztosan t�rli a kijel�lt t�telt?', mtConfirmation, [mbYes, mbNo], 0) of
    mrNo: Exit;
  end;
  FDMemTableVisszavetelezes.First;
  while not FDMemTableVisszavetelezes.Eof do
    begin
      if FDMemTableVisszavetelezes.FieldByName('select').AsBoolean = true then
        begin
          FDMemTableVisszavetelezes.Delete;
        end
      else
        begin
          FDMemTableVisszavetelezes.Next;
        end;
    end;
  RedrawOsszPanel;
end;

procedure TfrmPufferVisszavetel.Button6Click(Sender: TObject);
var i: integer;
    ids: array of integer;
    lcFgv: TfgvUnit;
    lcMeretCsoport: integer;
    lcUjMegnevezesVal, lcUjSzinVal, lcUjMeretVal: integer;
begin
  // Kiv�lasztott t�tel szerkeszt�se / Ment�se
  if GetSelectedRows() = 0 then
    exit;
  if Button6.Tag = 0 then
    begin
      dbgTextilList.Enabled := false;
      CheckAllBtn.Enabled := false;
      UncheckAllButton.Enabled := false;
      pubInEdit := true;
      DBLookupComboBoxMegnevezes_t.Enabled := true;
      Panel10.Enabled := true;
      DBLookupComboBoxTextilFajta_t.Enabled := True;
      Panel11.Enabled := true;
      DBLookupComboBoxTextilAllapot_t.Enabled := true;
      Panel12.Enabled := true;
      DBLookupComboTextilMagassag_t.Enabled := true;
      Panel15.Enabled := true;
      DBLookupComboBoxKivetelezesOka_t.Enabled := true;
      Panel17.Enabled := true;
      DBMemoTetelMegjegyzes_t.Enabled := false;
      RadioMegjegyzes.Enabled := true;
      Button6.Tag := 1;
      Button6.Caption := 'V�ltoztat�sok ment�se';
      button7.Enabled := true;
      Button5.Enabled := false;

      if (pubSameSelected) and (Length(pubSelectedIds) > 0) then
        begin
          lcFgv := TfgvUnit.Create;
          prCDSSzin.Filtered := false;
          prCDSSzin.Filter := 'kpsz_kpt_id = '+IntToStr(pubSelectedIds[0])+' OR kpsz_szin_id < 0';
          prCDSSzin.Filtered := true;

          lcMeretCsoport := lcFgv.GetMeretCsoportByTextilId(pubSelectedIds[0], false);

          prCDSMeret.Filtered := false;
          prCDSMeret.Filter := 'meret_csoport = '+IntToStr(lcMeretCsoport)+' OR meret_id < 0';
          prCDSMeret.Filtered := true;

          DBLookupComboBoxTextilSzin_t.Enabled := true;
          DBLookupComboBoxTextilSzin_t.KeyValue := -2;
          DBLookupComboBoxTextilMeret_t.Enabled := true;
          DBLookupComboBoxTextilMeret_t.KeyValue := -2;

          lcFgv.Free;
        end
      else
        begin
          DBLookupComboBoxTextilSzin_t.Enabled := false;
          DBLookupComboBoxTextilSzin_t.KeyValue := -2;
          DBLookupComboBoxTextilMeret_t.Enabled := false;
          DBLookupComboBoxTextilMeret_t.KeyValue := -2;
        end;
    end
  else
    begin
      lcUjMegnevezesVal := DBLookupComboBoxMegnevezes_t.KeyValue;
      lcUjSzinVal := DBLookupComboBoxTextilSzin_t.KeyValue;
      lcUjMeretVal := DBLookupComboBoxTextilMeret_t.KeyValue;
      FDMemTableVisszavetelezes.First;
      while not FDMemTableVisszavetelezes.Eof do
        begin
          if FDMemTableVisszavetelezes.FieldByName('select').AsBoolean then
            begin

              if (lcUjMegnevezesVal = -2) then
                begin
                  lcUjMegnevezesVal := FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger;
                end
              else if (lcUjMegnevezesVal = -1) then
                begin
                  lcUjMegnevezesVal := -999999;
                end
              else if (lcUjMegnevezesVal > -1) then
                begin
                  lcUjMegnevezesVal := DBLookupComboBoxMegnevezes_t.KeyValue;
                end;

              if (lcUjSzinVal = -2) then
                begin
                  lcUjSzinVal := FDMemTableVisszavetelezes.FieldByName('szinid').AsInteger;
                end
              else if (lcUjSzinVal = -1) then
                begin
                  lcUjSzinVal := -999999;
                end
              else if (lcUjSzinVal > -1) then
                begin
                  lcUjSzinVal := DBLookupComboBoxTextilSzin_t.KeyValue;
                end;

              if (lcUjMeretVal = -2) then
                begin
                  lcUjMeretVal := FDMemTableVisszavetelezes.FieldByName('meretid').AsInteger;
                end
              else if (lcUjMeretVal = -1) then
                begin
                  lcUjMeretVal := -999999;
                end
              else if (lcUjMeretVal > -1) then
                begin
                  lcUjMeretVal := DBLookupComboBoxTextilMeret_t.KeyValue;
                end;

              if (DBLookupComboBoxMegnevezes_t.KeyValue > -1) then
                begin
                  if not ChangesAllowed(
                    lcUjMegnevezesVal,
                    lcUjMeretVal,
                    lcUjSzinVal
                  ) then
                    begin
                      ShowMessage('A kiv�lasztott Ruha megnevez�se kijel�lt t�telek sz�n�vel �s/vagy m�ret�vel nem alkalmazhat�.');
                      exit;
                    end;
                end;
            end;
          FDMemTableVisszavetelezes.Next;
        end;

      FDMemTableVisszavetelezes.First;
      while not FDMemTableVisszavetelezes.Eof do
        begin
          if not FDMemTableVisszavetelezes.FieldByName('select').AsBoolean then
            begin
              FDMemTableVisszavetelezes.Next;
              Continue;
            end;
          FDMemTableVisszavetelezes.Edit;
          if DBLookupComboBoxMegnevezes_t.KeyValue > -1 then
            begin
              pubSelectedIds := SetSelectedIds(FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger, true);
              pubSelectedIds := SetSelectedIds(DBLookupComboBoxMegnevezes_t.KeyValue, false);
              FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant := DBLookupComboBoxMegnevezes_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('textilnev').AsVariant := DBLookupComboBoxMegnevezes_t.Text;
            end
          else if DBLookupComboBoxMegnevezes_t.KeyValue = -1 then
            begin
              pubSelectedIds := SetSelectedIds(FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger, true);
              FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('textilnev').AsVariant := Null;
            end;

          if DBLookupComboBoxTextilFajta_t.KeyValue > -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant := DBLookupComboBoxTextilFajta_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('fajtanev').AsVariant := DBLookupComboBoxTextilFajta_t.Text;
            end
          else if DBLookupComboBoxTextilFajta_t.KeyValue = -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('fajtanev').AsVariant := Null;
            end;

          if DBLookupComboBoxTextilAllapot_t.KeyValue > -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant := DBLookupComboBoxTextilAllapot_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('allapotnev').AsVariant := DBLookupComboBoxTextilAllapot_t.Text;
            end
          else if DBLookupComboBoxTextilAllapot_t.KeyValue = -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('allapotnev').AsVariant := Null;
            end;

          if DBLookupComboTextilMagassag_t.KeyValue > -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant := DBLookupComboTextilMagassag_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('magassagnev').AsVariant := DBLookupComboTextilMagassag_t.Text;
            end
          else if DBLookupComboTextilMagassag_t.KeyValue = -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('magassagnev').AsVariant := Null;
            end;

          if DBLookupComboBoxKivetelezesOka_t.KeyValue > -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsVariant := DBLookupComboBoxKivetelezesOka_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('kivezetesokanev').AsVariant := DBLookupComboBoxKivetelezesOka_t.Text;
            end
          else if DBLookupComboBoxKivetelezesOka_t.KeyValue = -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('kivezetesokanev').AsVariant := Null;
            end;

          if DBLookupComboBoxTextilSzin_t.KeyValue > -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant := DBLookupComboBoxTextilSzin_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('szinnev').AsVariant := DBLookupComboBoxTextilSzin_t.Text;
            end
          else if DBLookupComboBoxTextilSzin_t.KeyValue = -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('szinnev').AsVariant := Null;
            end;

          if DBLookupComboBoxTextilMeret_t.KeyValue > -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant := DBLookupComboBoxTextilMeret_t.KeyValue;
              FDMemTableVisszavetelezes.FieldByName('meretnev').AsVariant := DBLookupComboBoxTextilMeret_t.Text;
            end
          else if DBLookupComboBoxTextilMeret_t.KeyValue = -1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant := Null;
              FDMemTableVisszavetelezes.FieldByName('meretnev').AsVariant := Null;
            end;

          if RadioMegjegyzes.ItemIndex = 1 then
            begin
              FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsVariant := Null;
            end
          else if RadioMegjegyzes.ItemIndex = 2 then
            begin
              FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsVariant := DBMemoTetelMegjegyzes_t.Text;
            end;

          FDMemTableVisszavetelezes.Post;
          FDMemTableVisszavetelezes.Next;
        end;

      RedrawOsszPanel;
      DBLookupComboBoxMegnevezes_t.Enabled := false;
      Panel10.Enabled := false;
      DBLookupComboBoxTextilFajta_t.Enabled := false;
      Panel11.Enabled := false;
      DBLookupComboBoxTextilAllapot_t.Enabled := false;
      Panel12.Enabled := false;
      DBLookupComboTextilMagassag_t.Enabled := false;
      Panel15.Enabled := false;
      DBLookupComboBoxKivetelezesOka_t.Enabled := false;
      Panel17.Enabled := false;
      DBMemoTetelMegjegyzes_t.Enabled := false;
      RadioMegjegyzes.Enabled := false;
      pubInEdit := false;
      dbgTextilList.Enabled := true;
      CheckAllBtn.Enabled := true;
      UncheckAllButton.Enabled := true;
      Button6.Tag := 0;
      Button6.Caption := 'Szerkeszt�s';
      button7.Enabled := false;
      Button5.Enabled := true;

      DBLookupComboBoxTextilSzin_t.Enabled := false;
      DBLookupComboBoxTextilMeret_t.Enabled := false;
    end;
end;

procedure TfrmPufferVisszavetel.Button7Click(Sender: TObject);
begin
  DBLookupComboBoxTextilFajta_t.KeyValue := -2;
  DBLookupComboBoxTextilAllapot_t.KeyValue := -2;
  DBLookupComboTextilMagassag_t.KeyValue := -2;
  DBLookupComboBoxMegnevezes_t.KeyValue := -2;
  DBLookupComboBoxKivetelezesOka_t.KeyValue := -2;
  DBMemoTetelMegjegyzes_t.Lines.Clear;
  RadioMegjegyzes.ItemIndex := 0;
  DBLookupComboBoxMegnevezes_t.Enabled := false;
  Panel10.Enabled := false;
  DBLookupComboBoxTextilFajta_t.Enabled := false;
  Panel11.Enabled := false;
  DBLookupComboBoxTextilAllapot_t.Enabled := false;
  Panel12.Enabled := false;
  DBLookupComboTextilMagassag_t.Enabled := false;
  Panel15.Enabled := false;
  DBLookupComboBoxKivetelezesOka_t.Enabled := false;
  Panel17.Enabled := false;
  DBMemoTetelMegjegyzes_t.Enabled := false;
  RadioMegjegyzes.Enabled := false;
  pubInEdit := false;
  dbgTextilList.Enabled := true;
  CheckAllBtn.Enabled := true;
  UncheckAllButton.Enabled := true;
  Button6.Tag := 0;
  Button6.Caption := 'Szerkeszt�s';
  button7.Enabled := false;
  Button5.Enabled := true;

  DBLookupComboBoxTextilSzin_t.Enabled := false;
  DBLookupComboBoxTextilMeret_t.Enabled := false;
end;

procedure TfrmPufferVisszavetel.CheckAllBtnClick(Sender: TObject);
begin
  prSelectedRow := 0;
  FDMemTableVisszavetelezes.First;
  while not FDMemTableVisszavetelezes.Eof do
    begin
      pubSelectedIds := SetSelectedIds(FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger, false);
      FDMemTableVisszavetelezes.Edit;
      FDMemTableVisszavetelezes.FieldByName('select').AsBoolean := true;
      FDMemTableVisszavetelezes.Post;
      FDMemTableVisszavetelezes.Next;
      prSelectedRow := prSelectedRow + 1;
    end;
  if prSelectedRow > 1 then
    begin
      PageControl2.ActivePageIndex := 1;
      DBLookupComboBoxTextilFajta_t.KeyValue := -2;
      DBLookupComboBoxTextilAllapot_t.KeyValue := -2;
      DBLookupComboTextilMagassag_t.KeyValue := -2;
      DBLookupComboBoxMegnevezes_t.KeyValue := -2;
      DBLookupComboBoxKivetelezesOka_t.KeyValue := -2;
      DBLookupComboBoxTextilSzin_t.KeyValue := -2;
      DBLookupComboBoxTextilMeret_t.KeyValue := -2;
      DBMemoTetelMegjegyzes_t.Lines.Clear;
      RadioMegjegyzes.ItemIndex := 0;
    end
  else
    begin
      PageControl2.OnChange(nil);
    end;
end;

procedure TfrmPufferVisszavetel.dbgTextilListCellClick(Column: TColumn);
begin
  {Checkbox}
  if (Column.Field.DataType=ftBoolean) then
   begin
     {toggle True and False}
     Column.Grid.DataSource.DataSet.Edit;
     Column.Field.Value:= not Column.Field.AsBoolean;
    {immediate post - see for yourself whether you want this}
     if (Column.Field.Value = true) then
      begin
        prSelectedRow := prSelectedRow + 1;
        pubSelectedIds := SetSelectedIds(dbgTextilList.DataSource.DataSet.FieldByName('textilid').AsInteger, false);
      end
     else
      begin
        prSelectedRow := prSelectedRow - 1;
        pubSelectedIds := SetSelectedIds(dbgTextilList.DataSource.DataSet.FieldByName('textilid').AsInteger, true);
      end;

     Column.Grid.DataSource.DataSet.Post;
     {you may add additional functionality here,
    to be processed after the change was made}
   end;

  if (GetSelectedRows() <= 1) then
    begin
      PageControl2.ActivePageIndex := 0;
    end
  else if GetSelectedRows() > 1 then
    begin
      PageControl2.ActivePageIndex := 1;
    end;
  PageControl2.OnChange(nil);
end;

procedure TfrmPufferVisszavetel.dbgTextilListDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
   CtrlState: array[Boolean] of integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED) ;
begin
  if FDMemTableVisszaVetelezes.RecordCount = 0 then
    exit;
  if (FDMemTableVisszavetelezes.FieldByName('textilnev').AsString = '') OR
     (FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString = '') OR
     (FDMemTableVisszavetelezes.FieldByName('meretnev').AsString = '') OR
     (FDMemTableVisszavetelezes.FieldByName('kivezetesokanev').AsString = '') then
    begin
      dbgTextilList.Canvas.Brush.Color := clYellow;
      dbgTextilList.Canvas.Font.Color := clBlack;
    end;
  dbgTextilList.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  if (Column.Field.DataType=ftBoolean) then
    begin
      dbgTextilList.Canvas.FillRect(Rect) ;
      if VarIsNull(Column.Field.Value) then
        DrawFrameControl(dbgTextilList.Canvas.Handle,Rect, DFC_BUTTON, DFCS_BUTTONCHECK or DFCS_INACTIVE) {grayed}
      else
        begin
          DrawFrameControl(dbgTextilList.Canvas.Handle,Rect, DFC_BUTTON, CtrlState[Column.Field.AsBoolean]) ; {checked or unchecked}
        end;
    end;
end;

procedure TfrmPufferVisszavetel.DSTextilek_tDataChange(Sender: TObject; Field: TField);
var lcFgv: TfgvUnit;
    lcMeretCsoport: integer;
begin
  lcFgv := TfgvUnit.Create;
  if (FDMemTableVisszavetelezes.RecordCount > 0) AND pubInEdit then
    begin
      if (DBLookupComboBoxMegnevezes_t.KeyValue > 0) then
        begin
          prCDSSzin.Filtered := false;
          prCDSSzin.Filter := 'kpsz_kpt_id = '+string(DBLookupComboBoxMegnevezes_t.KeyValue)+' OR kpsz_szin_id < 0';
          prCDSSzin.Filtered := true;

          lcMeretCsoport := lcFgv.GetMeretCsoportByTextilId(DBLookupComboBoxMegnevezes_t.KeyValue, false);

          prCDSMeret.Filtered := false;
          prCDSMeret.Filter := 'meret_csoport = '+IntToStr(lcMeretCsoport)+' OR meret_id < 0';
          prCDSMeret.Filtered := true;

          DBLookupComboBoxTextilSzin_t.Enabled := true;
          DBLookupComboBoxTextilSzin_t.KeyValue := -2;
          DBLookupComboBoxTextilMeret_t.Enabled := true;
          DBLookupComboBoxTextilMeret_t.KeyValue := -2;
        end
      else if (DBLookupComboBoxMegnevezes_t.KeyValue = -1) then
        begin
          DBLookupComboBoxTextilSzin_t.Enabled := false;
          DBLookupComboBoxTextilSzin_t.KeyValue := null;
        end
      else if (DBLookupComboBoxMegnevezes_t.KeyValue = -2) and pubSameSelected and (length(pubSelectedIds) > 0) then
        begin
          prCDSSzin.Filtered := false;
          prCDSSzin.Filter := 'kpsz_kpt_id = '+IntToStr(pubSelectedIds[0])+' OR kpsz_szin_id < 0';
          prCDSSzin.Filtered := true;

          lcMeretCsoport := lcFgv.GetMeretCsoportByTextilId(pubSelectedIds[0], false);

          prCDSMeret.Filtered := false;
          prCDSMeret.Filter := 'meret_csoport = '+IntToStr(lcMeretCsoport)+' OR meret_id < 0';
          prCDSMeret.Filtered := true;

          DBLookupComboBoxTextilSzin_t.Enabled := true;
          DBLookupComboBoxTextilSzin_t.KeyValue := -2;
          DBLookupComboBoxTextilMeret_t.Enabled := true;
          DBLookupComboBoxTextilMeret_t.KeyValue := -2;
        end;
    end
  else
    begin
      DBLookupComboBoxTextilSzin_t.Enabled := false;
      DBLookupComboBoxTextilSzin_t.KeyValue := -2;
      DBLookupComboBoxTextilMeret_t.Enabled := false;
      DBLookupComboBoxTextilMeret_t.KeyValue := -2;
    end;
  lcFgv.Free;
{


  if prCDSSzin.RecordCount = 0 then
    begin
      DBLookupComboBoxTextilSzin_t.Enabled := false;
      DBLookupComboBoxTextilSzin_t.KeyValue := null;
    end
  else
    begin
      if pubInEdit then
        DBLookupComboBoxTextilSzin_t.Enabled := true;
    end;
  }
end;

procedure TfrmPufferVisszavetel.DSVisszavetelezesDataChange(Sender: TObject;
  Field: TField);
begin
  if FDMemTableVisszavetelezes.RecordCount > 0 then
    begin
      button2.Enabled := true;
      button5.Enabled := true;
    end
  else
    begin
      button2.Enabled := false;
      button5.Enabled := false;
    end;
end;

procedure TfrmPufferVisszavetel.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if prCloseOnSave then
    begin
      SerialReader.Stop;
      Action := caFree;
    end
  else
    begin
      case MessageDlg('Biztos megszak�tja a m�veletet?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            SerialReader.Stop;
            Action := caFree;
          end;
        mrNo: Action := caNone;
      end;
    end;
  if Action = caFree then
    frmPufferVisszavetel:= nil;
end;

procedure TfrmPufferVisszavetel.FormCreate(Sender: TObject);
var i :integer;
    IniFile: TIniFile;
    prPort, prPortConfig: string;
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;
  FDMemTableVisszavetelezes.Tag := 0;
  // Hide tab from TabControll
  for i := 0 to PageControl1.PageCount -1  do
    begin
      PageControl1.Pages[i].TabVisible := false;
    end;
  PageControl1.ActivePageIndex := 0;

  for i := 0 to PageControl2.PageCount -1  do
    begin
      PageControl2.Pages[i].TabVisible := false;
    end;
  PageControl2.ActivePageIndex := 0;
  // Set datafields
  DBLookupComboBoxTextilFajta.KeyField := 'fajta_id';
  DBLookupComboBoxTextilFajta.ListField := 'fajta_nev';
  DBLookupComboBoxTextilAllapot.KeyField := 'allapot_id';
  DBLookupComboBoxTextilAllapot.ListField := 'allapot_nev';
  DBLookupComboBoxTextilMeret.KeyField := 'meret_id';
  DBLookupComboBoxTextilMeret.ListField := 'meret_nev';
  DBLookupComboTextilMagassag.KeyField := 'magassag_id';
  DBLookupComboTextilMagassag.ListField := 'magassag_nev';
  DBLookupComboBoxMegnevezes.KeyField := 'kpt_id';
  DBLookupComboBoxMegnevezes.ListField := 'kpt_smegnev';
  DBLookupComboBoxKivetelezesOka.KeyField := 'kivezetesokaid';
  DBLookupComboBoxKivetelezesOka.ListField := 'kivezetesokanev';

  if DM.CDSTextilSzinek.Active then
    begin
      DBLookupComboBoxTextilSzin.KeyField := 'kpsz_szin_id';
      DBLookupComboBoxTextilSzin.ListField := 'szin_nev';
    end;
  DBLookupComboBoxTextilSzin.Enabled := false;

  pubSameSelected := false;

  pubCDSMegnevezes := CreateCustomDataset(DM.CDSTextilek, 'kpt_id', 'kpt_smegnev');
  prCDSFajta := CreateCustomDataset(DM.CDSTextilFajta, 'fajta_id', 'fajta_nev');
  prCDSAllapot := CreateCustomDataset(DM.CDSTextilAllapot, 'allapot_id', 'allapot_nev');
  prCDSMagassag := CreateCustomDataset(DM.CDSTextilMagassag, 'magassag_id', 'magassag_nev');
  prCDSKivetelezes := CreateCustomDataset(DM.CDSKivetelezesOka, 'kivezetesokaid', 'kivezetesokanev');
  prCDSMeret := CreateCustomDataset(DM.CDSTextilMeret, 'meret_id', 'meret_nev');
  prCDSSzin := CreateCustomDataset(DM.CDSTextilSzinek, 'kpsz_szin_id', 'szin_nev');

  DSTextilFajta_t.DataSet := prCDSFajta;
  DBLookupComboBoxTextilFajta_t.ListSource := DSTextilFajta_t;
  DBLookupComboBoxTextilFajta_t.KeyField := 'fajta_id';
  DBLookupComboBoxTextilFajta_t.ListField := 'fajta_nev';

  DSTextilAllapot_t.DataSet := prCDSAllapot;
  DBLookupComboBoxTextilAllapot_t.ListSource := DSTextilAllapot_t;
  DBLookupComboBoxTextilAllapot_t.KeyField := 'allapot_id';
  DBLookupComboBoxTextilAllapot_t.ListField := 'allapot_nev';

  DSTextilMagassag_t.DataSet := prCDSMagassag;
  DBLookupComboTextilMagassag_t.ListSource := DSTextilMagassag_t;
  DBLookupComboTextilMagassag_t.KeyField := 'magassag_id';
  DBLookupComboTextilMagassag_t.ListField := 'magassag_nev';

  DSTextilek_t.DataSet := pubCDSMegnevezes;
  DBLookupComboBoxMegnevezes_t.ListSource := DSTextilek_t;
  DBLookupComboBoxMegnevezes_t.KeyField := 'kpt_id';
  DBLookupComboBoxMegnevezes_t.ListField := 'kpt_smegnev';

  DSKivetelezesOka_t.DataSet := prCDSKivetelezes;
  DBLookupComboBoxKivetelezesOka_t.ListSource := DSKivetelezesOka_t;
  DBLookupComboBoxKivetelezesOka_t.KeyField := 'kivezetesokaid';
  DBLookupComboBoxKivetelezesOka_t.ListField := 'kivezetesokanev';

  DSTextilMeret_t.DataSet := prCDSMeret;
  DBLookupComboBoxTextilMeret_t.ListSource := DSTextilMeret_t;
  DBLookupComboBoxTextilMeret_t.KeyField := 'meret_id';
  DBLookupComboBoxTextilMeret_t.ListField := 'meret_nev';

  DSTextilSzinek_t.DataSet := prCDSSzin;
  DBLookupComboBoxTextilSzin_t.ListSource := DSTextilSzinek_t;
  DBLookupComboBoxTextilSzin_t.KeyField := 'kpsz_szin_id';
  DBLookupComboBoxTextilSzin_t.ListField := 'szin_nev';

  // Chip Beolvas�s
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  prPort := IniFile.ReadString('port', 'szam', prPort);
  prPortConfig := IniFile.ReadString('port', 'ertek', prPortConfig);
  IniFile.Free;
  SerialReader := TSerialReader.Create('visszavet', prPort, prPortConfig);
  prCloseOnSave := false;
  FDMemTableVisszavetelezes.Active := true;
end;

procedure TfrmPufferVisszavetel.PageControl2Change(Sender: TObject);
var lastSelect: string;
begin
  if (GetSelectedRows() = 0) then
    begin
      DBLookupComboBoxTextilFajta.KeyValue := FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant;
      DBLookupComboBoxTextilAllapot.KeyValue := FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant;
      DBLookupComboBoxTextilMeret.KeyValue := FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant;
      DBLookupComboTextilMagassag.KeyValue := FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant;
      DBLookupComboBoxMegnevezes.KeyValue := FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant;
      DBLookupComboBoxKivetelezesOka.KeyValue := FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsVariant;
      DM.DSTextilek.OnDataChange(nil, nil);
      DBLookupComboBoxTextilSzin.KeyValue := FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant;
      DBMemoTetelMegjegyzes.Lines.Clear;
      DBMemoTetelMegjegyzes.Text := FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsString;
    end
  else if (GetSelectedRows() = 1) then
    begin
      SendMessage(Handle, WM_SETREDRAW, WPARAM(False), 0);
      lastSelect := FDMemTableVisszavetelezes.FieldByName('chipkod').AsString;
      FDMemTableVisszavetelezes.First;
      while not FDMemTableVisszavetelezes.FieldByName('select').AsBoolean do
        FDMemTableVisszavetelezes.Next;
      DBLookupComboBoxTextilFajta.KeyValue := FDMemTableVisszavetelezes.FieldByName('fajtaid').AsVariant;
      DBLookupComboBoxTextilAllapot.KeyValue := FDMemTableVisszavetelezes.FieldByName('allapotid').AsVariant;
      DBLookupComboBoxTextilMeret.KeyValue := FDMemTableVisszavetelezes.FieldByName('meretid').AsVariant;
      DBLookupComboTextilMagassag.KeyValue := FDMemTableVisszavetelezes.FieldByName('magassagid').AsVariant;
      DBLookupComboBoxMegnevezes.KeyValue := FDMemTableVisszavetelezes.FieldByName('textilid').AsVariant;
      DBLookupComboBoxKivetelezesOka.KeyValue := FDMemTableVisszavetelezes.FieldByName('kivezetesokaid').AsVariant;
      DM.DSTextilek.OnDataChange(nil, nil);
      DBLookupComboBoxTextilSzin.KeyValue := FDMemTableVisszavetelezes.FieldByName('szinid').AsVariant;
      DBMemoTetelMegjegyzes.Lines.Clear;
      DBMemoTetelMegjegyzes.Text := FDMemTableVisszavetelezes.FieldByName('megjegyzes').AsString;
      FDMemTableVisszavetelezes.First;
      while FDMemTableVisszavetelezes.FieldByName('chipkod').AsString <> lastSelect do
        FDMemTableVisszavetelezes.Next;
      SendMessage(Handle, WM_SETREDRAW, WPARAM(True), 0);
      RedrawWindow(Self.Handle, nil, 0, RDW_ERASE or RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN);
    end
  else
    begin
      DBLookupComboBoxTextilFajta_t.KeyValue := -2;
      DBLookupComboBoxTextilAllapot_t.KeyValue := -2;
      DBLookupComboTextilMagassag_t.KeyValue := -2;
      DBLookupComboBoxMegnevezes_t.KeyValue := -2;
      DBLookupComboBoxKivetelezesOka_t.KeyValue := -2;
      DBLookupComboBoxTextilMeret_t.KeyValue := -2;
      DBLookupComboBoxTextilSzin_t.KeyValue := -2;
      DBMemoTetelMegjegyzes_t.Lines.Clear;
      RadioMegjegyzes.ItemIndex := 0;
    end;
end;

procedure TfrmPufferVisszavetel.Panel13Click(Sender: TObject);
begin
  DBLookupComboBoxMegnevezes.KeyValue := null;
  DBLookupComboBoxMegnevezes_t.KeyValue := -2;
  DM.DSTextilek.OnDataChange(nil, nil);
end;

procedure TfrmPufferVisszavetel.Panel1Click(Sender: TObject);
begin
  DBLookupComboBoxTextilFajta.KeyValue := null;
  DBLookupComboBoxTextilFajta_t.KeyValue := -2;
end;

procedure TfrmPufferVisszavetel.Panel3Click(Sender: TObject);
begin
  DBLookupComboBoxTextilAllapot.KeyValue := null;
  DBLookupComboBoxTextilAllapot_t.KeyValue := -2;
end;

procedure TfrmPufferVisszavetel.Panel4Click(Sender: TObject);
begin
  DBLookupComboBoxTextilMeret.KeyValue := null;
  DBLookupComboBoxTextilMeret_t.KeyValue := -2;
end;

procedure TfrmPufferVisszavetel.Panel5Click(Sender: TObject);
begin
  DBLookupComboTextilMagassag.KeyValue := null;
  DBLookupComboTextilMagassag_t.KeyValue := -2;
end;

procedure TfrmPufferVisszavetel.Panel6Click(Sender: TObject);
begin
  DBLookupComboBoxTextilSzin.KeyValue := null;
  DBLookupComboBoxTextilSzin_t.KeyValue := -2;
end;

procedure TfrmPufferVisszavetel.Panel7Click(Sender: TObject);
begin
  DBLookupComboBoxKivetelezesOka.KeyValue := null;
  DBLookupComboBoxKivetelezesOka_t.KeyValue := -2;
end;

procedure TfrmPufferVisszavetel.RadioMegjegyzesClick(Sender: TObject);
begin
  if RadioMegjegyzes.ItemIndex < 2 then
    DBMemoTetelMegjegyzes_t.Enabled := false
  else
    DBMemoTetelMegjegyzes_t.Enabled := true;
end;

procedure TfrmPufferVisszavetel.RedrawOsszPanel;

  function lcIndexOf(arr: array of TBevetTetel; haystack: TBevetTetel): integer;
  var i: integer;
  begin
    result := -1;
    for i := 0 to Length(arr)-1 do
    begin
      if (arr[i].Fajta = haystack.Fajta) and
         (arr[i].Allapot = haystack.Allapot) and
         (arr[i].Szin = haystack.Szin) and
         (arr[i].Meret = haystack.Meret) and
         (arr[i].Megnev = haystack.Megnev) and
         (arr[i].Magassag = haystack.Magassag) then
        begin
          result := i;
          break;
        end;
    end;
  end;

var i: integer;
    lcTetelek: array of TBevetTetel;
    lcCurrentTetel: TBevetTetel;
    lcFindIndex: integer;
    lcScrollPosition: integer;
begin
  if FDMemTableVisszavetelezes.Tag = 1 then
    Exit;
  FDMemTableVisszavetelezes.Tag := 1;
  SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(false), 0);
  try
    lcScrollPosition :=  ScrollBox1.VertScrollBar.ScrollPos;
    for i := 0 to length(prPanels)-1 do
      begin
        if assigned(prPanels[i]) then
          begin
            prPanels[i].Destroy;
          end;
      end;
    setLength(prPanels, 0);
    FDMemTableVisszavetelezes.First;
    for i := 0 to FDMemTableVisszavetelezes.RecordCount-1 do
      begin
        lcCurrentTetel.Fajta := FDMemTableVisszavetelezes.FieldByName('fajtanev').AsString;
        lcCurrentTetel.Allapot := FDMemTableVisszavetelezes.FieldByName('allapotnev').AsString;
        lcCurrentTetel.Szin := FDMemTableVisszavetelezes.FieldByName('szinnev').AsString;
        lcCurrentTetel.Meret := FDMemTableVisszavetelezes.FieldByName('meretnev').AsString;
        lcCurrentTetel.Magassag := FDMemTableVisszavetelezes.FieldByName('magassagnev').AsString;
        lcCurrentTetel.Megnev := FDMemTableVisszavetelezes.FieldByName('textilnev').AsString;
        lcCurrentTetel.Mennyiseg := 1;
        lcFindIndex := lcIndexOf(lcTetelek, lcCurrentTetel);
        if (lcFindIndex > -1) then
          begin
            lcTetelek[lcFindIndex].Mennyiseg := lcTetelek[lcFindIndex].Mennyiseg + lcCurrentTetel.Mennyiseg;
          end
        else
          begin
            SetLength(lcTetelek, length(lcTetelek)+1);
            lcTetelek[length(lcTetelek)-1] := lcCurrentTetel;
          end;
          FDMemTableVisszavetelezes.Next;
      end;
    setLength(prPanels, length(lcTetelek));
    for i := 0 to length(lcTetelek)-1 do
      begin
      if (lcTetelek[i].Mennyiseg <> 0) then
        begin
          prPanels[i] := CopyComponents(samplePanel, samplePanel.Owner, samplePanel.Parent, prNextPanel);
          SetPanelLabels(prPanels[i], lcTetelek[i].Fajta, lcTetelek[i].Allapot, lcTetelek[i].Szin, lcTetelek[i].Meret, lcTetelek[i].Magassag, lcTetelek[i].Megnev, lcTetelek[i].Mennyiseg);
          TPanel(prPanels[i]).Top := (samplePanel.Height*i)+4;
          TPanel(prPanels[i]).visible := true;
          prNextPanel := prNextPanel + 1;
        end;
      end;
  finally
    begin
      ScrollBox1.VertScrollBar.Position := lcScrollPosition;
      SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(true), 0);
      ScrollBox1.Repaint;
    end;
  end;
  FDMemTableVisszavetelezes.Tag := 0;
end;

procedure TfrmPufferVisszavetel.UncheckAllButtonClick(Sender: TObject);
begin
  FDMemTableVisszavetelezes.First;
  while not FDMemTableVisszavetelezes.Eof do
    begin
      pubSelectedIds := SetSelectedIds(FDMemTableVisszavetelezes.FieldByName('textilid').AsInteger, true);
      FDMemTableVisszavetelezes.Edit;
      FDMemTableVisszavetelezes.FieldByName('select').AsBoolean := false;
      FDMemTableVisszavetelezes.Post;
      FDMemTableVisszavetelezes.Next;
    end;
  prSelectedRow := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl2Change(nil);
end;

function TfrmPufferVisszavetel.ChangesAllowed(kpt_id, meret_id, szin_id: integer): boolean;
var meret_csoport: integer;
begin
  meret_csoport := -1;
  result := true;
  DM.CDSTextilMeret.First;
  while not DM.CDSTextilMeret.Eof do
    begin
      if DM.CDSTextilMeret.FieldByName('meret_id').AsInteger = meret_id then
        begin
          meret_csoport := DM.CDSTextilMeret.FieldByName('meret_csoport').AsInteger;
          break;
        end;
      DM.CDSTextilMeret.Next;
    end;
  DM.CDSTextilek.First;
  while not DM.CDSTextilek.Eof do
    begin
      if DM.CDSTextilek.FieldByName('kpt_id').AsInteger <> kpt_id then
        DM.CDSTextilek.Next
      else
        begin
          if (meret_csoport <> DM.CDSTextilek.FieldByName('kpt_meretcsoport').AsInteger) and (meret_id > 0) then
            begin
              result := false;
              exit;
            end;
            DM.CDSTextilSzinek.Filtered := false;
            DM.CDSTextilSzinek.Filter := 'kpsz_kpt_id = '+IntToStr(kpt_id)+' and kpsz_szin_id = '+IntToStr(szin_id);
            DM.CDSTextilSzinek.Filtered := true;
            if (DM.CDSTextilSzinek.RecordCount = 0)  and (szin_id > 0) then
              begin
                result := false;
                exit;
              end;
          DM.CDSTextilek.Next;
        end;
    end;
end;

end.
