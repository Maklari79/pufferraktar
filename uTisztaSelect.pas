unit uTisztaSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids, INIFiles;

type
  TfrmTisztaSelect = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    btnBezar: TButton;
    btnRendben: TButton;
    ilTisztaSelect: TImageList;
    gbSzallLIsta: TGroupBox;
    gbSzallTetel: TGroupBox;
    dbgSzallLista: TDBGrid;
    dbgSzallTetelek: TDBGrid;
    procedure btnBezarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure dbgSzallListaCellClick(Column: TColumn);
    procedure btnRendbenClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTisztaSelect: TfrmTisztaSelect;

implementation

uses uDM, uHttps, uFgv, uTiszta;
{$R *.dfm}

procedure TfrmTisztaSelect.btnBezarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmTisztaSelect.btnRendbenClick(Sender: TObject);
var
  lcFajl : String;
  lcFajlTalalt : Integer;
  lcSearchResult : TSearchRec;
begin
  //Kiv�lasztott sz�ll�t� bet�lt�se...
  lcFajl:= DM.CDSTSzallLista.FieldByName('sztet_fajlnev').AsString;
  lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
  if lcFajlTalalt = 0 then
    begin
      GTFajlNev := lcFajl;
      DM.CDSXMLMentes.LoadFromFile(lcFajl);
      DM.CDSXMLMentes.LogChanges := false;
      DM.CDSXMLMentes.Active := true;
    end;
  Close;
end;

procedure TfrmTisztaSelect.dbgSzallListaCellClick(Column: TColumn);
var
  lcFajlTalalt : Integer;
  lcFajl : String;
  lcSearchResult : TSearchRec;
begin
  //Kiv�lasztott sz�ll�t�t bet�lteni.
  lcFajl:= DM.CDSTSzallLista.FieldByName('sztet_fajlnev').AsString;
  lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);
  if lcFajlTalalt <> 0 then
    begin
      ShowMessage('Nincs ilyen f�jl!');
    end
  else if lcFajlTalalt = 0 then
    begin
      DM.CDSTSzallTetelek.LoadFromFile(lcFajl);
      DM.CDSTSzallTetelek.LogChanges := false;
      DM.CDSTSzallTetelek.Active := true;
      DM.CDSTSzallTetelek.FieldByName('szfej_id').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_partner').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_uzem').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_mozgid').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_osztid').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_delete').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_lezart').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_osszsuly').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_lastuser').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_lastup').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_createuser').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_createdate').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_szallszam').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_kijelol').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_mertsuly').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('szfej_kiszdatum').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_osztid').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_osztsuly').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_textilid').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_textilsuly').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_lastuser').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_lastup').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_delete').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_createuser').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_createdate').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_recsorsz').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_javitott').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_fajlnev').Visible := false;
      DM.CDSTSzallTetelek.FieldByName('sztet_textilnev').DisplayWidth := 50;
      DM.CDSTSzallTetelek.FieldByName('sztet_textilnev').DisplayLabel := 'Textilnev';
      DM.CDSTSzallTetelek.FieldByName('sztet_textildb').DisplayLabel := 'Bek�sz�tend�';
      DM.CDSTSzallTetelek.FieldByName('sztet_bekeszit').DisplayLabel := 'Bek�sz�tett';
    end;
end;

procedure TfrmTisztaSelect.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmTisztaSelect.FormShow(Sender: TObject);
var
  lcDataSetToXML : TfgvUnit;
  lcS : TfgvUnit;
  lcXmlSave : THttpsUnit;
  lcIPCim, lcServerUt, lcMentFajlNev, lcS_0 : String;
  lciniFile : TIniFile;
  lcI_0 : Integer;
  lcSearchResult : TSearchRec;
begin
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcServerUt := lciniFile.ReadString('utvonal','path','');

  DM.CDSTSzallLista.Active := true;
  DM.CDSTSzallLista.FieldByName('szfej_id').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_partner').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_uzem').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_mozgid').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_osztid').Visible := false;
  DM.CDSTSzallLista.FieldByName('oszt_nev').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_delete').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_lezart').Visible := false;
  DM.CDSTSzallLista.FieldByName('sztet_fajlnev').Visible := false;
  DM.CDSTSzallLista.FieldByName('szfej_szallszam').DisplayWidth := 50;
  DM.CDSTSzallLista.FieldByName('szfej_szallszam').DisplayLabel := 'Sz�ll�t� sorsz�m';
  DM.CDSTSzallLista.FieldByName('szfej_kiszdatum').DisplayLabel := 'Kisz�ll�t�s d�tuma';

  DM.CDSTSzallLista.Filtered := false;
  DM.CDSTSzallLista.Filter := 'szfej_osztid=' + IntToStr(GOsztalyID);
  DM.CDSTSzallLista.Filtered := true;
  DM.CDSTSzallLista.First;
  while not DM.CDSTSzallLista.Eof do
    begin
      //Sz�ll�t�levelek let�lt�se szfej_ID alapj�n �s ment�s.
      lcS := TfgvUnit.Create();
      lcXmlSave := THttpsUnit.Create();
      DM.CDSXMLMentes.XMLData := lcXmlSave.GetSzallito(lcIPCim,lcServerUt,'szallitolevel',DM.CDSTSzallLista.FieldByName('szfej_id').AsInteger);
      DM.CDSXMLMentes.First;
      lcMentFajlNev := 'T'+ GMappa + IntToStr(GOsztalyID);
      lcS_0 := lcMentFajlNev;
      lcI_0 := 0;
      if (FindFirst(lcS_0+'.xml', faAnyFile, lcSearchResult) = 0) then
        begin
          while FindFirst(lcMentFajlNev+'.xml', faAnyFile, lcSearchResult) = 0 do
            begin
              lcMentFajlNev := lcS_0 + '_' + IntToStr(lcI_0) +'.xml';
              inc(lcI_0);
            end;
        end
      else if (FindFirst(lcS_0+'.xml', faAnyFile, lcSearchResult) <> 0) then
        begin
          lcMentFajlNev := lcMentFajlNev + '.xml';
        end;

      while not DM.CDSXMLMentes.Eof do
        begin
          DM.CDSXMLMentes.Edit;
          DM.CDSXMLMentes.FieldByName('szfej_id').AsInteger := DM.CDSTSzallLista.FieldByName('szfej_id').AsInteger;
          DM.CDSXMLMentes.FieldByName('sztet_fajlnev').AsString := lcMentFajlNev;
          DM.CDSXMLMentes.FieldByName('sztet_recsorsz').AsInteger := DM.CDSXMLMentes.RecNo;
          DM.CDSXMLMentes.Post;
          DM.CDSXMLMentes.Next;
        end;
      DM.CDSTSzallLista.Edit;
      DM.CDSTSzallLista.FieldByName('sztet_fajlnev').AsString := lcMentFajlNev;
      DM.CDSTSzallLista.Post;
      lcDataSetToXML := TfgvUnit.Create();
      lcDataSetToXML.DataSetToXML(DM.CDSXMLMentes, lcMentFajlNev);
      lcXmlSave.Free;
      lcDataSetToXML.Free;
      lcS.Free;
      DM.CDSTSzallLista.Next;
    end;
end;

end.
