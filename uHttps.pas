unit uHttps;

interface

uses System.UITypes, System.SysUtils, Vcl.Forms, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
     FireDAC.Stan.Param, Datasnap.DBClient, Data.DB, IdHTTP, IdStack, System.Classes,
     IdIOHandler, System.Hash;

type
  THttpsUnit = class
  private
    function SzovegToHash(ASzoveg: string): string;
  public
    function GetHttps(AIPCim : String; AUtvonal : String; ATabla : String;  APartner : Integer) : String;
    function GetHttps_2(AIPCim : String; AUtvonal : String; ATabla : String) : String;
    function GetHttpsFajlok(AIPCim : String; AUtvonal : String; ATabla : String; AFajlnev : String; AUser : Integer) : String;
    function PostHttps(AXml: TFileName; AIPCim : String; AUtvonal : String; ATabla : String) : String;
    function PutHttps(AIPCim : String; AUtvonal : String; ATabla : String; AMozg : String; ASorszam : Integer; APartner : Integer; AUserID : Integer) : String;
    function GetHttpsFajlLetoltes(AIPCim : String; AUtvonal : String; ATabla : String; APartnerID : Integer; AUzemID : Integer; AMozgID : Integer; AKiszDatum : String) : String;
    function GetSzallito(AIPCim : String; AUtvonal : String; ATabla : String; ASzallitoID : Integer) : String;
    function TisztaPutHttps(AIPCim : String; AUtvonal : String; ATabla : String; ATSzID : Integer; ATXml: String) : String;
    function GetTisztaSzallito(AIPCim : String; AUtvonal : String; ATabla : String; ATSzID : Integer) : String;
    function GetRFIDMaxSzam(AIPCim : String; AUtvonal : String; ATabla : String; AResz: Integer) : String;
  end;

implementation

uses uDM, uFgv, uTMRMain, uLogs;

function THttpsUnit.SzovegToHash(ASzoveg: string): string;
var
  lcHasht : THashSHA2;
  lcStrHash : string;
begin
  lcHasht:=THashSHA2.Create;
  lcStrHash:=lcHasht.GetHashString(ASzoveg,SHA512);
  result:=UpperCase(lcStrHash);
end;

function THttpsUnit.GetTisztaSzallito(AIPCim: string; AUtvonal: string; ATabla: string; ATSzID: Integer) : String;
var
  lcUrl, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash, lcDatumTol, lcDatumig, lcUserID : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
  lcHTTP : TIdHTTP;
begin
  //A tiszta sz�ll�t�t ellen�r�zz�k, hogy fel van-e t�ltve.
  //Lehets�ges v�laszok: ID:F�jlnev; ID:0; 0:0 nem lehets�ges.
  lcUrl := '';
  lcHTTP := TIdHTTP.Create();
  lcHTTP.Request.Accept:='application/xml';
  lcHTTP.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  lcHTTP.Request.CustomHeaders.AddValue('ido',lcdtheader);
  lcHTTP.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  lcHTTP.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + '/'+ IntToStr(ATSzID);
      result := lcHTTP.GET(lcUrl);
      lcLogs := TlogsUnit.Create();
      lcLogs.LogFajlWrite(' - GetTisztaSzallito -' + ATabla + ' - ' + Result);
      lcLogs.Free;
      lcHTTP.Disconnect;
    except
      on E: Exception do
        begin
          lcHTTP.Disconnect;
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetTisztaSzallito -' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          lcHTTP.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    lcHTTP.Request.CustomHeaders.Clear;
  end;

end;


function THttpsUnit.GetHttps(AIPCim : String; AUtvonal : String; ATabla : String; APartner : Integer) : String;
var
  lcUrl, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash, lcDatumTol, lcDatumig : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
begin
  //Lek�rdez�s https-en kereszt�l.
  lcUrl := '';
  DM.IdHttp1.Request.Accept:='application/xml';
  DM.IdHttp1.Request.ContentType:='application/xml';
  DM.IdHttp1.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHttp1.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHttp1.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHttp1.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      if ATabla = 'osztalyokpartner/' then
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + intToStr(APartner);
          result:= DM.IdHttp1.Get(lcUrl);
        end
      else if ATabla = 'textilekpartner/' then
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + intToStr(APartner);
          result:= DM.IdHttp1.Get(lcUrl);
        end
      else if ATabla = 'naptarpartner/' then
        begin
          lcDatumIg := DateToStr(now());
          lcDatumTol := Copy(lcDatumIg, 1,5) + '01.01./';
          lcDatumIg := Copy(lcDatumIg, 1,5) + '12.31.';
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + intToStr(APartner)+ '/' + lcDatumTol + lcDatumIg;
          result:= DM.IdHttp1.Get(lcUrl);
        end;
      if ATabla = 'hello/' then
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + intToStr(APartner);
          result:= DM.IdHttp1.Get(lcUrl);
        end;
      if ATabla = 'textilszinek/' then
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + intToStr(APartner);
          result:= DM.IdHttp1.Get(lcUrl);
        end;
    except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetHttps - ' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHttp1.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.GetHttps_2(AIPCim : String; AUtvonal : String; ATabla : String) : String;
var
  ErrCode : Integer;
  lcUrl, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash, lcATabla : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
begin
  //Lek�rdez�s https-en kereszt�l.
  lcUrl := '';
  DM.IdHttp1.Request.Accept:='application/xml';
  DM.IdHttp1.Request.ContentType:='application/xml';
  DM.IdHttp1.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHttp1.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHttp1.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHttp1.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      if ATabla = 'chippartner/' then
        begin
          DM.CDSPartnerek.First;
          lcATabla := ATabla;
          while not DM.CDSPartnerek.Eof do
            begin
              lcATabla := lcATabla + IntToStr(DM.CDSPartnerek.FieldByName('par_id').AsInteger) + ',';
              DM.CDSPartnerek.Next;
            end;
          lcATabla := copy(lcATabla,1,lcATabla.Length-1);
          lcUrl := 'https://' +AIPCim + AUtvonal + lcATabla;
          result:= DM.IdHttp1.Get(lcUrl);
        //  GAdatMeret := DM.IdHttp1.response.contentLength;
        end
      else if ATAbla = 'prdarabosdb/' then
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla;
          lcUrl := lcUrl + '?uzemID='+DM.CDSUzem.FieldByName('uzem_id').AsString;
          result:= DM.IdHttp1.Get(lcUrl);
        end
      else if (ATabla = 'pkkszamok/') or (ATabla = 'pkkszallitok/') or (ATabla = 'pkkchipmozgas/') then
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla;
          lcUrl := lcUrl + DM.CDSUzem.FieldByName('uzem_id').AsString;
          result:= DM.IdHttp1.Get(lcUrl);
        end
      else
        begin
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla;
          result:= DM.IdHttp1.Get(lcUrl);
        end;
    except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetHttps_2 - ' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHttp1.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.GetRFIDMaxSzam(AIPCim, AUtvonal, ATabla: String; AResz: Integer): String;
var
  ErrCode : Integer;
  lcUrl, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash, lcATabla : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
begin
  //Lek�rdez�s https-en kereszt�l.
  lcUrl := '';
  DM.IdHttp1.Request.Accept:='application/xml';
  DM.IdHttp1.Request.ContentType:='application/xml';
  DM.IdHttp1.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHttp1.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHttp1.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHttp1.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      if ATabla = 'RFIDCount/' then
        begin
          DM.CDSPartnerek.First;
          lcATabla := ATabla;
          while not DM.CDSPartnerek.Eof do
            begin
              lcATabla := lcATabla + IntToStr(DM.CDSPartnerek.FieldByName('par_id').AsInteger) + ',';
              DM.CDSPartnerek.Next;
            end;
          lcATabla := copy(lcATabla,1,lcATabla.Length-1);
          lcUrl := 'https://' +AIPCim + AUtvonal + lcATabla;
          result:= DM.IdHttp1.Get(lcUrl);
        //  GAdatMeret := DM.IdHttp1.response.contentLength;
        end
      else if ATabla = 'chippartnerbin/' then
        begin
          DM.CDSPartnerek.First;
          lcATabla := ATabla;
          while not DM.CDSPartnerek.Eof do
            begin
              lcATabla := lcATabla + IntToStr(DM.CDSPartnerek.FieldByName('par_id').AsInteger) + ',';
              DM.CDSPartnerek.Next;
            end;
          lcATabla := copy(lcATabla,1,lcATabla.Length-1) + '/';

          lcUrl := 'https://' +AIPCim + AUtvonal + lcATabla + IntToStr(AResz);
          result:= DM.IdHttp1.Get(lcUrl);
        end;
    except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetRFIDMaxSzam - ' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHttp1.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.GetHttpsFajlok(AIPCim : String; AUtvonal : String; ATabla : String; AFajlnev : String; AUser : Integer) : String;
var
  lcUrl, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash, lcDatumTol, lcDatumig, lcUserID : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
begin
  lcUrl := '';
  DM.IdHTTP2.Request.Accept:='application/xml';
 // DM.IdHttp1.Request.ContentType:='application/xml';
  DM.IdHTTP2.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      if ATabla = 'feltoltottfajl/osszerendeles' then
        begin
          //http://127.0.0.1/emer/v1/feltoltottfajl/osszerendeles?fajlnev=ossze_20180905114743.xml&felhasznaloID=1
          lcUserID := intToStr(AUser);
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'?fajlnev='+ AFajlnev + '&felhasznaloID=' +lcUserID;
          //lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'/'+ AFajlnev + '/' +lcUserID;
          result:= DM.IdHTTP2.Get(lcUrl);
        end;
      if ATabla = 'feltoltottfajl/mozgas' then
        begin
          //http://127.0.0.1/emer/v1/feltoltottfajl/osszerendeles?fajlnev=ossze_20180905114743.xml&felhasznaloID=1
          lcUserID := intToStr(AUser);
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'?fajlnev='+ AFajlnev + '&felhasznaloID=' +lcUserID;
          result:= DM.IdHTTP2.Get(lcUrl);
        end;
      if ATabla = 'feltoltottfajl/chipmozgasell' then
        begin
          //http://127.0.0.1/emer/v1/feltoltottfajl/osszerendeles?fajlnev=ossze_20180905114743.xml&felhasznaloID=1
          lcUserID := intToStr(AUser);
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'?fajlnev='+ AFajlnev + '&felhasznaloID=' +lcUserID;
          result:= DM.IdHTTP2.Get(lcUrl);
        end;
    except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetHttpsFajlok - ' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHTTP2.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.GetHttpsFajlLetoltes(AIPCim : String; AUtvonal : String; ATabla : String; APartnerID : Integer; AUzemID : Integer; AMozgID : Integer; AKiszDatum : String) : String;
var
  lcUrl, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash, lcDatumTol, lcDatumig, lcUserID : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
begin
  lcUrl := '';
  DM.IdHTTP2.Request.Accept:='application/xml';
  DM.IdHTTP2.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      if ATabla = 'szallitok' then
        begin
          // https://127.0.0.1/emer/v1/fajlletoltes/tiszta?uzemID=1&partnerID=1&kiszallitasdatuma=20190214
          //http://127.0.0.1/emer/v1/szallitok/?partnerID=13&uzemID=1&mozgID=2&kiszallitasdatuma=20181205
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'/?partnerID=' + IntToStr(APartnerID) + '&uzemID='+ IntToStr(AUzemID) + '&mozgID=' + IntToStr(AMozgID) + '&kiszallitasdatuma=' + AKiszDatum;
          //lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'/'+ AFajlnev + '/' +lcUserID;
          result:= DM.IdHTTP2.Get(lcUrl);
        end;
      except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetHttpsFajlLetoltes - Nincs tiszta f�jl! -' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHTTP2.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.GetSzallito(AIPCim : String; AUtvonal : String; ATabla : String; ASzallitoID : Integer) : String;
var
  lcUrl, lcSzoveg, lcAuthHash, lcHashJelszo, lcdtheader, lcUt : String;
  lcLogs : TlogsUnit;
begin
  //Fej id alapj�n t�lti le a sz�ll�t�levelet.
  lcUrl := '';
  DM.IdHTTP2.Request.Accept:='application/xml';
  DM.IdHTTP2.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHTTP2.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      if ATabla = 'szallitolevel' then
        begin
          //http:127.0.0.1/emer/v1/szallitolevel/1
          lcUrl := 'https://' +AIPCim + AUtvonal + ATabla +'/'+ IntToStr(ASzallitoID);
          result:= DM.IdHTTP2.Get(lcUrl);
        end;
      except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - GetSzallito - Nincs tiszta f�jl! -' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHTTP2.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.PostHttps(AXml: TFileName; AIPCim : String; AUtvonal : String; ATabla : String) : String;
var
  lcUrl, lcBody, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash : String;
  lcUtvonal : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
begin
  //Adat k�ldes https-en kereszt�l.
  DM.IdHttp1.Request.Accept:='application/xml';
  DM.IdHttp1.Request.ContentType:='application/xml';
  DM.IdHttp1.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHttp1.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHttp1.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHttp1.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  sleep(1000);
  try
    try
      lcUrl := 'https://' +AIPCim + AUtvonal + ATabla;
      result := DM.IdHTTP1.Post(lcUrl, AXml);
      lcLogs := TlogsUnit.Create();
      lcLogs.LogFajlWrite(' - PostHttps - ' + ATabla + ' ID:' + result);
      lcLogs.Free;
    except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - PostHttps -' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          if Assigned(frmTMRMain) then
            frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
  // haszn�ld az E.ErrorCode, E.Message, and E.ErrorMessage ig�ny eset�n (pl. 500-as hibak�d)...
        end;
    end;
  finally
    DM.IdHttp1.Request.CustomHeaders.Clear;
  end;
end;

function THttpsUnit.PutHttps(AIPCim : String; AUtvonal : String; ATabla : String; AMozg : String; ASorszam : Integer; APartner : Integer; AUserID : Integer) : String;
var
  lcParamkuld : TStream;
  lcUrl, lcBody, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash : String;
  lcUtvonal, lcSorsz, szam : String;
  lcUt, lcMasol : String;
  lcLogs : TlogsUnit;
  lcHTTP : TIdHTTP;
begin
  lcHTTP := TIdHTTP.Create();
//  lcHTTP.Connect;
  lcHTTP.Request.ContentType:='application/x-www-form-urlencoded';
  lcHTTP.Request.ContentEncoding:='utf-8';
//  DM.IdHttp1.Request.ContentType:='application/x-www-form-urlencoded';
//  DM.IdHttp1.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  lcHTTP.Request.CustomHeaders.AddValue('ido',lcdtheader);
  lcHTTP.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
//  DM.IdHttp1.Request.CustomHeaders.AddValue('ido',lcdtheader);
//  DM.IdHttp1.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  lcHTTP.Request.CustomHeaders.AddValue('auth',lcAuthHash);
//  DM.IdHttp1.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      lcUrl := 'https://' +AIPCim + AUtvonal + ATabla + AMozg + '/'+ IntToStr(APartner) + '/'+ IntToStr(AUserID);
      szam := IntToStr(ASorszam);
      lcSorsz := 'sorszam='+ szam;
      lcParamkuld := TStringStream.Create(lcSorsz,TEncoding.UTF8);
//      lcHTTP.Connect;
      result := lcHTTP.Put(lcUrl,lcParamkuld);
      lcLogs := TlogsUnit.Create();
      lcLogs.LogFajlWrite(' - PutHttps -' + ATabla + ' - '  + AMozg + ' - ' + IntToStr(APartner) + ' - '+ IntToStr(AUserID) + ' - ' + Result);
      lcLogs.Free;

      //result := DM.IdHttp1.Put(lcUrl,lcParamkuld);
      lcHTTP.Disconnect;
    except
      on E: Exception do
        begin
//          frmTMRMain.StatusBar1.Panels.Items[1].Text := IntToStr(E.ErrorCode);
          lcHTTP.Disconnect;
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - PutHttps -' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          lcHTTP.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
  // haszn�ld az E.ErrorCode, E.Message, and E.ErrorMessage ig�ny eset�n (pl. 500-as hibak�d)...
        end;
    end;
  finally
    DM.IdHttp1.Request.CustomHeaders.Clear;
  end;
end;

//** Bek�szetett tiszta sz�ll�t�lev�l update **//
function THttpsUnit.TisztaPutHttps(AIPCim : String; AUtvonal : String; ATabla : String; ATSzID : Integer; ATXml: String) : String;
var
  lcUrl, lcBody, lcdtheader, lcHashJelszo, lcSzoveg, lcAuthHash : String;
  lcUtvonal : String;
  lcUt, lcMasol : String;
  lcParamkuld : TStream;
  lcLogs : TlogsUnit;
begin
  DM.IdHttp1.Request.ContentType:='application/x-www-form-urlencoded';
  DM.IdHttp1.Request.ContentEncoding:='utf-8';
  DateTimeToString(lcdtheader,'yyyymmddhhnnss',Now);
  DM.IdHttp1.Request.CustomHeaders.AddValue('ido',lcdtheader);
  DM.IdHttp1.Request.CustomHeaders.AddValue('felhasznalo',GUserNev);
  lcHashJelszo:=szovegToHash(GUserPassw);
  lcSzoveg:=lcdtheader+GUserNev+lcHashJelszo;
  lcAuthHash:=szovegToHash(lcSzoveg);
  DM.IdHttp1.Request.CustomHeaders.AddValue('auth',lcAuthHash);
  try
    try
      begin
        //http://127.0.0.1/emer/v1/tisztaszlev/1
        if ATabla = 'tisztaszlev' then
          begin
            lcUrl := 'https://' +AIPCim + AUtvonal + ATabla+'/'+ IntToStr(ATSzID);
            lcParamkuld := TStringStream.Create(ATXml,TEncoding.UTF8);
            result := DM.IdHttp1.Put(lcUrl,lcParamkuld);
            lcLogs := TlogsUnit.Create();
            lcLogs.LogFajlWrite(' - TisztaPutHttps - Sz�ll�t� ID: ' + IntToStr(ATSzID));
            lcLogs.LogFajlWrite(' - TisztaPutHttps - ' + result);
            lcLogs.Free;
          end;
      end;
    except
      on E: Exception do
        begin
          lcLogs := TlogsUnit.Create();
          lcLogs.LogFajlWrite(' - TisztaPutHttps -' + ATabla + ' - ' + E.Message);
          lcLogs.Free;
          frmTMRMain.StatusBar1.Panels.Items[1].Text := E.Message;
        end;
    end;
  finally
    DM.IdHttp1.Request.CustomHeaders.Clear;
  end;
//**Eddig.**//
end;

end.
