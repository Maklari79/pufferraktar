unit uSzallitoOssz;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Vcl.ComCtrls, System.DateUtils, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.FileCtrl, Datasnap.DBClient, IniFiles, frxClass, WinSpool,
  Printers;

type
  TPrinterInfo = record
    SeverName         : PChar;
    PrinterName       : PChar;
    ShareName         : PChar;
    PortName          : PChar;
    DriverName        : PChar;
    Comment           : PChar;
    Location          : PChar;
    DeviceMode        : PDeviceModeA;
    SepFile           : PChar;
    PrintProcessor    : PChar;
    DataType          : PChar;
    Parameters        : PChar;
    SecurityDescriptor: PSecurityDescriptor;
    Attributes        : Cardinal;
    DefaultPriority   : Cardinal;
    StartTime         : Cardinal;
    UntilTime         : Cardinal;
    Status            : Cardinal;
    Jobs              : Cardinal;
    AveragePPM        : Cardinal;
  end;

type
  TfrmSzallitoOssz = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    btnBezar: TButton;
    ilSzallOsszGomb: TImageList;
    rgSzennyTiszta: TRadioGroup;
    gbDatum: TGroupBox;
    gbProgress: TGroupBox;
    pbFolyamat: TProgressBar;
    btnMent: TButton;
    mdDatum: TMonthCalendar;
    mFajlList: TMemo;
    gbFaljdarab: TGroupBox;
    FDMOsszesit: TFDMemTable;
    CDSFajl: TClientDataSet;
    DSOsszesit: TDataSource;
    FDMOsszesitszfej_partner: TIntegerField;
    FDMOsszesitszfej_uzem: TIntegerField;
    FDMOsszesitszfej_mozgid: TIntegerField;
    FDMOsszesitszfej_osztid: TIntegerField;
    FDMOsszesitszfej_osszsuly: TFloatField;
    FDMOsszesitszfej_lastuser: TIntegerField;
    FDMOsszesitszfej_lastup: TDateTimeField;
    FDMOsszesitszfej_delete: TIntegerField;
    FDMOsszesitszfej_createuser: TIntegerField;
    FDMOsszesitszfej_createdate: TDateTimeField;
    FDMOsszesitszfej_szallszam: TStringField;
    FDMOsszesitszfej_kijelol: TIntegerField;
    FDMOsszesitszfej_mertsuly: TFloatField;
    FDMOsszesitsztet_osztid: TIntegerField;
    FDMOsszesitsztet_osztsuly: TFloatField;
    FDMOsszesitsztet_textilid: TIntegerField;
    FDMOsszesitsztet_textilnev: TStringField;
    FDMOsszesitsztet_textildb: TIntegerField;
    FDMOsszesitsztet_textilsuly: TFloatField;
    FDMOsszesitsztet_lastuser: TIntegerField;
    FDMOsszesitsztet_lastup: TDateTimeField;
    FDMOsszesitsztet_delete: TIntegerField;
    FDMOsszesitsztet_createuser: TIntegerField;
    FDMOsszesitsztet_createdate: TDateTimeField;
    FDMOsszesitsztet_fajlnev: TStringField;
    FDMOsszesitsztet_recsorsz: TIntegerField;
    FDMOsszesitsztet_javitott: TSmallintField;
    FDMOsszesitsztet_bekeszit: TIntegerField;
    Button1: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBezarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnMentClick(Sender: TObject);
    procedure mdDatumClick(Sender: TObject);
  private
    { Private declarations }
    prUtvonal, prUtMent, prFajlNev : String;
    prOsszSuly, prMertSuly : Double;
    procedure Keres(AUtvonal : String; ATipus : String);
    procedure FajlOsszesit(ATipus : String); //ATipus: szennyes vagy tiszta f�jlt kell �sszes�teni.
    procedure CDSToFDMEM(AMuvelet : String);
    function GetCurrentPrinterHandle: THandle;
    function GetCurrentPrinterInformation: TPrinterInfo;
  public
    { Public declarations }
    pubUtvonal : String;
  end;

var
  frmSzallitoOssz: TfrmSzallitoOssz;

implementation

uses uFgv, uDM, uHttps;

{$R *.dfm}

function TfrmSzallitoOssz.GetCurrentPrinterHandle: THandle;
var
  Device, Driver, Port : array[0..255] of char;
  hDeviceMode: THandle;
begin
  Printer.GetPrinter(Device, Driver, Port, hDeviceMode);
  if not OpenPrinter(@Device, Result, nil) then
    RaiseLastOSError;
end;

function TfrmSzallitoOssz.GetCurrentPrinterInformation: TPrinterInfo;
var
  hPrinter  : THandle;
  pInfo:  PPrinterInfo2;
  bytesNeeded: DWORD;
begin
  hprinter := GetCurrentPrinterHandle;
  try
    Winspool.GetPrinter( hPrinter, 4, Nil, 0, @bytesNeeded );
    pInfo := AllocMem( bytesNeeded );
    try
      Winspool.GetPrinter( hPrinter, 4, pInfo, bytesNeeded, @bytesNeeded );
       Result.SeverName          := pInfo^.pServerName;
       Result.PrinterName        := pInfo^.pPrinterName;
       Result.ShareName          := pInfo^.pShareName;
       Result.PortName           := pInfo^.pPortName;
       Result.DriverName         := pInfo^.pDriverName;
       Result.Comment            := pInfo^.pComment;
       Result.Location           := pInfo^.pLocation;
       Result.SepFile            := pInfo^.pSepFile;
       Result.PrintProcessor     := pInfo^.pPrintProcessor;
       Result.DataType           := pInfo^.pDatatype;
       Result.Parameters         := pInfo^.pParameters;
       Result.SecurityDescriptor := pInfo^.pSecurityDescriptor;
       Result.Attributes         := pInfo^.Attributes;
       Result.DefaultPriority    := pInfo^.DefaultPriority;
       Result.StartTime          := pInfo^.StartTime;
       Result.UntilTime          := pInfo^.UntilTime;
       Result.Status             := pInfo^.Status;
       Result.Jobs               := pInfo^.cJobs;
       Result.AveragePPM         := pInfo^.AveragePPM;
    finally
      FreeMem( pInfo );
    end;
  finally
    ClosePrinter( hPrinter );
  end;
end;

procedure TfrmSzallitoOssz.CDSToFDMEM(AMuvelet : String);
var
  lcMozg : TfgvUnit;
begin
  if AMuvelet = 'Osszead' then
    begin
      FDMOsszesit.Edit;
      FDMOsszesit.FieldByName('sztet_textildb').AsInteger := FDMOsszesit.FieldByName('sztet_textildb').AsInteger + CDSFajl.FieldByName('sztet_textildb').AsInteger;
      FDMOsszesit.FieldByName('sztet_osztsuly').AsFloat := FDMOsszesit.FieldByName('sztet_osztsuly').AsFloat + CDSFajl.FieldByName('sztet_osztsuly').AsFloat;
      FDMOsszesit.Post;
    end
  else if AMuvelet = 'Hozzaad' then
    begin
      FDMOsszesit.Append;
      FDMOsszesit.FieldByName('szfej_partner').AsInteger := CDSFajl.FieldByName('szfej_partner').AsInteger;
      FDMOsszesit.FieldByName('szfej_uzem').AsInteger := CDSFajl.FieldByName('szfej_uzem').AsInteger;
      lcMozg := TfgvUnit.Create();
      FDMOsszesit.FieldByName('szfej_mozgid').AsInteger := lcMozg.Mozgasnem;
      FDMOsszesit.FieldByName('szfej_osztid').AsInteger := GOsztalyID;
      FDMOsszesit.FieldByName('szfej_osszsuly').Asfloat := prOsszSuly;
      FDMOsszesit.FieldByName('szfej_lastuser').Asfloat := CDSFajl.FieldByName('szfej_lastuser').AsInteger;
      FDMOsszesit.FieldByName('szfej_lastup').AsDateTime := now();
      FDMOsszesit.FieldByName('szfej_delete').AsInteger := CDSFajl.FieldByName('szfej_delete').AsInteger;
      FDMOsszesit.FieldByName('szfej_createuser').AsInteger :=  CDSFajl.FieldByName('szfej_createuser').AsInteger;
      FDMOsszesit.FieldByName('szfej_createdate').AsDateTime := now();
      FDMOsszesit.FieldByName('szfej_szallszam').AsString := GSzallSorsz;
      FDMOsszesit.FieldByName('szfej_kijelol').AsInteger := CDSFajl.FieldByName('szfej_kijelol').AsInteger;
      FDMOsszesit.FieldByName('szfej_mertsuly').AsFloat := prMertSuly;
      FDMOsszesit.FieldByName('sztet_osztid').AsInteger := GOsztalyID;
      FDMOsszesit.FieldByName('sztet_osztsuly').AsFloat := CDSFajl.FieldByName('sztet_osztsuly').AsFloat;
      FDMOsszesit.FieldByName('sztet_textilid').AsInteger := CDSFajl.FieldByName('sztet_textilid').AsInteger;
      FDMOsszesit.FieldByName('sztet_textilnev').AsString := CDSFajl.FieldByName('sztet_textilnev').AsString;
      FDMOsszesit.FieldByName('sztet_textildb').AsInteger := CDSFajl.FieldByName('sztet_textildb').AsInteger;
      FDMOsszesit.FieldByName('sztet_textilsuly').AsFloat := CDSFajl.FieldByName('sztet_textilsuly').AsFloat;
      FDMOsszesit.FieldByName('sztet_lastuser').AsInteger := CDSFajl.FieldByName('sztet_lastuser').AsInteger;
      FDMOsszesit.FieldByName('sztet_lastup').AsDateTime := now();
      FDMOsszesit.FieldByName('sztet_delete').AsInteger := CDSFajl.FieldByName('sztet_delete').AsInteger;
      FDMOsszesit.FieldByName('sztet_createuser').AsInteger := CDSFajl.FieldByName('sztet_createuser').AsInteger;
      FDMOsszesit.FieldByName('sztet_createdate').AsDateTime := now();
      FDMOsszesit.FieldByName('sztet_fajlnev').AsString := prFajlNev;
      FDMOsszesit.FieldByName('sztet_recsorsz').AsInteger := FDMOsszesit.RecordCount + 1;
      FDMOsszesit.FieldByName('sztet_javitott').AsInteger := 0;
      FDMOsszesit.FieldByName('sztet_bekeszit').AsInteger := 0;
      FDMOsszesit.Post;
    end;
end;

procedure TfrmSzallitoOssz.FajlOsszesit(ATipus: string);
var
  lcFajlNev, lcUt : String;
  lcPartnerMappa, lcMappaCreate : TfgvUnit;

begin
  lcFajlNev := prUtvonal;
  SetCurrentDir(prUtvonal);
  lcPartnerMappa := TfgvUnit.Create();
  if not System.SysUtils.DirectoryExists(lcPartnerMappa.PartnerMappa(GPartner)) then
    begin
      prUtvonal := prUtvonal + '\' + lcPartnerMappa.PartnerMappa(GPartner);
    end
   else
     prUtvonal := prUtvonal + '\' + lcPartnerMappa.PartnerMappa(GPartner);
  SetCurrentDir(prUtvonal);
  lcMappaCreate := TfgvUnit.Create();
  if not System.SysUtils.DirectoryExists(lcMappaCreate.Mappa(GDate)) then
    begin
      prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate) + '\';
    end
  else
    prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(GDate) + '\';
  mFajlList.Clear;
  lcUt := prUtMent+'\adat'+prUtvonal;
  pubUtvonal := lcUt;
  Keres(lcUt, ATipus);
  SetCurrentDir(prUtMent);
  prUtvonal := '';
end;

procedure TfrmSzallitoOssz.Keres(AUtvonal : String; ATipus : String);
var
  lcSr : TSearchRec;
  lcFajlTipus : String;
begin
  if ATipus = 'S' then
    begin
      lcFajlTipus := 'S*.xml';
      GOldal := 'S';
    end
  else if ATipus = 'T' then
    begin
      lcFajlTipus := 'T*.xml';
      GOldal := 'T';
    end;
  if FindFirst(AUtvonal +lcFajlTipus, faAnyFile, lcSr) = 0 then
    begin
      repeat
          begin
            mFajlList.Lines.Add(lcSR.Name);
          End;
      until FindNext(lcSr) <> 0;
      FindClose(lcSR);
    end;
end;

procedure TfrmSzallitoOssz.btnBezarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSzallitoOssz.btnMentClick(Sender: TObject);
var
  I : Integer;
  lcVanAdat, lcHiba : Boolean;
  lcDataSetToXML, lcSzallSorszam, lcFajlGenNev, lcOsztNev, lcKorhaz, lcGeneral, lcMappa, lcPartnerMappa, lcMappaCreate: TfgvUnit;
  lcMentFajlNev, lcIPCim, lcUtvonal, lcFajlNev : String;
  lcSearchResult : TSearchRec;
  lcXmlPost : THttpsUnit;
  lciniFile : TIniFile;
begin
  //Sz�ll�t�levelek �sszes�t�se. Partnerenk�nt naponta az oszt�lyos szennyest v. tiszt�t.
  lcVanAdat := true;
  lcHiba := false;
  lcSzallSorszam := TfgvUnit.Create();
  GSzallSorsz := lcSzallSorszam.SzallSorGen;
  prFajlNev := '';
  prMertSuly := 0;
  prOsszSuly := 0;
  gbProgress.Caption := 'Folyamat...';
  Application.ProcessMessages;
  pbFolyamat.Position := 0;
  pbFolyamat.Max := (mFajlList.Lines.Count -1) + (mFajlList.Lines.Count -1) + 3 ;
  Application.ProcessMessages;
   for I := 0 to mFajlList.Lines.Count -1 do
    begin
      CDSFajl.LoadFromFile(pubUtvonal + mFajlList.Lines[I]);
      CDSFajl.LogChanges := false;
      CDSFajl.Active := true;
      CDSFajl.First;
      gbProgress.Caption := 'Folyamat...s�ly �sszes�t�s!';
      prMertSuly := prMertSuly + CDSFajl.FieldByName('szfej_mertsuly').AsFloat;
      prOsszSuly := prOsszSuly + CDSFajl.FieldByName('szfej_osszsuly').AsFloat;
      while not CDSFajl.Eof do
        begin
          prOsszSuly := prOsszSuly + (CDSFajl.FieldByName('sztet_textilsuly').AsFloat * CDSFajl.FieldByName('sztet_textildb').AsInteger);
          CDSFajl.Next;
        end;
      pbFolyamat.StepIt;
      Application.ProcessMessages;
    end;
  lcKorhaz := TfgvUnit.Create();
  GOsztalyID := lcKorhaz.SetOsztalyID('K�rh�z');
  lcFajlGenNev := TfgvUnit.Create();
  prFajlNev := 'O' + lcFajlGenNev.FajlNev(GDate);
    for I := 0 to mFajlList.Lines.Count -1 do
    begin
      CDSFajl.LoadFromFile(pubUtvonal + mFajlList.Lines[I]);
      CDSFajl.LogChanges := false;
      CDSFajl.Active := true;
      gbProgress.Caption := 'Folyamat...sz�ll�t�k �sszes�t�se! '+ mFajlList.Lines[I] ;
      try
        CDSFajl.First;
        while not CDSFajl.Eof do
          begin
            if (lcVanAdat) then
              begin
                FDMOsszesit.First;
                while (FDMOsszesit.Eof) and (lcVanAdat) do
                  begin
                    if FDMOsszesit.FieldByName('sztet_textilid').AsInteger = CDSFajl.FieldByName('sztet_textilid').AsInteger then
                      begin
                        CDSToFDMEM('Osszead');
                      end
                    else if FDMOsszesit.FieldByName('sztet_textilid').AsInteger <> CDSFajl.FieldByName('sztet_textilid').AsInteger then
                      begin
                        CDSToFDMEM('Hozzaad');
                      end;
                    FDMOsszesit.Next;
                    lcVanAdat := false;
                  end;
              end

            else if (not lcVanAdat) then
              begin
                FDMOsszesit.First;
                while (not FDMOsszesit.Eof) and (not lcVanAdat) do
                  begin
                    if FDMOsszesit.FieldByName('sztet_textilid').AsInteger = CDSFajl.FieldByName('sztet_textilid').AsInteger then
                      begin
                        CDSToFDMEM('Osszead');
                        lcVanAdat := true;
                      end;
                    FDMOsszesit.Next;
                  end;
                FDMOsszesit.First;
                while (not FDMOsszesit.Eof) and (not lcVanAdat) do
                  begin
                    if FDMOsszesit.FieldByName('sztet_textilid').AsInteger <> CDSFajl.FieldByName('sztet_textilid').AsInteger then
                      begin
                        CDSToFDMEM('Hozzaad');
                      end;
                    FDMOsszesit.Next;
                  end;
               end;
            lcVanAdat := false;
            CDSFajl.Next;
          end;
      except
        on E: Exception do
          begin
            ShowMessage('�sszegy�jt�si hiba: '+E.Message);
            lcHiba := true;
          end;
      end;
      pbFolyamat.StepIt;
      Application.ProcessMessages;
    end;

  if not lcHiba then
    begin
      try
        gbProgress.Caption := 'Folyamat...�sszes�tett sz�ll�t� ment�se!';
        if FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) <> 0 then
          begin
            lcDataSetToXML := TfgvUnit.Create();
//            lcFajlGenNev := TfgvUnit.Create();
//            prFajlNev := 'O' + lcFajlGenNev.FajlNev(GDate);
            lcMentFajlNev := pubUtvonal + prFajlNev;
            lcDataSetToXML.DataSetToXML(FDMOsszesit, lcMentFajlNev+'.xml');
            lcHiba := false;
        end;
      except
        on E: Exception do
          begin
            ShowMessage('Ment�si hiba: '+E.Message);
            lcHiba := true;
          end;
      end;
      pbFolyamat.StepIt;
      Application.ProcessMessages;
    end;

    //A tiszta f�jl gener�l�s...
    try
      DM.CDSParameterek.First;
      GOldal := 'T';
      lcMappa := TfgvUnit.Create;
      lcMappa.SetMappa(true, GPartner, 'textil.xml');
//Tiszta �tvonal be�ll�t�s kezd�s.
      prUtvonal := prUtMent+ '\adat\adat_'+IntToStr(GPartner);
      SetCurrentDir(prUtvonal);
//Tiszta �tvonal be�ll�t�s v�ge.
      while not DM.CDSParameterek.Eof do
        begin
          if (DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner) AND (DM.CDSParameterek.FieldByName('prm_tisztagenpart').AsString = 'Automatikus') then
            begin
              if GVer = 'TH' then
                begin
                  if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'O') AND (GOlTipus = 'O') then
                    begin
                      //Oszt�lyos tiszta gener�l�s....
                      DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                      DM.CDSLoadXml.LogChanges := false;
                      DM.CDSPartnerek.First;
                      lcGeneral := TfgvUnit.Create();
                      if lcGeneral.GenTiszta(DM.CDSLoadXml, true) then
                        begin
                          lcGeneral.Free;
                        end;
                    end
                  else if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'K') AND (GOlTipus = 'K') then
                    begin
                      //K�rh�zi szint�.....
                      DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                      DM.CDSLoadXml.LogChanges := false;
                      DM.CDSPartnerek.First;
                      lcGeneral := TfgvUnit.Create();
                      if lcGeneral.GenTiszta(DM.CDSLoadXml, true) then
                        begin
                          lcGeneral.Free;
                        end;
                    end;
                end
              else if GVer = 'RA' then
                begin
                  if DM.CDSParameterek.FieldByName('prm_tisztagenuzem').AsString = 'Automatikus' then
                    begin
                      if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'O') AND (GOlTipus = 'O') then
                        begin
                          //Oszt�lyos tiszta gener�l�s....
                          DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                          DM.CDSLoadXml.LogChanges := false;
                          DM.CDSPartnerek.First;
                          lcGeneral := TfgvUnit.Create();
                          if lcGeneral.GenTiszta(DM.CDSLoadXml, true) then
                            begin
                              lcGeneral.Free;
                            end;
                        end
                      else if (DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString = 'K') AND (GOlTipus = 'K') then
                        begin
                          //K�rh�zi szint�.....
                          DM.CDSLoadXml.LoadFromFile(lcMentFajlNev + '.xml');
                          DM.CDSLoadXml.LogChanges := false;
                          DM.CDSPartnerek.First;
                          lcGeneral := TfgvUnit.Create();
                          if lcGeneral.GenTiszta(DM.CDSLoadXml, true) then
                            begin
                              lcGeneral.Free;
                            end;
                        end;
                    end;
                end;
            end;
          DM.CDSParameterek.Next;
        end;
    except
      on E : Exception do
         begin
           MessageBox(handle,'Sikertelen tiszta gener�l�s!', 'Hibajelz�s!', MB_ICONERROR);
         end;
    end;
  SetCurrentDir(prUtMent);
  GOldal := 'S';
  if not lcHiba then
    begin
      try
        gbProgress.Caption := 'Folyamat...�sszes�tett sz�ll�t� k�ld�se!';
        if FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) = 0 then
          begin
            lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
            lcIPCim := lciniFile.ReadString('serverip','ip','');
            lcUtvonal := lciniFile.ReadString('utvonal','path','');
            lcXmlPost := THttpsUnit.Create();
            lcXmlPost.PostHttps(lcMentFajlNev+'.xml', lcIPCim, lcUtvonal,'szallitolevel/' );
            lcHiba := false;
          end;
      except
        on E: Exception do
          begin
            ShowMessage('Https k�ld�s hiba: '+E.Message);
            lcHiba := true;
          end;
      end;
      pbFolyamat.StepIt;
      Application.ProcessMessages;
    end;

  if not lcHiba then
    begin
      gbProgress.Caption := 'Folyamat...�sszes�tett sz�ll�t� nyomtat�sa!';
      try
        if FindFirst(lcMentFajlNev + '.xml', faAnyFile, lcSearchResult) = 0 then
          begin
            DM.CDSSzallMent.LoadFromFile(lcMentFajlNev + '.xml');

            DM.CDSPartnerek.First;
            DM.frxDBDataset1.DataSet := DM.CDSSzallMent;
            lcOsztNev := TfgvUnit.Create();
            if GOldal = 'S' then
              TfrxMemoView(DM.frxRepSzall.FindObject('mLBekeszit')).Visible := false;
            TfrxMemoView(DM.frxRepSzall.FindObject('mOsztNev')).Text := lcOsztNev.GetOsztalyNev(GOsztalyID);
            TfrxMemoView(DM.frxRepSzall.FindObject('mMozgnem')).Text := GOldal+GOlTipus; //lcMozg.Mozgasnem;     // lcMozg := TfgvUnit.Create();

            DM.CDSUzem.First;
            while not DM.CDSUzem.Eof do
              begin
                if DM.CDSUzem.FieldByName('uzem_id').AsInteger =  CDSFajl.FieldByName('szfej_uzem').AsInteger then
                  begin
                    TfrxMemoView(DM.frxRepSzall.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
                    TfrxMemoView(DM.frxRepSzall.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
                    TfrxMemoView(DM.frxRepSzall.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
                    TfrxMemoView(DM.frxRepSzall.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
                    TfrxMemoView(DM.frxRepSzall.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
                  end;
                DM.CDSUzem.Next;
              end;

            DM.CDSFelhasz.First;
            while not DM.CDSFelhasz.Eof do
              begin
                if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
                  begin
                    TfrxMemoView(DM.frxRepSzall.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  end;
                DM.CDSFelhasz.Next;
              end;

            while not DM.CDSPartnerek.Eof do
              begin
                if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
                  begin
                    TfrxMemoView(DM.frxRepSzall.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                  end;
                DM.CDSPartnerek.Next;
              end;
            try
              {PDF ment�s Start}
              DM.frxPDFExport1.FileName := lcMentFajlNev;
              DM.frxPDFExport1.DefaultPath := GetCurrentDir;
              DM.frxRepSzall.PrepareReport(true);
              DM.frxRepSzall.Export(DM.frxPDFExport1);
              {PDF ment�s End}

              DM.frxRepSzall.PrepareReport(true);
              DM.frxRepSzall.ShowPreparedReport;
              lcHiba := false;
            except
              on E: Exception do
                begin
                  ShowMessage('Nyomtat�si hiba: '+E.Message);
                  lcHiba := true;
                end;
            end;
        end;
      except
        on E: Exception do
          begin
            ShowMessage('Ment�si hiba: '+E.Message);
            lcHiba := true;
          end;
      end;
      pbFolyamat.StepIt;
      Application.ProcessMessages;
    end;

    if not lcHiba then
    begin
      gbProgress.Caption := 'Folyamat...sikeresen v�grehajt�dott!';
      MessageBox(handle, 'Sikeres ment�s �s k�ld�s!', 'T�j�koztat�s!', MB_ICONINFORMATION);
    end;

  pubUtvonal := '';
  CDSFajl.Active := false;
  DM.CDSLoadXml.Active := false;
  DM.CDSSzallMent.Active := false;
  Close;
end;


procedure TfrmSzallitoOssz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSzallitoOssz.FormShow(Sender: TObject);
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;
begin
  prUtMent := GetCurrentDir;
  mdDatum.Date := now();
  FDMOsszesit.Close;
  FDMOsszesit.Open;
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
      DM.CDSNaptar.LoadFromFile(lcUtvonal + '\naptar.xml');
      DM.CDSNaptar.LogChanges := false;
    end;
end;

procedure TfrmSzallitoOssz.mdDatumClick(Sender: TObject);
var
  lcDatumEll : TfgvUnit;
begin
  //
  GDate := mdDatum.Date;
  lcDatumEll := TfgvUnit.Create();
  if lcDatumEll.GetNaptar(GDate) then
    begin
      MessageBox(handle, 'Ezen a napon nem dolgozunk!', 'Figyelmeztet�s!', MB_ICONWARNING);
      exit;
    end;

  if rgSzennyTiszta.ItemIndex = 1 then
    begin
      FajlOsszesit('T');
    end;
  if rgSzennyTiszta.ItemIndex = 0 then
    begin
      FajlOsszesit('S');
    end;

  if mFajlList.Lines.Count = 0 then
    begin
      gbFaljdarab.Caption := 'Nincs f�jl erre a napra! ' + IntToStr(mFajlList.Lines.Count) +' f�jl.';
    end
  else
    begin
      gbFaljdarab.Caption := 'F�jl(ok) bet�lt�se k�sz! ' + IntToStr(mFajlList.Lines.Count) +' f�jl.';
    end;
end;

end.
