unit uSzennyes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ComCtrls, System.ImageList, Vcl.ImgList,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.Provider, Datasnap.DBClient, Vcl.Menus, System.DateUtils, System.UITypes;

type
  TfrmSzennyes = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    gbOsztalyok: TGroupBox;
    gbDatum: TGroupBox;
    mcDatum: TMonthCalendar;
    gbSzallitoAdat: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    eSzallSorszam: TEdit;
    eSuly: TEdit;
    btnAdatok: TButton;
    btnVissza: TButton;
    ilSzennyes: TImageList;
    FDQuery1: TFDQuery;
    DataSetProvider1: TDataSetProvider;
    dbgOsztalyok: TDBGrid;
    MainMenu1: TMainMenu;
    Osztly1: TMenuItem;
    btnSorGen: TButton;
    gbValPartner: TGroupBox;
    lValPartner: TLabel;
    lsInfo: TLabel;
    lSDatumVal: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnVisszaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgOsztalyokDblClick(Sender: TObject);
    procedure mcDatumDblClick(Sender: TObject);
    procedure btnAdatokClick(Sender: TObject);
    procedure eSulyKeyPress(Sender: TObject; var Key: Char);
    procedure eSulyExit(Sender: TObject);
    procedure btnSorGenClick(Sender: TObject);
    procedure eSzallSorszamKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    prUtvonal, prUtMent : String;
    procedure TextilBetolt;
  public
    { Public declarations }

  end;

var
  frmSzennyes: TfrmSzennyes;

implementation

uses uDM, uTMRMain, uSzennyesUj, uFgv, uChipBeolvas, uLogs;
{$R *.dfm}

procedure TfrmSzennyes.TextilBetolt;
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;

begin
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  DM.FDMemTextilek.Close;
  DM.FDMemTextilek.Open;
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\torzs_' + IntToStr(GPartner);
      DM.CDSTextilek.LoadFromFile(lcUtvonal+'\textil.xml');
      DM.CDSTextilek.LogChanges := false;
      DM.CDSTextilek.FieldByName('kpt_smegnev').DisplayLabel := 'Textil n�v';
      DM.CDSTextilek.FieldByName('kpt_id').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_textilId').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_partnerId').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_textilkod').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_textsuly').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texcsop').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texora').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texdbkg').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texberelt').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_term').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texar').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texallv').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texmos').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_keszlet').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_lastuser').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_lastup').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_delete').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_uzem').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_szamlaegyseg').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_szamlazando').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texchip').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_tersuly').Visible := false;
    end
  else
    begin
      MessageBox(handle, 'Nincs t�rzs adata ennek a partnernek!', 'Hiba!', MB_ICONERROR);
    end;
  SetCurrentDir(prUtMent);
end;

procedure TfrmSzennyes.btnAdatokClick(Sender: TObject);
var
  lcS : TfgvUnit;
begin
  GNincsTiszta := true;
  lcS := TfgvUnit.Create();
  GTFajlNev := 'S'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
  lcS.Free;
{  if (eSzallSorszam.Text = '') then
    begin
      MessageBox(handle, 'Nincs megadva a sz�ll�t�lev�l sorsz�ma! K�rem adja meg!', 'Figyelmeztet�s!', MB_ICONWARNING);
      eSzallSorszam.SetFocus;
    end }
  if (eSuly.Text = '') then
    begin
      MessageBox(handle, 'Nincs megadva a s�ly! K�rem adja meg!', 'Figyelmeztet�s!', MB_ICONWARNING);
      eSuly.SetFocus;
    end
  else
    begin
      //frmTMRMain.CreateModal(TfrmSzennyesUj);
      TextilBetolt;
      DM.CDSTextilek.Filtered := false;
      DM.CDSTextilek.Filter := 'kpt_texchip = TRUE';
      DM.CDSTextilek.Filtered := true;
      if DM.CDSTextilek.RecordCount > 0 then
        begin
          GEnable := false;
          frmTMRMain.CreateModal(TfrmChipBeolvas);
          Close;
        end
      else if DM.CDSTextilek.RecordCount = 0 then
        begin
          GEnable := true;
          DM.CDSTextilek.Filtered := false;
          DM.CDSTextilek.Filter := '';
          DM.CDSTextilek.Filtered := true;
          frmTMRMain.CreateModal(TfrmSzennyesUj);
          Close;
        end;
    end;
end;

procedure TfrmSzennyes.btnSorGenClick(Sender: TObject);
var
  lcSzallSorszam : TfgvUnit;
  lclabel : String;
begin
//  DM.CDSMozgnem.LoadFromFile('mozgasnem.xml');
//  DM.CDSMozgnem.LogChanges := false;
//  lclabel := DM.Szinkronizalas_2('mozgsorszam/');
//  DM.CDSSzallSorszam.LoadFromFile('sorszam.xml');
//  DM.CDSSzallSorszam.LogChanges := false;
//  lcSzallSorszam := TfgvUnit.Create();
//  eSzallSorszam.Text := lcSzallSorszam.SzallSorGen;
//  GSzallSorsz := eSzallSorszam.Text;
//  eSzallSorszam.ReadOnly := true;
//  btnSorGen.Enabled := false;
end;

procedure TfrmSzennyes.btnVisszaClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSzennyes.dbgOsztalyokDblClick(Sender: TObject);
var
  lcOsztEll : TfgvUnit;
begin
// ellen�rz�s hoztak-e m�r l�tre ilyen oszt�yt aznapra.
  SetCurrentDir(prUtvonal);
  GOsztalyID := dbgOsztalyok.DataSource.DataSet.FieldByName('kpo_id').AsInteger;
  lcOsztEll := TfgvUnit.Create();
  if lcOsztEll.FajlOsztalyEll(dbgOsztalyok.DataSource.DataSet.FieldByName('kpo_id').AsInteger) = '' then
    begin
      Osztly1.Caption :=  'Kiv�lasztott oszt�ly: ' + DM.CDSOsztalyok.FieldByName('oszt_nev').Value;
      GOsztNev := DM.CDSOsztalyok.FieldByName('oszt_nev').Value;
      mcDatum.Enabled := true;
      mcDatum.SetFocus;
      dbgOsztalyok.Enabled := false;
    end
  else
    begin
      case MessageDlg('Folytatni akarja a szennyes r�gz�t�st '
          + DM.CDSOsztalyok.FieldByName('oszt_nev').Value
          +' oszt�lynak? ', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
          mrNo:
            begin
              Abort;
            end;
          mrCancel:
            begin
              Abort;
            end;
          mrYes:
            begin

            end;
          end;
    end;
  SetCurrentDir(prUtMent);
end;

procedure TfrmSzennyes.eSulyExit(Sender: TObject);
begin
  if eSuly.Text = '' then
    begin
      GMertSuly := 0;
    end
  else
    begin
      GMertSuly := StrToFloat(eSuly.Text);
    end;
end;

procedure TfrmSzennyes.eSulyKeyPress(Sender: TObject; var Key: Char);
begin
 if (key in ['0'..'9',',']) or (key = #8) then
      inherited
 else key:= #0;
end;

procedure TfrmSzennyes.eSzallSorszamKeyPress(Sender: TObject; var Key: Char);
begin
  if eSzallSorszam.Text <> '' then
    btnSorGen.Enabled := false
  else
    btnSorGen.Enabled := true;
end;

procedure TfrmSzennyes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSzennyes.FormCreate(Sender: TObject);
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;
end;

procedure TfrmSzennyes.FormShow(Sender: TObject);
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;
  lcLogs : TlogsUnit;
begin
  lcLogs := TlogsUnit.Create();
  lcLogs.LogFajlWrite(' - Program - frmSzennyes megnyit�sa! ');
  lcLogs.Free;
  mcDatum.Date := now();
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  dbgOsztalyok.SetFocus;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
      DM.CDSNaptar.LoadFromFile(lcUtvonal + '\naptar.xml');
      DM.CDSNaptar.LogChanges := false;
      DM.CDSOsztalyok.LoadFromFile(lcUtvonal+'\osztaly.xml');
      DM.CDSOsztalyok.LogChanges := false;
      DM.CDSOsztalyok.FieldByName('kpo_id').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_partner').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_osztaly').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_kod').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_csoport').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_uzem').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastuser').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastup').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_delete').Visible := false;
      DM.CDSOsztalyok.FieldByName('oszt_nev').DisplayLabel := 'Oszt�ly n�v';
      btnAdatok.Enabled := false;
    end
  else
    begin
      MessageBox(handle, 'Nincs t�rzs adata ennek a partnernek!', 'Hiba!', MB_ICONERROR);
      btnAdatok.Enabled := false;
      gbOsztalyok.Enabled := false;
    end;

  lValPartner.Caption := '';
  DM.CDSPartnerek.First;
  while not DM.CDSPartnerek.Eof do
    begin
      if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
        begin
          lValPartner.Caption := DM.CDSPartnerek.FieldByName('par_nev').AsString;
          exit;
        end;
      DM.CDSPartnerek.Next;
    end;
end;

procedure TfrmSzennyes.mcDatumDblClick(Sender: TObject);
var
  lcDatum : TDate;
  lcKorhaz, lcFajlGenNev, lcMappa, lcDatumEll : TfgvUnit;
  lcOsztalyID : Integer;
  lcOsszFajlNev, lcUtvonal : String;
  lcSearchResult : TSearchRec;
begin
  lcDatum := now();
  GDate := mcDatum.Date;
  lcDatumEll := TfgvUnit.Create();
  if lcDatumEll.GetNaptar(GDate) then
    begin
      MessageBox(handle, 'Ezen a napon nem dolgozunk!', 'Figyelmeztet�s!', MB_ICONWARNING);
      exit;
    end
  else
    begin
    //  gbSzallitoAdat.Enabled := true;
      btnAdatok.Enabled := true;
     // eSzallSorszam.SetFocus;
      lSDatumVal.Caption := 'V�lasztott d�tum: ' + DateToStr(mcDatum.Date);
    end;
 { if CompareDate(mcDatum.Date,lcDatum) <> 0  then
    begin
      MessageBox(handle, 'Nem lehet visszad�tumozni!', 'Figyelmeztet�s!', MB_ICONWARNING);
      mcDatum.Date := now();
      mcDatum.SetFocus;
    end
  else if CompareDate(mcDatum.Date,lcDatum) = 0 then
    begin
      //Ha l�tezik az �sszes�tett f�jl akkor m�r nem lehet tov�bb szennyest r�gz�teni arra a napra.
      //�tvonalat be kell �ll�tani.
      lcKorhaz := TfgvUnit.Create();
      lcOsztalyID := lcKorhaz.SetOsztalyID('K�rh�z');
      lcFajlGenNev := TfgvUnit.Create();
      //mappa kell...
      lcMappa := TfgvUnit.Create();
      lcOsszFajlNev := 'O' + lcFajlGenNev.FajlNev_2(GDate, lcOsztalyID);
      lcUtvonal := (prUtvonal + '\adat_'+ IntToStr(GPartner) + '\' + lcMappa.Mappa(mcDatum.Date) + '\' +lcOsszFajlNev+'.xml');
      if FindFirst(lcUtvonal, faAnyFile, lcSearchResult) = 0 then
        begin
          MessageBox(handle, 'Nem lehet sz�ll�t�t r�gz�teni! Mert m�r l�tezik az �sszes�tett sz�ll�t�!', 'Figyelmeztet�s!', MB_ICONWARNING);
          Close;
        end
      else
        begin
          gbSzallitoAdat.Enabled := true;
          btnAdatok.Enabled := true;
          eSzallSorszam.SetFocus;
        end;
    end; }
end;

end.
