unit uSplash;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  Vcl.StdCtrls, WinInet, Datasnap.DBClient, System.IniFiles;

type
  TfrmSplash = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MakeSplash;
  end;

var
  frmSplash: TfrmSplash;

implementation

uses uTMRMain, uDM, uHttps, uLogs, uFgv;

{$R *.dfm}

procedure TfrmSplash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  GRootPath := GetCurrentDir;
end;

procedure TfrmSplash.MakeSplash;
begin
  BorderStyle := bsNone;
  Show;
  Update;
  Timer1Timer(application);
  sleep(1000);
end;

procedure TfrmSplash.Timer1Timer(Sender: TObject);
var
  lcValasz : String;
  lcLogs : TlogsUnit;
  lcMozgasSorszam, lcPufferPartnerek : TClientDataSet;
  lcXml : TfgvUnit;
  lcSorszamPut : THttpsUnit;
  lciniFile : TIniFile;
begin
  //Program indulás logolás.
  lcLogs := TlogsUnit.Create();
  lcLogs.LogFajlWrite(' Program - Indulás! ');

  lcValasz := '';
  DM.CDSUzem.LoadFromFile('uzem.xml');
  DM.CDSUzem.LogChanges := false;
  DM.CDSUzem.First;
  GPufferRaktarOsztaly := DM.CDSUzem.FieldByName('uzem_puffer_osztaly_id').AsInteger;
  Label3.Caption := 'Internet kapcsolat ellenőrzés...';
  Label3.Update;
  sleep(1000);
  Timer1.Enabled := false;
  if DM.InternetEll then
    begin
      Label3.Caption := Label3.Caption + 'Rendben!';
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Internet kapcsolódás -> Rendben! ');

{      Label3.Caption := 'Máv cikktörzs letöltés...';
      Label3.Update;
      Label3.Caption := Label3.Caption + DM.GetFtp;
      Label3.Update;
      sleep(500);}

      Label3.Caption := 'Szinkronizálás partnerek...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('partnerek/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás partnerek -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás mozgásnem...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('mozgasnem/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás mozgásnem -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás textilméretek...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('meretek/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás textilméretek -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás textilmagasság...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('magassag/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás textilmagasság -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás textilállapot...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('textilallapot/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás textilállapot -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás textilfajta...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('textilfajta/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás textilfajta -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás visszavételezés oka...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('kivezetesoka/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás visszavételezés oka -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás pufferraktárak...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('uzemek/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktárak -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás üzem-partner kapcsolatok...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('uzemekpartnerei/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás üzem-partner kapcsolatok -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás szállítósorszám...';
      Label3.Update;
      lcMozgasSorszam := TClientDataSet.Create(self);
      lcMozgasSorszam.LoadFromFile('sorszam.xml');
      lcMozgasSorszam.LogChanges := false;
      lcValasz := DM.Szinkronizalas_2('mozgsorszam/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      DM.CDSSzallSorszam.LoadFromFile('sorszam.xml');
      DM.CDSSzallSorszam.LogChanges := false;
      DM.CDSSzallSorszam.First;
      if lcValasz = 'Sikeres!' then
        begin
          while not DM.CDSSzallSorszam.Eof do
            begin
              lcMozgasSorszam.First;
              while not lcMozgasSorszam.Eof do
                begin
                  if (DM.CDSSzallSorszam.FieldByName('szam_id').AsInteger = lcMozgasSorszam.FieldByName('szam_id').AsInteger) AND
                     (DM.CDSSzallSorszam.FieldByName('szam_partnerid').AsInteger = lcMozgasSorszam.FieldByName('szam_partnerid').AsInteger) AND
                     (DM.CDSSzallSorszam.FieldByName('szam_felhasznalo').AsInteger = lcMozgasSorszam.FieldByName('szam_felhasznalo').AsInteger) AND
                     (DM.CDSSzallSorszam.FieldByName('szam_nev').AsString = lcMozgasSorszam.FieldByName('szam_nev').AsString) AND
                     (DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger <= lcMozgasSorszam.FieldByName('szam_sorszam').AsInteger) then
                     Begin
                       DM.CDSSzallSorszam.Edit;
                       DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger := lcMozgasSorszam.FieldByName('szam_sorszam').AsInteger;
                       DM.CDSSzallSorszam.Post;
                     End;
                  lcMozgasSorszam.Next;
                end;
              DM.CDSSzallSorszam.Next;
            end;
          DM.CDSSzallSorszam.Filtered := false;
          DM.CDSSzallSorszam.Filter := 'szam_felhasznalo='+ IntToStr(GUserID) ;
          DM.CDSSzallSorszam.Filtered := true;
          DM.CDSSzallSorszam.First;
          lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
          lcSorszamPut := THttpsUnit.Create();
          while not DM.CDSSzallSorszam.Eof do
            begin
              lcSorszamPut.PutHttps(lciniFile.ReadString('serverip','ip',''), lciniFile.ReadString('utvonal','path',''),
              'upmozgsorszam/', DM.CDSSzallSorszam.FieldByName('szam_nev').AsString, DM.CDSSzallSorszam.FieldByName('szam_sorszam').AsInteger,
              DM.CDSSzallSorszam.FieldByName('szam_partnerid').AsInteger, DM.CDSSzallSorszam.FieldByName('szam_felhasznalo').AsInteger);
              DM.CDSSzallSorszam.Next;
            end;
          DM.CDSSzallSorszam.Filtered := false;
          DM.CDSSzallSorszam.Filter := '';
          DM.CDSSzallSorszam.Filtered := true;
          lcXml := TfgvUnit.Create();
          lcXml.DataSetToXML(DM.CDSSzallSorszam, 'sorszam.xml');
          lcXml.Free;
          lciniFile.Free;
        end;
      sleep(500);
      lcMozgasSorszam.Free;
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás szállítósorszám -> ' + lcValasz);

     { Label3.Caption := 'Szinkronizálás paraméterek...';
      Label3.Caption := Label3.Caption + DM.Szinkronizálás_3('parameterpartner/');
      Label3.Update;
      sleep(500);}

      lcPufferPartnerek := TClientDataSet.Create(self);
      lcPufferPartnerek.LoadFromFile('partnerek2.xml');
      lcPufferPartnerek.LogChanges := false;
      lcPufferPartnerek.First;
      while not lcPufferPartnerek.Eof do
        begin
          if lcPufferPartnerek.FieldByName('par_puffer_raktar').AsBoolean then
            begin
              DM.CDSPartnerek.Insert;
              DM.CDSPartnerek.FieldByName('par_id').AsInteger := lcPufferPartnerek.FieldByName('par_id').AsInteger;
              DM.CDSPartnerek.FieldByName('par_nev').AsString := lcPufferPartnerek.FieldByName('par_nev').AsString;
              DM.CDSPartnerek.Post;
            end;
          lcPufferPartnerek.Next;
        end;

      DM.CDSPartnerek.First;
        while not DM.CDSPartnerek.Eof do
          begin
            try
              GPartner := DM.CDSPartnerek.FieldByName('par_id').AsInteger;
              Label3.Caption := 'Szinkronizálás osztályok '+ DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('osztalyokpartner/');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás osztályok ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              Label3.Caption := 'Szinkronizálás texilek ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('textilekpartner/');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás texilek ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              Label3.Caption := 'Szinkronizálás textilszínek ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('textilszinek/');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás textilszínek ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              Label3.Caption := 'Szinkronizálás naptár ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('naptarpartner/');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás naptár ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              Label3.Caption := 'Szinkronizálás összerendelés ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('feltoltottfajl/osszerendeles');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás összerendelés ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              DM.CDSParameterek.LoadFromFile('parameterek.xml');
              DM.CDSParameterek.LogChanges := false;

              Label3.Caption := 'Szinkronizálás szállítólevél ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('feltoltottfajl/mozgas');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás szállítólevél ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              Label3.Caption := 'Szinkronizálás chip mozgás ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              Label3.Update;
              lcValasz := DM.Szinkronizalas('feltoltottfajl/chipmozgasell');
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás mozgás ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              //Puffer raktár bevételezések szinkronja
              Label3.Caption := 'Szinkronizálás puffer raktár bevételezések '+ DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              label3.Update;
              lcValasz := DM.Szinkronizalas_puffer_raktar(GWHazUrls.pufBevRakXMLUpload, GPartner);
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktár bevételezések ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              //Puffer raktár kiadás-összerendelés szinkronja
              Label3.Caption := 'Szinkronizálás puffer raktár kiadás '+ DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              label3.Update;
              lcValasz := DM.Szinkronizalas_puffer_raktar(GWHazUrls.pufKiadRakXMLUpload, GPartner);
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktár kiadás ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              //Puffer raktár visszavételezés szinkronja
              Label3.Caption := 'Szinkronizálás puffer raktár visszavételezés '+ DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              label3.Update;
              lcValasz := DM.Szinkronizalas_puffer_raktar(GWHazUrls.pufVisszaXMLUpload, GPartner);
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktár visszavételezés ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

              //Puffer raktár KK szinkronja
              Label3.Caption := 'Szinkronizálás puffer raktárak közötti kiadás '+ DM.CDSPartnerek.FieldByName('par_nev').AsString + '...';
              label3.Update;
              lcValasz := DM.Szinkronizalas_puffer_raktar(GWHazUrls.pufMoveRakXMLUpload, GPartner);
              Label3.Caption := Label3.Caption + lcValasz;
              Label3.Update;
              sleep(500);
              lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktárak közötti kiadás ' + DM.CDSPartnerek.FieldByName('par_nev').AsString + ' -> ' + lcValasz);

            except
               on E : Exception do
                 begin
                   Label3.Font.Color := clRed;
                   Label3.Update;
                   Label3.Caption :=  Label3.Caption + 'Sikertelen!';
                   Label3.Update;
           //        raise Exception.Create(Exception(ExceptObject).Message);
                  end;
             end;
            DM.CDSPartnerek.Next;
          end;
        Label3.Caption := 'Szinkronizálás chip...';
        Label3.Update;
        lcValasz := DM.RFIDSzinkronizalas('RFIDCount/','chippartnerbin/');
//      lcValasz := DM.Szinkronizalas_2('chippartner/');
        Label3.Caption := Label3.Caption + lcValasz;
        Label3.Update;
        sleep(500);
        lcLogs.LogFajlWrite(' - Program - Szinkronizálás chip -> ' + lcValasz);
        lcPufferPartnerek.First;
      while not lcPufferPartnerek.Eof do
        begin
          if lcPufferPartnerek.FieldByName('par_puffer_raktar').AsBoolean then
            begin
              DM.CDSPartnerek.First;
              while DM.CDSPartnerek.FieldByName('par_id').AsInteger <> lcPufferPartnerek.FieldByName('par_id').AsInteger do
                DM.CDSPartnerek.Next;
              DM.CDSPartnerek.Delete;
            end;
          lcPufferPartnerek.Next;
        end;

     { Label3.Caption := 'Szinkronizálás chip...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('chippartner/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás chip -> ' + lcValasz);}




      Label3.Caption := 'Szinkronizálás puffer raktár(chip-ek)...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('chippuffer/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktár(chip-ek) -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás puffer raktár (darabos)...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('prdarabosdb/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás puffer raktár (darabos) -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás pufferraktárak közötti mozgások...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('pkkszamok/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás pufferraktárak közötti mozgások -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás pufferraktárak közötti mozgások (darabos)...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('pkkszallitok/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás pufferraktárak közötti mozgások (darabos) -> ' + lcValasz);

      Label3.Caption := 'Szinkronizálás pufferraktárak közötti mozgások (chippes)...';
      Label3.Update;
      lcValasz := DM.Szinkronizalas_2('pkkchipmozgas/');
      Label3.Caption := Label3.Caption + lcValasz;
      Label3.Update;
      sleep(500);
      lcLogs.LogFajlWrite(' - Program - Szinkronizálás pufferraktárak közötti mozgások (chippes) -> ' + lcValasz);
    end
  else
    begin
      Label3.Font.Color := clRed;
      Label3.Caption := Label3.Caption + 'Nincs kapcsolat!';
      lcLogs.LogFajlWrite(' - Program - Internet kapcsolódás -> Sikertelen!');
      Label3.Update;
      sleep(500);
    end;
  lcLogs.Free;
  Timer1.Enabled := false;
  DM.CDSFelhasz.LoadFromFile('felhasznalo.xml');
  DM.CDSFelhasz.LogChanges := false;
  DM.CDSMozg.LoadFromFile('mozgasnem.xml');
  DM.CDSMozg.LogChanges := false;
  DM.CDSMozgnem.LoadFromFile('mozgasnem.xml');
  DM.CDSMozgnem.LogChanges := false;

  DM.CDSMozgnem.First;
  while not DM.CDSMozgnem.Eof do
    begin
      if DM.CDSMozgnem.FieldByName('moz_kod').AsString = 'PB' then
        begin
          GPufferRaktarMozgasnemek.PB := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
        end
      else if DM.CDSMozgnem.FieldByName('moz_kod').AsString = 'PK' then
        begin
          GPufferRaktarMozgasnemek.PK := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
        end
      else if DM.CDSMozgnem.FieldByName('moz_kod').AsString = 'PV' then
        begin
          GPufferRaktarMozgasnemek.PV := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
        end
      else if DM.CDSMozgnem.FieldByName('moz_kod').AsString = 'PKK' then
        begin
          GPufferRaktarMozgasnemek.PKK := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
        end
      else if DM.CDSMozgnem.FieldByName('moz_kod').AsString = 'PKB' then
        begin
          GPufferRaktarMozgasnemek.PKB := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
        end
      else if DM.CDSMozgnem.FieldByName('moz_kod').AsString = 'PKT' then
        begin
          GPufferRaktarMozgasnemek.PTK := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
        end;
      DM.CDSMozgnem.Next;
    end;

//  DM.CDSChip.LoadFromFile('chip.xml');
//  DM.CDSChip.LogChanges := false;
  { Force load }
  DM.CDSTextilFajta.LoadFromFile('textil_fajta.xml');
  DM.CDSTextilFajta.LogChanges := false;
  DM.CDSTextilAllapot.LoadFromFile('textil_allapot.xml');
  DM.CDSTextilAllapot.LogChanges := false;
  DM.CDSTextilMagassag.LoadFromFile('textil_magassag.xml');
  DM.CDSTextilMagassag.LogChanges := false;
  DM.CDSTextilMeret.LoadFromFile('textil_meret.xml');
  DM.CDSTextilMeret.LogChanges := false;
  DM.CDSChipPuffer.LoadFromFile('chippuffer.xml');
  DM.CDSChipPuffer.LogChanges := false;
  DM.CDSDarabosPuffer.LoadFromFile('darabospuffer.xml');
  DM.CDSDarabosPuffer.LogChanges := false;
  DM.CDSKivetelezesOka.LoadFromFile('kivetelezesoka.xml');
  DM.CDSKivetelezesOka.LogChanges := false;
  DM.CDSUzemek.LoadFromFile('uzemek.xml');
  DM.CDSUzemek.LogChanges := false;
  DM.CDSPKKSzamok.LoadFromFile('pkkszamok.xml');
  DM.CDSPKKSzamok.LogChanges := false;
  DM.CDSPKKSzallitok.LoadFromFile('pkkszallitok.xml');
  DM.CDSPKKSzallitok.LogChanges := false;
  DM.CDSPKKChipmozgas.LoadFromFile('pkkchipmozgas.xml');
  DM.CDSPKKChipmozgas.LogChanges := false;
  DM.CDSUzemPartner.LoadFromFile('uzem_partner.xml');
  DM.CDSUzemPartner.LogChanges := false;

  { Szinkronra váró bevételezések betöltése a FDMemTablePufferRaktarSynchs-be }
  DM.LoadSynchChip;
end;

end.
