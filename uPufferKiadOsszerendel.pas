unit uPufferKiadOsszerendel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, INIFiles, System.WideStrUtils, Data.DB,
  Vcl.Grids, Vcl.DBGrids, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, System.UITypes,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ComCtrls, Datasnap.DBClient, frxClass, frxDBSet;

type
  TfrmPufferKiadOsszerendel = class(TForm)
    IdFTP1: TIdFTP;
    ilGombok: TImageList;
    tOlvasas: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    pAlso: TPanel;
    btnBezar: TButton;
    btnOsszerendel: TButton;
    Button1: TButton;
    pFelso: TPanel;
    gbVonalOlv: TGroupBox;
    eVonalkod: TEdit;
    gbChipOlv: TGroupBox;
    Label1: TLabel;
    btnChipStart: TButton;
    btnChipEnd: TButton;
    eChipKod: TEdit;
    gbOsszLista: TGroupBox;
    dbgOsszerendel: TDBGrid;
    pMessage: TPanel;
    GroupBox1: TGroupBox;
    lUzenet: TLabel;
    gbValPartner: TGroupBox;
    lPartnernev: TLabel;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Button2: TButton;
    Button3: TButton;
    DSOsszerendel: TDataSource;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    FDMemTableOsszerendel: TFDMemTable;
    FDMemTableOsszerendelChipkod: TStringField;
    FDMemTableOsszerendelVonalkod: TStringField;
    FDMemTableOsszerendelDatum: TDateTimeField;
    FDMemTableOsszerendelDolgozonev: TStringField;
    FDMemTableOsszerendelcikknev: TStringField;
    FDMemTableOsszerendelFelhasznalo: TIntegerField;
    FDMemTableOsszerendelDarabosbol: TBooleanField;
    lBeolvasasModja: TLabel;
    GroupBox2: TGroupBox;
    lKivalasztottTetel: TLabel;
    Button4: TButton;
    Button5: TButton;
    DSDarabosValaszto: TDataSource;
    CDSDarabosValaszto: TClientDataSet;
    CDSDarabosValasztouzemid: TIntegerField;
    CDSDarabosValasztopartnerid: TIntegerField;
    CDSDarabosValasztotextilid: TIntegerField;
    CDSDarabosValasztotextilnev: TStringField;
    CDSDarabosValasztofajtaid: TIntegerField;
    CDSDarabosValasztoallapotid: TIntegerField;
    CDSDarabosValasztomeretid: TIntegerField;
    CDSDarabosValasztomagassagid: TIntegerField;
    CDSDarabosValasztoszinid: TIntegerField;
    CDSDarabosValasztodarabosossz: TIntegerField;
    CDSDarabosValasztofajtanev: TStringField;
    CDSDarabosValasztoallapotnev: TStringField;
    CDSDarabosValasztomeretnev: TStringField;
    CDSDarabosValasztomagassagnev: TStringField;
    CDSDarabosValasztoszinnev: TStringField;
    FDMemTableOsszerendeltextilid: TIntegerField;
    FDMemTableOsszerendeltextilnev: TStringField;
    FDMemTableOsszerendelfajtaid: TIntegerField;
    FDMemTableOsszerendelfajtanev: TStringField;
    FDMemTableOsszerendelallapotid: TIntegerField;
    FDMemTableOsszerendelallapotnev: TStringField;
    FDMemTableOsszerendelmeretid: TIntegerField;
    FDMemTableOsszerendelmeretnev: TStringField;
    FDMemTableOsszerendelmagassagid: TIntegerField;
    FDMemTableOsszerendelmagassagnev: TStringField;
    FDMemTableOsszerendelszinid: TIntegerField;
    FDMemTableOsszerendelszinnev: TStringField;
    FDMemTableOsszeXML: TFDMemTable;
    FDMemTableOsszeXMLChipkod: TStringField;
    FDMemTableOsszeXMLVonalkod: TStringField;
    FDMemTableOsszeXMLDatum: TDateTimeField;
    FDMemTableOsszeXMLUzem: TIntegerField;
    FDMemTableOsszeXMLFelhasznalo: TIntegerField;
    FDMemTableOsszeXMLFajlnev: TStringField;
    FDMemTablePKXML: TFDMemTable;
    FDMemTablePKXMLpbfej_id: TIntegerField;
    FDMemTablePKXMLpbfej_uzem_id: TIntegerField;
    FDMemTablePKXMLpbfej_partner_id: TIntegerField;
    FDMemTablePKXMLpbfej_osztaly_id: TIntegerField;
    FDMemTablePKXMLpbfej_sorszam: TStringField;
    FDMemTablePKXMLpbfej_mozgas_id: TIntegerField;
    FDMemTablePKXMLpbfej_felhasznalo_id: TIntegerField;
    FDMemTablePKXMLpbfej_megjegyzes: TStringField;
    FDMemTablePKXMLpbfej_create_date: TDateTimeField;
    FDMemTablePKXMLpbfej_create_user: TIntegerField;
    FDMemTablePKXMLpbfej_lastupdate_date: TDateTimeField;
    FDMemTablePKXMLpbfej_lastupdate_user: TIntegerField;
    FDMemTablePKXMLpbfej_selected_date: TDateTimeField;
    FDMemTablePKXMLpbtet_darabszam: TIntegerField;
    FDMemTablePKXMLpbtet_fajta_id: TIntegerField;
    FDMemTablePKXMLpbtet_allapot_id: TIntegerField;
    FDMemTablePKXMLpbtet_meret_id: TIntegerField;
    FDMemTablePKXMLpbtet_magassag_id: TIntegerField;
    FDMemTablePKXMLpbtet_szin_id: TIntegerField;
    FDMemTablePKXMLpbtet_textil_id: TIntegerField;
    FDMemTablePKXMLpbtet_megjegyzes: TStringField;
    FDMemTablePKXMLpbtet_create_date: TDateTimeField;
    FDMemTablePKXMLpbtet_create_user: TIntegerField;
    FDMemTablePKXMLpbtet_lastupdate_date: TDateTimeField;
    FDMemTablePKXMLpbtet_lastupdate_user: TIntegerField;
    FDMemTablePKXMLpbtet_darabosdb: TIntegerField;
    FDMemTablePKXMLpbtet_chiphez_rendelve: TIntegerField;
    FDMemTablePKXMLpbtet_fajlnev: TStringField;
    FDMemTablePKXMLallapotText: TStringField;
    FDMemTablePKXMLfajtaText: TStringField;
    FDMemTablePKXMLszinText: TStringField;
    FDMemTablePKXMLmeretText: TStringField;
    FDMemTablePKXMLmagassagText: TStringField;
    FDMemTablePKXMLmegnevezesText: TStringField;
    FDMemTablePKMXML: TFDMemTable;
    FDMemTablePKMXMLsorszam: TStringField;
    FDMemTablePKMXMLpartner_id: TIntegerField;
    FDMemTablePKMXMLuzem_id: TIntegerField;
    FDMemTablePKMXMLosztaly_id: TIntegerField;
    FDMemTablePKMXMLprszall_fej_id: TIntegerField;
    FDMemTablePKMXMLvonalkod: TStringField;
    FDMemTablePKMXMLchipkod: TStringField;
    FDMemTablePKMXMLmozgas_id: TIntegerField;
    FDMemTablePKMXMLtextil_kpt_id: TIntegerField;
    FDMemTablePKMXMLtextil: TStringField;
    FDMemTablePKMXMLfajta_id: TIntegerField;
    FDMemTablePKMXMLfajta: TStringField;
    FDMemTablePKMXMLallapot_id: TIntegerField;
    FDMemTablePKMXMLallapot: TStringField;
    FDMemTablePKMXMLmeret_id: TIntegerField;
    FDMemTablePKMXMLmeret: TStringField;
    FDMemTablePKMXMLmagassag_id: TIntegerField;
    FDMemTablePKMXMLmagassag: TStringField;
    FDMemTablePKMXMLszin_id: TIntegerField;
    FDMemTablePKMXMLszin: TStringField;
    FDMemTablePKMXMLdatum: TDateTimeField;
    FDMemTablePKMXMLfelhasznalo_id: TIntegerField;
    FDMemTablePKMXMLfajlnev: TStringField;
    FDMemTableTXML: TFDMemTable;
    FDMemTableTXMLszfej_partner: TIntegerField;
    FDMemTableTXMLszfej_uzem: TIntegerField;
    FDMemTableTXMLszfej_mozgid: TIntegerField;
    FDMemTableTXMLszfej_osztid: TIntegerField;
    FDMemTableTXMLszfej_osszsuly: TFloatField;
    FDMemTableTXMLszfej_lastuser: TIntegerField;
    FDMemTableTXMLszfej_lastup: TDateTimeField;
    FDMemTableTXMLszfej_delete: TIntegerField;
    FDMemTableTXMLszfej_createuser: TIntegerField;
    FDMemTableTXMLszfej_createdate: TDateTimeField;
    FDMemTableTXMLszfej_szallszam: TStringField;
    FDMemTableTXMLszfej_kijelol: TIntegerField;
    FDMemTableTXMLszfej_mertsuly: TFloatField;
    FDMemTableTXMLszfej_kiszdatum: TDateField;
    FDMemTableTXMLsztet_osztid: TIntegerField;
    FDMemTableTXMLsztet_osztsuly: TFloatField;
    FDMemTableTXMLsztet_textilid: TIntegerField;
    FDMemTableTXMLsztet_textilnev: TStringField;
    FDMemTableTXMLsztet_textildb: TIntegerField;
    FDMemTableTXMLsztet_textilsuly: TFloatField;
    FDMemTableTXMLsztet_lastuser: TIntegerField;
    FDMemTableTXMLsztet_lastup: TDateTimeField;
    FDMemTableTXMLsztet_delete: TIntegerField;
    FDMemTableTXMLsztet_createuser: TIntegerField;
    FDMemTableTXMLsztet_createdate: TDateTimeField;
    FDMemTableTXMLsztet_fajlnev: TStringField;
    FDMemTableTXMLsztet_recsorsz: TIntegerField;
    FDMemTableTXMLsztet_javitott: TIntegerField;
    FDMemTableTXMLsztet_bekeszit: TIntegerField;
    FDMemTableTXMLszfej_lezart: TIntegerField;
    FDMemTableTXMLszfej_id: TIntegerField;
    FDMemTableTMXML: TFDMemTable;
    FDMemTableTMXMLVonalkod: TStringField;
    FDMemTableTMXMLChipkod: TStringField;
    FDMemTableTMXMLHelyszinnev: TStringField;
    FDMemTableTMXMLDolgozonev: TStringField;
    FDMemTableTMXMLcikknev: TStringField;
    FDMemTableTMXMLDatum: TDateTimeField;
    FDMemTableTMXMLMozgasTipus: TStringField;
    FDMemTableTMXMLKivezetesoka: TStringField;
    FDMemTableTMXMLFelhasznalo: TIntegerField;
    FDMemTableTMXMLBizonylat: TStringField;
    FDMemTableTMXMLMosoda: TStringField;
    FDMemTableTMXMLSzekreny: TStringField;
    FDMemTableTMXMLFajlnev: TStringField;
    frxReportPK: TfrxReport;
    frxDBDatasetPK: TfrxDBDataset;
    frxReportPKM: TfrxReport;
    frxDBDatasetPKM: TfrxDBDataset;
    FDMemTableOsszeXMLuploaded: TIntegerField;
    CDSDarabosValasztoid: TIntegerField;
    FDMemTableOsszerendelid: TIntegerField;
    frxReportT: TfrxReport;
    frxDBDatasetT: TfrxDBDataset;
    frxReportTM: TfrxReport;
    frxDBDatasetTM: TfrxDBDataset;
    FDMemTableOsszerendelHelyszinnev: TStringField;
    FDMemTableOsszerendelTelephelykod: TStringField;
    FDMemTableOsszerendelMosoda: TStringField;
    FDMemTableOsszerendelSzekreny: TStringField;
    CDSDarabosValasztotextilsuly: TFloatField;
    FDMemTableOsszerendeltextilsuly: TFloatField;
    FDMemTableOsszerendelpartner: TIntegerField;
    FDMemTableTMXMLuploaded: TIntegerField;
    Button6: TButton;
    FDMemTableOsszerendeltmpSelectedDate: TDateTimeField;
    Timer1: TTimer;
    FDMemTableTMXMLMozgasId: TIntegerField;
    FDMemTablePKMXMLDarabosbol: TBooleanField;
    FDMemTablePKMXMLuploaded: TIntegerField;
    FDMemTableTMXMLTelephelykod: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure eVonalkodKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgOsszerendelCellClick(Column: TColumn);
    procedure Button1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure PageControl1Change(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure DSOsszerendelDataChange(Sender: TObject; Field: TField);
    procedure btnOsszerendelClick(Sender: TObject);
    procedure dbgOsszerendelDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnBezarClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FDMemTableOsszerendelAfterPost(DataSet: TDataSet);
    procedure FDMemTableOsszerendelAfterDelete(DataSet: TDataSet);
    procedure Button6Click(Sender: TObject);
    procedure FDMemTableOsszerendelBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    prPort, prPortConfig : String;
    prKiadasDarabosbol: boolean;
    prCloseOnSave, prLoaded: boolean;
    procedure ClearKivalasztottTetel;
    procedure doTmpOszerendelesSave();
  public
    { Public declarations }
    procedure pubHandleRFID(rfid: string);
  end;

var
  frmPufferKiadOsszerendel: TfrmPufferKiadOsszerendel;

implementation
uses uDM, uFgv, uHttps, uSerialReader;

{$R *.dfm}

var SerialReader: TSerialReader;
    prKivalasztottTetel: TPufferRaktarTextil;

procedure TfrmPufferKiadOsszerendel.doTmpOszerendelesSave();
var lcCurrentPath: string;
    lcFgv: TfgvUnit;
Begin
  if FDMemTableOsszerendel.RecordCount > 0 then
    Begin
      FDMemTableOsszerendel.SaveToFile(GRootPath + '\tmp_osszerendel.xml', sfXML);
    End
  else
    Begin
      DeleteFile(GRootPath + '\tmp_osszerendel.xml');
    End;
End;

procedure TfrmPufferKiadOsszerendel.ClearKivalasztottTetel;
begin
  prKivalasztottTetel.textilid := -1;
  prKivalasztottTetel.textilnev := '';
  prKivalasztottTetel.textilsuly := 0;
  prKivalasztottTetel.fajtaid := -1;
  prKivalasztottTetel.fajtanev := '';
  prKivalasztottTetel.allapotid := -1;
  prKivalasztottTetel.allapotnev := '';
  prKivalasztottTetel.meretid := -1;
  prKivalasztottTetel.meretnev := '';
  prKivalasztottTetel.magassagid := -1;
  prKivalasztottTetel.magassagnev := '';
  prKivalasztottTetel.szinid := -1;
  prKivalasztottTetel.szinnev := '';
  prKivalasztottTetel.darabosossz := -1;
  prKivalasztottTetel.id := -1;
  prKiadasDarabosbol := false;
end;

procedure TfrmPufferKiadOsszerendel.btnBezarClick(Sender: TObject);
begin
  case MessageDlg('Ebben az esetben az eddig elv�gzett munk�ja nem ker�l ment�sre, biztos megszak�tja a folyamatot?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        prCloseOnSave := true;
        Close;
      end;
  end;
end;

procedure TfrmPufferKiadOsszerendel.btnChipEndClick(Sender: TObject);
begin
  btnChipStart.Enabled := True;
  btnChipEnd.Enabled := False;
  btnOsszerendel.Enabled := True;
  button1.Enabled := True;
  button4.Enabled := True;
  button5.Enabled := True;
  btnBezar.Enabled := True;
  SerialReader.Stop;
  eVonalkod.Text := '';
  eChipKod.Text := '';
  if FDMemTableOsszerendel.RecordCount > 0 then
    begin
      btnOsszerendel.Enabled := true;
      button1.Enabled := true;
    end
end;

procedure TfrmPufferKiadOsszerendel.btnChipStartClick(Sender: TObject);
begin
  btnChipStart.Enabled := False;
  btnChipEnd.Enabled := true;
  btnOsszerendel.Enabled := false;
  button1.Enabled := false;
  button4.Enabled := false;
  button5.Enabled := false;
  btnBezar.Enabled := false;
  SerialReader.Execute;
end;

procedure TfrmPufferKiadOsszerendel.btnOsszerendelClick(Sender: TObject);

  procedure ChangeFieldValueInCDS(ADataSet: TClientDataSet; AFilePath, AFieldName: String; AValue: Variant; Rename: boolean);
  var lcFgv: TFgvUnit;
  begin
    lcFgv := TfgvUnit.Create;
    ADataSet.LoadFromFile(AFilePath);
    ADataSet.LogChanges := false;
    ADataSet.First;
    while not ADataSet.Eof do
      begin
        ADataSet.Edit;
        ADataSet.FieldByName(AFieldName).AsVariant := AValue;
        ADataSet.Post;
        ADataSet.Next;
      end;
    lcFgv.DataSetToXML(ADataSet, AFilePath);
    if Rename then
      RenameFile(AFilePath, '_uploaded_'+AFilePath);
    lcFgv.Free;
  end;

  function FindInOsszesitesPK: boolean;
  begin
    result := false;
    FDMemTablePKXML.First;
    while not FDMemTablePKXML.Eof do
      begin
        if
          (FDMemTablePKXML.FieldByName('megnevezesText').AsString = FDMemTableOsszerendel.FieldByName('textilnev').AsString) and
          (FDMemTablePKXML.FieldByName('fajtaText').AsString = FDMemTableOsszerendel.FieldByName('fajtanev').AsString) and
          (FDMemTablePKXML.FieldByName('allapotText').AsString = FDMemTableOsszerendel.FieldByName('allapotnev').AsString) and
          (FDMemTablePKXML.FieldByName('meretText').AsString = FDMemTableOsszerendel.FieldByName('meretnev').AsString) and
          (FDMemTablePKXML.FieldByName('magassagText').AsString = FDMemTableOsszerendel.FieldByName('magassagnev').AsString) and
          (FDMemTablePKXML.FieldByName('szinText').AsString = FDMemTableOsszerendel.FieldByName('szinnev').AsString)
        then
          begin
            result := true;
            exit;
          end;
        FDMemTablePKXML.Next;
      end;
  end;

  function FindInOsszesitesT: boolean;
  begin
    result := false;
    FDMemTableTXML.First;
    while not FDMemTableTXML.Eof do
      begin
        if (FDMemTableTXML.FieldByName('sztet_textilid').AsString = FDMemTableOsszerendel.FieldByName('textilid').AsString) then
          begin
            result := true;
            exit;
          end;
        FDMemTableTXML.Next;
      end;
  end;

  function lcGetOsztalyId(var A_oszt_nev: string): Integer;
  var
    lcPath: String;
  Begin
    Result:= 0;
    lcpath:= GRootPath +'\torzs\' +'torzs_' +IntToStr(GPartner);
    DM.CDSOsztalyok.LoadFromFile(lcPath+'\osztaly.xml');
    DM.CDSOsztalyok.IndexFieldNames := 'kpo_kod';
    DM.CDSOsztalyok.First;
    while not DM.CDSOsztalyok.eof do
      begin
          if (DM.CDSOsztalyok.FieldByName('kpo_uzem').asInteger = DM.CDSUzem.FieldByName('uzem_id').AsInteger) then
            begin
              result:= DM.CDSOsztalyok.FieldByName('kpo_id').asInteger;
              A_oszt_nev:= DM.CDSOsztalyok.FieldByName('oszt_nev').asString;
              exit;
            end;
        DM.CDSOsztalyok.Next;
      end;
    DM.CDSOsztalyok.IndexFieldNames := '';
  End;

  function prGetMozgasnemNev(A_mozgId: Integer): String;
  Begin
    Result:= '';
    DM.CDSMozgnem.First;
    while not DM.CDSMozgnem.Eof do
      begin
        if DM.CDSMozgnem.FieldByName('moz_id').AsInteger = A_mozgId then
          Begin
            Result:= DM.CDSMozgnem.FieldByName('moz_kod').asString;
            exit;
          End;
        DM.CDSMozgnem.Next;
      end;
  End;

var
  lcFgv: TfgvUnit;
  lcMost: TDateTime;
  lcRootDir: string;
  lcAdatMappa: string;
  lcAdatUtvonal: string;
  lcTorzsMappa: string;
  lcSearchResult: TSearchRec;
  lcHttpRequest: THttpsUnit;
  lcHttpResponse: string;
  lcIniFile: TIniFile;
  lcIPCim, lcUtvonal: string;
  lcDataset: TClientDataSet;
  lcOsszerendelFajlNev: String;
  lcPKFajlNev: String;
  lcPKMFajlNev: String;
  lcTFajlNev: String;
  lcTMFajlNev: String;
  lcKiadSorszam: String;
  lcTisztaSorszam: String;
  lcTmpId: string;
  lcOsztalyId: Integer;
  lcOsztalyNev: String;
  i: integer;
  lcFejOsszSuly: double;
begin
  if FDMemTableOsszerendel.RecordCount = 0 then
    begin
      showMessage('�res bev�telez�st nem lehet menteni, vegyen fel t�teleket a bev�telez�sre!');
      exit;
    end;
  case MessageDlg('Biztosan lez�rja az �sszerendel�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        FDMemTableOsszerendel.First;
        while not FDMemTableOsszerendel.Eof do
          begin
            if FDMemTableOsszerendel.FieldByName('cikknev').AsString <> FDMemTableOsszerendel.FieldByName('textilnev').AsString then
              begin
                case MessageDlg('A felvitt t�telek k�z�tt elt�r� nev� (SAP <-> TMR) term�kek vannak! Ennek ellen�re folytatja?', mtConfirmation, [mbYes, mbNo], 0) of
                  mrNo: exit;
                  mrYes: break;
                end;
              end;
            FDMemTableOsszerendel.Next;
          end;

        lcOsztalyId:= lcGetOsztalyId(lcOsztalyNev);
        if lcOsztalyId = 0 then
          begin
            ShowMessage('Nem tal�lhat� a megfelel� oszt�ly, nem lehet menteni az �sszerendel�st.');
            exit;
          end;

        // Create-k
        lcFgv := TfgvUnit.Create;
        lcHttpRequest := THttpsUnit.Create();
        lcDataset := TClientDataSet.Create(Application);
        lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
        lcIPCim := lciniFile.ReadString('serverip','ip','');
        lcUtvonal := lciniFile.ReadString('utvonal','path','');

        //K�vetkez� sorsz�m a lok�lis f�jlb�l. Nem k�ldj�k el a v�ltoztat�st a szervernek!
        lcKiadSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PK', GPartner, GUserID, true, false);
        lcTisztaSorszam := lcFgv.SorszamGeneralas('T_'+IntToStr(CurrentYear)+'_PKT', GPartner, GUserID, true, false);

        FDMemTableOsszeXML.Close;
        FDMemTableOsszeXML.Open;

        FDMemTableTXML.Close;
        FDMemTableTXML.Open;

        FDMemTableTMXML.Close;
        FDMemTableTMXML.Open;

        FDMemTablePKXML.Close;
        FDMemTablePKXML.Open;

        FDMemTablePKMXML.Close;
        FDMemTablePKMXML.Open;
        lcMost := now();

        // �tvonalak, f�jlok
        lcRootDir := GetCurrentDir;

        lcTorzsMappa := GetCurrentDir + '\torzs\torzs_'+IntToStr(GPartner)+'\';
        lcAdatMappa := GetCurrentDir + '\adat\';

        SetCurrentDir(lcAdatMappa);

        if not DirectoryExists(lcFgv.PartnerMappa(GPartner)) then
          CreateDir(lcFgv.PartnerMappa(GPartner));

        lcAdatUtvonal := lcAdatMappa + lcFgv.PartnerMappa(GPartner);
        SetCurrentDir(lcAdatUtvonal);

        if not DirectoryExists(Trim(lcFgv.Mappa(lcMost))) then
          CreateDir(Trim(lcFgv.Mappa(lcMost)));

        lcAdatUtvonal := lcAdatUtvonal + '\' + Trim(lcFgv.Mappa(lcMost));

        lcOsszerendelFajlNev := 'p_ossze_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        GOldal := 'PK';
        lcPKFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        GOldal := 'PKM';
        lcPKMFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        GOldal := 'PKT';
        lcTFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';
        GOldal := 'PKTM';
        lcTMFajlNev := Trim(lcFgv.FajlNev_2(lcMost, -1)) + '_' + formatDateTime('YYYYMMDDHHNNSSZZZ', lcMost) + '.xml';

        i := 0;
        try
          // Adapterek felt�lt�se
          FDMemTableOsszerendel.Filtered := false;
          FDMemTableOsszerendel.First;
          while not FDMemTableOsszerendel.Eof do
            begin

              // �sszerenedl�s.xml
              FDMemTableOsszeXML.Insert;
              FDMemTableOsszeXML.FieldByName('Chipkod').AsString := FDMemTableOsszerendel.FieldByName('Chipkod').AsString;
              FDMemTableOsszeXML.FieldByName('Vonalkod').AsString := FDMemTableOsszerendel.FieldByName('Vonalkod').AsString;
              FDMemTableOsszeXML.FieldByName('Datum').AsDateTime := FDMemTableOsszerendel.FieldByName('Datum').AsDateTime;
              FDMemTableOsszeXML.FieldByName('Uzem').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
              FDMemTableOsszeXML.FieldByName('Felhasznalo').AsInteger := GUserId;
              FDMemTableOsszeXML.FieldByName('Fajlnev').AsString := lcOsszerendelFajlNev;
              FDMemTableOsszeXML.FieldByName('uploaded').AsInteger := -1;
              FDMemTableOsszeXML.Post;

              // PK.XML
              if not FindInOsszesitesPK then
                begin
                  FDMemTablePKXML.Insert;
                  FDMemTablePKXML.FieldByName('fajtaText').AsString := FDMemTableOsszerendel.FieldByName('fajtanev').AsString;
                  FDMemTablePKXML.FieldByName('allapotText').AsString := FDMemTableOsszerendel.FieldByName('allapotnev').AsString;
                  FDMemTablePKXML.FieldByName('meretText').AsString := FDMemTableOsszerendel.FieldByName('meretnev').AsString;
                  FDMemTablePKXML.FieldByName('magassagText').AsString := FDMemTableOsszerendel.FieldByName('magassagnev').AsString;
                  FDMemTablePKXML.FieldByName('szinText').AsString := FDMemTableOsszerendel.FieldByName('szinnev').AsString;
                  FDMemTablePKXML.FieldByName('megnevezesText').AsString := FDMemTableOsszerendel.FieldByName('textilnev').AsString;

                  FDMemTablePKXML.FieldByName('pbfej_id').AsInteger := -1;
                  FDMemTablePKXML.FieldByName('pbfej_sorszam').AsString := lcKiadSorszam;
                  FDMemTablePKXML.FieldByName('pbfej_uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePKXML.FieldByName('pbfej_partner_id').AsInteger := GPartner;
                  FDMemTablePKXML.FieldByName('pbfej_osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePKXML.FieldByName('pbfej_mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PK;
                  FDMemTablePKXML.FieldByName('pbfej_felhasznalo_id').AsInteger := GUserID;
                  FDMemTablePKXML.FieldByName('pbfej_megjegyzes').AsString := '';
                  FDMemTablePKXML.FieldByName('pbfej_create_date').AsDateTime := lcMost;
                  FDMemTablePKXML.FieldByName('pbfej_create_user').AsInteger := GUserID;
                  FDMemTablePKXML.FieldByName('pbfej_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePKXML.FieldByName('pbfej_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePKXML.FieldByName('pbfej_selected_date').AsDateTime := GDate;// lcMost;
                  FDMemTablePKXML.FieldByName('pbtet_darabszam').AsInteger := 1;
                  FDMemTablePKXML.FieldByName('pbtet_fajta_id').AsInteger := FDMemTableOsszerendel.FieldByName('fajtaid').AsInteger;
                  FDMemTablePKXML.FieldByName('pbtet_allapot_id').AsInteger := FDMemTableOsszerendel.FieldByName('allapotid').AsInteger;
                  FDMemTablePKXML.FieldByName('pbtet_textil_id').AsInteger := FDMemTableOsszerendel.FieldByName('textilid').AsInteger;
                  FDMemTablePKXML.FieldByName('pbtet_meret_id').AsInteger := FDMemTableOsszerendel.FieldByName('meretid').AsInteger;
                  FDMemTablePKXML.FieldByName('pbtet_magassag_id').AsInteger := FDMemTableOsszerendel.FieldByName('magassagid').AsInteger;
                  FDMemTablePKXML.FieldByName('pbtet_szin_id').AsInteger := FDMemTableOsszerendel.FieldByName('szinid').AsInteger;
                  FDMemTablePKXML.FieldByName('pbtet_megjegyzes').AsString := '';
                  FDMemTablePKXML.FieldByName('pbtet_create_date').AsDateTime := lcMost;
                  FDMemTablePKXML.FieldByName('pbtet_create_user').AsInteger := GUserID;
                  FDMemTablePKXML.FieldByName('pbtet_lastupdate_date').AsDateTime := lcMost;
                  FDMemTablePKXML.FieldByName('pbtet_lastupdate_user').AsInteger := GUserID;
                  FDMemTablePKXML.FieldByName('pbtet_fajlnev').AsString := lcPKFajlNev;
                  FDMemTablePKXML.FieldByName('pbtet_chiphez_rendelve').AsInteger := 0;
                  if (FDMemTableOsszerendel.FieldByName('darabosbol').AsBoolean) then
                    begin
                      FDMemTablePKXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePKXML.FieldByName('pbtet_darabosdb').AsInteger + 1;
                    end;

                  FDMemTablePKXML.Post;
                end
              else
                begin
                  // PK
                  FDMemTablePKXML.Edit;
                  FDMemTablePKXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTablePKXML.FieldByName('pbtet_darabszam').AsInteger + 1;
                  if (FDMemTableOsszerendel.FieldByName('darabosbol').AsBoolean) then
                    begin
                      FDMemTablePKXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePKXML.FieldByName('pbtet_darabosdb').AsInteger + 1;
                    end;
                  FDMemTablePKXML.Post;
                end;

              if not FindInOsszesitesT then
                begin
                  // T-s XML
                  FDMemTableTXML.Insert;
                  FDMemTableTXML.FieldByName('szfej_partner').AsInteger := GPartner;
                  FDMemTableTXML.FieldByName('szfej_uzem').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTableTXML.FieldByName('szfej_mozgid').AsInteger := GPufferRaktarMozgasnemek.PTK;
                  FDMemTableTXML.FieldByName('szfej_lastuser').AsInteger := GUserID;
                  FDMemTableTXML.FieldByName('szfej_lastup').AsDateTime := lcMost;
                  FDMemTableTXML.FieldByName('szfej_delete').AsInteger := 0;
                  FDMemTableTXML.FieldByName('szfej_createuser').AsInteger := GUserID;
                  FDMemTableTXML.FieldByName('szfej_createdate').AsDateTime := lcMost;
                  FDMemTableTXML.FieldByName('szfej_szallszam').AsString := lcTisztaSorszam;
                  FDMemTableTXML.FieldByName('szfej_kiszdatum').AsDateTime := GDate;
                  FDMemTableTXML.FieldByName('sztet_textilid').AsInteger := FDMemTableOsszerendel.FieldByName('textilid').AsInteger;
                  FDMemTableTXML.FieldByName('sztet_textilnev').AsString := FDMemTableOsszerendel.FieldByName('textilnev').AsString;
                  FDMemTableTXML.FieldByName('sztet_textildb').AsInteger := 0;
                  FDMemTableTXML.FieldByName('sztet_textilsuly').AsFloat := FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat;
                  FDMemTableTXML.FieldByName('sztet_osztsuly').AsFloat := FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat;
                  FDMemTableTXML.FieldByName('szfej_osztid').asInteger := lcOsztalyId;
                  FDMemTableTXML.FieldByName('sztet_osztid').AsInteger := lcOsztalyId;
                  lcFejOsszSuly := lcFejOsszSuly + FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat;


                  FDMemTableTXML.FieldByName('sztet_bekeszit').AsInteger := 1;
                  FDMemTableTXML.FieldByName('sztet_recsorsz').AsInteger := i;


                  FDMemTableTXML.FieldByName('sztet_lastuser').AsInteger := GUserID;
                  FDMemTableTXML.FieldByName('sztet_lastup').AsDateTime := lcMost;
                  FDMemTableTXML.FieldByName('sztet_delete').AsInteger := 0;
                  FDMemTableTXML.FieldByName('sztet_createuser').AsInteger := GUserID;
                  FDMemTableTXML.FieldByName('sztet_createdate').AsDateTime := lcMost;
                  FDMemTableTXML.FieldByName('sztet_fajlnev').AsString := lcTFajlNev;
                  FDMemTableTXML.FieldByName('sztet_javitott').AsInteger := 0;
                  FDMemTableTXML.FieldByName('szfej_lezart').AsInteger := 1;
                  FDMemTableTXML.FieldByName('szfej_id').AsInteger := -1;

                  FDMemTableTXML.Post;
                end
              else
                begin
                  // T
                  FDMemTableTXML.Edit;
                  FDMemTableTXML.FieldByName('sztet_bekeszit').AsInteger := FDMemTableTXML.FieldByName('sztet_bekeszit').AsInteger + 1;
                  FDMemTableTXML.FieldByName('sztet_osztsuly').AsFloat := FDMemTableTXML.FieldByName('sztet_osztsuly').AsFloat + FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat;
                  lcFejOsszSuly := lcFejOsszSuly + FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat;
                  FDMemTableTXML.Post;
                end;

              // PKM.xml
              FDMemTablePkMXML.Insert;
              FDMemTablePkMXML.FieldByName('sorszam').AsString := lcKiadSorszam;
              FDMemTablePkMXML.FieldByName('partner_id').AsInteger := GPartner;
              FDMemTablePkMXML.FieldByName('uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
              FDMemTablePkMXML.FieldByName('osztaly_id').AsInteger := GPufferRaktarOsztaly;
              FDMemTablePkMXML.FieldByName('prszall_fej_id').AsInteger := -1;
              FDMemTablePkMXML.FieldByName('vonalkod').AsString := FDMemTableOsszerendel.FieldByName('Vonalkod').AsString;
              FDMemTablePkMXML.FieldByName('chipkod').AsString := FDMemTableOsszerendel.FieldByName('Chipkod').AsString;
              FDMemTablePkMXML.FieldByName('mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PK;
              FDMemTablePkMXML.FieldByName('textil_kpt_id').AsVariant := FDMemTableOsszerendel.FieldByName('textilid').AsVariant;
              FDMemTablePkMXML.FieldByName('textil').AsString := FDMemTableOsszerendel.FieldByName('textilnev').AsString;
              FDMemTablePkMXML.FieldByName('fajta_id').AsVariant := FDMemTableOsszerendel.FieldByName('fajtaid').AsVariant;
              FDMemTablePkMXML.FieldByName('fajta').AsString := FDMemTableOsszerendel.FieldByName('fajtanev').AsString;
              FDMemTablePkMXML.FieldByName('allapot_id').AsVariant := FDMemTableOsszerendel.FieldByName('allapotid').AsVariant;
              FDMemTablePkMXML.FieldByName('allapot').AsString := FDMemTableOsszerendel.FieldByName('allapotnev').AsString;
              FDMemTablePkMXML.FieldByName('meret_id').AsVariant := FDMemTableOsszerendel.FieldByName('meretid').AsVariant;
              FDMemTablePkMXML.FieldByName('meret').AsString := FDMemTableOsszerendel.FieldByName('meretnev').AsString;
              FDMemTablePkMXML.FieldByName('magassag_id').AsVariant := FDMemTableOsszerendel.FieldByName('magassagid').AsVariant;
              FDMemTablePkMXML.FieldByName('magassag').AsString := FDMemTableOsszerendel.FieldByName('magassagnev').AsString;
              FDMemTablePkMXML.FieldByName('szin_id').AsVariant := FDMemTableOsszerendel.FieldByName('szinid').AsVariant;
              FDMemTablePkMXML.FieldByName('szin').AsString := FDMemTableOsszerendel.FieldByName('szinnev').AsString;
              FDMemTablePkMXML.FieldByName('datum').AsDateTime := FDMemTableOsszerendel.FieldByName('Datum').AsDateTime;
              FDMemTablePkMXML.FieldByName('felhasznalo_id').AsInteger := GUserId;
              FDMemTablePkMXML.FieldByName('fajlnev').AsString := lcPKMFajlNev;
              FDMemTablePkMXML.FieldByName('Darabosbol').AsBoolean := FDMemTableOsszerendel.FieldByName('Darabosbol').AsBoolean;
              FDMemTablePkMXML.FieldByName('uploaded').AsInteger := -1;
              FDMemTablePkMXML.Post;

              //TM
              FDMemTableTMXML.Insert;
              FDMemTableTMXML.FieldByName('Vonalkod').AsString := FDMemTableOsszerendel.FieldByName('Vonalkod').AsString;
              FDMemTableTMXML.FieldByName('Chipkod').AsString := FDMemTableOsszerendel.FieldByName('Chipkod').AsString;
              FDMemTableTMXML.FieldByName('Dolgozonev').AsString := FDMemTableOsszerendel.FieldByName('Dolgozonev').AsString;
              FDMemTableTMXML.FieldByName('cikknev').AsString := FDMemTableOsszerendel.FieldByName('textilnev').AsString;
              FDMemTableTMXML.FieldByName('Datum').AsDateTime := lcMost;

              FDMemTableTMXML.FieldByName('MozgasTipus').AsString := 'Telephelyre kihelyez�s';
              FDMemTableTMXML.FieldByName('Felhasznalo').AsInteger := GUserID;
              FDMemTableTMXML.FieldByName('Bizonylat').AsString := lcTisztaSorszam;
              FDMemTableTMXML.FieldByName('Fajlnev').AsString := lcTMFajlNev;

              FDMemTableTMXML.FieldByName('Helyszinnev').AsString := FDMemTableOsszerendel.FieldByName('Helyszinnev').AsString;
              FDMemTableTMXML.FieldByName('Telephelykod').AsString := FDMemTableOsszerendel.FieldByName('Telephelykod').AsString;
              FDMemTableTMXML.FieldByName('Mosoda').AsString := DM.CDSUzem.FieldByName('uzem_SAPparkod').AsString;
              FDMemTableTMXML.FieldByName('Szekreny').AsString := FDMemTableOsszerendel.FieldByName('Szekreny').AsString;
              FDMemTableTMXML.FieldByName('uploaded').asInteger := -1;
              FDMemTableTMXML.FieldByName('MozgasId').asInteger := GPufferRaktarMozgasnemek.PTK;

              FDMemTableTMXML.Post;

              i := i + 1;
              FDMemTableOsszerendel.Next;
            end;

          // T. S�lyok be�r�sa
          FDMemTableTXML.First;
          while not FDMemTableTXML.Eof do
            begin
              FDMemTableTXML.Edit;
              FDMemTableTXML.FieldByName('szfej_osszsuly').AsFloat := lcFejOsszSuly;
              FDMemTableTXML.Post;
              FDMemTableTXML.Next;
            end;

          // XML-ek, PDF-ek ment�se �s k�ld�se szervernek
          DM.CDSUzem.First;
          TfrxMemoView(frxReportPK.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportPK.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPK.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPK.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPK.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPK.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPK.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PK);  //(16)
          TfrxMemoView(frxReportPK.FindObject('Memo7')).Text := GOsztNev;

          TfrxMemoView(frxReportPKM.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportPKM.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPKM.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPKM.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPKM.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPKM.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPKM.FindObject('frxDBDSSzallszfej_szallszam')).Text := lcKiadSorszam;
          TfrxMemoView(frxReportPKM.FindObject('frxDBDSSzallszfej_szallszam1')).Text := lcKiadSorszam;
        //  TfrxMemoView(frxReportPKM.FindObject('frxDBDSSzallszfej_createdate')).Text := DateToStr(lcMost);
          TfrxMemoView(frxReportPKM.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PK); //(16)


          TfrxMemoView(frxReportT.FindObject('mKiszdatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportT.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportT.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportT.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportT.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportT.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportT.FindObject('mOsztNev')).Text := lcOsztalyNev;
          TfrxMemoView(frxReportT.FindObject('mMozgnem')).Text := prGetMozgasnemNev(GPufferRaktarMozgasnemek.PTK); //(20)

          TfrxMemoView(frxReportTM.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportTM.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportTM.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportTM.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportTM.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportTM.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportTM.FindObject('mOsztNev')).Text := lcOsztalyNev;

          DM.CDSFelhasz.First;
          while not DM.CDSFelhasz.Eof do
            begin
              if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
                begin
                  TfrxMemoView(frxReportPK.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  TfrxMemoView(frxReportPKM.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  TfrxMemoView(frxReportT.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
//elvileg ez nem kell                  TfrxMemoView(frxReportTM.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                end;
              DM.CDSFelhasz.Next;
            end;

          while not DM.CDSPartnerek.Eof do
            begin
              if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
                begin
                  TfrxMemoView(frxReportPK.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                  TfrxMemoView(frxReportPKM.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                  TfrxMemoView(frxReportT.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                  TfrxMemoView(frxReportTM.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                end;
              DM.CDSPartnerek.Next;
            end;

          SetCurrentDir(lcAdatUtvonal);
          FDMemTablePKXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPKFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPK.PrepareReport(true);
          frxReportPK.Export(DM.frxPDFExport1);

          frxReportPK.PrintOptions.Copies := 3;
          frxReportPK.PrintOptions.Collate := True;
          frxReportPK.PrepareReport(true);
          frxReportPK.ShowPreparedReport;

          // A Megn�velt sorsz�m felk�ld�se a szervernek
          SetCurrentDir(lcRootDir);
          lcKiadSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PK', GPartner, GUserID, false, true);
          SetCurrentDir(lcAdatUtvonal);

          lcFgv.DataSetToXML(TDataSet(FDMemTablePKXML), lcPKFajlNev);

          FDMemTablePKMXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcPKMFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPKM.PrepareReport(true);
          frxReportPKM.Export(DM.frxPDFExport1);

          frxReportPKM.PrintOptions.Copies := 3;
          frxReportPKM.PrintOptions.Collate := True;
          frxReportPKM.PrepareReport(true);
          frxReportPKM.ShowPreparedReport;

          lcFgv.DataSetToXML(TDataSet(FDMemTablePKMXML), lcPKMFajlNev);

          // T. pdf, xml
          FDMemTableTXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcTFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportT.PrepareReport(true);
          frxReportT.Export(DM.frxPDFExport1);

          frxReportT.PrintOptions.Copies := 3;
          frxReportT.PrintOptions.Collate := True;
          frxReportT.PrepareReport(true);
          frxReportT.ShowPreparedReport;

          // A Megn�velt sorsz�m felk�ld�se a szervernek
          SetCurrentDir(lcRootDir);
          lcKiadSorszam := lcFgv.SorszamGeneralas('T_'+IntToStr(CurrentYear)+'_PKT', GPartner, GUserID, false, true);
          SetCurrentDir(lcAdatUtvonal);

          lcFgv.DataSetToXML(TDataSet(FDMemTableTXML), lcTFajlNev);
          // TM. pdf, xml
          FDMemTableTMXML.First;
          DM.frxPDFExport1.FileName := StringReplace(ExtractFileName(lcTMFajlNev)+'.pdf', '.xml', '', [rfReplaceAll, rfIgnoreCase]);
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportTM.PrepareReport(true);
          frxReportTM.Export(DM.frxPDFExport1);

          frxReportTM.PrintOptions.Copies := 3;
          frxReportTM.PrintOptions.Collate := True;
          frxReportTM.PrepareReport(true);
          frxReportTM.ShowPreparedReport;

          lcFgv.DataSetToXML(TDataSet(FDMemTableTMXML), lcTMFajlNev);

//          SetCurrentDir(lcRootDir);
          SetCurrentDir(lcTorzsMappa);

          lcFgv.DataSetToXML(TDataSet(FDMemTableOsszeXML), lcOsszerendelFajlNev);


          lcFgv.SetPufferRaktar('kiad', false, false, TClientDataSet(FDMemTablePKXML));
          lcFgv.SetPufferRaktar('kiad', true, true, TClientDataSet(FDMemTablePKMXML));

          SetCurrentDir(lcRootDir);
          SetCurrentDir(lcTorzsMappa);
          lcHttpResponse := lcHttpRequest.PostHttps(lcOsszerendelFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufOsszerendXMLUpload);
          if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
            begin
              ChangeFieldValueInCDS(lcDataset, lcOsszerendelFajlNev, 'uploaded', 1, true);
              SetCurrentDir(lcAdatUtvonal);
              lcHttpResponse := '';
              // Az�rt pufBevRakXMLUpload-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s, nincs r� k�l�n request.
              lcHttpResponse := lcHttpRequest.PostHttps(lcPKFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
              if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                begin
                  lcTmpId := lcHttpResponse;
                  ChangeFieldValueInCDS(lcDataset, lcPKFajlNev, 'pbfej_id', lcTmpId, true);
                  lcHttpResponse := '';
                  // Az�rt pufBevRakXMLUpload_PBM-ra k�ldj�k mert ugyan oda ker�l, mint a bev�telez�s chip mozg�sok, nincs r� k�l�n request.
                  ChangeFieldValueInCDS(lcDataset, lcPKMFajlNev, 'prszall_fej_id', lcTmpId, false);
                  lcHttpResponse := lcHttpRequest.PostHttps(lcPKMFajlNev, lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                  if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                    begin
                      ChangeFieldValueInCDS(lcDataset, lcPKMFajlNev, 'uploaded', 1, true);
                      lcHttpResponse := '';
                      lcHttpResponse := lcHttpRequest.PostHttps(lcTFajlNev, lcIPCim, lcUtvonal, 'szallitolevel/');
                      if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                        begin
                          lcTmpId := lcHttpResponse;
                          ChangeFieldValueInCDS(lcDataset, lcTFajlNev, 'szfej_id', lcTmpId, true);
                          lcHttpResponse := '';
                          lcHttpResponse := lcHttpRequest.PostHttps(lcTMFajlNev, lcIPCim, lcUtvonal, 'chipmozgas/');
                          if (lcHttpResponse <> '') and (lcFgv.IsNumber(lcHttpResponse)) then
                            begin
                              ChangeFieldValueInCDS(lcDataset, lcTMFajlNev, 'uploaded', 1, true);
                            end
                          else
                            showmessage('A felt�lt�s nem lehets�ges. (PKTMxxxx.xml szakasz) - ' + lcHttpResponse);
                        end
                      else
                        begin
                          showmessage('A felt�lt�s nem lehets�ges. (PKTxxxx.xml szakasz) - ' + lcHttpResponse);
                        end;
                    end
                  else
                    begin
                      showmessage('A felt�lt�s nem lehets�ges. (PKMxxxx.xml szakasz) - ' + lcHttpResponse);
                    end;
                end
              else
               showmessage('A felt�lt�s nem lehets�ges. (PKxxxx.xml szakasz) - ' + lcHttpResponse);
            end
          else
            showmessage('A felt�lt�s nem lehets�ges. (p_osszerend.xml szakasz) - ' + lcHttpResponse);
        finally
          SetCurrentDir(lcRootDir);
        end;

        // Destroy-ok
        lcFgv.Destroy;
        lcHttpRequest.Destroy;
        lcDataset.Destroy;
        lcIniFile.Destroy;
        prCloseOnSave := true;
        Close;
      end;
  end;
end;

procedure TfrmPufferKiadOsszerendel.Button1Click(Sender: TObject);
var lcId: integer;
begin
  if FDMemTableOsszerendel.RecordCount = 0 then
    exit;
  case MessageDlg('Biztos t�r�lni akarja a kiv�lasztott �sszerendel�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrNo:
      begin
        Abort;
      end;
    mrYes:
      begin
        lcId := CDSDarabosValaszto.FieldByName('id').AsInteger;
        if FDMemTableOsszerendel.FieldByName('id').AsInteger > -1 then
          begin
            CDSDarabosValaszto.Filtered := false;
            CDSDarabosValaszto.Filter := 'id='+IntToStr(FDMemTableOsszerendel.FieldByName('id').AsInteger);
            CDSDarabosValaszto.Filtered := true;
            CDSDarabosValaszto.First;
            CDSDarabosValaszto.Edit;
            CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger + 1;
            CDSDarabosValaszto.Post;
            if FDMemTableOsszerendel.FieldByName('id').AsInteger = prKivalasztottTetel.id then
              begin
                lKivalasztottTetel.Caption := CDSDarabosValaszto.FieldByName('textilnev').AsString + ' ' +
                                  CDSDarabosValaszto.FieldByName('fajtanev').AsString + ' ' +
                                  CDSDarabosValaszto.FieldByName('allapotnev').AsString + ' ' +
                                  CDSDarabosValaszto.FieldByName('meretnev').AsString + ' ' +
                                  CDSDarabosValaszto.FieldByName('magassagnev').AsString + ' ' +
                                  CDSDarabosValaszto.FieldByName('szinnev').AsString + ' ' + #13#10 +
                                  'M�g el�rhet�: ' + CDSDarabosValaszto.FieldByName('darabosossz').AsString + ' db';
              end;
            CDSDarabosValaszto.Filtered := false;
            CDSDarabosValaszto.First;
            while not lcId = CDSDarabosValaszto.FieldByName('id').AsInteger do
              CDSDarabosValaszto.Next;
          end;
        FDMemTableOsszerendel.Delete;
      end;
  end;
  if FDMemTableOsszerendel.RecordCount = 0 then
    Button1.Enabled := false;
  CDSDarabosValaszto.Filtered := false;
  CDSDarabosValaszto.Filter := 'partnerid=' + IntToStr(GPartner);
  CDSDarabosValaszto.Filtered := true;
end;

procedure TfrmPufferKiadOsszerendel.Button2Click(Sender: TObject);
begin
  DBGrid1.OnDblClick(nil);
end;

procedure TfrmPufferKiadOsszerendel.Button3Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl1.OnChange(nil);
end;

procedure TfrmPufferKiadOsszerendel.Button4Click(Sender: TObject);
begin
  ClearKivalasztottTetel;
  lKivalasztottTetel.Caption := 'Chip-es beolvas�s';
  lBeolvasasModja.Caption := 'Chip-es beolvas�s';
  button4.Visible := false;
  eVonalkod.Text := '';
  eChipKod.Text := '';
end;

procedure TfrmPufferKiadOsszerendel.Button5Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  PageControl1.OnChange(nil);
end;

procedure TfrmPufferKiadOsszerendel.Button6Click(Sender: TObject);
begin
  //Ment�s
  doTmpOszerendelesSave();
  Close;
end;

procedure TfrmPufferKiadOsszerendel.dbgOsszerendelCellClick(Column: TColumn);
begin
  dbgOsszerendel.SelectedRows.CurrentRowSelected := True;
end;

procedure TfrmPufferKiadOsszerendel.dbgOsszerendelDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if FDMemTableOsszerendel.FieldByName('cikknev').AsString <> FDMemTableOsszerendel.FieldByName('textilnev').AsString then
    begin
      dbgOsszerendel.Canvas.Brush.Color := clYellow;
      dbgOsszerendel.Canvas.Font.Color := clBlack;
    end;
  dbgOsszerendel.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TfrmPufferKiadOsszerendel.DBGrid1CellClick(Column: TColumn);
begin
  if CDSDarabosValaszto.Active then
    begin
      DBGrid1.SelectedRows.CurrentRowSelected := True;
    end;
end;

procedure TfrmPufferKiadOsszerendel.DBGrid1DblClick(Sender: TObject);
begin
  if CDSDarabosValaszto.Active then
    begin
      if DBGrid1.SelectedRows.Count > 0 then
        begin
          if CDSDarabosValaszto.FieldByName('darabosossz').AsInteger = 0 then
            begin
              ShowMessage('Ebb�l a t�telb�l nincs m�r �sszerendelhet� mennyis�g!');
              exit;
            end;
          prKivalasztottTetel.textilid := CDSDarabosValaszto.FieldByName('textilid').AsInteger;
          prKivalasztottTetel.textilnev := CDSDarabosValaszto.FieldByName('textilnev').AsString;
          prKivalasztottTetel.fajtaid := CDSDarabosValaszto.FieldByName('fajtaid').AsInteger;
          prKivalasztottTetel.fajtanev := CDSDarabosValaszto.FieldByName('fajtanev').AsString;
          prKivalasztottTetel.allapotid := CDSDarabosValaszto.FieldByName('allapotid').AsInteger;
          prKivalasztottTetel.allapotnev := CDSDarabosValaszto.FieldByName('allapotnev').AsString;
          prKivalasztottTetel.meretid := CDSDarabosValaszto.FieldByName('meretid').AsInteger;
          prKivalasztottTetel.meretnev := CDSDarabosValaszto.FieldByName('meretnev').AsString;
          prKivalasztottTetel.magassagid := CDSDarabosValaszto.FieldByName('magassagid').AsInteger;
          prKivalasztottTetel.magassagnev := CDSDarabosValaszto.FieldByName('magassagnev').AsString;
          prKivalasztottTetel.szinid := CDSDarabosValaszto.FieldByName('szinid').AsInteger;
          prKivalasztottTetel.szinnev := CDSDarabosValaszto.FieldByName('szinnev').AsString;
          prKivalasztottTetel.darabosossz := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger;
          prKivalasztottTetel.id := CDSDarabosValaszto.FieldByName('id').AsInteger;
          lKivalasztottTetel.Caption := CDSDarabosValaszto.FieldByName('textilnev').AsString + ' ' +
                                        CDSDarabosValaszto.FieldByName('fajtanev').AsString + ' ' +
                                        CDSDarabosValaszto.FieldByName('allapotnev').AsString + ' ' +
                                        CDSDarabosValaszto.FieldByName('meretnev').AsString + ' ' +
                                        CDSDarabosValaszto.FieldByName('magassagnev').AsString + ' ' +
                                        CDSDarabosValaszto.FieldByName('szinnev').AsString + ' ' + #13#10 +
                                        'M�g el�rhet�: ' + CDSDarabosValaszto.FieldByName('darabosossz').AsString + ' db';
          lBeolvasasModja.Caption := 'Darabos beolvas�s';
          prKiadasDarabosbol := true;
          PageControl1.ActivePageIndex := 0;
          PageControl1.OnChange(nil);
        end;
    end;
end;

procedure TfrmPufferKiadOsszerendel.DSOsszerendelDataChange(Sender: TObject;
  Field: TField);
begin
  if FDMemTableOsszerendel.RecordCount > 0 then
    begin
      if not SerialReader.IsStarted then
        begin
          btnOsszerendel.Enabled := true;
          button1.Enabled := true;
        end;
    end
  else
    begin
      btnOsszerendel.Enabled := false;
      button1.Enabled := false;
    end;
end;

procedure TfrmPufferKiadOsszerendel.eVonalkodKeyPress(Sender: TObject; var Key: Char);
var lcFgv: TfgvUnit;
    lcKivalasztottTetel: TPufferRaktarTextil;
    lcVonalkodAdatok: TVonalkodAdatok;
begin
  lcFgv := TfgvUnit.Create;
  if Ord(Key) <> VK_RETURN then
    exit;
  if (eChipKod.Text = '') or (eChipKod.Color = clRed) then
    begin
      eVonalkod.Text := '';
      eChipKod.Color := clWhite;
      eChipKod.Text := '';
      exit;
    end;
  FDMemTableOsszerendel.First;
  while not FDMemTableOsszerendel.Eof do
    begin
      if FDMemTableOsszerendel.FieldByName('Vonalkod').AsString = eVonalkod.Text then
        begin
          MessageBox(handle, 'Ez a vonalk�d m�r szerepel ezen az �sszerendel�sen!', 'Figyelmeztet�s!', MB_ICONWARNING);
          eVonalkod.Text := '';
          eChipKod.Color := clWhite;
          eChipKod.Text := '';
          Exit;
        end;
      FDMemTableOsszerendel.Next;
    end;
  if not lcFgv.EmptyVonalkod(eVonalkod.Text) then
    begin
      // Nincs az adatb�zisban
      MessageBox(handle, 'Nincs ilyen vonalk�d az adatb�zisban!', 'Figyelmeztet�s!', MB_ICONWARNING);
      eVonalkod.Clear;
      eChipKod.Clear;
      eChipKod.Color := clWhite;
      exit;
    end
  else
    begin
      if lcFgv.GetVonalkod(eVonalkod.Text) then
        begin
          // M�r chip-hez van rendelve
          case MessageDlg('Ehhez a vonalk�dhoz m�r rendeltek chipk�dot! Biztos folytatja?', mtConfirmation, [mbYes, mbNo], 0) of
            mrNo:
              begin
                eVonalkod.Clear;
                eChipKod.Clear;
                eChipKod.Color := clWhite;
                Abort;
              end;
            mrYes:
              begin
                FDMemTableOsszerendel.Insert;
                FDMemTableOsszerendel.FieldByName('Chipkod').AsString := eChipKod.Text;
                FDMemTableOsszerendel.FieldByName('Vonalkod').AsString := eVonalkod.Text;
                FDMemTableOsszerendel.FieldByName('Datum').AsDateTime := now();
                FDMemTableOsszerendel.FieldByName('Dolgozonev').AsString := lcFgv.GetDolgozonev(eVonalkod.Text);
                FDMemTableOsszerendel.FieldByName('cikknev').AsString := lcFgv.GetCikknev(eVonalkod.Text);
                FDMemTableOsszerendel.FieldByName('Felhasznalo').AsInteger := GUserID;
                FDMemTableOsszerendel.FieldByName('Darabosbol').AsBoolean := prKiadasDarabosbol;
                FDMemTableOsszerendel.FieldByName('partner').AsInteger := GPartner;

                lcVonalkodAdatok := lcFgv.GetVonalkodAdatokByVonalkod(eVonalkod.Text);

                FDMemTableOsszerendel.FieldByName('Helyszinnev').AsString := lcVonalkodAdatok.Helyszinnev;
                FDMemTableOsszerendel.FieldByName('Telephelykod').AsString := lcVonalkodAdatok.Telephelykod;
                FDMemTableOsszerendel.FieldByName('Szekreny').AsString := lcVonalkodAdatok.Szekreny;


                if prKivalasztottTetel.textilid > -1 then
                  begin
                    FDMemTableOsszerendel.FieldByName('textilid').AsInteger := prKivalasztottTetel.textilid;
                    FDMemTableOsszerendel.FieldByName('textilnev').AsString := prKivalasztottTetel.textilnev;
                    FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat := prKivalasztottTetel.textilsuly;
                    FDMemTableOsszerendel.FieldByName('fajtaid').AsInteger := prKivalasztottTetel.fajtaid;
                    FDMemTableOsszerendel.FieldByName('fajtanev').AsString := prKivalasztottTetel.fajtanev;
                    FDMemTableOsszerendel.FieldByName('allapotid').AsInteger := prKivalasztottTetel.allapotid;
                    FDMemTableOsszerendel.FieldByName('allapotnev').AsString := prKivalasztottTetel.allapotnev;
                    FDMemTableOsszerendel.FieldByName('meretid').AsInteger := prKivalasztottTetel.meretid;
                    FDMemTableOsszerendel.FieldByName('meretnev').AsString := prKivalasztottTetel.meretnev;
                    FDMemTableOsszerendel.FieldByName('magassagid').AsInteger := prKivalasztottTetel.magassagid;
                    FDMemTableOsszerendel.FieldByName('magassagnev').AsString := prKivalasztottTetel.magassagnev;
                    FDMemTableOsszerendel.FieldByName('szinid').AsInteger := prKivalasztottTetel.szinid;
                    FDMemTableOsszerendel.FieldByName('szinnev').AsString := prKivalasztottTetel.szinnev;
                    FDMemTableOsszerendel.FieldByName('id').AsInteger := prKivalasztottTetel.id;
                    FDMemTableOsszerendel.Post;

                    CDSDarabosValaszto.Edit;
                    CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger - 1;
                    CDSDarabosValaszto.Post;
                    lKivalasztottTetel.Caption := CDSDarabosValaszto.FieldByName('textilnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('fajtanev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('allapotnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('meretnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('magassagnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('szinnev').AsString + ' ' + #13#10 +
                                            'M�g el�rhet�: ' + CDSDarabosValaszto.FieldByName('darabosossz').AsString + ' db';
                  end
                else
                  begin
                    lcKivalasztottTetel := lcFgv.GetTextilFromPufferRaktarByChipkod(eChipKod.Text);
                    FDMemTableOsszerendel.FieldByName('textilid').AsInteger := lcKivalasztottTetel.textilid;
                    FDMemTableOsszerendel.FieldByName('textilnev').AsString := lcKivalasztottTetel.textilnev;
                    FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat := lcKivalasztottTetel.textilsuly;
                    FDMemTableOsszerendel.FieldByName('fajtaid').AsInteger := lcKivalasztottTetel.fajtaid;
                    FDMemTableOsszerendel.FieldByName('fajtanev').AsString := lcKivalasztottTetel.fajtanev;
                    FDMemTableOsszerendel.FieldByName('allapotid').AsInteger := lcKivalasztottTetel.allapotid;
                    FDMemTableOsszerendel.FieldByName('allapotnev').AsString := lcKivalasztottTetel.allapotnev;
                    FDMemTableOsszerendel.FieldByName('meretid').AsInteger := lcKivalasztottTetel.meretid;
                    FDMemTableOsszerendel.FieldByName('meretnev').AsString := lcKivalasztottTetel.meretnev;
                    FDMemTableOsszerendel.FieldByName('magassagid').AsInteger := lcKivalasztottTetel.magassagid;
                    FDMemTableOsszerendel.FieldByName('magassagnev').AsString := lcKivalasztottTetel.magassagnev;
                    FDMemTableOsszerendel.FieldByName('szinid').AsInteger := lcKivalasztottTetel.szinid;
                    FDMemTableOsszerendel.FieldByName('szinnev').AsString := lcKivalasztottTetel.szinnev;
                    FDMemTableOsszerendel.FieldByName('id').AsInteger := -1;
                    FDMemTableOsszerendel.Post;
                  end;

                eVonalkod.Clear;
                eChipKod.Clear;
                eChipKod.Color := clWhite;
              end;
          end;
        end
      else
        begin
          // Van ilyen vonalk�d, de m�g nem chip-es
          FDMemTableOsszerendel.Insert;
          FDMemTableOsszerendel.FieldByName('Chipkod').AsString := eChipKod.Text;
          FDMemTableOsszerendel.FieldByName('Vonalkod').AsString := eVonalkod.Text;
          FDMemTableOsszerendel.FieldByName('Datum').AsDateTime := now();
          FDMemTableOsszerendel.FieldByName('Dolgozonev').AsString := lcFgv.GetDolgozonev(eVonalkod.Text);
          FDMemTableOsszerendel.FieldByName('cikknev').AsString := lcFgv.GetCikknev(eVonalkod.Text);
          FDMemTableOsszerendel.FieldByName('Felhasznalo').AsInteger := GUserID;
          FDMemTableOsszerendel.FieldByName('Darabosbol').AsBoolean := prKiadasDarabosbol;
          FDMemTableOsszerendel.FieldByName('partner').AsInteger := GPartner;

          lcVonalkodAdatok := lcFgv.GetVonalkodAdatokByVonalkod(eVonalkod.Text);

          FDMemTableOsszerendel.FieldByName('Helyszinnev').AsString := lcVonalkodAdatok.Helyszinnev;
          FDMemTableOsszerendel.FieldByName('Telephelykod').AsString := lcVonalkodAdatok.Telephelykod;
          FDMemTableOsszerendel.FieldByName('Szekreny').AsString := lcVonalkodAdatok.Szekreny;

          if prKivalasztottTetel.textilid > -1 then
            begin
              FDMemTableOsszerendel.FieldByName('textilid').AsInteger := prKivalasztottTetel.textilid;
              FDMemTableOsszerendel.FieldByName('textilnev').AsString := prKivalasztottTetel.textilnev;
              FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat := prKivalasztottTetel.textilsuly;
              FDMemTableOsszerendel.FieldByName('fajtaid').AsInteger := prKivalasztottTetel.fajtaid;
              FDMemTableOsszerendel.FieldByName('fajtanev').AsString := prKivalasztottTetel.fajtanev;
              FDMemTableOsszerendel.FieldByName('allapotid').AsInteger := prKivalasztottTetel.allapotid;
              FDMemTableOsszerendel.FieldByName('allapotnev').AsString := prKivalasztottTetel.allapotnev;
              FDMemTableOsszerendel.FieldByName('meretid').AsInteger := prKivalasztottTetel.meretid;
              FDMemTableOsszerendel.FieldByName('meretnev').AsString := prKivalasztottTetel.meretnev;
              FDMemTableOsszerendel.FieldByName('magassagid').AsInteger := prKivalasztottTetel.magassagid;
              FDMemTableOsszerendel.FieldByName('magassagnev').AsString := prKivalasztottTetel.magassagnev;
              FDMemTableOsszerendel.FieldByName('szinid').AsInteger := prKivalasztottTetel.szinid;
              FDMemTableOsszerendel.FieldByName('szinnev').AsString := prKivalasztottTetel.szinnev;
              FDMemTableOsszerendel.FieldByName('id').AsInteger := prKivalasztottTetel.id;

              CDSDarabosValaszto.Edit;
              CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger - 1;
              CDSDarabosValaszto.Post;
              lKivalasztottTetel.Caption := CDSDarabosValaszto.FieldByName('textilnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('fajtanev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('allapotnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('meretnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('magassagnev').AsString + ' ' +
                                            CDSDarabosValaszto.FieldByName('szinnev').AsString + ' ' + #13#10 +
                                            'M�g el�rhet�: ' + CDSDarabosValaszto.FieldByName('darabosossz').AsString + ' db';
            end
          else
            begin
              lcKivalasztottTetel := lcFgv.GetTextilFromPufferRaktarByChipkod(eChipKod.Text);
              FDMemTableOsszerendel.FieldByName('textilid').AsInteger := lcKivalasztottTetel.textilid;
              FDMemTableOsszerendel.FieldByName('textilnev').AsString := lcKivalasztottTetel.textilnev;
              FDMemTableOsszerendel.FieldByName('textilsuly').AsFloat := lcKivalasztottTetel.textilsuly;
              FDMemTableOsszerendel.FieldByName('fajtaid').AsInteger := lcKivalasztottTetel.fajtaid;
              FDMemTableOsszerendel.FieldByName('fajtanev').AsString := lcKivalasztottTetel.fajtanev;
              FDMemTableOsszerendel.FieldByName('allapotid').AsInteger := lcKivalasztottTetel.allapotid;
              FDMemTableOsszerendel.FieldByName('allapotnev').AsString := lcKivalasztottTetel.allapotnev;
              FDMemTableOsszerendel.FieldByName('meretid').AsInteger := lcKivalasztottTetel.meretid;
              FDMemTableOsszerendel.FieldByName('meretnev').AsString := lcKivalasztottTetel.meretnev;
              FDMemTableOsszerendel.FieldByName('magassagid').AsInteger := lcKivalasztottTetel.magassagid;
              FDMemTableOsszerendel.FieldByName('magassagnev').AsString := lcKivalasztottTetel.magassagnev;
              FDMemTableOsszerendel.FieldByName('szinid').AsInteger := lcKivalasztottTetel.szinid;
              FDMemTableOsszerendel.FieldByName('szinnev').AsString := lcKivalasztottTetel.szinnev;
              FDMemTableOsszerendel.FieldByName('id').AsInteger := -1;
            end;

          FDMemTableOsszerendel.Post;
          eVonalkod.Clear;
          eChipKod.Clear;
          eChipKod.Color := clWhite;
        end;
    end;
  if (prKivalasztottTetel.textilid > -1) AND (CDSDarabosValaszto.FieldByName('darabosossz').AsInteger = 0) then
    begin
      ShowMessage('A kiv�lasztott t�telb�l nincs t�bb �sszerendelhet� darab.');
      Button4.Click;
    end;
  lcFgv.Destroy;
end;

procedure TfrmPufferKiadOsszerendel.FDMemTableOsszerendelAfterDelete(
  DataSet: TDataSet);
begin
  //Ment�s
  doTmpOszerendelesSave();
end;

procedure TfrmPufferKiadOsszerendel.FDMemTableOsszerendelAfterPost(
  DataSet: TDataSet);
begin
  // Ment�s
  doTmpOszerendelesSave();
end;

procedure TfrmPufferKiadOsszerendel.FDMemTableOsszerendelBeforePost(
  DataSet: TDataSet);
begin
  FDMemTableOsszerendel.FieldByName('tmpSelectedDate').AsDateTime:= GDate;
end;

procedure TfrmPufferKiadOsszerendel.FormClose(Sender: TObject;
  var Action: TCloseAction);
var lcCurrPath: string;
begin
  if prCloseOnSave then
    begin
      lcCurrPath := GetCurrentDir;
      SetCurrentDir(GRootPath);
      DeleteFile('tmp_osszerendel.xml');
      SetCurrentDir(lcCurrPath);
      SerialReader.Stop;
      Action := caFree;
    end
  else
    begin
      case MessageDlg('Biztos bez�rja az ablakot?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            SerialReader.Stop;
            Action := caFree;
          end;
        mrNo: Action := caNone;
      end;
    end;
  if Action = caFree then
    frmPufferKiadOsszerendel:= nil;
end;

procedure TfrmPufferKiadOsszerendel.FormCreate(Sender: TObject);
var
  IniFile: TIniFile;
  i: integer;
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;

  btnChipEnd.Enabled := false;
  btnOsszerendel.Enabled := false;
  FDMemTableOsszerendel.Active:= True;

  // Hide tab from TabControll

  for i := 0 to PageControl1.PageCount -1  do
    begin
      PageControl1.Pages[i].TabVisible := false;
    end;
  PageControl1.ActivePageIndex := 0;

  lPartnernev.Caption := '';
  DM.CDSPartnerek.First;
  while not DM.CDSPartnerek.Eof do
    begin
      if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
        begin
          lPartnernev.Caption := DM.CDSPartnerek.FieldByName('par_nev').AsString;
          break;
        end;
      DM.CDSPartnerek.Next;
    end;
  // Chip Beolvas�s
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  prPort := IniFile.ReadString('port', 'szam', prPort);
  prPortConfig := IniFile.ReadString('port', 'ertek', prPortConfig);
  IniFile.Free;
  SerialReader := TSerialReader.Create('kiad_osszerendel', prPort, prPortConfig);
  prKiadasDarabosbol := false;
  prCloseOnSave := false;
  lKivalasztottTetel.Caption := 'Chip-es beolvas�s';
  lBeolvasasModja.Caption := 'Chip-es beolvas�s';
  if FileExists('./darabospuffer.xml') then
    begin
      CDSDarabosValaszto.LoadFromFile('./darabospuffer.xml');
      CDSDarabosValaszto.Active := true;
      CDSDarabosValaszto.LogChanges := false;
      CDSDarabosValaszto.Filtered := false;
      CDSDarabosValaszto.Filter := 'partnerid='+IntToStr(GPartner);
      CDSDarabosValaszto.Filtered := true;
      CDSDarabosValaszto.First;
    end;
  ClearKivalasztottTetel;
end;

procedure TfrmPufferKiadOsszerendel.FormShow(Sender: TObject);
begin
  eVonalkod.SetFocus;
  if not prLoaded then
    begin
      prLoaded := true;
      FDMemTableOsszerendel.First;
      while not FDMemTableOsszerendel.Eof do
        begin
          if FDMemTableOsszerendel.FieldByName('Darabosbol').AsBoolean then
            begin
              CDSDarabosValaszto.First;
              while not CDSDarabosValaszto.Eof do
                begin
                  if (CDSDarabosValaszto.FieldByName('textilnev').AsString = FDMemTableOsszerendel.FieldByName('textilnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('fajtanev').AsString = FDMemTableOsszerendel.FieldByName('fajtanev').AsString) and
                      (CDSDarabosValaszto.FieldByName('allapotnev').AsString = FDMemTableOsszerendel.FieldByName('allapotnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('meretnev').AsString = FDMemTableOsszerendel.FieldByName('meretnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('magassagnev').AsString = FDMemTableOsszerendel.FieldByName('magassagnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('szinnev').AsString = FDMemTableOsszerendel.FieldByName('szinnev').AsString) and
                      (CDSDarabosValaszto.FieldByName('partnerid').AsInteger = FDMemTableOsszerendel.FieldByName('partner').AsInteger) then
                    begin
                      CDSDarabosValaszto.Edit;
                      CDSDarabosValaszto.FieldByName('darabosossz').AsInteger := CDSDarabosValaszto.FieldByName('darabosossz').AsInteger-1;
                      CDSDarabosValaszto.Post;
                    end;
                  CDSDarabosValaszto.Next;
                end;
            end;
          FDMemTableOsszerendel.Next;
        end;
    end;
end;

procedure TfrmPufferKiadOsszerendel.PageControl1Change(Sender: TObject);
begin
  if (prKivalasztottTetel.textilid > -1) and Button5.Enabled and (PageControl1.ActivePageIndex = 0) then
    begin
      button4.Enabled := true;
      button4.Visible := true;
    end;
end;

procedure TfrmPufferKiadOsszerendel.pubHandleRFID(rfid: string);
var lcFgv: TFgvUnit;
    lcMsgString: string;
    lcFind: boolean;
begin
  lcFgv := TFgvUnit.Create;
  lcMsgString := '';
  eVonalkod.SetFocus;
  eChipKod.Text := lcMsgString;
  eChipKod.Color := clWhite;
  FDMemTableOsszerendel.First;
  while not FDMemTableOsszerendel.Eof do
    begin
      if FDMemTableOsszerendel.FieldByName('Chipkod').AsString = rfid then
        begin
          eChipKod.Text := 'Ez a chipk�d m�r szerepel ezen az �sszerendel�sen.';
          eChipKod.Color := clRed;
          eVonalkod.Text := '';
          exit;
        end;
      FDMemTableOsszerendel.Next;
    end;
  if prKivalasztottTetel.textilid = -1 then
    begin
      if (lcFgv.IsInPufferRaktar(rfid, true, TClientDataset(FDMemTableOsszerendel), ' chipkod = '''+rfid+'''', lcMsgString)) then
        begin
          eChipKod.Text := rfid;
        end
      else
        begin
          eChipKod.Text := lcMsgString;
          eChipKod.Color := clRed;
          eVonalkod.Text := '';
        end;
    end
  else
    begin
      DM.CDSChip.Filtered := false;
      DM.CDSChip.Filter := 'cms_chipkod='''+rfid+'''';
      DM.CDSChip.Filtered := true;
      if DM.CDSChip.RecordCount > 0 then
        begin
          with DM.CDSChip.IndexDefs.AddIndexDef do
          begin
            Name := 'datum';
            Fields := 'cms_jelolesdatum';
            Options := [ixDescending, ixCaseInsensitive];
          end;
          DM.CDSChip.IndexName := 'datum';
          DM.CDSChip.First;
          if DM.CDSChip.FieldByName('cms_vonalkod').AsString <> '' then
            begin
              DM.CDSChip.Filtered := false;
              DM.CDSChip.Filter := '';
              DM.CDSChip.IndexDefs.Clear;
              case MessageDlg('A(z) ' + rfid + 'chipk�d m�r hozz� van rendelve egy vonalk�dhoz. Biztos folytatja? ', mtConfirmation, [mbYes, mbNo], 0) of
                mrYes:
                  begin
                    eChipKod.Text := rfid;
                  end;
                mrNo:
                  begin
                    eChipKod.Text := '';
                  end;
              end;
            end;
        end
      else
        begin
          // Netr�l leszedett chipk�dok
          DM.CDSChipPuffer.Filtered := false;
          DM.CDSChipPuffer.Filter := 'chipkod = '''+rfid+'''';
          DM.CDSChipPuffer.Filtered := true;
          // M�g nem szinkroniz�lt bev�telez�sek
          (*
          DM.FDMemTablePufferRaktarSynchs.Filtered := false;
          DM.FDMemTablePufferRaktarSynchs.Filter := 'chip_kod = '''+rfid+'''';
          DM.FDMemTablePufferRaktarSynchs.Filtered := true;
          *)

          if (DM.CDSChipPuffer.RecordCount = 0) (* and (DM.FDMemTablePufferRaktarSynchs.RecordCount = 0) *) then
            begin
              eChipKod.Text := rfid;
            end
          else
            begin
              eChipKod.Text := 'Nem szabad chipk�d! Ez a chipk�d m�r egy ruh�hoz tartozik.';
              eChipKod.Color := clRed;
              eVonalkod.Text := '';
            end;

          DM.CDSChipPuffer.Filtered := false;
          DM.FDMemTablePufferRaktarSynchs.Filtered := false;
        end;
      DM.CDSChip.Filtered := false;
    end;

  lcFgv.Destroy;
end;

end.

