unit uPufferRaktarBevetelezesParameterek;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.UITypes, System.DateUtils,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ComCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtCtrls, System.ImageList, Vcl.ImgList,
  Vcl.DBCtrls, Vcl.Mask, INIFiles, System.Win.ScktComp, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, frxClass, frxDBSet, Datasnap.DBClient;

type
  TfrmPufferRaktarBevetelezesParameterek = class(TForm)
    pFull: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    gbOsszLista: TGroupBox;
    Panel3: TPanel;
    btnVissza: TButton;
    iPufferRaktarBevetParameterek: TImageList;
    Button1: TButton;
    Button3: TButton;
    rogzitChipBeolvasBtn: TButton;
    gbChipList: TGroupBox;
    mChipList: TMemo;
    gbInfo: TGroupBox;
    gbTextilList: TGroupBox;
    dbgTextilList: TDBGrid;
    pAlso: TPanel;
    btnChipStart: TButton;
    btnChipEnd: TButton;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    Panel5: TPanel;
    Button4: TButton;
    Button6: TButton;
    Label8: TLabel;
    DBLookupComboBoxTextilFajta: TDBLookupComboBox;
    DBLookupComboBoxTextilAllapot: TDBLookupComboBox;
    DBLookupComboBoxTextilMeret: TDBLookupComboBox;
    DBLookupComboTextilMagassag: TDBLookupComboBox;
    DBLookupComboBoxTextilSzin: TDBLookupComboBox;
    TabSheet4: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Button5: TButton;
    ScrollBox1: TScrollBox;
    samplePanel: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    DBGridBevetCsoportokTetel: TDBGrid;
    DBGridBevetCsoportokFej: TDBGrid;
    GroupBox1: TGroupBox;
    Label21: TLabel;
    Button7: TButton;
    Button8: TButton;
    FDMemTableBevetelezesCsoportokFej: TFDMemTable;
    FDMemTableBevetelezesCsoportokTetel: TFDMemTable;
    FDMemTableBevetelezesCsoportokFejfajta: TStringField;
    FDMemTableBevetelezesCsoportokFejallapot: TStringField;
    FDMemTableBevetelezesCsoportokFejszin: TStringField;
    FDMemTableBevetelezesCsoportokFejmeret: TStringField;
    FDMemTableBevetelezesCsoportokFejmagassag: TStringField;
    FDMemTableBevetelezesCsoportokFejmennyiseg: TFloatField;
    FDMemTableBevetelezesCsoportokFejcsoport: TIntegerField;
    DSBevetelezesCsoportokFej: TDataSource;
    DSBevetelezesCsoportokTetel: TDataSource;
    FDMemTableBevetelezesCsoportokTetelchipkod: TStringField;
    FDMemTableBevetelezesCsoportokTeteldatum: TDateTimeField;
    FDMemTableBevetelezesCsoportokTetelcsoport: TIntegerField;
    DBEditMennyiseg: TEdit;
    lInfoString: TLabel;
    Memo1: TMemo;
    FDMemTableBeolvasottTextil: TFDMemTable;
    DSBeolvasottTextil: TDataSource;
    FDMemTableBeolvasottTextilchipkod: TStringField;
    DSOsszesites: TDataSource;
    FDMemTableOsszesites: TFDMemTable;
    FDMemTableOsszesitessorszam: TStringField;
    FDMemTableOsszesitesfajta: TStringField;
    FDMemTableOsszesitesallapot: TStringField;
    FDMemTableOsszesitesmeret: TStringField;
    FDMemTableOsszesitesmagassag: TStringField;
    FDMemTableOsszesitesszin: TStringField;
    FDMemTableOsszesitesdarabszam: TIntegerField;
    frxReportPuffBevetOssz: TfrxReport;
    frxReportPuffBevetReszletes: TfrxReport;
    frxDBDatasetPuffBevetOssz: TfrxDBDataset;
    frxDBDatasetPuffBevetReszletes: TfrxDBDataset;
    FDMemTableOsszesitesuj: TStringField;
    Label22: TLabel;
    DBLookupComboBoxMegnevezes: TDBLookupComboBox;
    Panel13: TPanel;
    FDMemTableOsszesitesmegnevezes: TStringField;
    Label23: TLabel;
    Label24: TLabel;
    FDMemTableBevetelezesCsoportokFejmegnevezes: TStringField;
    FDMemTableOsszesitesmegjegyzes: TStringField;
    FDMemTableBevetelezesCsoportokFejmegjegyzes: TStringField;
    Memo2: TMemo;
    DBMemoTetelMegjegyzes: TMemo;
    FDMemTablePBXML: TFDMemTable;
    FDMemTablePBMXML: TFDMemTable;
    FDMemTablePBXMLpbfej_uzem_id: TIntegerField;
    FDMemTablePBXMLpbfej_partner_id: TIntegerField;
    FDMemTablePBXMLpbfej_osztaly_id: TIntegerField;
    FDMemTablePBXMLpbfej_sorszam: TStringField;
    FDMemTablePBXMLpbfej_mozgas_id: TIntegerField;
    FDMemTablePBXMLpbfej_felhasznalo_id: TIntegerField;
    FDMemTablePBXMLpbfej_megjegyzes: TStringField;
    FDMemTablePBXMLpbfej_create_date: TDateTimeField;
    FDMemTablePBXMLpbfej_create_user: TIntegerField;
    FDMemTablePBXMLpbfej_lastupdate_date: TDateTimeField;
    FDMemTablePBXMLpbfej_lastupdate_user: TIntegerField;
    FDMemTablePBXMLpbfej_selected_date: TDateTimeField;
    FDMemTablePBXMLpbtet_darabszam: TIntegerField;
    FDMemTablePBXMLpbtet_fajta_id: TIntegerField;
    FDMemTablePBXMLpbtet_allapot_id: TIntegerField;
    FDMemTablePBXMLpbtet_meret_id: TIntegerField;
    FDMemTablePBXMLpbtet_magassag_id: TIntegerField;
    FDMemTablePBXMLpbtet_szin_id: TIntegerField;
    FDMemTablePBXMLpbtet_kpt_id: TIntegerField;
    FDMemTablePBXMLpbtet_megjegyzes: TStringField;
    FDMemTablePBXMLpbtet_create_date: TDateTimeField;
    FDMemTablePBXMLpbtet_create_user: TIntegerField;
    FDMemTablePBXMLpbtet_lastupdate_date: TDateTimeField;
    FDMemTablePBXMLpbtet_lastupdate_user: TIntegerField;
    FDMemTablePBXMLpbtet_fajlnev: TStringField;
    FDMemTablePBXMLallapotText: TStringField;
    FDMemTablePBXMLfajtaText: TStringField;
    FDMemTablePBXMLszinText: TStringField;
    FDMemTablePBXMLmeretText: TStringField;
    FDMemTablePBXMLmagassagText: TStringField;
    FDMemTablePBXMLmegnevezesText: TStringField;
    FDMemTablePBMXMLsorszam: TStringField;
    FDMemTablePBMXMLpartner_id: TIntegerField;
    FDMemTablePBMXMLuzem_id: TIntegerField;
    FDMemTablePBMXMLosztaly_id: TIntegerField;
    FDMemTablePBMXMLprszall_fej_id: TIntegerField;
    FDMemTablePBMXMLvonalkod: TStringField;
    FDMemTablePBMXMLchipkod: TStringField;
    FDMemTablePBMXMLmozgas_id: TIntegerField;
    FDMemTablePBMXMLtextil_kpt_id: TIntegerField;
    FDMemTablePBMXMLtextil: TStringField;
    FDMemTablePBMXMLfajta_id: TIntegerField;
    FDMemTablePBMXMLfajta: TStringField;
    FDMemTablePBMXMLallapot_id: TIntegerField;
    FDMemTablePBMXMLallapot: TStringField;
    FDMemTablePBMXMLmeret_id: TIntegerField;
    FDMemTablePBMXMLmeret: TStringField;
    FDMemTablePBMXMLmagassag_id: TIntegerField;
    FDMemTablePBMXMLmagassag: TStringField;
    FDMemTablePBMXMLszin_id: TIntegerField;
    FDMemTablePBMXMLszin: TStringField;
    FDMemTablePBMXMLdatum: TDateTimeField;
    FDMemTablePBMXMLfelhasznalo_id: TIntegerField;
    FDMemTablePBMXMLfajlnev: TStringField;
    FDMemTablePBXMLpbfej_id: TIntegerField;
    Button2: TButton;
    Button9: TButton;
    FDMemTablePBXMLpbtet_darabos_beolvasasok: TIntegerField;
    FDMemTablePBXMLpbtet_chiphez_rendelve: TIntegerField;
    FDMemTablePBMXMLuploaded: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure rogzitChipBeolvasBtnClick(Sender: TObject);
    procedure DBEditMennyisegChange(Sender: TObject);
    procedure btnChipStartClick(Sender: TObject);
    procedure btnChipEndClick(Sender: TObject);
    procedure RedrawOsszPanel;
    procedure Panel8Click(Sender: TObject);
    procedure Panel9Click(Sender: TObject);
    procedure Panel10Click(Sender: TObject);
    procedure Panel11Click(Sender: TObject);
    procedure Panel12Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure DSBevetelezesCsoportokFejDataChange(Sender: TObject;
      Field: TField);
    procedure Button7Click(Sender: TObject);
    procedure DBGridBevetCsoportokFejDblClick(Sender: TObject);
    procedure DBGridBevetCsoportokFejCellClick(Column: TColumn);
    procedure btnVisszaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button3Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Panel13Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { Private declarations }
    prPort, prPortConfig : String;
    prNextPanel: integer;
    prDataLoaded: bool;
    prInEdit: bool;
    prEditCsoport: integer;
    prCloseOnSave: boolean;
    prPanels: array of TComponent;
  public
    { Public declarations }
    pubBeolvasasokSzama: integer;
  end;

var
  frmPufferRaktarBevetelezesParameterek: TfrmPufferRaktarBevetelezesParameterek;

implementation
  uses uDM, uSerialReader, uFgv, uHttps;

var SerialReader: TSerialReader;

{$R *.dfm}

{ Component Copy }
function CopyComponent(Component,AParent: TComponent; NewComponentName: String): TComponent;
var
 Stream: TMemoryStream;
 S: String;
begin
 Result := TComponentClass(Component.ClassType).Create(Component.Owner);
 S := Component.Name;
 Component.Name := NewComponentName;
 Stream := TMemoryStream.Create;
 try
   Stream.WriteComponent(Component);
   Component.Name := S;
   Stream.Seek(0, soFromBeginning);
   TWinControl(AParent).InsertControl(TControl(Result));
   Stream.ReadComponent(Result);
 finally
   Stream.Free;
 end;
end;
// AComponent: the original component
// AOwner, AParent: the owner and parent of the new copy
// returns the new component

function CopyComponents(AComponent, AOwner, AParent: TComponent;Index:Integer): TComponent;
var
  i : integer;
  NewComponent: TComponent;
begin
  NewComponent := CopyComponent(AComponent,AParent,AComponent.Name+IntToStr(Index))as TComponentClass(AComponent.ClassType);{}
 // now, search for subcomponents
 for i:=0 to AComponent.Owner.ComponentCount-1 do
   if TWinControl(AComponent.Owner.Components[i]).Parent=AComponent then
     // and copy them too
     CopyComponents(AComponent.Owner.Components[i], AOwner, NewComponent,Index);
  Result:=NewComponent;
end;

procedure SetPanelLabels(AComponent: TComponent; AFajta, AAllapot, ASzin, AMeret, AMagassag, AMegnev: string; AMennyiseg: integer);
var i: integer;
begin
  for i := 0 to AComponent.Owner.ComponentCount-1 do
    begin
      if (AComponent.Owner.Components[i].ClassType = TLabel) and (TWinControl(AComponent.Owner.Components[i]).Parent = AComponent) then
        begin
          if TLabel(AComponent.Owner.Components[i]).Caption = 'lFajta' then
            TLabel(AComponent.Owner.Components[i]).Caption := AFajta
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'l�llapot' then
            TLabel(AComponent.Owner.Components[i]).Caption := AAllapot
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lSz�n' then
            TLabel(AComponent.Owner.Components[i]).Caption := ASzin
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lM�' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMeret
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMa' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMagassag
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lDarab' then
            TLabel(AComponent.Owner.Components[i]).Caption := IntToStr(AMennyiseg)
          else if TLabel(AComponent.Owner.Components[i]).Caption = 'lMegnev' then
            TLabel(AComponent.Owner.Components[i]).Caption := AMegnev
        end;
    end;
end;

{ End of Component Copy }

procedure TfrmPufferRaktarBevetelezesParameterek.btnChipEndClick(
  Sender: TObject);
begin
  case MessageDlg('Befejezte a chip beolvas�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        case MessageDlg('Biztos vagy benne?', mtConfirmation, [mbYes, mbNo], 0) of
          mrYes:
            begin
//              tBeolvasas.Enabled := false;
              btnChipStart.Enabled := True;
              btnChipEnd.Enabled := False;
              DBEditMennyiseg.Text := '';
              PageControl1.ActivePageIndex := 0;
              PageControl1.OnChange(nil);
              SerialReader.Stop;
              RedrawOsszPanel;
            end;
          mrNo:
            begin
              exit;
            end;
        end;
      end;
    mrNo:
      begin
        exit;
      end;
  end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.btnChipStartClick(
  Sender: TObject);
begin
  button8.Enabled := false;
  btnChipStart.Enabled := False;
  btnChipEnd.Enabled := true;
  SerialReader.Execute;
//  tOsszBevet.Enabled := true;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.btnVisszaClick(
  Sender: TObject);
begin
  Close;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Button1Click(Sender: TObject);
begin
  if (DM.FDMemTablePufferRakBevet.RecordCount <> 0) then
    begin
      PageControl1.ActivePageIndex := 3;
      PageControl1.OnChange(nil);
    end
  else
    ShowMessage('M�g nincsenek t�telek r�gz�tve a bev�telez�sen');
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Button2Click(Sender: TObject);
begin
  DBGridBevetCsoportokFej.OnDblClick(nil);
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Button3Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl1.OnChange(nil);
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Button6Click(Sender: TObject);

  function FindInOsszesites: boolean;
  begin
    result := false;
    FDMemTableOsszesites.First;
    while not FDMemTableOsszesites.Eof do
      begin
        if
          (FDMemTableOsszesites.FieldByName('megnevezes').AsString = DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString) and
          (FDMemTableOsszesites.FieldByName('fajta').AsString = DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString) and
          (FDMemTableOsszesites.FieldByName('allapot').AsString = DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString) and
          (FDMemTableOsszesites.FieldByName('meret').AsString = DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString) and
          (FDMemTableOsszesites.FieldByName('magassag').AsString = DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString) and
          (FDMemTableOsszesites.FieldByName('szin').AsString = DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString)
        then
          begin
            result := true;
            exit;
          end;
        FDMemTableOsszesites.Next;
      end;
  end;

  function FindInOsszesitesPB: boolean;
  begin
    result := false;
    FDMemTablePBXML.First;
    while not FDMemTablePBXML.Eof do
      begin
        if
          (FDMemTablePBXML.FieldByName('megnevezesText').AsString = DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString) and
          (FDMemTablePBXML.FieldByName('fajtaText').AsString = DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString) and
          (FDMemTablePBXML.FieldByName('allapotText').AsString = DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString) and
          (FDMemTablePBXML.FieldByName('meretText').AsString = DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString) and
          (FDMemTablePBXML.FieldByName('magassagText').AsString = DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString) and
          (FDMemTablePBXML.FieldByName('szinText').AsString = DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString)
        then
          begin
            result := true;
            exit;
          end;
        FDMemTablePBXML.Next;
      end;
  end;

var
  lcFgv: TfgvUnit;
  lcBevSorszam: string;
  lcMost: TDateTime;
  lcMentFajlNev, lcMentUtvonal, lcMentFajlNevPBM: string;
  lcTmpNev, lcTmpNevPBM: string;
  lcRootDir: string;
  lcSearchResult: TSearchRec;
  lcHttpRequest: THttpsUnit;
  lcHttpResponse: string;
  lcIniFile: TIniFile;
  lcIPCim, lcUtvonal: string;
  i: integer;
  lcDataset: TClientDataSet;
  lcTmpUtvonal: string;
  lcTmpFej: string;
begin
  if DM.FDMemTablePufferRakBevet.RecordCount = 0 then
    begin
      showMessage('�res bev�telez�st nem lehet menteni, vegyen fel t�teleket a bev�telez�sre!');
      exit;
    end;

  case MessageDlg('Biztosan lez�rja a bev�telez�st?', mtConfirmation, [mbYes, mbNo], 0) of
    mrYes:
      begin
        DM.FDMemTablePufferRakBevet.Tag := 1;
        lcFgv := TfgvUnit.Create;
        lcHttpRequest := THttpsUnit.Create();
        lcDataset := TClientDataSet.Create(Application);
        lcIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
        lcIPCim := lciniFile.ReadString('serverip','ip','');
        lcUtvonal := lciniFile.ReadString('utvonal','path','');
        FDMemTableOsszesites.Close;
        FDMemTableOsszesites.Open;
        //K�vetkez� sorsz�m a lok�lis f�jlb�l. Nem k�ldj�k el a v�ltoztat�st a szervernek!
        lcBevSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PB', GPartner, GUserID, true, false);
        PageControl1.Enabled := false;
        Application.ProcessMessages;

        DM.FDMemTablePufferRakBevet.Filtered := false;
        DM.FDMemTablePufferRakBevet.First;

        FDMemTablePBXML.Close;
        FDMemTablePBXML.Open;

        FDMemTablePBMXML.Close;
        FDMemTablePBMXML.Open;

        lcRootDir := GetCurrentDir;
        lcMentUtvonal := GetCurrentDir + '\adat\';
        try
          SetCurrentDir(lcMentUtvonal);

          if not DirectoryExists(lcFgv.PartnerMappa(GPartner)) then
            CreateDir(lcFgv.PartnerMappa(GPartner));

          lcMentUtvonal := lcMentUtvonal + lcFgv.PartnerMappa(GPartner);
          SetCurrentDir(lcMentUtvonal);

          if not DirectoryExists(Trim(lcFgv.Mappa(GDate))) then
            CreateDir(Trim(lcFgv.Mappa(GDate)));

          lcMentUtvonal := lcMentUtvonal + '\' + Trim(lcFgv.Mappa(GDate));

          SetCurrentDir(lcMentUtvonal);

          GOldal := 'PB';
          lcMentFajlNev := Trim(lcFgv.FajlNev_2(GDate, -1));
          lcTmpNev := lcMentUtvonal + '\*' + lcMentFajlNev;

          if (FindFirst(lcTmpNev+'.xml', faAnyFile, lcSearchResult) = 0) then
            begin
              i := 1;
              lcTmpNev := lcMentUtvonal + '\*' + lcMentFajlNev + '_' + IntToStr(i);
              while FindFirst(lcTmpNev +'.xml', faAnyFile, lcSearchResult) = 0 do
                begin
                  i:= i+1;
                  lcTmpNev := lcMentUtvonal + '\*' + lcMentFajlNev + '_' + IntToStr(i);
                end;
              lcMentFajlNev := lcTmpNev;
            end;

          GOldal := 'PBM';
          lcMentFajlNevPBM := Trim(lcFgv.FajlNev_2(GDate, -1));
          lcTmpNevPBM := lcMentUtvonal + '\*' + lcMentFajlNevPBM;

          if (FindFirst(lcTmpNevPBM+'.xml', faAnyFile, lcSearchResult) = 0) then
            begin
              i := 1;
              lcTmpNevPBM := lcMentUtvonal + '\*' + lcMentFajlNevPBM + '_' + IntToStr(i);
              while FindFirst(lcTmpNevPBM +'.xml', faAnyFile, lcSearchResult) = 0 do
                begin
                  i:= i+1;
                  lcTmpNevPBM := lcMentUtvonal + '\*' + lcMentFajlNevPBM + '_' + IntToStr(i);
                end;
              lcMentFajlNevPBM := lcTmpNevPBM;
            end;

          lcMost := now();

          lcMentFajlNev := StringReplace(lcMentFajlNev, '*', '', [rfReplaceAll, rfIgnoreCase]);
          lcMentFajlNevPBM := StringReplace(lcMentFajlNevPBM, '*', '', [rfReplaceAll, rfIgnoreCase]);

          while not DM.FDMemTablePufferRakBevet.Eof do
            begin
              DM.FDMemTablePufferRakBevet.Edit;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_id').AsInteger := -1;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_sorszam').AsString := lcBevSorszam;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_create_date').AsDateTime := lcMost;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_create_user').AsInteger := GUserID;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_lastupdate_date').AsDateTime := lcMost;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_lastupdate_user').AsInteger := GUserID;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_create_user').AsInteger := GUserID;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_lastupdate_user').AsInteger := GUserID;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_partner_id').AsInteger := GPartner;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_osztaly_id').AsInteger := GPufferRaktarOsztaly;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_selected_date').AsDateTime := GDate;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_megjegyzes').AsString := Memo1.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('pbfej_mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PB;
              DM.FDMemTablePufferRakBevet.Post;

              if DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString <> '' then
                begin
                  FDMemTablePBMXML.Insert;
                  FDMemTablePBMXML.FieldByName('sorszam').AsString := lcBevSorszam;
                  FDMemTablePBMXML.FieldByName('partner_id').AsInteger := GPartner;
                  FDMemTablePBMXML.FieldByName('uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePBMXML.FieldByName('osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePBMXML.FieldByName('prszall_fej_id').AsInteger := -1;
                  FDMemTablePBMXML.FieldByName('vonalkod').AsString := '';


                  FDMemTablePBMXML.FieldByName('chipkod').AsString := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString;
                  FDMemTablePBMXML.FieldByName('mozgas_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_mozgas_id').AsInteger;
                  FDMemTablePBMXML.FieldByName('textil_kpt_id').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_kpt_id').AsVariant;
                  FDMemTablePBMXML.FieldByName('textil').AsString := DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString;
                  FDMemTablePBMXML.FieldByName('fajta_id').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_fajta_id').AsVariant;
                  FDMemTablePBMXML.FieldByName('fajta').AsString := DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString;
                  FDMemTablePBMXML.FieldByName('allapot_id').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_allapot_id').AsVariant;
                  FDMemTablePBMXML.FieldByName('allapot').AsString := DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString;
                  FDMemTablePBMXML.FieldByName('meret_id').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_meret_id').AsVariant;
                  FDMemTablePBMXML.FieldByName('meret').AsString := DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString;
                  FDMemTablePBMXML.FieldByName('magassag_id').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_magassag_id').AsVariant;
                  FDMemTablePBMXML.FieldByName('magassag').AsString := DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString;
                  FDMemTablePBMXML.FieldByName('szin_id').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_szin_id').AsVariant;
                  FDMemTablePBMXML.FieldByName('szin').AsString := DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString;
                  FDMemTablePBMXML.FieldByName('datum').AsDateTime := GDate;
                  FDMemTablePBMXML.FieldByName('felhasznalo_id').AsInteger := GUserId;
                  FDMemTablePBMXML.FieldByName('fajlnev').AsString := ExtractFileName(lcMentFajlNevPBM)+'.xml';
                  FDMemTablePBMXML.FieldByName('uploaded').AsInteger := -1;

                  FDMemTablePBMXML.Post;
                end;

              if not FindInOsszesites then
                begin
                  FDMemTableOsszesites.Insert;
                  FDMemTableOsszesites.FieldByName('sorszam').AsString := lcBevSorszam;
                  FDMemTableOsszesites.FieldByName('fajta').AsString := DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString;
                  FDMemTableOsszesites.FieldByName('allapot').AsString := DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString;
                  FDMemTableOsszesites.FieldByName('meret').AsString := DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString;
                  FDMemTableOsszesites.FieldByName('magassag').AsString := DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString;
                  FDMemTableOsszesites.FieldByName('szin').AsString := DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString;
                  FDMemTableOsszesites.FieldByName('megnevezes').AsString := DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString;
                  FDMemTableOsszesites.FieldByName('darabszam').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;
                  FDMemTableOsszesites.FieldByName('uj').AsString := '�J';
                  FDMemTableOsszesites.FieldByName('megjegyzes').AsString := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_megjegyzes').AsString;

                  FDMemTableOsszesites.Post;
                end
              else
                begin
                  FDMemTableOsszesites.Edit;
                  FDMemTableOsszesites.FieldByName('darabszam').AsInteger := FDMemTableOsszesites.FieldByName('darabszam').AsInteger + DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;
                  FDMemTableOsszesites.Post;
                end;

              if not FindInOsszesitesPB then
                begin
                  FDMemTablePBXML.Insert;
                  FDMemTablePBXML.FieldByName('fajtaText').AsString := DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString;
                  FDMemTablePBXML.FieldByName('allapotText').AsString := DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString;
                  FDMemTablePBXML.FieldByName('meretText').AsString := DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString;
                  FDMemTablePBXML.FieldByName('magassagText').AsString := DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString;
                  FDMemTablePBXML.FieldByName('szinText').AsString := DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString;
                  FDMemTablePBXML.FieldByName('megnevezesText').AsString := DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString;

                  FDMemTablePBXML.FieldByName('pbfej_id').AsInteger := -1;
                  FDMemTablePBXML.FieldByName('pbfej_sorszam').AsString := lcBevSorszam;
                  FDMemTablePBXML.FieldByName('pbfej_uzem_id').AsInteger := DM.CDSUzem.FieldByName('uzem_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbfej_partner_id').AsInteger := GPartner;
                  FDMemTablePBXML.FieldByName('pbfej_osztaly_id').AsInteger := GPufferRaktarOsztaly;
                  FDMemTablePBXML.FieldByName('pbfej_mozgas_id').AsInteger := GPufferRaktarMozgasnemek.PB;
                  FDMemTablePBXML.FieldByName('pbfej_felhasznalo_id').AsInteger := GUserID;
                  FDMemTablePBXML.FieldByName('pbfej_megjegyzes').AsString := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_megjegyzes').AsString;
                  FDMemTablePBXML.FieldByName('pbfej_create_date').AsDateTime := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_create_date').AsDateTime;
                  FDMemTablePBXML.FieldByName('pbfej_create_user').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_create_user').AsInteger;
                  FDMemTablePBXML.FieldByName('pbfej_lastupdate_date').AsDateTime := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_lastupdate_date').AsDateTime;
                  FDMemTablePBXML.FieldByName('pbfej_lastupdate_user').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_lastupdate_user').AsInteger;
                  FDMemTablePBXML.FieldByName('pbfej_selected_date').AsDateTime := DM.FDMemTablePufferRakBevet.FieldByName('pbfej_selected_date').AsDateTime;
                  FDMemTablePBXML.FieldByName('pbtet_darabszam').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_fajta_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_fajta_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_allapot_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_allapot_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_textil_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_kpt_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_meret_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_meret_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_magassag_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_magassag_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_szin_id').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_szin_id').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_megjegyzes').AsString := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_megjegyzes').AsString;
                  FDMemTablePBXML.FieldByName('pbtet_create_date').AsDateTime := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_create_date').AsDateTime;
                  FDMemTablePBXML.FieldByName('pbtet_create_user').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_create_user').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_lastupdate_date').AsDateTime := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_lastupdate_date').AsDateTime;
                  FDMemTablePBXML.FieldByName('pbtet_lastupdate_user').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_lastupdate_user').AsInteger;
                  FDMemTablePBXML.FieldByName('pbtet_fajlnev').AsString := ExtractFileName(lcMentFajlNev)+'.xml';
                  FDMemTablePBXML.FieldByName('pbtet_chiphez_rendelve').AsInteger := 0;

                  if DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString <> '' then
                    FDMemTablePBXML.FieldByName('pbtet_darabosdb').AsInteger := 0
                  else
                    FDMemTablePBXML.FieldByName('pbtet_darabosdb').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;

                  FDMemTablePBXML.Post;
                end
              else
                begin
                  FDMemTablePBXML.Edit;
                  FDMemTablePBXML.FieldByName('pbtet_darabszam').AsInteger := FDMemTablePBXML.FieldByName('pbtet_darabszam').AsInteger + DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;

                  if DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString = '' then
                    FDMemTablePBXML.FieldByName('pbtet_darabosdb').AsInteger := FDMemTablePBXML.FieldByName('pbtet_darabosdb').AsInteger + DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;
                  FDMemTablePBXML.Post;
                end;

              DM.FDMemTablePufferRakBevet.Next;
            end;

          DM.CDSUzem.First;
          TfrxMemoView(frxReportPuffBevetOssz.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportPuffBevetOssz.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPuffBevetOssz.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPuffBevetOssz.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPuffBevetOssz.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPuffBevetOssz.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;

          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mKiszDatum')).Text := DateToStr(GDate);
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mUzemNev')).Text := DM.CDSUzem.FieldByName('uzem_nev').AsString;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mUzemCim')).Text := DM.CDSUzem.FieldByName('uzem_telephely').AsString;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mUzemTel')).Text := DM.CDSUzem.FieldByName('uzem_tel').AsString;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mUzemMobil')).Text := DM.CDSUzem.FieldByName('uzem_mobil').AsString;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mUzemEmail')).Text := DM.CDSUzem.FieldByName('uzem_email').AsString;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('frxDBDSSzallszfej_szallszam')).Text := lcBevSorszam;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('frxDBDSSzallszfej_szallszam1')).Text := lcBevSorszam;
          TfrxMemoView(frxReportPuffBevetReszletes.FindObject('SzCreateDate')).Text := DateToStr(lcMost);

          DM.CDSFelhasz.First;
          while not DM.CDSFelhasz.Eof do
            begin
              if GUserID = DM.CDSFelhasz.FieldByName('felh_id').AsInteger then
                begin
                  TfrxMemoView(frxReportPuffBevetOssz.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                  TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mLetrehozta')).Text := DM.CDSFelhasz.FieldByName('felh_teljesnev').AsString;
                end;
              DM.CDSFelhasz.Next;
            end;

          while not DM.CDSPartnerek.Eof do
            begin
              if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
                begin
                  TfrxMemoView(frxReportPuffBevetOssz.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                  TfrxMemoView(frxReportPuffBevetReszletes.FindObject('mPartnerNev')).Text := DM.CDSPartnerek.FieldByName('par_nev').AsString;
                end;
              DM.CDSPartnerek.Next;
            end;

          FDMemTableOsszesites.First;
          DM.frxPDFExport1.FileName := lcMentFajlNev+'.pdf';
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPuffBevetOssz.PrepareReport(true);
          frxReportPuffBevetOssz.Export(DM.frxPDFExport1);

          frxReportPuffBevetOssz.PrintOptions.Copies := 3;
          frxReportPuffBevetOssz.PrintOptions.Collate := True;
          frxReportPuffBevetOssz.PrepareReport(true);
          frxReportPuffBevetOssz.ShowPreparedReport;

          // A Megn�velt sorsz�m felk�ld�se a szervernek
          lcTmpUtvonal := GetCurrentDir;
          SetCurrentDir(lcRootDir);
          lcBevSorszam := lcFgv.SorszamGeneralas('P_'+IntToStr(CurrentYear)+'_PB', GPartner, GUserID, false, true);
          SetCurrentDir(lcTmpUtvonal);

          lcFgv.DataSetToXML(TDataSet(FDMemTablePBXML), lcMentFajlNev+'.xml');

          DM.FDMemTablePufferRakBevet.Filtered := false;
          DM.FDMemTablePufferRakBevet.Filter := 'pbtet_chip_kod <> null';
          DM.FDMemTablePufferRakBevet.Filtered := true;

          FDMemTableOsszesites.First;
          DM.frxPDFExport1.FileName := lcMentFajlNevPBM+'.pdf';
          DM.frxPDFExport1.DefaultPath := GetCurrentDir;
          DM.frxPDFExport1.ShowDialog := false;
          frxReportPuffBevetReszletes.PrepareReport(true);
          frxReportPuffBevetReszletes.Export(DM.frxPDFExport1);

          frxReportPuffBevetReszletes.PrintOptions.Copies := 3;
          frxReportPuffBevetReszletes.PrintOptions.Collate := True;
          frxReportPuffBevetReszletes.PrepareReport(true);
          frxReportPuffBevetReszletes.ShowPreparedReport;

          DM.FDMemTablePufferRakBevet.Filtered := false;

          lcFgv.DataSetToXML(TDataSet(FDMemTablePBMXML), lcMentFajlNevPBM+'.xml');

          lcFgv.SetPufferRaktar('bevet', false, false, TClientDataSet(FDMemTablePBXML));
          lcFgv.SetPufferRaktar('bevet', true, false, TClientDataSet(FDMemTablePBMXML));

          { ... Mentett f�jlok elk�ld�se szervernek ... }
          lcHttpResponse := lcHttpRequest.PostHttps(lcMentFajlNev+'.xml', lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload);
          if (lcHttpResponse <> '') and lcFgv.IsNumber(lcHttpResponse) then
            begin
              //Siker�lt a k�ld�s
              lcDataset.LoadFromFile(lcMentFajlNev+'.xml');
              lcDataset.LogChanges := false;
              lcDataset.First;
              while not lcDataset.Eof do
                begin
                  lcDataset.Edit;
                  lcDataset.FieldByName('pbfej_id').AsInteger := StrToInt(lcHttpResponse);
                  lcDataset.Post;
                  lcDataset.Next;
                end;
              lcFgv.DataSetToXML(lcDataset, lcMentFajlNev+'.xml');
              RenameFile(lcMentFajlNev+'.xml', '_uploaded_'+ExtractFileName(lcMentFajlNev)+'.xml');
              lcTmpFej := lcHttpResponse;
              if FDMemTablePBMXML.RecordCount > 0 then
                begin
                  lcHttpResponse := '';
                  Application.ProcessMessages;
                  lcDataset.Close;
                  lcDataset.LoadFromFile(lcMentFajlNevPBM+'.xml');
                  lcDataset.LogChanges := false;
                  lcDataset.First;
                  while not lcDataset.Eof do
                    begin
                      lcDataset.Edit;
                      lcDataset.FieldByName('prszall_fej_id').AsInteger := StrToInt(lcTmpFej);
                      lcDataset.Post;
                      lcDataset.Next;
                    end;
                  lcFgv.DataSetToXML(lcDataset, lcMentFajlNevPBM+'.xml');
                  lcHttpResponse := lcHttpRequest.PostHttps(lcMentFajlNevPBM+'.xml', lcIPCim, lcUtvonal, GWHazUrls.pufBevRakXMLUpload_PBM);
                  if (lcHttpResponse <> '') and lcFgv.IsNumber(lcHttpResponse) then
                    begin
                      lcDataset.First;
                      while not lcDataset.Eof do
                        begin
                          lcDataset.Edit;
                          lcDataset.FieldByName('uploaded').AsInteger := 1;
                          lcDataset.Post;
                          lcDataset.Next;
                        end;
                      lcFgv.DataSetToXML(lcDataset, lcMentFajlNevPBM+'.xml');
                      RenameFile(lcMentFajlNevPBM+'.xml', '_uploaded_'+ExtractFileName(lcMentFajlNevPBM)+'.xml');
                    end
                  else
                    begin
                      MessageBox(handle,'Sikertelen pufferrakt�r bev�telez�s (PBM) k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
                    end;
                end
              else
                RenameFile(lcMentFajlNevPBM+'.xml', '_uploaded_'+ExtractFileName(lcMentFajlNevPBM)+'.xml');
            end
          else
            begin
              MessageBox(handle,'Sikertelen pufferrakt�r bev�telez�s (PB) k�ld�s!', 'Hibajelz�s!', MB_ICONERROR);
            end;
        finally
          begin
            SetCurrentDir(lcRootDir);
          end;
        end;
        DM.LoadSynchChip;
        lcFgv.Destroy;
        prCloseOnSave := true;
        Close;
      end;
  end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.rogzitChipBeolvasBtnClick(Sender: TObject);
begin
  if (DBLookupComboBoxTextilFajta.Text = '') OR
     (DBLookupComboBoxTextilMeret.Text = '') OR
     (DBLookupComboBoxMegnevezes.Text = '')
  then
    begin
      ShowMessage('A textil megnevez�se, fajt�ja �s m�rete mez�ket ki kell t�lteni!');
      exit;
    end;

  if prInEdit then
    begin
      DM.FDMemTablePufferRakBevet.Tag := 1;
      DM.FDMemTablePufferRakBevet.Filtered := false;
      DM.FDMemTablePufferRakBevet.First;
      while not DM.FDMemTablePufferRakBevet.Eof do
        begin
          if DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger = prEditCsoport then
            begin
              DM.FDMemTablePufferRakBevet.Edit;
              if not (DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString <> '') then
                begin
                  if (DBEditMennyiseg.Text = '') then
                    begin
                      ShowMessage('A mennyis�g mez�t k�telez� kit�lteni!');
                    end
                  else
                    DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsVariant := DBEditMennyiseg.Text;
                end;
//              DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger := pubBeolvasasokSzama;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_fajta_id').AsVariant := DBLookupComboBoxTextilFajta.KeyValue;
              DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString := DBLookupComboBoxTextilFajta.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_allapot_id').AsVariant := DBLookupComboBoxTextilAllapot.KeyValue;
              DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString := DBLookupComboBoxTextilAllapot.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString := DBLookupComboBoxTextilSzin.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_szin_id').AsVariant := DBLookupComboBoxTextilSzin.KeyValue;
              DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString := DBLookupComboBoxTextilMeret.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_meret_id').AsVariant := DBLookupComboBoxTextilMeret.KeyValue;
              DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString := DBLookupComboTextilMagassag.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_magassag_id').AsVariant := DBLookupComboTextilMagassag.KeyValue;
              DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString := DBLookupComboBoxMegnevezes.Text;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_kpt_id').AsVariant := DBLookupComboBoxMegnevezes.KeyValue;
              DM.FDMemTablePufferRakBevet.FieldByName('pbtet_megjegyzes').AsString := DBMemoTetelMegjegyzes.Text;
              DM.FDMemTablePufferRakBevet.Post;
            end;
          DM.FDMemTablePufferRakBevet.Next;
        end;
        prInEdit := false;
        DM.FDMemTablePufferRakBevet.Tag := 0;
        RedrawOsszPanel;
        Panel3.Visible := true;
        Button9.Visible := false;
        PageControl1.ActivePageIndex := 3;
        PageControl1.OnChange(nil);
    end
  else
    begin
      if (DBEditMennyiseg.Text = '') OR ((DBEditMennyiseg.Text <> '') AND (StrToFloat(DBEditMennyiseg.Text) < 1)) then
        begin
          // Move to chip reading tab
          PageControl1.ActivePageIndex := 1;
          PageControl1.OnChange(nil);
          FDMemTableBeolvasottTextil.Close;
          FDMemTableBeolvasottTextil.Open;
        end
      else
        begin
          DM.FDMemTablePufferRakBevet.Insert;
          DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger := pubBeolvasasokSzama;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_fajta_id').AsVariant := DBLookupComboBoxTextilFajta.KeyValue;
          DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString := DBLookupComboBoxTextilFajta.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_allapot_id').AsVariant := DBLookupComboBoxTextilAllapot.KeyValue;
          DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString := DBLookupComboBoxTextilAllapot.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString := DBLookupComboBoxTextilSzin.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_szin_id').AsVariant := DBLookupComboBoxTextilSzin.KeyValue;
          DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString := DBLookupComboBoxTextilMeret.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_meret_id').AsVariant := DBLookupComboBoxTextilMeret.KeyValue;
          DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString := DBLookupComboTextilMagassag.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_magassag_id').AsVariant := DBLookupComboTextilMagassag.KeyValue;
          DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString := DBLookupComboBoxMegnevezes.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_kpt_id').AsVariant := DBLookupComboBoxMegnevezes.KeyValue;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_megjegyzes').AsString := DBMemoTetelMegjegyzes.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsVariant := DBEditMennyiseg.Text;
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_create_date').AsDateTime := now();
          DM.FDMemTablePufferRakBevet.FieldByName('pbtet_lastupdate_date').AsDateTime := now();
          DM.FDMemTablePufferRakBevet.Append();
          pubBeolvasasokSzama := pubBeolvasasokSzama + 1;
          DBEditMennyiseg.Text := '';
          RedrawOsszPanel;
        end;
    end;
end;


procedure TfrmPufferRaktarBevetelezesParameterek.Button7Click(Sender: TObject);
var lcCsoport: integer;
begin
  case MessageDlg('Biztosan t�rli a kijel�lt t�teleket?', mtConfirmation, [mbYes, mbNo], 0) of
    mrNo: Exit;
  end;

  lcCsoport := FDMemTableBevetelezesCsoportokFej.FieldByName('csoport').AsInteger;
  FDMemTableBevetelezesCsoportokFej.Delete;

  if DM.FDMemTablePufferRakBevet.RecordCount > 0 then
    begin
      DM.FDMemTablePufferRakBevet.First;
      while not DM.FDMemTablePufferRakBevet.Eof do
        begin
          if (DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger = lcCsoport) then
            DM.FDMemTablePufferRakBevet.Delete
          else
            DM.FDMemTablePufferRakBevet.Next;
        end;
    end;
  RedrawOsszPanel;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Button8Click(Sender: TObject);
begin
  // Move back to parameters
  Button9.Visible := false;
  Panel3.Visible := true;
  PageControl1.ActivePageIndex := 0;
  PageControl1.OnChange(nil);
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Button9Click(Sender: TObject);
begin
  prInEdit := false;
  DM.FDMemTablePufferRakBevet.Filtered := false;
  DM.FDMemTablePufferRakBevet.Filter := '';
  DM.FDMemTablePufferRakBevet.Filtered := true;
  DM.FDMemTablePufferRakBevet.First;
  Panel3.Visible := true;
  PageControl1.ActivePageIndex := 3;
  PageControl1.OnChange(nil);
end;

procedure TfrmPufferRaktarBevetelezesParameterek.DBEditMennyisegChange(
  Sender: TObject);
begin
  if prInEdit then //Ha t�tel szerkeszt�se m�dban vagyunk akkor nem v�ltozik a sz�veg a gombon.
    Exit;
  if (DBEditMennyiseg.Text <> '') AND (StrToFloat(DBEditMennyiseg.Text) > 0) then
    begin
      rogzitChipBeolvasBtn.Caption := 'R�gz�t�s';
    end
  else
    begin
      rogzitChipBeolvasBtn.Caption := 'Chip beolvas�s';
    end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.DBGridBevetCsoportokFejCellClick(
  Column: TColumn);
begin
  DBGridBevetCsoportokFej.SelectedRows.CurrentRowSelected := True;
  if DBGridBevetCsoportokFej.SelectedRows.Count > 0 then
    begin
      Memo2.Text := FDMemTableBevetelezesCsoportokFej.FieldByName('megjegyzes').AsString;
    end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.DBGridBevetCsoportokFejDblClick(
  Sender: TObject);
begin
  if DBGridBevetCsoportokFej.SelectedRows.Count > 0 then
    begin
      prInEdit := true;
      prEditCsoport := FDMemTableBevetelezesCsoportokFej.FieldByName('csoport').AsInteger;

      DM.FDMemTablePufferRakBevet.Filtered := false;
      DM.FDMemTablePufferRakBevet.Filter := 'tmp_bevet_group =' + IntToStr(prEditCsoport);
      DM.FDMemTablePufferRakBevet.Filtered := true;
      DM.FDMemTablePufferRakBevet.First;

      DBLookupComboBoxTextilFajta.KeyValue := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_fajta_id').AsVariant;
      DBLookupComboBoxTextilAllapot.KeyValue := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_allapot_id').AsVariant;
      DBLookupComboBoxTextilMeret.KeyValue := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_meret_id').AsVariant;
      DBLookupComboTextilMagassag.KeyValue := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_magassag_id').AsVariant;
      DBLookupComboBoxMegnevezes.KeyValue := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_kpt_id').AsVariant;
      DM.DSTextilek.OnDataChange(nil, nil);
      DBLookupComboBoxTextilSzin.KeyValue := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_szin_id').AsVariant;
      DBMemoTetelMegjegyzes.Lines.Clear;
      DBMemoTetelMegjegyzes.Text := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_megjegyzes').AsString;
      DBEditMennyiseg.Text := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsString;
      if (DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString <> '') then
        begin
          DBEditMennyiseg.Enabled := false;
          DBEditMennyiseg.Text := IntToStr(FDMemTableBevetelezesCsoportokTetel.FilteredData.DataView.Rows.Count);
        end;
      Panel3.Visible := false;
      Button9.Visible := true;
      PageControl1.ActivePageIndex := 0;
      PageControl1.OnChange(nil);
    end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.DSBevetelezesCsoportokFejDataChange(
  Sender: TObject; Field: TField);
begin
  if prDataLoaded then
    begin
      if FDMemTableBevetelezesCsoportokFej.RecordCount > 0 then
        begin
          FDMemTableBevetelezesCsoportokTetel.Filtered := false;
          FDMemTableBevetelezesCsoportokTetel.Filter := 'csoport = '+FDMemTableBevetelezesCsoportokFej.FieldByName('csoport').AsString;
          FDMemTableBevetelezesCsoportokTetel.Filtered := true;
        end
      else
        begin
          PageControl1.ActivePageIndex := 0;
          PageControl1.OnChange(nil);
          RedrawOsszPanel;
        end;
    end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if prCloseOnSave then
    begin
      SerialReader.Stop;
      Action := caFree;
    end
  else
    begin
      case MessageDlg('Biztos megszak�tja a m�veletet?', mtConfirmation, [mbYes, mbNo], 0) of
        mrYes:
          begin
            SerialReader.Stop;
            Action := caFree;
          end;
        mrNo: Action := caNone;
      end;
    end;
  if Action = caFree then
    frmPufferRaktarBevetelezesParameterek:= nil;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.FormCreate(Sender: TObject);
var
  i: Integer;
  IniFile: TIniFile;
begin
  DM.FDMemTablePufferRakBevet.Close;
  DM.FDMemTablePufferRakBevet.Open;
  DM.FDMemTablePufferRakBevet.Tag := 0;
  // Hide tab from TabControll
  for i := 0 to PageControl1.PageCount -1  do
    begin
      PageControl1.Pages[i].TabVisible := false;
    end;
  PageControl1.ActivePageIndex := 0;
  // Set datafields
  DBLookupComboBoxTextilFajta.KeyField := 'fajta_id';
  DBLookupComboBoxTextilFajta.ListField := 'fajta_nev';
  DBLookupComboBoxTextilAllapot.KeyField := 'allapot_id';
  DBLookupComboBoxTextilAllapot.ListField := 'allapot_nev';
  DBLookupComboBoxTextilMeret.KeyField := 'meret_id';
  DBLookupComboBoxTextilMeret.ListField := 'meret_nev';
  DBLookupComboTextilMagassag.KeyField := 'magassag_id';
  DBLookupComboTextilMagassag.ListField := 'magassag_nev';
  DBLookupComboBoxMegnevezes.KeyField := 'kpt_id';
  DBLookupComboBoxMegnevezes.ListField := 'kpt_smegnev';

  if DM.CDSTextilSzinek.Active then
    begin
      DBLookupComboBoxTextilSzin.KeyField := 'kpsz_szin_id';
      DBLookupComboBoxTextilSzin.ListField := 'szin_nev';
    end;
  DBLookupComboBoxTextilSzin.Enabled := false;

  DM.FDMemTablePufferRakBevet.Active := true;
  // Chip Beolvas�s
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  prPort := IniFile.ReadString('port', 'szam', prPort);
  prPortConfig := IniFile.ReadString('port', 'ertek', prPortConfig);
  IniFile.Free;
  SerialReader := TSerialReader.Create('bevet', prPort, prPortConfig);
  // Bev�telez�s "k�r�k"
  pubBeolvasasokSzama := 0;
  prNextPanel := 0;
  prInEdit := false;
  prCloseOnSave := false;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.PageControl1Change(Sender: TObject);
var
  lcLastCsoport: integer;
begin
  { T�telek szerkeszt�s�re v�lt. }
  if PageControl1.ActivePageIndex = 3 then
    begin
      prDataLoaded := false;
      FDMemTableBevetelezesCsoportokFej.Close;
      FDMemTableBevetelezesCsoportokFej.Open;
      FDMemTableBevetelezesCsoportokTetel.Close;
      FDMemTableBevetelezesCsoportokTetel.Open;
      FDMemTableBevetelezesCsoportokFej.Active := true;
      FDMemTableBevetelezesCsoportokTetel.Active := true;
      lcLastCsoport := -1;
      DM.FDMemTablePufferRakBevet.IndexFieldNames := 'tmp_bevet_group';
      DM.FDMemTablePufferRakBevet.First;
      while not DM.FDMemTablePufferRakBevet.Eof do
        begin
          { Fejek �ssze�ll�t�sa }
          if lcLastCsoport <> DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger then
            begin
              FDMemTableBevetelezesCsoportokFej.Insert;
              FDMemTableBevetelezesCsoportokFej.FieldByName('fajta').AsString := DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').asString;
              FDMemTableBevetelezesCsoportokFej.FieldByName('allapot').AsString := DM.FDMemTablePufferRakBevet.FieldByName('allapotText').asString;
              FDMemTableBevetelezesCsoportokFej.FieldByName('meret').AsString := DM.FDMemTablePufferRakBevet.FieldByName('meretText').asString;
              FDMemTableBevetelezesCsoportokFej.FieldByName('magassag').AsString := DM.FDMemTablePufferRakBevet.FieldByName('magassagText').asString;
              FDMemTableBevetelezesCsoportokFej.FieldByName('szin').AsString := DM.FDMemTablePufferRakBevet.FieldByName('szinText').asString;
              FDMemTableBevetelezesCsoportokFej.FieldByName('megnevezes').AsString := DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').asString;
              FDMemTableBevetelezesCsoportokFej.FieldByName('mennyiseg').AsFloat := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsFloat;
              FDMemTableBevetelezesCsoportokFej.FieldByName('csoport').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger;
              FDMemTableBevetelezesCsoportokFej.FieldByName('megjegyzes').AsVariant := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_megjegyzes').AsVariant;
              lcLastCsoport := DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger;
              FDMemTableBevetelezesCsoportokFej.Post;
            end
          else
            begin
              FDMemTableBevetelezesCsoportokFej.Edit;
              FDMemTableBevetelezesCsoportokFej.FieldByName('mennyiseg').AsFloat := FDMemTableBevetelezesCsoportokFej.FieldByName('mennyiseg').AsFloat + DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsFloat;
              FDMemTableBevetelezesCsoportokFej.Post;
            end;
          DM.FDMemTablePufferRakBevet.Next;
        end;

      DM.FDMemTablePufferRakBevet.First;
      while not DM.FDMemTablePufferRakBevet.Eof do
        begin
          FDMemTableBevetelezesCsoportokTetel.Insert;
          if DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString <> '' then
            FDMemTableBevetelezesCsoportokTetel.FieldByName('chipkod').AsString := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_chip_kod').AsString
          else
            FDMemTableBevetelezesCsoportokTetel.FieldByName('chipkod').AsString := '-';
          FDMemTableBevetelezesCsoportokTetel.FieldByName('datum').AsDateTime := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_create_date').AsDateTime;
          FDMemTableBevetelezesCsoportokTetel.FieldByName('csoport').AsInteger := DM.FDMemTablePufferRakBevet.FieldByName('tmp_bevet_group').AsInteger;
          FDMemTableBevetelezesCsoportokTetel.Post;
          DM.FDMemTablePufferRakBevet.Next;
        end;
      prDataLoaded := true;
      FDMemTableBevetelezesCsoportokFej.First;
    end
  else if PageControl1.ActivePageIndex = 1 then
    begin
      lInfoString.Caption := 'Kiv�lasztott partner: '+ GPartnerNev +#13+#10+
                             'Megnevez�s: '+DBLookupComboBoxMegnevezes.Text+#13#10+
                             'Fajta: '+DBLookupComboBoxTextilFajta.Text+#13#10+
                             '�llapot: '+DBLookupComboBoxTextilAllapot.Text+#13#10+
                             'Sz�n: '+DBLookupComboBoxTextilSzin.Text+#13#10+
                             'M�ret: '+DBLookupComboBoxTextilMeret.Text+#13#10+
                             'Magass�g: '+DBLookupComboTextilMagassag.Text+#13#10;
      mChipList.Lines.Clear;
      Button8.Enabled := true;
    end
  else if PageControl1.ActivePageIndex = 0 then
    begin
      if prInEdit then
        begin
          rogzitChipBeolvasBtn.Caption := 'V�ltoztat�sok ment�se';
        end
      else
        begin
          DBEditMennyiseg.Enabled := true;
          DBEditMennyiseg.Text := '';
          rogzitChipBeolvasBtn.Caption := 'Chip beolvas�s';
        end;
    end
  else
    begin
      FDMemTableBevetelezesCsoportokFej.Active := false;
      FDMemTableBevetelezesCsoportokTetel.Active := false;
    end;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Panel10Click(Sender: TObject);
begin
  DBLookupComboBoxTextilMeret.KeyValue := null;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Panel11Click(Sender: TObject);
begin
  DBLookupComboTextilMagassag.KeyValue := null;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Panel12Click(Sender: TObject);
begin
  DBLookupComboBoxTextilSzin.KeyValue := null;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Panel13Click(Sender: TObject);
begin
  DBLookupComboBoxMegnevezes.KeyValue := null;
  DM.DSTextilek.OnDataChange(nil, nil);
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Panel8Click(Sender: TObject);
begin
  DBLookupComboBoxTextilFajta.KeyValue := null;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.Panel9Click(Sender: TObject);
begin
  DBLookupComboBoxTextilAllapot.KeyValue := null;
end;

procedure TfrmPufferRaktarBevetelezesParameterek.RedrawOsszPanel;

  function lcIndexOf(arr: array of TBevetTetel; haystack: TBevetTetel): integer;
  var i: integer;
  begin
    result := -1;
    for i := 0 to Length(arr)-1 do
    begin
      if (arr[i].Fajta = haystack.Fajta) and
         (arr[i].Allapot = haystack.Allapot) and
         (arr[i].Szin = haystack.Szin) and
         (arr[i].Meret = haystack.Meret) and
         (arr[i].Megnev = haystack.Megnev) and
         (arr[i].Magassag = haystack.Magassag) then
        begin
          result := i;
          break;
        end;
    end;
  end;

var i: integer;
    lcTetelek: array of TBevetTetel;
    lcCurrentTetel: TBevetTetel;
    lcFindIndex: integer;
    lcScrollPosition: integer;
begin
  if DM.FDMemTablePufferRakBevet.Tag = 1 then
    Exit;
  SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(false), 0);
  try
    lcScrollPosition :=  ScrollBox1.VertScrollBar.ScrollPos;
    for i := 0 to length(prPanels)-1 do
      begin
        if assigned(prPanels[i]) then
          begin
            prPanels[i].Destroy;
          end;
      end;
    setLength(prPanels, 0);
    DM.FDMemTablePufferRakBevet.First;
    for i := 0 to DM.FDMemTablePufferRakBevet.RecordCount-1 do
      begin
        lcCurrentTetel.Fajta := DM.FDMemTablePufferRakBevet.FieldByName('fajtaText').AsString;
        lcCurrentTetel.Allapot := DM.FDMemTablePufferRakBevet.FieldByName('allapotText').AsString;
        lcCurrentTetel.Szin := DM.FDMemTablePufferRakBevet.FieldByName('szinText').AsString;
        lcCurrentTetel.Meret := DM.FDMemTablePufferRakBevet.FieldByName('meretText').AsString;
        lcCurrentTetel.Magassag := DM.FDMemTablePufferRakBevet.FieldByName('magassagText').AsString;
        lcCurrentTetel.Megnev := DM.FDMemTablePufferRakBevet.FieldByName('megnevezesText').AsString;
        lcCurrentTetel.Mennyiseg := DM.FDMemTablePufferRakBevet.FieldByName('pbtet_darabszam').AsInteger;
        lcFindIndex := lcIndexOf(lcTetelek, lcCurrentTetel);
        if (lcFindIndex > -1) then
          begin
            lcTetelek[lcFindIndex].Mennyiseg := lcTetelek[lcFindIndex].Mennyiseg + lcCurrentTetel.Mennyiseg;
          end
        else
          begin
            SetLength(lcTetelek, length(lcTetelek)+1);
            lcTetelek[length(lcTetelek)-1] := lcCurrentTetel;
          end;
          DM.FDMemTablePufferRakBevet.Next;
      end;
    setLength(prPanels, length(lcTetelek));
    for i := 0 to length(lcTetelek)-1 do
      begin
      if (lcTetelek[i].Mennyiseg <> 0) then
        begin
          prPanels[i] := CopyComponents(samplePanel, samplePanel.Owner, samplePanel.Parent, prNextPanel);
          SetPanelLabels(prPanels[i], lcTetelek[i].Fajta, lcTetelek[i].Allapot, lcTetelek[i].Szin, lcTetelek[i].Meret, lcTetelek[i].Magassag, lcTetelek[i].Megnev, lcTetelek[i].Mennyiseg);
          TPanel(prPanels[i]).Top := (samplePanel.Height*i)+4;
          TPanel(prPanels[i]).visible := true;
          prNextPanel := prNextPanel + 1;
        end;
      end;
  finally
    begin
      ScrollBox1.VertScrollBar.Position := lcScrollPosition;
      SendMessage(ScrollBox1.Handle, WM_SETREDRAW, WPARAM(true), 0);
      ScrollBox1.Repaint;
    end;
  end;
end;

end.
