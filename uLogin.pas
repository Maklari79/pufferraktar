unit uLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  System.ImageList, Vcl.ImgList, IdBaseComponent, IdCoder, IdCoder3to4,
  IdCoder00E, IdCoderXXE, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TLogin = (lOk, lExpired, lInvalid, lNoDefStorage);

  TfrmLogin = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    btnOk: TButton;
    btnKilep: TButton;
    ImageList1: TImageList;
    eUserName: TEdit;
    ePassw: TEdit;
    lStatus: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnKilepClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FUserID : Integer;
    FUsername, FRealName : String;
    function AppServerVersion: string;
    function Login(const UserName, Passwd: string): TLogin;
  public
    { Public declarations }
    class function Execute: Byte;
  end;

var
  frmLogin: TfrmLogin;

implementation

uses uTMRMain, uDM;
{$R *.dfm}

function TfrmLogin.AppServerVersion: string;
var
  QVersion : TFDQuery;

begin
  QVersion:= TFDQuery.Create(Application);
  QVersion.Connection := DM.TMRConn;
  QVersion.Active := False;
  if DM.TMRConn.Connected then
    Begin
      QVersion.SQL.Text:=('SELECT ver_name, ver_verzioszam, ver_build FROM dat_verzio WHERE ver_name=''TMR'' order by ver_id desc ');
      QVersion.Active := True;
      QVersion.First;
      result:= QVersion.FieldByName('ver_name').AsString + '  ' + QVersion.FieldByName('ver_verzioszam').AsString + ' build '
               + QVersion.FieldByName('ver_build').AsString;
    End
  else result:='';
  QVersion.Free;
end;


function TfrmLogin.Login(const UserName, Passwd: string): TLogin;
var
  QLogin : TFDQuery;
begin
  QLogin:= TFDQuery.Create(Application);
  QLogin.Connection := DM.TMRConn;
  QLogin.Active := False;
  QLogin.SQL.Text := ('SELECT felh_id, felh_nev, felh_teljesnev '+
                      'FROM dat_felhasznalok '+
                      'WHERE felh_nev='''+UserName+''' AND felh_passw='''+Passwd+''' AND felh_delete=False ');
  QLogin.Active := True;
  if QLogin.RecordCount = 0 then result:=lInvalid
    else if QLogin.RecordCount > 0 then
      begin
        result:=lOk;
        FUserID   := QLogin.FieldByName('felh_id').AsInteger;
        GUserId := FUserId;
        FUsername := QLogin.FieldByName('felh_nev').AsString;
        FRealName := QLogin.FieldByName('felh_teljesnev').AsString;
        GUserRealname := QLogin.FieldByName('felh_teljesnev').AsString;
        GVersion := AppServerVersion;
      end
     else begin
      result:=lExpired;
    end;
end;

procedure TfrmLogin.btnKilepClick(Sender: TObject);
begin
  close;
end;

procedure TfrmLogin.btnOkClick(Sender: TObject);
begin
  try
    case Login(LowerCase(eUserName.Text),ePassw.Text) of
      lOk: ModalResult:=mrOk;
      lExpired: ModalResult:=mrIgnore;
      lInvalid: ModalResult:=mrAbort;
      lNoDefStorage: ModalResult:=mrAbort;
    end;
  except
    On E:Exception do
    begin
      ShowMessage('Hiba: '+E.Message);
      ModalResult:=mrNo;
    end;
  end;
end;

class function TfrmLogin.Execute: Byte;
var mr: TModalResult;
    Status: String;
begin
  Result:=255;
  with TfrmLogin.Create(nil) do begin
    try
      try
        try
          Status:=AppServerVersion;
        except
          on E:Exception do begin
            Status:='';
    //        Log.WriteToLog('Error while connecting to server: '+E.Message,lsCore,llError,0);
          end;
        end;
        if Status='' then begin
          lStatus.Font.Color:=clRed;
          lStatus.Caption:='Hiba történt az alkalmazásszerverhez történő csatlakozás során!';
          eUserName.Enabled:=False;
          ePassw.Enabled:=False;
          btnOk.Enabled:=False;
        end
        else begin
          lStatus.Caption:='Sikeres kapcsolódás: '+Status;
        end;
        mr := ShowModal;
        case mr of
              mrOk: Result:=0;
          mrCancel: Result:=1;
           mrAbort: Result:=2;
          mrIgnore: Result:=3;
              mrNo: Result:=4;
        end;
      except
        on E:Exception do begin
          lStatus.Font.Color:=clRed;
          lStatus.Caption:='Hiba történt az alkalmazásszerverhez történő csatlakozás során: '+E.Message;
          eUserName.Enabled:=False;
          ePassw.Enabled:=False;
          btnOk.Enabled:=False;
        end;
      end;
    finally
      Free;
    end;
  end;
end;


procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
  BringToFront;
end;

end.
