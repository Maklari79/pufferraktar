unit uTiszta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, Data.DB, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.Provider, Vcl.Menus, System.DateUtils, System.UITypes, INIFiles;

type
  TfrmTiszta = class(TForm)
    pAlso: TPanel;
    pFelso: TPanel;
    btnAdatok: TButton;
    btnVissza: TButton;
    gbOsztalyok: TGroupBox;
    dbgOsztalyok: TDBGrid;
    gbValPartner: TGroupBox;
    lValPartner: TLabel;
    gbDatum: TGroupBox;
    mcDatum: TMonthCalendar;
    gbSzallitoAdat: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    eSzallSorszam: TEdit;
    eSuly: TEdit;
    btnSorGen: TButton;
    MainMenu1: TMainMenu;
    Osztly1: TMenuItem;
    DataSetProvider1: TDataSetProvider;
    FDQuery1: TFDQuery;
    ilTiszta: TImageList;
    lInformacio: TLabel;
    lDatumVal: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnVisszaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgOsztalyokDblClick(Sender: TObject);
    procedure mcDatumDblClick(Sender: TObject);
    procedure btnSorGenClick(Sender: TObject);
    procedure btnAdatokClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    prUtvonal, prUtMent : String;
    procedure TextilBetolt;
  public
    { Public declarations }
  end;

var
  frmTiszta: TfrmTiszta;

implementation

uses uDM, uFgv, uChipBeolvas, uTMRMain, uHttps, uSzennyesUj, uTisztaSelect;

{$R *.dfm}

procedure TfrmTiszta.TextilBetolt;
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;

begin
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  DM.FDMemTextilek.Close;
  DM.FDMemTextilek.Open;
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\torzs_' + IntToStr(GPartner);
      DM.CDSTextilek.LoadFromFile(lcUtvonal+'\textil.xml');
      DM.CDSTextilek.LogChanges := false;
      DM.CDSTextilek.FieldByName('kpt_smegnev').DisplayLabel := 'Textil n�v';
      DM.CDSTextilek.FieldByName('kpt_id').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_textilId').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_partnerId').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_textilkod').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_textsuly').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texcsop').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texora').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texdbkg').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texberelt').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_term').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texar').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texallv').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texmos').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_keszlet').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_lastuser').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_lastup').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_delete').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_uzem').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_szamlaegyseg').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_szamlazando').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_texchip').Visible := false;
      DM.CDSTextilek.FieldByName('kpt_tersuly').Visible := false;
    end
  else
    begin
      MessageBox(handle, 'Nincs t�rzs adata ennek a partnernek!', 'Hiba!', MB_ICONERROR);
    end;
  SetCurrentDir(prUtMent);
end;


procedure TfrmTiszta.btnAdatokClick(Sender: TObject);
var
  lcEllenorzes, lcPartnerMappa, lcMappaCreate, lcDataSetToXML, lcS : TfgvUnit;
  lcUtvonal, lcMappa, lcFajl, lcUtvonalMent, lcIPCim, lcServerUt : String;
  lcSearchResult : TSearchRec;
  lcFajlTalalt, lcMozgID, lcVanMeg, lcRekordSzam{Ha nincs net �s hib�ra fut, akkor ne dobjon hib�t.} : Integer;
  lcXmlSave : THttpsUnit;
  lciniFile : TIniFile;
  lcLetolt, lcUres, lcMunkaruha : Boolean;
  lcXML, lcMozgKod, lcParamGen : String;
begin
  lcRekordSzam := 0;
  GTFajlNev := '';
  GTobbSzallito := false;
//Tiszta lek�rdez�se. M�r tudjuk melyik napra szeretn�nek bek�sz�teni.
  lcUtvonal := '';
  lcUres := false;
  GNincsTiszta := false;
  GDatasetValtas := false;
  DM.CDSXMLMentes.Active := false;
  lcUtvonalMent := GetCurrentDir;
  prUtvonal := lcUtvonalMent+'\adat\';
  GMappa := '';
  lciniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  lcIPCim := lciniFile.ReadString('serverip','ip','');
  lcServerUt := lciniFile.ReadString('utvonal','path','');
  lcLetolt := true;
  lcUtvonal := GetCurrentDir + '\adat';
  SetCurrentDir(lcUtvonal);
  lcPartnerMappa := TfgvUnit.Create();

  DM.CDSParameterek.First;
  while not DM.CDSParameterek.Eof do
    begin
      if DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner then
        begin
          lcParamGen := DM.CDSParameterek.FieldByName('prm_tisztagenuzem').AsString;
        end;
      DM.CDSParameterek.Next;
    end;

  if not DirectoryExists(lcPartnerMappa.PartnerMappa(GPartner)) then
    begin
      CreateDir(lcPartnerMappa.PartnerMappa(GPartner));
      prUtvonal := prUtvonal + lcPartnerMappa.PartnerMappa(GPartner);
    end
  else
    prUtvonal := prUtvonal + lcPartnerMappa.PartnerMappa(GPartner);
  SetCurrentDir(prUtvonal);
  lcMappaCreate := TfgvUnit.Create();
  if not directoryexists(lcMappaCreate.Mappa(mcDatum.Date)) then
    begin
      CreateDir(lcMappaCreate.Mappa(mcDatum.Date));
      prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(mcDatum.Date);
    end
  else
    prUtvonal := prUtvonal + '\' + lcMappaCreate.Mappa(mcDatum.Date);
  SetCurrentDir(prUtvonal);
  lcS := TfgvUnit.Create();
  lcFajl := prUtvonal + '\' + 'T'+lcS.Mappa(mcDatum.Date)+IntToStr(GOsztalyID) + '.xml';
  SetCurrentDir(lcUtvonal+'\' +lcS.Mappa(mcDatum.Date)+ '\');

  if lcParamGen = 'Manu�lis' then
    begin
      lcFajlTalalt := FindFirst(lcFajl, faAnyFile, lcSearchResult);

      if (lcFajlTalalt = 0) and (lcParamGen = 'Nincs') then
        begin
         // lcFajlTalalt := 3;
        end;

      if (lcFajlTalalt <> 0) then
        begin
         //Ha nem tal�l tiszta f�jlt le kell t�lteni!
          try
            DM.CDSParameterek.First;
            while not DM.CDSParameterek.Eof do
              begin
                if DM.CDSParameterek.FieldByName('prm_parID').AsInteger = GPartner then
                  begin
                    lcMozgKod := DM.CDSParameterek.FieldByName('prm_KorhOszt').AsString;
                  end;
                DM.CDSParameterek.Next;
              end;

            if (lcMozgKod <> '') and (lcMozgKod = 'O') then
              begin
                lcMozgKod := 'T'+ lcMozgKod;
              end;
            if (lcMozgKod <> '') and (lcMozgKod = 'K') then
              begin
                lcMozgKod := 'T'+ lcMozgKod;
              end;

            DM.CDSMozgnem.First;
            while not DM.CDSMozgnem.Eof do
              begin
                if DM.CDSMozgnem.FieldByName('moz_kod').AsString = lcMozgKod then
                  begin
                    lcMozgID := DM.CDSMozgnem.FieldByName('moz_id').AsInteger;
                  end;
                DM.CDSMozgnem.Next;
              end;
            lcXML := lcXmlSave.GetHttpsFajlLetoltes(lcIPCim,lcServerUt,'szallitok', GPartner, DM.CDSUzem.FieldByName('uzem_id').AsInteger, lcMozgID, lcS.Mappa(mcDatum.Date));
            if (lcXML <> '0') and (lcXML <> '') then
              begin
                DM.CDSBetoltTiszta.XMLData := lcXML;
              end;
            DM.CDSBetoltTiszta.Filtered := false;
            DM.CDSBetoltTiszta.filter := 'szfej_osztid=' + IntToStr(GOsztalyID);
            DM.CDSBetoltTiszta.Filtered := true;
            if (lcXML <> '') and (lcXML <> '0') then
              lcRekordSzam := DM.CDSBetoltTiszta.RecordCount;
            if (lcXML <> '0') AND (lcRekordSzam > 1)  and (lcXML <> '')  then
              begin
               DM.CDSTSzallLista.Active := false;
               DM.CDSTSzallLista.XMLData := lcXML;
               GMappa := lcS.Mappa(mcDatum.Date); //Itt nem �tvonal, csak a d�tum stringg� konvert�l�st adom �t, hogy a f�jlnevet l�trehozzam. CSAK egy sz�ll�t� visszaad�s�n�l j�.
               //Itt kell a tiszta sz�ll�t�lev�l lista formot megnyitni.
               frmTMRMain.CreateModal(TfrmTisztaSelect);
              end;
            if (lcXML <> '0') and (lcRekordSzam = 1) and (lcXML <> '') then
              begin
                lcS := TfgvUnit.Create();
                lcXmlSave := THttpsUnit.Create();
                DM.CDSBetoltTiszta.XMLData := lcXmlSave.GetSzallito(lcIPCim,lcServerUt,'szallitolevel',DM.CDSBetoltTiszta.FieldByName('szfej_id').AsInteger);
                DM.CDSBetoltTiszta.First;
                while not DM.CDSBetoltTiszta.Eof do
                  begin
                    DM.CDSBetoltTiszta.Edit;
                    DM.CDSBetoltTiszta.FieldByName('szfej_id').AsInteger := DM.CDSBetoltTiszta.FieldByName('szfej_id').AsInteger;
                    DM.CDSBetoltTiszta.FieldByName('sztet_fajlnev').AsString := 'T'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
                    DM.CDSBetoltTiszta.FieldByName('sztet_recsorsz').AsInteger := DM.CDSBetoltTiszta.RecNo;
                    DM.CDSBetoltTiszta.Post;
                    DM.CDSBetoltTiszta.Next;
                  end;
                DM.CDSBetoltTiszta.Filtered := false;
                DM.CDSBetoltTiszta.filter := '';
                DM.CDSBetoltTiszta.Filtered := true;
                lcDataSetToXML := TfgvUnit.Create();
                lcDataSetToXML.DataSetToXML(DM.CDSBetoltTiszta, 'T'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml');
                lcDataSetToXML.Free;
                DM.CDSBetoltTiszta.LoadFromFile(lcFajl);
                DM.CDSBetoltTiszta.LogChanges := false;
                GTFajlNev := 'T'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
                GDatasetValtas := true;
              end
            else if (lcXML = '0') then
              begin
                GMappa := lcS.Mappa(mcDatum.Date);
                MessageBox(handle,'Nincs bek�sz�tend� erre a napra!','T�j�koztat�s',MB_ICONINFORMATION);
                lcUres := true;
                GNincsTiszta := true;
              end
            else if (lcXML = '') then //Abban az esetben, ha nincs net.
              begin
                GMappa := lcS.Mappa(mcDatum.Date);
                MessageBox(handle,'Nincs bek�sz�tend� erre a napra!','T�j�koztat�s',MB_ICONINFORMATION);
                lcUres := true;
                GNincsTiszta := true;
              end;
          except
          on E: Exception do
            begin
              SetCurrentDir(lcUtvonalMent);
              GNincsTiszta := true;
              //GTFajlNev := 'T'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
            end;
          end;
        end;

      if lcFajlTalalt = 0 then
        begin
          try
            DM.CDSXMLMentes.LoadFromFile(lcFajl);
            DM.CDSXMLMentes.LogChanges := false;
            DM.CDSXMLMentes.First;
            while not DM.CDSXMLMentes.Eof do
              begin
                if DM.CDSXMLMentes.FieldByName('sztet_bekeszit').AsInteger > 0 then
                  begin
                    lcLetolt := false;
                  end;
                DM.CDSXMLMentes.Next;
              end;
              DM.CDSParameterek.First;
              while not DM.CDSParameterek.Eof do
                begin
                  if DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner then
                    begin
                      lcParamGen := DM.CDSParameterek.FieldByName('prm_tisztagenuzem').AsString;
                    end;
                  DM.CDSParameterek.Next;
                end;
            DM.CDSXMLMentes.First;
            if (DM.CDSXMLMentes.FieldByName('szfej_lezart').AsInteger = 1) and (lcParamGen <> 'Nincs') then //Itt az lcParamGen azt jelenti, hogy ak�rh�ny sz�ll�t� lehet.
              begin
                MessageBox(handle,'Ez a tiszta sz�ll�t� m�r be lett k�sz�tve!', 'T�j�kosztat�s!', MB_ICONINFORMATION);
                GMappa := '';
                SetCurrentDir(lcUtvonalMent);
                lcS.Free; //Az�rt kell itt is mindent indul�si alaphelyzetbe vissza�ll�tani, mert megszak�tom a folyamatot.
                exit;
              end
            else
              begin
                DM.CDSXMLMentes.LoadFromFile(lcFajl);
                DM.CDSXMLMentes.LogChanges := false;
                GNincsTiszta := false;
                GDatasetValtas := false;
                GTFajlNev := 'T'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
              end;
          except
          on E: Exception do
            begin
              SetCurrentDir(lcUtvonalMent);
              GNincsTiszta := true;
            end;
          end;
        end;

      SetCurrentDir(lcUtvonalMent);
      lcS.Free;
    //Tiszta...eddig.

      {if (eSzallSorszam.Text = '') then
        begin
          MessageBox(handle, 'Nincs megadva a sz�ll�t�lev�l sorsz�ma! K�rem adja meg!', 'Figyelmeztet�s!', MB_ICONWARNING);
          eSzallSorszam.SetFocus;
        end
      else} if (eSuly.Text = '') then
        begin
          MessageBox(handle, 'Nincs megadva a s�ly! K�rem adja meg!', 'Figyelmeztet�s!', MB_ICONWARNING);
          eSuly.SetFocus;
        end
      else
        begin
          if lcUres then
            begin
              DM.CDSParameterek.First;
              while not DM.CDSParameterek.Eof do
                begin
                  if DM.CDSParameterek.FieldByName('prm_parId').AsInteger = GPartner then
                    begin
                      lcParamGen := DM.CDSParameterek.FieldByName('prm_tisztagenuzem').AsString;
                    end;
                  DM.CDSParameterek.Next;
                end;
              if lcParamGen = 'Manu�lis' then
                begin
                  case MessageDlg('�res bek�sz�tend� l�trej�jj�n? ', mtConfirmation, [mbYes, mbNo], 0) of
                    mrNo:
                      begin
                        Abort;
                      end;
                    mrYes:
                      begin
                        GNincsTiszta := true;
                        TextilBetolt;
                      end;
                  end;
                end
              else
                begin
                  GNincsTiszta := true;
                  TextilBetolt;
                end;
            end
          else if not lcUres  then
            begin
              TextilBetolt;
              GNincsTiszta := false;
            end;

          DM.CDSTextilek.Filtered := false;
          DM.CDSTextilek.Filter := 'kpt_texchip = TRUE';
          DM.CDSTextilek.Filtered := true;
          if DM.CDSTextilek.RecordCount > 0 then
            begin
              GEnable := false;
    //          GNincsTiszta := true;
              frmTMRMain.CreateModal(TfrmChipBeolvas);
              Close;
            end
          else if DM.CDSTextilek.RecordCount = 0 then
            begin
              GEnable := true;
              DM.CDSTextilek.Filtered := false;
              DM.CDSTextilek.Filter := '';
              DM.CDSTextilek.Filtered := true;
              frmTMRMain.CreateModal(TfrmSzennyesUj);
              Close;
            end;
        end;
    end
  else if lcParamGen = 'Nincs' then
    begin
      GEnable := false;
      GNincsTiszta := true;
      SetCurrentDir(lcUtvonalMent);
      lcS := TfgvUnit.Create();
      GTFajlNev := 'T'+lcS.Mappa(mcDatum.Date) + IntToStr(GOsztalyID) + '.xml';
      TextilBetolt;
      lcS.Free;
      frmTMRMain.CreateModal(TfrmChipBeolvas);
      Close;
    end;
  SetCurrentDir(lcUtvonalMent);
  GMappa := '';
end;

procedure TfrmTiszta.btnSorGenClick(Sender: TObject);
var
  lcSzallSorszam : TfgvUnit;
  lclabel : String;
begin
  DM.CDSMozgnem.LoadFromFile('mozgasnem.xml');
  DM.CDSMozgnem.LogChanges := false;
  lclabel := DM.Szinkronizalas_2('mozgsorszam/');
  DM.CDSSzallSorszam.LoadFromFile('sorszam.xml');
  DM.CDSSzallSorszam.LogChanges := false;
  lcSzallSorszam := TfgvUnit.Create();
  eSzallSorszam.Text := lcSzallSorszam.SzallSorGen;
  GSzallSorsz := eSzallSorszam.Text;
  eSzallSorszam.ReadOnly := true;
  btnSorGen.Enabled := false;
  DM.CDSSzallSorszam.EmptyDataSet;
end;

procedure TfrmTiszta.btnVisszaClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmTiszta.dbgOsztalyokDblClick(Sender: TObject);
var
  lcOsztEll : TfgvUnit;
begin
// ellen�rz�s hoztak-e m�r l�tre ilyen oszt�yt aznapra.
  SetCurrentDir(prUtvonal);
  GOsztalyID := dbgOsztalyok.DataSource.DataSet.FieldByName('kpo_id').AsInteger;
  lcOsztEll := TfgvUnit.Create();
  if lcOsztEll.FajlOsztalyEll(dbgOsztalyok.DataSource.DataSet.FieldByName('kpo_id').AsInteger) = '' then
    begin
      Osztly1.Caption :=  'Kiv�lasztott oszt�ly: ' + DM.CDSOsztalyok.FieldByName('oszt_nev').Value;
      GOsztNev := DM.CDSOsztalyok.FieldByName('oszt_nev').Value;
      mcDatum.Enabled := true;
      mcDatum.SetFocus;
      dbgOsztalyok.Enabled := false;
    end
  else
    begin
      case MessageDlg('Folytatni akarja a Tiszta r�gz�t�st '
          + DM.CDSOsztalyok.FieldByName('oszt_nev').Value
          +' oszt�lynak? ', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
          mrNo:
            begin
              Abort;
            end;
          mrCancel:
            begin
              Abort;
            end;
          mrYes:
            begin

            end;
          end;
    end;
  SetCurrentDir(prUtMent);
end;

procedure TfrmTiszta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmTiszta.FormCreate(Sender: TObject);
begin
  DM.CDSChip.LoadFromFile('chip.xml');
  DM.CDSChip.LogChanges := false;
end;

procedure TfrmTiszta.FormShow(Sender: TObject);
var
  lcUtvonal, lcMappa : String;
  lcEllenorzes : TfgvUnit;
begin
  mcDatum.Date := now();
  prUtvonal := GetCurrentDir+ '\adat\';
  prUtMent := GetCurrentDir;
  dbgOsztalyok.SetFocus;
  lcUtvonal := GetCurrentDir + '\torzs\';
  lcMappa := 'torzs_' + IntToStr(GPartner);
  lcEllenorzes := TfgvUnit.Create();
  if lcEllenorzes.TorzsAdatEll(GPartner, lcUtvonal, lcMappa) then
    begin
      lcUtvonal := GetCurrentDir + '\torzs\' + 'torzs_' + IntToStr(GPartner);
      DM.CDSNaptar.LoadFromFile(lcUtvonal + '\naptar.xml');
      DM.CDSNaptar.LogChanges := false;
      DM.CDSOsztalyok.LoadFromFile(lcUtvonal+'\osztaly.xml');
      DM.CDSOsztalyok.LogChanges := false;
      DM.CDSOsztalyok.FieldByName('kpo_id').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_partner').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_osztaly').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_kod').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_csoport').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_uzem').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastuser').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_lastup').Visible := false;
      DM.CDSOsztalyok.FieldByName('kpo_delete').Visible := false;
      DM.CDSOsztalyok.FieldByName('oszt_nev').DisplayLabel := 'Oszt�ly n�v';
      btnAdatok.Enabled := false;
    end
  else
    begin
      MessageBox(handle, 'Nincs t�rzs adata ennek a partnernek!', 'Tiszta r�gz�t�s - Hiba!', MB_ICONERROR);
      btnAdatok.Enabled := false;
      gbOsztalyok.Enabled := false;
    end;

  lValPartner.Caption := '';
  DM.CDSPartnerek.First;
  while not DM.CDSPartnerek.Eof do
    begin
      if DM.CDSPartnerek.FieldByName('par_id').AsInteger = GPartner then
        begin
          lValPartner.Caption := DM.CDSPartnerek.FieldByName('par_nev').AsString;
          exit;
        end;
      DM.CDSPartnerek.Next;
    end;
end;

procedure TfrmTiszta.mcDatumDblClick(Sender: TObject);
var
  lcDatum : TDate;
  lcKorhaz, lcFajlGenNev, lcMappa, lcDatumEll : TfgvUnit;
  lcOsztalyID : Integer;
  lcOsszFajlNev, lcUtvonal : String;
  lcSearchResult : TSearchRec;
begin
  lcDatum := now();
  GDate := mcDatum.Date;
  lcDatumEll := TfgvUnit.Create();
  if lcDatumEll.GetNaptar(GDate) then
    begin
      MessageBox(handle, 'Ezen a napon nem dolgozunk!', 'Figyelmeztet�s!', MB_ICONWARNING);
      exit;
    end
  else
    begin
      lDatumVal.Caption := 'Kiv�lasztott d�tum: ' +  DateToStr(mcDatum.Date);
     // gbSzallitoAdat.Enabled := true;
      btnAdatok.Enabled := true;
    //  eSzallSorszam.SetFocus;
    end;
{  if CompareDate(mcDatum.Date,lcDatum) <> 0  then
    begin
      MessageBox(handle, 'Nem lehet visszad�tumozni!', 'Figyelmeztet�s!', MB_ICONWARNING);
      mcDatum.Date := now();
      mcDatum.SetFocus;  �
    end}
{  if CompareDate(mcDatum.Date,lcDatum) = 0 then
    begin
      //Ha l�tezik az �sszes�tett f�jl akkor m�r nem lehet tov�bb szennyest r�gz�teni arra a napra.
      //�tvonalat be kell �ll�tani.
      lcKorhaz := TfgvUnit.Create();
      lcOsztalyID := lcKorhaz.SetOsztalyID('K�rh�z');
      lcFajlGenNev := TfgvUnit.Create();
      //mappa kell...
      lcMappa := TfgvUnit.Create();
      lcOsszFajlNev := 'O' + lcFajlGenNev.FajlNev_2(GDate, lcOsztalyID);
      lcUtvonal := (prUtvonal + '\adat_'+ IntToStr(GPartner) + '\' + lcMappa.Mappa(mcDatum.Date) + '\' +lcOsszFajlNev+'.xml');
      if FindFirst(lcUtvonal, faAnyFile, lcSearchResult) = 0 then
        begin
          MessageBox(handle, 'Nem lehet sz�ll�t�t r�gz�teni! Mert m�r l�tezik az �sszes�tett sz�ll�t�!', 'Figyelmeztet�s!', MB_ICONWARNING);
          Close;
        end
      else
        begin
          lDatumVal.Caption := 'Kiv�lasztott d�tum: ' +  DateToStr(mcDatum.Date);
          gbSzallitoAdat.Enabled := true;
          btnAdatok.Enabled := true;
          eSzallSorszam.SetFocus;
        end;
    end;}
end;

end.
